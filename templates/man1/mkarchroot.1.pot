# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-01-10 10:28+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "MKARCHROOT"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2025-01-06"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"mkarchroot - Creates an arch chroot in a specified location with a specified "
"set of packages"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "mkarchroot [options] [location] [packages]"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<mkarchroot> is a script to create an Arch Linux chroot at a specified "
"location with specified packages. Typically used by I<makechrootpkg> to "
"create build chroots. Apart from installing specified packages the chroot is "
"created with an en_US.UTF-8 and de_DE.UTF-8 locale and a generated machine-"
"id."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-U>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Use I<pacman -U> to install packages."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-C> E<lt>fileE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Location of a pacman config file."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-M> E<lt>fileE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Location of a makepkg config file."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-c> E<lt>dirE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Set pacman cache."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-f> E<lt>srcE<gt>[:E<lt>dstE<gt>]"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Copy file from the host to the chroot.  If I<dst> is not provided, it "
"defaults to I<src> inside of the chroot."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-s>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Do not run setarch."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Output command line options."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pacman(1)"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<Please report bugs and feature requests in the issue tracker. Please do "
"your best to provide a reproducible test case for bugs.>"
msgstr ""
