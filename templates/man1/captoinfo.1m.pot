# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-16 05:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "captoinfo"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "2024-03-23"
msgstr ""

#. type: TH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ncurses 6.5"
msgstr ""

#. type: TH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "User commands"
msgstr ""

#. type: SH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"B<\\%captoinfo> - convert a I<termcap> description into a I<\\%term\\%info> "
"description"
msgstr ""

#. type: SH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid "B<captoinfo> [I<tic-option>] [I<file> \\&.\\|.\\|.]"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid "B<captoinfo -V>"
msgstr ""

#. type: SH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"B<\\%captoinfo> translates terminal descriptions.  It looks in each given "
"text I<file> for I<\\%termcap> entries and, for each one found, writes an "
"equivalent I<\\%\\%term\\%info> description to the standard output stream.  "
"I<\\%termcap> B<tc> capabilities translate to I<\\%\\%term\\%info> "
"\\*(``B<use>\\*('' capabilities."
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"If no I<file>s are specified, B<\\%captoinfo> interprets the content of the "
"environment variable I<\\%TERMCAP> as a file name, and extracts only the "
"entry for the terminal named in the environment variable I<TERM> from it.  "
"If the environment variable I<\\%TERMCAP> is not set, B<\\%captoinfo> reads "
"I<\\%/etc/termcap>."
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"This utility is implemented as a link to B<\\%tic>(1M), with the latter's B<-"
"I> option implied.  You can use other B<\\%tic> options such as B<-1>, B<-"
"f>, B<-v>, B<-w>, and B<-x>.  The B<-V> option reports the version of I<\\"
"%ncurses> associated with this program and exits with a successful status."
msgstr ""

#. type: SS
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Translations from Nonstandard Capabilities"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"B<\\%captoinfo> translates some obsolete, nonstandard capabilities into "
"standard (SVr4/XSI Curses)  I<\\%\\%term\\%info> capabilities.  It issues a "
"diagnostic to the standard error stream for each, inviting the user to check "
"that it has not mistakenly translated an unknown or mistyped capability name."
msgstr ""

#. type: tbl table
#: archlinux mageia-cauldron
#, no-wrap
msgid "Name"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Obsolete"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Standard"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Origin"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\f(BIterminfo\\fR capability"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "_"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "BO"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "mr"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AT&T"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "enter_reverse_mode"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CI"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "vi"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "cursor_invisible"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CV"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ve"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "cursor_normal"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DS"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "mh"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "enter_dim_mode"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EE"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "me"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "exit_attribute_mode"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FE"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "LF"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "label_on"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FL"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "LO"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "label_off"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "XS"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "mk"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "enter_secure_mode"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EN"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "@7"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "XENIX"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "key_end"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GE"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ae"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "exit_alt_charset_mode"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GS"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "as"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "enter_alt_charset_mode"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "HM"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "kh"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "key_home"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "LD"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "kL"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "key_dl"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "PD"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "kN"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "key_npage"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "PN"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "po"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "prtr_off"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "PS"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "pf"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "prtr_on"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "PU"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "kP"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "key_ppage"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "RT"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "@8"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "kent"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "UP"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ku"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "kcuu1"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KA"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "k;"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Tektronix"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "key_f10"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KB"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "F1"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "key_f11"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KC"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "F2"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "key_f12"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KD"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "F3"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "key_f13"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KE"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "F4"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "key_f14"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KF"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "F5"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "key_f15"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "BC"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Sb"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "set_background"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FC"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Sf"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "set_foreground"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "HS"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "IRIX"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"XENIX I<\\%termcap> had a set of extension capabilities, corresponding to "
"box drawing characters of CCSID (\\*(``code page\\*('') 437, as follows."
msgstr ""

#. type: tbl table
#: archlinux mageia-cauldron
#, no-wrap
msgid "\\f(BItermcap\\fR Name"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Graphic"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "G2"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "upper left corner"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "G3"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "lower left corner"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "G1"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "upper right corner"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "G4"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "lower right corner"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GR"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "tee pointing right"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GL"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "tee pointing left"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GU"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "tee pointing up"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GD"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "tee pointing down"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GH"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "horizontal line"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GV"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "vertical line"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GC"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "intersection"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "G6"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "double upper left corner"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "G7"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "double lower left corner"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "G5"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "double upper right corner"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "G8"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "double lower right corner"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Gr"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "double tee pointing right"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "double tee pointing left"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Gu"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "double tee pointing up"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Gd"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "double tee pointing down"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Gh"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "double horizontal line"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Gv"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "double vertical line"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Gc"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "double intersection"
msgstr ""

#
#.  TODO: There are about 40 box drawing code points in CCSID 437;
#.  were there no XENIX capabilities for the mixed single- and double-
#.  line intersections?
#.  TODO: GG doesn't seem to fit with the others; explain it.
#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GG"
msgstr ""

#
#.  TODO: There are about 40 box drawing code points in CCSID 437;
#.  were there no XENIX capabilities for the mixed single- and double-
#.  line intersections?
#.  TODO: GG doesn't seem to fit with the others; explain it.
#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ACS magic cookie count"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"B<\\%captoinfo> composes single-line capabilities into an B<acsc> string, "
"and discards B<GG> and double-line capabilities with a warning diagnostic."
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"IBM's AIX has a I<\\%\\%term\\%info> facility descended from SVr1 I<\\%\\"
"%term\\%info>, but which is incompatible with the SVr4 format.  B<\\"
"%captoinfo> translates the following AIX extensions."
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "IBM"
msgstr ""

#. type: tbl table
#: archlinux mageia-cauldron
#, no-wrap
msgid "XSI"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ksel"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "kslt"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "kbtab"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "kcbt"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "font0"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "s0ds"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "font1"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "s1ds"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "font2"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "s2ds"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "font3"
msgstr ""

#. type: tbl table
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "s3ds"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"Additionally, this program translates the AIX B<box1> capability to an "
"B<acsc> string."
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"The HP-UX I<\\%\\%term\\%info> library supports two nonstandard I<\\%\\"
"%term\\%info> capabilities, B<meml> (memory lock) and B<memu> (memory "
"unlock).  B<\\%captoinfo> discards these with a warning message."
msgstr ""

#. type: SH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr ""

#. type: TP
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</etc/termcap>"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid "default I<\\%termcap> terminal capability database"
msgstr ""

#. type: SH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "PORTABILITY"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"X/Open Curses, Issue 7 (2009) describes B<tic> briefly, but omits this "
"program."
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"SVr4 systems provide B<\\%captoinfo> as a separate application from B<tic>.  "
"Its B<-v> option does not accept a trace level argument I<n>; repeat B<-v> "
"I<n> times instead."
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid "NetBSD does not provide this application."
msgstr ""

#. type: SH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid "Eric S. Raymond E<lt>esr@snark.thyrsus.comE<gt> and"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid "Thomas E. Dickey E<lt>dickey@invisible-island.netE<gt>"
msgstr ""

#. type: SH
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid "B<\\%infocmp>(1M), B<\\%tic>(1M), B<\\%curses>(3X), B<\\%terminfo>(5)"
msgstr ""

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "2025-01-18"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"B<\\%captoinfo> translates some obsolete, nonstandard capabilities into "
"standard I<\\%\\%term\\%info> capabilities.  It issues a diagnostic to the "
"standard error stream for each, inviting the user to check that it has not "
"mistakenly translated an unknown or mistyped capability name."
msgstr ""

#. type: tbl table
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "\\f(BItermcap\\fR Code"
msgstr ""

#. type: tbl table
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "X/Open"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"X/Open Curses Issue\\ 7 (2009) describes B<tic> briefly, but omits this "
"program."
msgstr ""
