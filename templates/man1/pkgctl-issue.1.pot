# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-26 08:53-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "PKGCTL-ISSUE"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2025-01-06"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl-issue - Work with GitLab packaging issues"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl issue [SUBCOMMAND] [OPTIONS]"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Work with GitLab packaging issues."
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Show a help text"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SUBCOMMANDS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl issue close"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Close an issue"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl issue comment"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Comment on an issue"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl issue create"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Create a new issue"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl issue edit"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Edit and modify an issue"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl issue list"
msgstr ""

#. type: Plain text
#: archlinux
msgid "List project or group issues"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl issue move"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Move an issue to another project"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl issue reopen"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Reopen a closed issue"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl issue view"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Display information about an issue"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"pkgctl-issue-close(1)  pkgctl-issue-comment(1)  pkgctl-issue-create(1)  "
"pkgctl-issue-edit(1)  pkgctl-issue-list(1)  pkgctl-issue-move(1)  pkgctl-"
"issue-reopen(1)  pkgctl-issue-view(1)"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"I<Please report bugs and feature requests in the issue tracker. Please do "
"your best to provide a reproducible test case for bugs.>"
msgstr ""
