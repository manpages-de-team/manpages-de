# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:57+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Xwdtopnm User Manual"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "08 January 2010"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "xwdtopnm - convert an X11 or X10 window dump file to a PNM image"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<xwdtopnm> [B<-verbose>] [B<-headerdump>] [I<xwdfile>]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)  \\&."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<xwdtopnm> reads an X11 or X10 window dump file as input and produces a PNM "
"image as output.  The type of the output image depends on the input file - "
"if it's black and white, the output is PBM.  If it's grayscale, the output "
"is PGM.  Otherwise, it's PPM.  The program tells you which type it is "
"writing."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Using this program, you can convert anything you can display on an X "
"workstation's screen into a PNM image.  Just display whatever you're "
"interested in, run the B<xwd> program to capture the contents of the window, "
"run it through B<xwdtopnm>, and then use B<pamcut> to select the part you "
"want."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that a pseudocolor XWD image (typically what you get when you make a "
"dump of a pseudocolor X window) has maxval 65535, which means the PNM file "
"that B<xwdtopnm> generates has maxval 65535.  Many older image processing "
"programs (that aren't part of the Netpbm package and don't use the Netpbm "
"programming library) don't know how to handle a PNM image with maxval "
"greater than 255 (because there are two bytes instead of one for each sample "
"in the image).  So you may want to run the output of B<xwdtopnm> through "
"B<pamdepth> before feeding it to one of these old programs."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<xwdtopnm> can't convert every kind of XWD image (which essentially means "
"it can't convert an XWD created from every kind of X display "
"configuration).  In particular, it cannot convert one with more than 24 bits "
"per pixel."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"In addition to the options common to all programs based on libnetpbm\n"
"(most notably B<-quiet>, see \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&), B<xwdtopnm> recognizes the following\n"
"command line options:\n"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This option causes B<xwdtopnm> to display handy information about the input "
"image and the conversion process"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-headerdump>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This option causes B<xwdtopnm> to display the contents of the X11 header.  "
"It has no effect when the input is X10.  This option was new in Netpbm 10.26 "
"(December 2004)."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: SS
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Two Byte Samples"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<xwdtopnm> sometimes produces output with a maxval greater than 255, which "
"means the maximum value of a sample (one intensity value, e.g. the red "
"component of a pixel) is greater than 255 and therefore each sample takes 2 "
"bytes to represent.  This can be a problem because some programs expect "
"those bytes in a different order from what the Netpbm format specs say, "
"which is what B<xwdtopnm> produces, which means they will see totally "
"different colors than they should.  B<xv> is one such program."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If this is a problem (e.g. you want to look at the output of B<xwdtopnm> "
"with B<xv>), there are two ways to fix it:"
msgstr ""

#. type: IP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Pass the output through B<pamendian> to produce the format the program "
"expects."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Pass the output through B<pamdepth> to reduce the maxval below 256 so there "
"is only one byte per sample."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Often, there is no good reason to have a maxval greater than 255.  It "
"happens because in XWD, but not PNM, each color component of a pixel can "
"have different resolution, for example 5 bits for blue (maxval 31), 5 bits "
"for red (maxval 31), and 6 bits for green (maxval 63), for a total of 16 "
"bits per pixel.  In order to reproduce the colors as closely as possible, "
"B<xwdtopnm> has to use a large maxval.  In this example, it would use 31 * "
"63 = 1953, and use 48 bits per pixel."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Because this is a common and frustrating problem when using B<xwdtopnm>, the "
"program issues a warning whenever it generates output with two byte "
"samples.  You can quiet this warning with the B<-quiet> E<.UR "
"index.html#commonoptions> common option E<.UE> \\&.  The warning was new in "
"Netpbm 10.46 (March 2009)."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"B<pnmtoxwd>(1)  \\&, B<pamendian>(1)  \\&, B<pamdepth>(1)  \\&, B<pnm>(1)  "
"\\&, B<xwd> man page"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Copyright (C) 1989, 1991 by Jef Poskanzer."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/xwdtopnm.html>"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide
msgid ""
"This option causes B<xwdtopnm> to display handy information about the input "
"image and the conversion process."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide
msgid ""
"Often, there is no good reason to have a maxval greater than 255.  It "
"happens because in XWD, but not PNM, each color component of a pixel can "
"have different resolution, for example 5 bits for blue (maxval 31), 5 bits "
"for red (maxval 31), and 6 bits for green (maxval 63), for a total of 16 "
"bits per pixel.  In order to reproduce the colors as closely as possible, "
"B<xwdtopnm> has to use a large maxval.  In this example, it would use maxval "
"31 * 63 = 1953, which requires two bytes per sample, totalling 48 bits per "
"pixel."
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pnmtoxwd>(1)  \\&, B<pamendian>(1)  \\&, B<pamdepth>(1)  \\&, B<pnm>(5)  "
"\\&, B<xwd> man page"
msgstr ""
