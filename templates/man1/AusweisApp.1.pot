# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-16 05:41+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "AusweisApp"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"AusweisApp - Official authentication app for German ID cards and residence "
"permits"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "AusweisApp [-h|--help]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "AusweisApp [--help-all]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "AusweisApp [-v|--version]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "AusweisApp [--show]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"AusweisApp [--keep] [--no-logfile] [--no-loghandler] [--show] [--no-proxy] "
"[--ui { qml|websocket }] [--port I<\\,PORT\\/>] [--address I<\\,ADDRESS\\/>]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"AusweisApp allows you to authenticate yourself against websites via your "
"German ID card and residence permits."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "You will need:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "* an ID card that is enabled for online identification"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"* A compatible NFC device (most NFC readers should work, NFC-enabled phones "
"can * also be used)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "* AusweisApp"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "* A browser"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "* A website that supports authentication via German ID card"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"When you visit such a website, AusweisApp will be triggered and will ask you "
"if you want to authenticate against the website."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"This program will provide a local webserver for your browser to interface "
"against."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-h, --help>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Displays a short help message."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--help-all>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Displays help including Qt specific options."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-v, --version>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Displays version information."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--keep>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"By default, AusweisApp writes a log to a file matching ${TMP}/"
"AusweisApp.*.log. When the program terminates, it will be deleted. This "
"setting prevents deletion."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--no-logfile>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Suppress writing a log file to ${TMP}/AusweisApp.*.log. Logs will still be "
"written to STDOUT."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--no-loghandler>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Disable default log handler. This disables logging to STDOUT."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--show>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Show window on startup."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--no-proxy>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Disable system proxy."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--ui { qml|webservice|websocket }>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"This option allows multiple values.  - \"qml\" will start the program with a "
"visible UI.  - \"websocket\" will let it start in the background as an SDK. "
"This is only useful when integrating AusweisApp into other programs.  - "
"\"webservice\" starts listening on given port/address."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "Default is \"qml,webservice,websocket\"."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--port >I<\\,PORT\\/>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Change the listening port for the WebSocket. Default is 24727. Selecting "
"\"0\" is a special case. AusweisApp will then select a random port and write "
"the port number to a file in ${TMP}/AusweisApp.E<lt>PIDE<gt>.port."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--address >I<\\,ADDRESS\\/>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Use given addresses for interface binding. Normally AusweisApp is bound to "
"localhost only as it is a security requirement. Useful for testing only.  "
"This option allows multiple values."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "AusweisApp will return 0 when successfully terminated."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<QT_QPA_PLATFORM={ wayland|X11 }>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"You may force the session type to wayland or X11. This is only needed for "
"debugging purposes. XDG_SESSION_TYPE will be ignored on gnome."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"I<~/.config/AusweisApp_CE/AusweisApp2.conf> File path where the user config "
"is saved."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Currently there is no way to terminate the program on gnome as the TrayIcon "
"is mandatory."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"This man page was written by Lee Garrett (debian@rocketjump.eu) for Debian "
"(but may be used by others)."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "https://www.ausweisapp.bund.de/en"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "https://www.ausweisapp.bund.de/sdk"
msgstr ""
