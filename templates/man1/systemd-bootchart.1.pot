# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:51+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYSTEMD-BOOTCHART"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "systemd 235"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "systemd-bootchart"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "systemd-bootchart - Boot performance graphing tool"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"B<systemd-bootchart> is a tool, usually run at system startup, that collects "
"the CPU load, disk load, memory usage, as well as per-process information "
"from a running system\\&. Collected results are output as an SVG graph\\&. "
"Normally, systemd-bootchart is invoked by the kernel by passing B<init=>B</"
"usr/lib/systemd/systemd-bootchart> on the kernel command line, adding "
"B<initcall_debug> to collect data on kernel init threads\\&. systemd-"
"bootchart will then fork the real init off to resume normal system startup, "
"while monitoring and logging startup information in the background\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"After collecting a certain amount of data (usually 15-30 seconds, default 20 "
"s) the logging stops and a graph is generated from the logged "
"information\\&. This graph contains vital clues as to which resources are "
"being used, in which order, and where possible problems exist in the startup "
"sequence of the system\\&. It is essentially a more detailed version of the "
"B<systemd-analyze plot> function\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Of course, bootchart can also be used at any moment in time to collect and "
"graph some data for an amount of time\\&. It is recommended to use the B<--"
"rel> switch in this case\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Bootchart does not require root privileges, and will happily run as a normal "
"user\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Bootchart graphs are by default written time-stamped in /run/log and saved "
"to the journal with I<MESSAGE_ID=9f26aa562cf440c2b16c773d0479b518>\\&. "
"Journal field I<BOOTCHART=> contains the bootchart in SVG format\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "INVOCATION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<systemd-bootchart> can be invoked in several different ways:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<Kernel invocation>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"The kernel can invoke B<systemd-bootchart> instead of the init process\\&. "
"In turn, B<systemd-bootchart> will invoke B</usr/lib/systemd/systemd>\\&. "
"Data will be collected on kernel init threads and also processes including "
"the services started by systemd\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<systemd unit>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"A unit file is provided, B<systemd-bootchart\\&.service>\\&. If enabled when "
"the system starts it will collect data on processes including the services "
"started by systemd\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<Started as a standalone program>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"One can execute B<systemd-bootchart> as normal application from the command "
"line\\&. In this mode it is highly recommended to pass the B<-r> flag in "
"order to not graph the time elapsed since boot and before systemd-bootchart "
"was started, as it may result in extremely large graphs\\&. The time elapsed "
"since boot might also include any time that the system was suspended\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"These options can also be set in the /etc/systemd/bootchart\\&.conf file\\&. "
"See B<bootchart.conf>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-n>, B<--sample >I<N>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Specify the number of samples, I<N>, to record\\&. Samples will be recorded "
"at intervals defined with B<--freq>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-f>, B<--freq >I<f>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Specify the sample log frequency, a positive real I<f>, in Hz\\&. Most "
"systems can cope with values up to 25-50 without creating too much "
"overhead\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-r>, B<--rel>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Use relative times instead of absolute times\\&. This is useful for using "
"bootchart at post-boot time to profile an already booted system\\&. Without "
"this option the graph would become extremely large\\&. If set, the "
"horizontal axis starts at the first recorded sample instead of time "
"0\\&.0\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-F>, B<--no-filter>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Disable filtering of tasks that did not contribute significantly to the "
"boot\\&. Processes that are too short-lived (only seen in one sample) or "
"that do not consume any significant CPU time (less than 0\\&.001 s) will not "
"be displayed in the output graph\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-C>, B<--cmdline>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Display the full command line with arguments of processes, instead of only "
"the process name\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-g>, B<--control-group>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "Display process control group"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-o>, B<--output >I<path>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Specify the output directory for the graphs\\&. By default, bootchart writes "
"the graphs to /run/log\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-i>, B<--init >I<path>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Use this init binary\\&. Defaults to B</usr/lib/systemd/systemd>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-p>, B<--pss>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Enable logging and graphing of processes\\*(Aq PSS (Proportional Set Size) "
"memory consumption\\&. See filesystems/proc\\&.txt in the kernel "
"documentation for an explanation of this field\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-e>, B<--entropy>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "Enable logging and graphing of the kernel random entropy pool size\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-x>, B<--scale-x >I<N>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "Horizontal scaling factor for all variable graph components\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<-y>, B<--scale-y >I<N>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "Vertical scaling factor for all variable graph components\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "OUTPUT"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd-bootchart> generates SVG graphs\\&. In order to render those on a "
"graphical display any SVG capable viewer can be used\\&. It should be noted "
"that the SVG render engines in most browsers (including Chrome and Firefox) "
"are many times faster than dedicated graphical applications like Gimp and "
"Inkscape\\&. Just point your browser at \\m[blue]B<\\%file:///run/log/>\\m[]!"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"This version of bootchart was implemented from scratch, but is inspired by "
"former bootchart incantations:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<Original bash>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"The original bash/shell code implemented bootchart\\&. This version created "
"a compressed tarball for processing with external applications\\&. This "
"version did not graph anything, only generated data\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<Ubuntu C Implementation>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"This version replaced the shell version with a fast and efficient data "
"logger, but also did not graph the data\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<Java bootchart>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"This was the original graphing application for charting the data, written in "
"java\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<pybootchartgui\\&.py>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"pybootchart created a graph from the data collected by either the bash or C "
"version\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"The version of bootchart you are using now combines both the data collection "
"and the charting into a single application, making it more efficient and "
"simpler\\&. There are no longer any timing issues with the data collector "
"and the grapher, as the graphing cannot be run until the data has been "
"collected\\&. Also, the data kept in memory is reduced to the absolute "
"minimum needed\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<bootchart.conf>(5)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-bootchart does not get the model information for the hard drive "
"unless the root device is specified with root=/dev/sdxY\\&. Using UUIDs or "
"PARTUUIDs will boot fine, but the hard drive model will not be added to the "
"chart\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "For bugs, please contact the author and current maintainer:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "Auke Kok E<lt>auke-jan\\&.h\\&.kok@intel\\&.comE<gt>"
msgstr ""

#. type: TH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "systemd 234"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<systemd-bootchart> is a tool, usually run at system startup, that collects "
"the CPU load, disk load, memory usage, as well as per-process information "
"from a running system\\&. Collected results are output as an SVG graph\\&. "
"Normally, systemd-bootchart is invoked by the kernel by passing B<init=>B</"
"lib/systemd/systemd-bootchart> on the kernel command line, adding "
"B<initcall_debug> to collect data on kernel init threads\\&. systemd-"
"bootchart will then fork the real init off to resume normal system startup, "
"while monitoring and logging startup information in the background\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The kernel can invoke B<systemd-bootchart> instead of the init process\\&. "
"In turn, B<systemd-bootchart> will invoke B</lib/systemd/systemd>\\&. Data "
"will be collected on kernel init threads and also processes including the "
"services started by systemd\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use this init binary\\&. Defaults to B</lib/systemd/systemd>\\&."
msgstr ""
