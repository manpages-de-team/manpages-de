# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-14 20:36+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "mkosi-addon"
msgstr ""

#. type: SH
#: archlinux debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"mkosi-addon \\[em] Build addons for unified kernel images for the current "
"system using mkosi"
msgstr ""

#. type: SH
#: archlinux debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "\\f[CR]mkosi-addon [options\\&...]\\fR"
msgstr ""

#. type: SH
#: archlinux debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"\\f[CR]mkosi-addon\\f[R] is a wrapper on top of \\f[CR]mkosi\\f[R] to "
"simplify the generation of PE addons containing customizations for unified "
"kernel images specific to the running or local system.  Will include entries "
"in \\f[CR]/etc/crypttab\\f[R] marked with \\f[CR]x-initrd.attach\\f[R], and "
"\\f[CR]/etc/kernel/cmdline\\f[R].  Kernel modules and firmwares for the "
"running hardware can be included if a local configuration with the option "
"\\f[CR]KernelModulesIncludeHost=\\f[R] is provided.\\fR"
msgstr ""

#. type: SH
#: archlinux debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "\\f[CR]--kernel-version=\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"Kernel version where to look for the kernel modules to include.  Defaults to "
"the kernel version of the running system (\\f[CR]uname -r\\f[R]).\\fR"
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "\\f[CR]--output=\\f[R], \\f[CR]-o\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"Name to use for the generated output addon.  Defaults to \\f[CR]mkosi-"
"local.addon.efi\\f[R].\\fR"
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "\\f[CR]--output-dir=\\f[R], \\f[CR]-O\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"Path to a directory where to place all generated artifacts.  Defaults to the "
"current working directory."
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "\\f[CR]--debug=\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "Enable additional debugging output."
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "\\f[CR]--debug-shell=\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "Spawn debug shell in sandbox if a sandboxed command fails."
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "\\f[CR]--version\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "Show package version."
msgstr ""

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "\\f[CR]--help\\f[R], \\f[CR]-h\\fR"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "Show brief usage information."
msgstr ""

#. type: SH
#: archlinux debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid "\\f[CR]mkosi(1)\\fR"
msgstr ""
