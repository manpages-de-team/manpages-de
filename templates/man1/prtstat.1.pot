# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-08-02 17:27+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "PRTSTAT"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "2020-09-09"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "psmisc"
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: opensuse-leap-16-0
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "prtstat - print statistics of a process"
msgstr ""

#. type: SH
#: opensuse-leap-16-0
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "B<prtstat> [B<-r>|B<--raw>] I<pid>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "B<prtstat> B<-V>|B<--version>"
msgstr ""

#. type: SH
#: opensuse-leap-16-0
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"B<prtstat> prints the statistics of the specified process.  This information "
"comes from the B</proc/>I<pid>B</stat> file."
msgstr ""

#. type: SH
#: opensuse-leap-16-0
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: opensuse-leap-16-0
#, no-wrap
msgid "B<-r>,B<\\ --raw>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "Print the information in raw format."
msgstr ""

#. type: TP
#: opensuse-leap-16-0
#, no-wrap
msgid "B<-V>,B<\\ --version>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "Show the version information for B<prtstat>."
msgstr ""

#. type: SH
#: opensuse-leap-16-0
#, no-wrap
msgid "FILES"
msgstr ""

#. type: TP
#: opensuse-leap-16-0
#, no-wrap
msgid "B</proc/>I<pid>B</stat>"
msgstr ""

#. type: Plain text
#: opensuse-leap-16-0
msgid "source of the information B<prtstat> uses."
msgstr ""
