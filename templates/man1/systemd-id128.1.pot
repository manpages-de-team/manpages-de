# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:52+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-ID128"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd 257.3"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "systemd-id128"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-id128 - Generate and print sd-128 identifiers"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<systemd-id128> [OPTIONS...] new"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<systemd-id128> [OPTIONS...] machine-id"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<systemd-id128> [OPTIONS...] boot-id"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<systemd-id128> [OPTIONS...] invocation-id"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<systemd-id128> [OPTIONS...] var-partition-uuid"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<systemd-id128> [OPTIONS...] show [NAME|UUID...]"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<id128> may be used to conveniently print B<sd-id128>(3)  UUIDs\\&. What "
"identifier is printed depends on the specific verb\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "With B<new>, a new random identifier will be generated\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"With B<machine-id>, the identifier of the current machine will be "
"printed\\&. See B<machine-id>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "With B<boot-id>, the identifier of the current boot will be printed\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"With B<invocation-id>, the identifier of the current service invocation will "
"be printed\\&. This is available in systemd services\\&. See "
"B<systemd.exec>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"With B<show>, well-known IDs are printed (for now, only GPT partition type "
"UUIDs), along with brief identifier strings\\&. When no arguments are "
"specified, all known IDs are shown\\&. When arguments are specified, they "
"may be the identifiers or ID values of one or more known IDs, which are then "
"printed with their name, or arbitrary IDs, which are then printed with a "
"placeholder name\\&. Combine with B<--uuid> to list the IDs in UUID style, "
"i\\&.e\\&. the way GPT partition type UUIDs are usually shown\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<machine-id>, B<boot-id>, and B<show> may be combined with the B<--app-"
"specific=>I<app-id> switch to generate application-specific IDs\\&. See "
"B<sd_id128_get_machine>(3)  for the discussion when this is useful\\&. "
"Support for B<show --app-specific=> was added in version 255\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<var-partition-uuid> prints a UUID which, following the "
"\\m[blue]B<Discoverable Partitions Specification>\\m[]"
"\\&\\s-2\\u[1]\\d\\s+2, should be used as the GPT partition UUID for /var/, "
"being derived from the GPT partition type, keyed by the local /etc/machine-"
"id\\&. Added in version 257\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following options are understood:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<-p>, B<--pretty>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Generate output as programming language snippets\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 240\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<-P>, B<--value>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Only print the value\\&. May be combined with B<-u>/B<--uuid>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 255\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<-a >I<app-id>, B<--app-specific=>I<app-id>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"With this option, identifiers will be printed that are the result of hashing "
"the application identifier I<app-id> and another ID\\&. The I<app-id> "
"argument must be a valid sd-id128 string identifying the application\\&. "
"When used with B<machine-id>, the other ID will be the machine ID as "
"described in B<machine-id>(5), when used with B<boot-id>, the other ID will "
"be the boot ID, and when used with B<show>, the other ID or IDs should be "
"specified via the positional arguments\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<-u>, B<--uuid>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Generate output as a UUID formatted in the \"canonical representation\", "
"with five groups of digits separated by hyphens\\&. See the Wikipedia entry "
"for \\m[blue]B<Universally Unique Identifiers>\\m[]\\&\\s-2\\u[2]\\d\\s+2 "
"for more discussion\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 244\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<--no-pager>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Do not pipe output into a pager\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<--no-legend>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Do not print the legend, i\\&.e\\&. column headers and the footer with "
"hints\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<--json=>I<MODE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Shows output formatted as JSON\\&. Expects one of \"short\" (for the "
"shortest possible output without any redundant whitespace or line breaks), "
"\"pretty\" (for a pretty version of the same, with indentation and line "
"breaks) or \"off\" (to turn off JSON output, the default)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<-j>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Equivalent to B<--json=pretty> if running on a terminal, and B<--json=short> "
"otherwise\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "On success 0 is returned, and a non-zero failure code otherwise\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<Example\\ \\&1.\\ \\&Show a well-known UUID>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ systemd-id128 show --value user-home\n"
"773f91ef66d449b5bd83d683bf40ad16\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ systemd-id128 show --value --uuid user-home\n"
"773f91ef-66d4-49b5-bd83-d683bf40ad16\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ systemd-id128 show 773f91ef-66d4-49b5-bd83-d683bf40ad16\n"
"NAME      ID\n"
"user-home 773f91ef66d449b5bd83d683bf40ad16\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<Example\\ \\&2.\\ \\&Generate an application-specific UUID>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ systemd-id128 machine-id -u\n"
"3a9d668b-4db7-4939-8a4a-5e78a03bffb7\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ systemd-id128 new -u\n"
"1fb8f24b-02df-458d-9659-cc8ace68e28a\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ systemd-id128 machine-id -u -a 1fb8f24b-02df-458d-9659-cc8ace68e28a\n"
"47b82cb1-5339-43da-b2a6-1c350aef1bd1\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ systemd-id128 -Pu show 3a9d668b-4db7-4939-8a4a-5e78a03bffb7 \\e\n"
"    -a 1fb8f24b-02df-458d-9659-cc8ace68e28a\n"
"47b82cb1-5339-43da-b2a6-1c350aef1bd1\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On a given machine with the ID 3a9d668b-4db7-4939-8a4a-5e78a03bffb7, for the "
"application 1fb8f24b-02df-458d-9659-cc8ace68e28a, we generate an application-"
"specific machine ID (47b82cb1-5339-43da-b2a6-1c350aef1bd1)\\&. If we want to "
"later recreate the same calculation on a different machine, we need to "
"specify both IDs explicitly as parameters to B<show>\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<systemd>(1), B<sd-id128>(3), B<sd_id128_get_machine>(3)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Discoverable Partitions Specification"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"\\%https://uapi-group.org/specifications/specs/"
"discoverable_partitions_specification"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Universally Unique Identifiers"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://en.wikipedia.org/wiki/Universally_unique_identifier#Format"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Both B<machine-id> and B<boot-id> may be combined with the B<--app-"
"specific=>I<app-id> switch to generate application-specific IDs\\&. See "
"B<sd_id128_get_machine>(3)  for the discussion when this is useful\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"With B<show>, well-known IDs are printed (for now, only GPT partition type "
"UUIDs), along with brief identifier strings\\&. When no arguments are "
"specified, all known IDs are shown\\&. When arguments are specified, they "
"must be the identifiers or ID values of one or more known IDs, which are "
"then printed\\&. Combine with B<--uuid> to list the IDs in UUID style, "
"i\\&.e\\&. the way GPT partition type UUIDs are usually shown\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"With this option, an identifier that is the result of hashing the "
"application identifier I<app-id> and the machine identifier will be "
"printed\\&. The I<app-id> argument must be a valid sd-id128 string "
"identifying the application\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Generate output as a UUID formatted in the \"canonical representation\", "
"with five groups of digits separated by hyphens\\&. See the "
"\\m[blue]B<wikipedia>\\m[]\\&\\s-2\\u[1]\\d\\s+2 for more discussion\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "On success, 0 is returned, a non-zero failure code otherwise\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "wikipedia"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Generate output as a UUID formatted in the \"canonical representation\", "
"with five groups of digits separated by hyphens\\&. See the Wikipedia entry "
"for \\m[blue]B<Universally Unique Identifiers>\\m[]\\&\\s-2\\u[1]\\d\\s+2 "
"for more discussion\\&."
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "systemd 257.2"
msgstr ""
