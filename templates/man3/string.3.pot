# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:51+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "string"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-12-22"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"stpcpy, strcasecmp, strcat, strchr, strcmp, strcoll, strcpy, strcspn, "
"strdup, strfry, strlen, strncat, strncmp, strncpy, strncasecmp, strpbrk, "
"strrchr, strsep, strspn, strstr, strtok, strxfrm, index, rindex - string "
"operations"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<#include E<lt>strings.hE<gt>>"
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strcasecmp>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Compare two strings, ignoring case."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strncasecmp>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Compare the first bytes of two strings, ignoring case."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<index>(3)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Identical to B<strchr>(3)."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<rindex>(3)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Identical to B<strrchr>(3)."
msgstr ""

#. #-#-#-#-#  archlinux: string.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: string.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: string.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-42: string.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: string.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: string.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-16-0: string.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: string.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>"
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<stpcpy>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Copy a string, returning a pointer to the end of the resulting string."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strcat>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Append a string into an existing string."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strchr>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Find the first occurrence of a character in a string."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strcmp>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Compare two strings."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strcoll>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Compare two strings, using the current locale."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strcpy>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Copy a string."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strcspn>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Calculate the length of the initial segment of a string which does not "
"contain any of the rejected bytes."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strdup>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Duplicate a string in memory allocated using B<malloc>(3)."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strfry>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Randomly swap the characters in a string."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strlen>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Return the length of a string."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strncat>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Append non-null bytes from an array to a string, and null-terminate the "
"result."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strncmp>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Compare the first bytes of two strings."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strpbrk>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Find the first occurrence in a string of one of the bytes in the accepted "
"bytes."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strrchr>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Find the last occurrence of a character in a string."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strsep>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Extract the initial field in a string that is delimited by one of the "
"delimiter bytes."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strspn>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Calculate the length of the initial segment of a string that consists "
"entirely of accepted bytes."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strstr>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Find the first occurrence of a substring in a string."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strtok>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Extract tokens from a string that are delimited by one of the delimiter "
"bytes."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strxfrm>(3)"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"Transforms a string to the current locale and copies the first bytes to a "
"buffer."
msgstr ""

#. type: TP
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<strncpy>(3)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Fill a fixed-size buffer with leading non-null bytes from a source array, "
"padding with null bytes as needed."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The string functions perform operations on null-terminated strings.  See the "
"individual man pages for descriptions of each function."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<bstring>(3), B<stpcpy>(3), B<strcasecmp>(3), B<strcat>(3), B<strchr>(3), "
"B<strcmp>(3), B<strcoll>(3), B<strcpy>(3), B<strcspn>(3), B<strdup>(3), "
"B<strfry>(3), B<strlen>(3), B<strncasecmp>(3), B<strncat>(3), B<strncmp>(3), "
"B<strncpy>(3), B<strpbrk>(3), B<strrchr>(3), B<strsep>(3), B<strspn>(3), "
"B<strstr>(3), B<strtok>(3), B<strxfrm>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-22"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<int strcasecmp(const char *>I<s1>B<, const char *>I<s2>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Compare the strings I<s1> and I<s2> ignoring case."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<int strncasecmp(const char >I<s1>B<[.>I<n>B<], const char >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Compare the first I<n> bytes of the strings I<s1> and I<s2> ignoring case."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *index(const char *>I<s>B<, int >I<c>B<);>"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *rindex(const char *>I<s>B<, int >I<c>B<);>"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *stpcpy(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Copy a string from I<src> to I<dest>, returning a pointer to the end of the "
"resulting string at I<dest>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *strcat(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Append the string I<src> to the string I<dest>, returning a pointer I<dest>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *strchr(const char *>I<s>B<, int >I<c>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Return a pointer to the first occurrence of the character I<c> in the string "
"I<s>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<int strcmp(const char *>I<s1>B<, const char *>I<s2>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Compare the strings I<s1> with I<s2>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<int strcoll(const char *>I<s1>B<, const char *>I<s2>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Compare the strings I<s1> with I<s2> using the current locale."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *strcpy(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Copy the string I<src> to I<dest>, returning a pointer to the start of "
"I<dest>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<size_t strcspn(const char *>I<s>B<, const char *>I<reject>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Calculate the length of the initial segment of the string I<s> which does "
"not contain any of bytes in the string I<reject>,"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *strdup(const char *>I<s>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Return a duplicate of the string I<s> in memory allocated using B<malloc>(3)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *strfry(char *>I<string>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Randomly swap the characters in I<string>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<size_t strlen(const char *>I<s>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Return the length of the string I<s>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<char *strncat(char >I<dest>B<[restrict strlen(.>I<dest>B<) + .>I<n>B< + 1],>\n"
"B<       const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<       size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Append at most I<n> bytes from the unterminated string I<src> to the string "
"I<dest>, returning a pointer to I<dest>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<int strncmp(const char >I<s1>B<[.>I<n>B<], const char >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Compare at most I<n> bytes of the strings I<s1> and I<s2>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *strpbrk(const char *>I<s>B<, const char *>I<accept>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Return a pointer to the first occurrence in the string I<s> of one of the "
"bytes in the string I<accept>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *strrchr(const char *>I<s>B<, int >I<c>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Return a pointer to the last occurrence of the character I<c> in the string "
"I<s>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *strsep(char **restrict >I<stringp>B<, const char *restrict >I<delim>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Extract the initial token in I<stringp> that is delimited by one of the "
"bytes in I<delim>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<size_t strspn(const char *>I<s>B<, const char *>I<accept>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Calculate the length of the starting segment in the string I<s> that "
"consists entirely of bytes in I<accept>."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *strstr(const char *>I<haystack>B<, const char *>I<needle>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Find the first occurrence of the substring I<needle> in the string "
"I<haystack>, returning a pointer to the found substring."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<char *strtok(char *restrict >I<s>B<, const char *restrict >I<delim>B<);>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Extract tokens from the string I<s> that are delimited by one of the bytes "
"in I<delim>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<size_t strxfrm(char >I<dest>B<[restrict .>I<n>B<], const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<        size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"Transforms I<src> to the current locale and copies the first I<n> bytes to "
"I<dest>."
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Obsolete functions"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<char *strncpy(char >I<dest>B<[restrict .>I<n>B<], const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<       size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Copy at most I<n> bytes from string I<src> to I<dest>, returning a pointer "
"to the start of I<dest>."
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-11-14"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
