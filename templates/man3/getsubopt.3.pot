# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "getsubopt"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "getsubopt - parse suboption arguments from a string"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getsubopt(char **restrict >I<optionp>B<, char *const *restrict >I<tokens>B<,>\n"
"B<              char **restrict >I<valuep>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<getsubopt>():"
msgstr ""

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<getsubopt>()  parses the list of comma-separated suboptions provided in "
"I<optionp>.  (Such a suboption list is typically produced when B<getopt>(3)  "
"is used to parse a command line; see for example the I<-o> option of "
"B<mount>(8).)  Each suboption may include an associated value, which is "
"separated from the suboption name by an equal sign.  The following is an "
"example of the kind of string that might be passed in I<optionp>:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ro,name=xyz>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<tokens> argument is a pointer to a NULL-terminated array of pointers "
"to the tokens that B<getsubopt>()  will look for in I<optionp>.  The tokens "
"should be distinct, null-terminated strings containing at least one "
"character, with no embedded equal signs or commas."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each call to B<getsubopt>()  returns information about the next unprocessed "
"suboption in I<optionp>.  The first equal sign in a suboption (if any) is "
"interpreted as a separator between the name and the value of that "
"suboption.  The value extends to the next comma, or (for the last suboption) "
"to the end of the string.  If the name of the suboption matches a known name "
"from I<tokens>, and a value string was found, B<getsubopt>()  sets "
"I<*valuep> to the address of that string.  The first comma in I<optionp> is "
"overwritten with a null byte, so I<*valuep> is precisely the \"value "
"string\" for that suboption."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the suboption is recognized, but no value string was found, I<*valuep> is "
"set to NULL."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"When B<getsubopt>()  returns, I<optionp> points to the next suboption, or to "
"the null byte (\\[aq]\\[rs]0\\[aq]) at the end of the string if the last "
"suboption was just processed."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the first suboption in I<optionp> is recognized, B<getsubopt>()  returns "
"the index of the matching suboption element in I<tokens>.  Otherwise, -1 is "
"returned and I<*valuep> is the entire I<name>B<[=>I<value>B<]> string."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since I<*optionp> is changed, the first suboption before the call to "
"B<getsubopt>()  is not (necessarily) the same as the first suboption after "
"B<getsubopt>()."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<getsubopt>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Since B<getsubopt>()  overwrites any commas it finds in the string "
"I<*optionp>, that string must be writable; it cannot be a string constant."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following program expects suboptions following a \"-o\" option."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _XOPEN_SOURCE 500\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"#include E<lt>assert.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    enum {\n"
"        RO_OPT = 0,\n"
"        RW_OPT,\n"
"        NAME_OPT\n"
"    };\n"
"    char *const token[] = {\n"
"        [RO_OPT]   = \"ro\",\n"
"        [RW_OPT]   = \"rw\",\n"
"        [NAME_OPT] = \"name\",\n"
"        NULL\n"
"    };\n"
"    char *subopts;\n"
"    char *value;\n"
"    int opt;\n"
"\\&\n"
"    int readonly = 0;\n"
"    int readwrite = 0;\n"
"    char *name = NULL;\n"
"    int errfnd = 0;\n"
"\\&\n"
"    while ((opt = getopt(argc, argv, \"o:\")) != -1) {\n"
"        switch (opt) {\n"
"        case \\[aq]o\\[aq]:\n"
"            subopts = optarg;\n"
"            while (*subopts != \\[aq]\\[rs]0\\[aq] && !errfnd) {\n"
"\\&\n"
"                switch (getsubopt(&subopts, token, &value)) {\n"
"                case RO_OPT:\n"
"                    readonly = 1;\n"
"                    break;\n"
"\\&\n"
"                case RW_OPT:\n"
"                    readwrite = 1;\n"
"                    break;\n"
"\\&\n"
"                case NAME_OPT:\n"
"                    if (value == NULL) {\n"
"                        fprintf(stderr,\n"
"                                \"Missing value for suboption \\[aq]%s\\[aq]\\[rs]n\",\n"
"                                token[NAME_OPT]);\n"
"                        errfnd = 1;\n"
"                        continue;\n"
"                    }\n"
"\\&\n"
"                    name = value;\n"
"                    break;\n"
"\\&\n"
"                default:\n"
"                    fprintf(stderr,\n"
"                            \"No match found for token: /%s/\\[rs]n\", value);\n"
"                    errfnd = 1;\n"
"                    break;\n"
"                }\n"
"            }\n"
"            if (readwrite && readonly) {\n"
"                fprintf(stderr,\n"
"                        \"Only one of \\[aq]%s\\[aq] and \\[aq]%s\\[aq] can be specified\\[rs]n\",\n"
"                        token[RO_OPT], token[RW_OPT]);\n"
"                errfnd = 1;\n"
"            }\n"
"            break;\n"
"\\&\n"
"        default:\n"
"            errfnd = 1;\n"
"        }\n"
"    }\n"
"\\&\n"
"    if (errfnd || argc == 1) {\n"
"        fprintf(stderr, \"\\[rs]nUsage: %s -o E<lt>suboptstringE<gt>\\[rs]n\", argv[0]);\n"
"        fprintf(stderr,\n"
"                \"suboptions are \\[aq]ro\\[aq], \\[aq]rw\\[aq], and \\[aq]name=E<lt>valueE<gt>\\[aq]\\[rs]n\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* Remainder of program... */\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<getopt>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"When B<getsubopt>()  returns, I<optionp> points to the next suboption, or to "
"the null byte (\\[aq]\\e0\\[aq]) at the end of the string if the last "
"suboption was just processed."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define _XOPEN_SOURCE 500\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "#include E<lt>assert.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    enum {\n"
"        RO_OPT = 0,\n"
"        RW_OPT,\n"
"        NAME_OPT\n"
"    };\n"
"    char *const token[] = {\n"
"        [RO_OPT]   = \"ro\",\n"
"        [RW_OPT]   = \"rw\",\n"
"        [NAME_OPT] = \"name\",\n"
"        NULL\n"
"    };\n"
"    char *subopts;\n"
"    char *value;\n"
"    int opt;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    int readonly = 0;\n"
"    int readwrite = 0;\n"
"    char *name = NULL;\n"
"    int errfnd = 0;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    while ((opt = getopt(argc, argv, \"o:\")) != -1) {\n"
"        switch (opt) {\n"
"        case \\[aq]o\\[aq]:\n"
"            subopts = optarg;\n"
"            while (*subopts != \\[aq]\\e0\\[aq] && !errfnd) {\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"                switch (getsubopt(&subopts, token, &value)) {\n"
"                case RO_OPT:\n"
"                    readonly = 1;\n"
"                    break;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"                case RW_OPT:\n"
"                    readwrite = 1;\n"
"                    break;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"                case NAME_OPT:\n"
"                    if (value == NULL) {\n"
"                        fprintf(stderr,\n"
"                                \"Missing value for suboption \\[aq]%s\\[aq]\\en\",\n"
"                                token[NAME_OPT]);\n"
"                        errfnd = 1;\n"
"                        continue;\n"
"                    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"                    name = value;\n"
"                    break;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"                default:\n"
"                    fprintf(stderr,\n"
"                            \"No match found for token: /%s/\\en\", value);\n"
"                    errfnd = 1;\n"
"                    break;\n"
"                }\n"
"            }\n"
"            if (readwrite && readonly) {\n"
"                fprintf(stderr,\n"
"                        \"Only one of \\[aq]%s\\[aq] and \\[aq]%s\\[aq] can be specified\\en\",\n"
"                        token[RO_OPT], token[RW_OPT]);\n"
"                errfnd = 1;\n"
"            }\n"
"            break;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"        default:\n"
"            errfnd = 1;\n"
"        }\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (errfnd || argc == 1) {\n"
"        fprintf(stderr, \"\\enUsage: %s -o E<lt>suboptstringE<gt>\\en\", argv[0]);\n"
"        fprintf(stderr,\n"
"                \"suboptions are \\[aq]ro\\[aq], \\[aq]rw\\[aq], and \\[aq]name=E<lt>valueE<gt>\\[aq]\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* Remainder of program... */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#define _XOPEN_SOURCE 500\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"#include E<lt>assert.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    enum {\n"
"        RO_OPT = 0,\n"
"        RW_OPT,\n"
"        NAME_OPT\n"
"    };\n"
"    char *const token[] = {\n"
"        [RO_OPT]   = \"ro\",\n"
"        [RW_OPT]   = \"rw\",\n"
"        [NAME_OPT] = \"name\",\n"
"        NULL\n"
"    };\n"
"    char *subopts;\n"
"    char *value;\n"
"    int opt;\n"
"\\&\n"
"    int readonly = 0;\n"
"    int readwrite = 0;\n"
"    char *name = NULL;\n"
"    int errfnd = 0;\n"
"\\&\n"
"    while ((opt = getopt(argc, argv, \"o:\")) != -1) {\n"
"        switch (opt) {\n"
"        case \\[aq]o\\[aq]:\n"
"            subopts = optarg;\n"
"            while (*subopts != \\[aq]\\e0\\[aq] && !errfnd) {\n"
"\\&\n"
"                switch (getsubopt(&subopts, token, &value)) {\n"
"                case RO_OPT:\n"
"                    readonly = 1;\n"
"                    break;\n"
"\\&\n"
"                case RW_OPT:\n"
"                    readwrite = 1;\n"
"                    break;\n"
"\\&\n"
"                case NAME_OPT:\n"
"                    if (value == NULL) {\n"
"                        fprintf(stderr,\n"
"                                \"Missing value for suboption \\[aq]%s\\[aq]\\en\",\n"
"                                token[NAME_OPT]);\n"
"                        errfnd = 1;\n"
"                        continue;\n"
"                    }\n"
"\\&\n"
"                    name = value;\n"
"                    break;\n"
"\\&\n"
"                default:\n"
"                    fprintf(stderr,\n"
"                            \"No match found for token: /%s/\\en\", value);\n"
"                    errfnd = 1;\n"
"                    break;\n"
"                }\n"
"            }\n"
"            if (readwrite && readonly) {\n"
"                fprintf(stderr,\n"
"                        \"Only one of \\[aq]%s\\[aq] and \\[aq]%s\\[aq] can be specified\\en\",\n"
"                        token[RO_OPT], token[RW_OPT]);\n"
"                errfnd = 1;\n"
"            }\n"
"            break;\n"
"\\&\n"
"        default:\n"
"            errfnd = 1;\n"
"        }\n"
"    }\n"
"\\&\n"
"    if (errfnd || argc == 1) {\n"
"        fprintf(stderr, \"\\enUsage: %s -o E<lt>suboptstringE<gt>\\en\", argv[0]);\n"
"        fprintf(stderr,\n"
"                \"suboptions are \\[aq]ro\\[aq], \\[aq]rw\\[aq], and \\[aq]name=E<lt>valueE<gt>\\[aq]\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* Remainder of program... */\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
