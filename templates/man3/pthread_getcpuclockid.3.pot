# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:45+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_getcpuclockid"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-11-03"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "pthread_getcpuclockid - retrieve ID of a thread's CPU time clock"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>,\\ I<-lpthread>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>pthread.hE<gt>>\n"
"B<#include E<lt>time.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<int pthread_getcpuclockid(pthread_t >I<thread>B<, clockid_t *>I<clockid>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#.  The clockid is constructed as follows:
#.  *clockid = CLOCK_THREAD_CPUTIME_ID | (pd->tid << CLOCK_IDFIELD_SIZE)
#.  where CLOCK_IDFIELD_SIZE is 3.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pthread_getcpuclockid>()  function obtains the ID of the CPU-time "
"clock of the thread whose ID is given in I<thread>, and returns it in the "
"location pointed to by I<clockid>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, this function returns 0; on error, it returns a nonzero error "
"number."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr ""

#
#.  CLOCK_THREAD_CPUTIME_ID not defined
#.  Looking at nptl/pthread_getcpuclockid.c an ERANGE error would
#.  be possible if kernel thread IDs took more than 29 bits (which
#.  they currently cannot).
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Per-thread CPU time clocks are not supported by the system."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "No thread with the ID I<thread> could be found."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<pthread_getcpuclockid>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.2.  POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When I<thread> refers to the calling thread, this function returns an "
"identifier that refers to the same clock manipulated by B<clock_gettime>(2)  "
"and B<clock_settime>(2)  when given the clock ID B<CLOCK_THREAD_CPUTIME_ID>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The program below creates a thread and then uses B<clock_gettime>(2)  to "
"retrieve the total process CPU time, and the per-thread CPU time consumed by "
"the two threads.  The following shell session shows an example run:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./a.out>\n"
"Main thread sleeping\n"
"Subthread starting infinite loop\n"
"Main thread consuming some CPU time...\n"
"Process total CPU time:    1.368\n"
"Main thread CPU time:      0.376\n"
"Subthread CPU time:        0.992\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"/* Link with \"-lrt\" */\n"
"\\&\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/time.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define handle_error(msg) \\[rs]\n"
"        do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"#define handle_error_en(en, msg) \\[rs]\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void *\n"
"thread_start(void *arg)\n"
"{\n"
"    printf(\"Subthread starting infinite loop\\[rs]n\");\n"
"    for (;;)\n"
"        continue;\n"
"}\n"
"\\&\n"
"static void\n"
"pclock(char *msg, clockid_t cid)\n"
"{\n"
"    struct timespec ts;\n"
"\\&\n"
"    printf(\"%s\", msg);\n"
"    if (clock_gettime(cid, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"
"    printf(\"%4jd.%03ld\\[rs]n\", (intmax_t) ts.tv_sec, ts.tv_nsec / 1000000);\n"
"}\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thread;\n"
"    clockid_t cid;\n"
"    int s;\n"
"\\&\n"
"    s = pthread_create(&thread, NULL, thread_start, NULL);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_create\");\n"
"\\&\n"
"    printf(\"Main thread sleeping\\[rs]n\");\n"
"    sleep(1);\n"
"\\&\n"
"    printf(\"Main thread consuming some CPU time...\\[rs]n\");\n"
"    for (unsigned int j = 0; j E<lt> 2000000; j++)\n"
"        getppid();\n"
"\\&\n"
"    pclock(\"Process total CPU time: \", CLOCK_PROCESS_CPUTIME_ID);\n"
"\\&\n"
"    s = pthread_getcpuclockid(pthread_self(), &cid);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_getcpuclockid\");\n"
"    pclock(\"Main thread CPU time:   \", cid);\n"
"\\&\n"
"    /* The preceding 4 lines of code could have been replaced by:\n"
"       pclock(\"Main thread CPU time:   \", CLOCK_THREAD_CPUTIME_ID); */\n"
"\\&\n"
"    s = pthread_getcpuclockid(thread, &cid);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_getcpuclockid\");\n"
"    pclock(\"Subthread CPU time: 1    \", cid);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);         /* Terminates both threads */\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<clock_gettime>(2), B<clock_settime>(2), B<timer_create>(2), "
"B<clock_getcpuclockid>(3), B<pthread_self>(3), B<pthreads>(7), B<time>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This function is available since glibc 2.2."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "/* Link with \"-lrt\" */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define handle_error(msg) \\e\n"
"        do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void *\n"
"thread_start(void *arg)\n"
"{\n"
"    printf(\"Subthread starting infinite loop\\en\");\n"
"    for (;;)\n"
"        continue;\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void\n"
"pclock(char *msg, clockid_t cid)\n"
"{\n"
"    struct timespec ts;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"%s\", msg);\n"
"    if (clock_gettime(cid, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"
"    printf(\"%4jd.%03ld\\en\", (intmax_t) ts.tv_sec, ts.tv_nsec / 1000000);\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thread;\n"
"    clockid_t cid;\n"
"    int s;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    s = pthread_create(&thread, NULL, thread_start, NULL);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_create\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"Main thread sleeping\\en\");\n"
"    sleep(1);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"Main thread consuming some CPU time...\\en\");\n"
"    for (unsigned int j = 0; j E<lt> 2000000; j++)\n"
"        getppid();\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    pclock(\"Process total CPU time: \", CLOCK_PROCESS_CPUTIME_ID);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    s = pthread_getcpuclockid(pthread_self(), &cid);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_getcpuclockid\");\n"
"    pclock(\"Main thread CPU time:   \", cid);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* The preceding 4 lines of code could have been replaced by:\n"
"       pclock(\"Main thread CPU time:   \", CLOCK_THREAD_CPUTIME_ID); */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    s = pthread_getcpuclockid(thread, &cid);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_getcpuclockid\");\n"
"    pclock(\"Subthread CPU time: 1    \", cid);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);         /* Terminates both threads */\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid ""
"/* Link with \"-lrt\" */\n"
"\\&\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define handle_error(msg) \\[rs]\n"
"        do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"#define handle_error_en(en, msg) \\[rs]\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void *\n"
"thread_start(void *arg)\n"
"{\n"
"    printf(\"Subthread starting infinite loop\\[rs]n\");\n"
"    for (;;)\n"
"        continue;\n"
"}\n"
"\\&\n"
"static void\n"
"pclock(char *msg, clockid_t cid)\n"
"{\n"
"    struct timespec ts;\n"
"\\&\n"
"    printf(\"%s\", msg);\n"
"    if (clock_gettime(cid, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"
"    printf(\"%4jd.%03ld\\[rs]n\", (intmax_t) ts.tv_sec, ts.tv_nsec / 1000000);\n"
"}\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thread;\n"
"    clockid_t cid;\n"
"    int s;\n"
"\\&\n"
"    s = pthread_create(&thread, NULL, thread_start, NULL);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_create\");\n"
"\\&\n"
"    printf(\"Main thread sleeping\\[rs]n\");\n"
"    sleep(1);\n"
"\\&\n"
"    printf(\"Main thread consuming some CPU time...\\[rs]n\");\n"
"    for (unsigned int j = 0; j E<lt> 2000000; j++)\n"
"        getppid();\n"
"\\&\n"
"    pclock(\"Process total CPU time: \", CLOCK_PROCESS_CPUTIME_ID);\n"
"\\&\n"
"    s = pthread_getcpuclockid(pthread_self(), &cid);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_getcpuclockid\");\n"
"    pclock(\"Main thread CPU time:   \", cid);\n"
"\\&\n"
"    /* The preceding 4 lines of code could have been replaced by:\n"
"       pclock(\"Main thread CPU time:   \", CLOCK_THREAD_CPUTIME_ID); */\n"
"\\&\n"
"    s = pthread_getcpuclockid(thread, &cid);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_getcpuclockid\");\n"
"    pclock(\"Subthread CPU time: 1    \", cid);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);         /* Terminates both threads */\n"
"}\n"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"/* Link with \"-lrt\" */\n"
"\\&\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define handle_error(msg) \\e\n"
"        do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void *\n"
"thread_start(void *arg)\n"
"{\n"
"    printf(\"Subthread starting infinite loop\\en\");\n"
"    for (;;)\n"
"        continue;\n"
"}\n"
"\\&\n"
"static void\n"
"pclock(char *msg, clockid_t cid)\n"
"{\n"
"    struct timespec ts;\n"
"\\&\n"
"    printf(\"%s\", msg);\n"
"    if (clock_gettime(cid, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"
"    printf(\"%4jd.%03ld\\en\", (intmax_t) ts.tv_sec, ts.tv_nsec / 1000000);\n"
"}\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thread;\n"
"    clockid_t cid;\n"
"    int s;\n"
"\\&\n"
"    s = pthread_create(&thread, NULL, thread_start, NULL);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_create\");\n"
"\\&\n"
"    printf(\"Main thread sleeping\\en\");\n"
"    sleep(1);\n"
"\\&\n"
"    printf(\"Main thread consuming some CPU time...\\en\");\n"
"    for (unsigned int j = 0; j E<lt> 2000000; j++)\n"
"        getppid();\n"
"\\&\n"
"    pclock(\"Process total CPU time: \", CLOCK_PROCESS_CPUTIME_ID);\n"
"\\&\n"
"    s = pthread_getcpuclockid(pthread_self(), &cid);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_getcpuclockid\");\n"
"    pclock(\"Main thread CPU time:   \", cid);\n"
"\\&\n"
"    /* The preceding 4 lines of code could have been replaced by:\n"
"       pclock(\"Main thread CPU time:   \", CLOCK_THREAD_CPUTIME_ID); */\n"
"\\&\n"
"    s = pthread_getcpuclockid(thread, &cid);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_getcpuclockid\");\n"
"    pclock(\"Subthread CPU time: 1    \", cid);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);         /* Terminates both threads */\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
