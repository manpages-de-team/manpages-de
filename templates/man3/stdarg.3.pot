# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:50+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "stdarg"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "stdarg, va_start, va_arg, va_end, va_copy - variable argument lists"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdarg.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void va_start(va_list >I<ap>B<, >I<last>B<);>\n"
"I<type>B< va_arg(va_list >I<ap>B<, >I<type>B<);>\n"
"B<void va_end(va_list >I<ap>B<);>\n"
"B<void va_copy(va_list >I<dest>B<, va_list >I<src>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A function may be called with a varying number of arguments of varying "
"types.  The include file I<E<lt>stdarg.hE<gt>> declares a type I<va_list> "
"and defines three macros for stepping through a list of arguments whose "
"number and types are not known to the called function."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The called function must declare an object of type I<va_list> which is used "
"by the macros B<va_start>(), B<va_arg>(), and B<va_end>()."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "va_start()"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<va_start>()  macro initializes I<ap> for subsequent use by "
"B<va_arg>()  and B<va_end>(), and must be called first."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The argument I<last> is the name of the last argument before the variable "
"argument list, that is, the last argument of which the calling function "
"knows the type."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Because the address of this argument may be used in the B<va_start>()  "
"macro, it should not be declared as a register variable, or as a function or "
"an array type."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "va_arg()"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<va_arg>()  macro expands to an expression that has the type and value "
"of the next argument in the call.  The argument I<ap> is the I<va_list> "
"I<ap> initialized by B<va_start>().  Each call to B<va_arg>()  modifies "
"I<ap> so that the next call returns the next argument.  The argument I<type> "
"is a type name specified so that the type of a pointer to an object that has "
"the specified type can be obtained simply by adding a * to I<type>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The first use of the B<va_arg>()  macro after that of the B<va_start>()  "
"macro returns the argument after I<last>.  Successive invocations return the "
"values of the remaining arguments."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If there is no next argument, or if I<type> is not compatible with the type "
"of the actual next argument (as promoted according to the default argument "
"promotions), random errors will occur."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<ap> is passed to a function that uses B<va_arg(>I<ap>B<,>I<type>B<),> "
"then the value of I<ap> is undefined after the return of that function."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "va_end()"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each invocation of B<va_start>()  must be matched by a corresponding "
"invocation of B<va_end>()  in the same function.  After the call "
"B<va_end(>I<ap>B<)> the variable I<ap> is undefined.  Multiple traversals of "
"the list, each bracketed by B<va_start>()  and B<va_end>()  are possible.  "
"B<va_end>()  may be a macro or a function."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "va_copy()"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<va_copy>()  macro copies the (previously initialized) variable "
"argument list I<src> to I<dest>.  The behavior is as if B<va_start>()  were "
"applied to I<dest> with the same I<last> argument, followed by the same "
"number of B<va_arg>()  invocations that was used to reach the current state "
"of I<src>."
msgstr ""

#.  Proposal from clive@demon.net, 1997-02-28
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"An obvious implementation would have a I<va_list> be a pointer to the stack "
"frame of the variadic function.  In such a setup (by far the most common) "
"there seems nothing against an assignment"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "va_list aq = ap;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Unfortunately, there are also systems that make it an array of pointers (of "
"length 1), and there one needs"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"va_list aq;\n"
"*aq = *ap;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Finally, on systems where arguments are passed in registers, it may be "
"necessary for B<va_start>()  to allocate memory, store the arguments there, "
"and also an indication of which argument is next, so that B<va_arg>()  can "
"step through the list.  Now B<va_end>()  can free the allocated memory "
"again.  To accommodate this situation, C99 adds a macro B<va_copy>(), so "
"that the above assignment can be replaced by"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"va_list aq;\n"
"va_copy(aq, ap);\n"
"\\&...\n"
"va_end(aq);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Each invocation of B<va_copy>()  must be matched by a corresponding "
"invocation of B<va_end>()  in the same function.  Some systems that do not "
"supply B<va_copy>()  have B<__va_copy> instead, since that was the name used "
"in the draft proposal."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<va_start>(),\n"
"B<va_end>(),\n"
"B<va_copy>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. #-#-#-#-#  archlinux: stdarg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: stdarg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: stdarg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-42: stdarg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: stdarg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  mageia-cauldron: stdarg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-16-0: stdarg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: stdarg.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<va_arg>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe race:ap"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<va_start>()"
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<va_end>()"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C89, POSIX.1-2001."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<va_copy>()"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Unlike the historical B<varargs> macros, the B<stdarg> macros do not permit "
"programmers to code a function with no fixed arguments.  This problem "
"generates work mainly when converting B<varargs> code to B<stdarg> code, but "
"it also creates difficulties for variadic functions that wish to pass all of "
"their arguments on to a function that takes a I<va_list> argument, such as "
"B<vfprintf>(3)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The function I<foo> takes a string of format characters and prints out the "
"argument associated with each format character based on the type."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdarg.hE<gt>\n"
"\\&\n"
"void\n"
"foo(char *fmt, ...)   /* \\[aq]...\\[aq] is C syntax for a variadic function */\n"
"\\&\n"
"{\n"
"    va_list ap;\n"
"    int d;\n"
"    char c;\n"
"    char *s;\n"
"\\&\n"
"    va_start(ap, fmt);\n"
"    while (*fmt)\n"
"        switch (*fmt++) {\n"
"        case \\[aq]s\\[aq]:              /* string */\n"
"            s = va_arg(ap, char *);\n"
"            printf(\"string %s\\[rs]n\", s);\n"
"            break;\n"
"        case \\[aq]d\\[aq]:              /* int */\n"
"            d = va_arg(ap, int);\n"
"            printf(\"int %d\\[rs]n\", d);\n"
"            break;\n"
"        case \\[aq]c\\[aq]:              /* char */\n"
"            /* need a cast here since va_arg only\n"
"               takes fully promoted types */\n"
"            c = (char) va_arg(ap, int);\n"
"            printf(\"char %c\\[rs]n\", c);\n"
"            break;\n"
"        }\n"
"    va_end(ap);\n"
"}\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<vprintf>(3), B<vscanf>(3), B<vsyslog>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "C99."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdarg.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"void\n"
"foo(char *fmt, ...)   /* \\[aq]...\\[aq] is C syntax for a variadic function */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"{\n"
"    va_list ap;\n"
"    int d;\n"
"    char c;\n"
"    char *s;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    va_start(ap, fmt);\n"
"    while (*fmt)\n"
"        switch (*fmt++) {\n"
"        case \\[aq]s\\[aq]:              /* string */\n"
"            s = va_arg(ap, char *);\n"
"            printf(\"string %s\\en\", s);\n"
"            break;\n"
"        case \\[aq]d\\[aq]:              /* int */\n"
"            d = va_arg(ap, int);\n"
"            printf(\"int %d\\en\", d);\n"
"            break;\n"
"        case \\[aq]c\\[aq]:              /* char */\n"
"            /* need a cast here since va_arg only\n"
"               takes fully promoted types */\n"
"            c = (char) va_arg(ap, int);\n"
"            printf(\"char %c\\en\", c);\n"
"            break;\n"
"        }\n"
"    va_end(ap);\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdarg.hE<gt>\n"
"\\&\n"
"void\n"
"foo(char *fmt, ...)   /* \\[aq]...\\[aq] is C syntax for a variadic function */\n"
"\\&\n"
"{\n"
"    va_list ap;\n"
"    int d;\n"
"    char c;\n"
"    char *s;\n"
"\\&\n"
"    va_start(ap, fmt);\n"
"    while (*fmt)\n"
"        switch (*fmt++) {\n"
"        case \\[aq]s\\[aq]:              /* string */\n"
"            s = va_arg(ap, char *);\n"
"            printf(\"string %s\\en\", s);\n"
"            break;\n"
"        case \\[aq]d\\[aq]:              /* int */\n"
"            d = va_arg(ap, int);\n"
"            printf(\"int %d\\en\", d);\n"
"            break;\n"
"        case \\[aq]c\\[aq]:              /* char */\n"
"            /* need a cast here since va_arg only\n"
"               takes fully promoted types */\n"
"            c = (char) va_arg(ap, int);\n"
"            printf(\"char %c\\en\", c);\n"
"            break;\n"
"        }\n"
"    va_end(ap);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
