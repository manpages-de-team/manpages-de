# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:45+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_cleanup_push_defer_np"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-12-13"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"pthread_cleanup_push_defer_np, pthread_cleanup_pop_restore_np - push and pop "
"thread cancelation clean-up handlers while saving cancelability type"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>,\\ I<-lpthread>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void pthread_cleanup_push_defer_np(typeof(void (void *)) *>I<routine>B<,>\n"
"B<                                   void *>I<arg>B<);>\n"
"B<void pthread_cleanup_pop_restore_np(int >I<execute>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pthread_cleanup_push_defer_np>(), B<pthread_cleanup_pop_defer_np>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions are the same as B<pthread_cleanup_push>(3)  and "
"B<pthread_cleanup_pop>(3), except for the differences noted on this page."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Like B<pthread_cleanup_push>(3), B<pthread_cleanup_push_defer_np>()  pushes "
"I<routine> onto the thread's stack of cancelation clean-up handlers.  In "
"addition, it also saves the thread's current cancelability type, and sets "
"the cancelability type to \"deferred\" (see B<pthread_setcanceltype>(3)); "
"this ensures that cancelation clean-up will occur even if the thread's "
"cancelability type was \"asynchronous\" before the call."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Like B<pthread_cleanup_pop>(3), B<pthread_cleanup_pop_restore_np>()  pops "
"the top-most clean-up handler from the thread's stack of cancelation clean-"
"up handlers.  In addition, it restores the thread's cancelability type to "
"its value at the time of the matching B<pthread_cleanup_push_defer_np>()."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The caller must ensure that calls to these functions are paired within the "
"same function, and at the same lexical nesting level.  Other restrictions "
"apply, as described in B<pthread_cleanup_push>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "This sequence of calls:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"pthread_cleanup_push_defer_np(routine, arg);\n"
"pthread_cleanup_pop_restore_np(execute);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "is equivalent to (but shorter and more efficient than):"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"int oldtype;\n"
"\\&\n"
"pthread_cleanup_push(routine, arg);\n"
"pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &oldtype);\n"
"\\&...\n"
"pthread_setcanceltype(oldtype, NULL);\n"
"pthread_cleanup_pop(execute);\n"
msgstr ""

#. #-#-#-#-#  archlinux: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SH VERSIONS
#.  Available since glibc 2.0
#. type: SH
#. #-#-#-#-#  debian-unstable: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-42: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-rawhide: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  mageia-cauldron: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-leap-16-0: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: pthread_cleanup_push_defer_np.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU; hence the suffix \"_np\" (nonportable) in the names."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.0"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pthread_cancel>(3), B<pthread_cleanup_push>(3), "
"B<pthread_setcancelstate>(3), B<pthread_testcancel>(3), B<pthreads>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<void pthread_cleanup_push_defer_np(void (*>I<routine>B<)(void *), void *>I<arg>B<);>\n"
"B<void pthread_cleanup_pop_restore_np(int >I<execute>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "int oldtype;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"pthread_cleanup_push(routine, arg);\n"
"pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &oldtype);\n"
"\\&...\n"
"pthread_setcanceltype(oldtype, NULL);\n"
"pthread_cleanup_pop(execute);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"These functions are nonstandard GNU extensions; hence the suffix \"_np\" "
"(nonportable) in the names."
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
