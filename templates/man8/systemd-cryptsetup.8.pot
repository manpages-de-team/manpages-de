# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:52+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYSTEMD-CRYPTSETUP"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "systemd 257.3"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "systemd-cryptsetup"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"systemd-cryptsetup, systemd-cryptsetup@.service - Full disk decryption logic"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"B<systemd-cryptsetup> [OPTIONS...] attach VOLUME SOURCE-DEVICE [KEY-FILE] "
"[CRYPTTAB-OPTIONS]"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "B<systemd-cryptsetup> [OPTIONS...] detach VOLUME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "systemd-cryptsetup@\\&.service"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "system-systemd\\ex2dcryptsetup\\&.slice"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"systemd-cryptsetup is used to set up (with B<attach>) and tear down (with "
"B<detach>) access to an encrypted block device\\&. It is primarily used via "
"systemd-cryptsetup@\\&.service during early boot, but may also be called "
"manually\\&. The positional arguments I<VOLUME>, I<SOURCE-DEVICE>, I<KEY-"
"FILE>, and I<CRYPTTAB-OPTIONS> have the same meaning as the fields in "
"B<crypttab>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"systemd-cryptsetup@\\&.service is a service responsible for providing access "
"to encrypted block devices\\&. It is instantiated for each device that "
"requires decryption\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"systemd-cryptsetup@\\&.service instances are part of the system-"
"systemd\\ex2dcryptsetup\\&.slice slice, which is destroyed only very late in "
"the shutdown procedure\\&. This allows the encrypted devices to remain up "
"until filesystems have been unmounted\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"systemd-cryptsetup@\\&.service will ask for hard disk passwords via the "
"\\m[blue]B<password agent logic>\\m[]\\&\\s-2\\u[1]\\d\\s+2, in order to "
"query the user for the password using the right mechanism at boot and during "
"runtime\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"At early boot and when the system manager configuration is reloaded, /etc/"
"crypttab is translated into systemd-cryptsetup@\\&.service units by "
"B<systemd-cryptsetup-generator>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"In order to unlock a volume a password or binary key is required\\&.  "
"systemd-cryptsetup@\\&.service tries to acquire a suitable password or "
"binary key via the following mechanisms, tried in order:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"If a key file is explicitly configured (via the third column in /etc/"
"crypttab), a key read from it is used\\&. If a PKCS#11 token, FIDO2 token or "
"TPM2 device is configured (using the I<pkcs11-uri=>, I<fido2-device=>, "
"I<tpm2-device=> options) the key is decrypted before use\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"If no key file is configured explicitly this way, a key file is "
"automatically loaded from /etc/cryptsetup-keys\\&.d/I<volume>\\&.key and /"
"run/cryptsetup-keys\\&.d/I<volume>\\&.key, if present\\&. Here too, if a "
"PKCS#11/FIDO2/TPM2 token/device is configured, any key found this way is "
"decrypted before use\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"If the I<try-empty-password> option is specified then unlocking the volume "
"with an empty password is attempted\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"If the I<password-cache=> option is set to \"yes\" or \"read-only\", the "
"kernel keyring is then checked for a suitable cached password from previous "
"attempts\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"Finally, the user is queried for a password, possibly multiple times, unless "
"the I<headless> option is set\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"If no suitable key may be acquired via any of the mechanisms describes "
"above, volume activation fails\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "CREDENTIALS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"B<systemd-cryptsetup> supports the service credentials logic as implemented "
"by I<ImportCredential=>/I<LoadCredential=>/I<SetCredential=> (see "
"B<systemd.exec>(5)  for details)\\&. The following credentials are used by "
"\"systemd-crypsetup@root\\&.service\" (generated by B<systemd-gpt-auto-"
"generator>) when passed in:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "I<cryptsetup\\&.passphrase>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "This credential specifies the passphrase of the LUKS volume\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "Added in version 256\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "I<cryptsetup\\&.tpm2-pin>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "This credential specifies the TPM pin\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "I<cryptsetup\\&.fido2-pin>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "This credential specifies the FIDO2 token pin\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "I<cryptsetup\\&.pkcs11-pin>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "This credential specifies the PKCS11 token pin\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "I<cryptsetup\\&.luks2-pin>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"This credential specifies the pin requested by generic LUKS2 token "
"modules\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"B<systemd>(1), B<systemd-cryptsetup-generator>(8), B<crypttab>(5), B<systemd-"
"cryptenroll>(1), B<cryptsetup>(8), \\m[blue]B<TPM2 PCR Measurements Made by "
"systemd>\\m[]\\&\\s-2\\u[2]\\d\\s+2"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "password agent logic"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "\\%https://systemd.io/PASSWORD_AGENTS/"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "TPM2 PCR Measurements Made by systemd"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "\\%https://systemd.io/TPM2_PCR_MEASUREMENTS"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"B<systemd-cryptsetup> [OPTIONS...] attach VOLUME SOURCE-DEVICE [KEY-FILE] "
"[CONFIG]"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"systemd-cryptsetup is used to set up (with B<attach>) and tear down (with "
"B<detach>) access to an encrypted block device\\&. It is primarily used via "
"systemd-cryptsetup@\\&.service during early boot, but may also be be called "
"manually\\&. The positional arguments I<VOLUME>, I<SOURCE-DEVICE>, I<KEY-"
"FILE>, and I<CRYPTTAB-OPTIONS> have the same meaning as the fields in "
"B<crypttab>(5)\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"The kernel keyring is then checked for a suitable cached password from "
"previous attempts\\&."
msgstr ""
