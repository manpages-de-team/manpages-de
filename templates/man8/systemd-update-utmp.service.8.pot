# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:54+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYSTEMD-UPDATE-UTMP\\&.SERVICE"
msgstr ""

#. type: TH
#: archlinux fedora-42 fedora-rawhide
#, no-wrap
msgid "systemd 257.3"
msgstr ""

#. type: TH
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "systemd-update-utmp.service"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"systemd-update-utmp.service, systemd-update-utmp-runlevel.service, systemd-"
"update-utmp - Write audit and utmp updates at bootup, runlevel changes and "
"shutdown"
msgstr ""

#. type: SH
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
msgid "systemd-update-utmp\\&.service"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
msgid "systemd-update-utmp-runlevel\\&.service"
msgstr ""

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid "/usr/lib/systemd/systemd-update-utmp"
msgstr ""

#. type: SH
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"systemd-update-utmp-runlevel\\&.service is a service that writes SysV "
"runlevel changes to utmp and wtmp, as well as the audit logs, as they "
"occur\\&.  systemd-update-utmp\\&.service does the same for system reboots "
"and shutdown requests\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-42 fedora-rawhide mageia-cauldron
msgid "B<systemd>(1), B<utmp>(5), B<auditd>(8)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/systemd-update-utmp"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""
