# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-09-18 20:37+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "RPM-FAPOLICYD"
msgstr ""

#. type: TH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "28 Jan 2021"
msgstr ""

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "rpm-plugin-fapolicyd - Fapolicyd plugin for the RPM Package Manager"
msgstr ""

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"The plugin gathers metadata of currently installed files.  It sends the "
"information about files and about ongoing rpm transaction to the fapolicyd "
"daemon.  The information is written to Linux pipe which is placed in /var/"
"run/fapolicyd/fapolicyd.fifo."
msgstr ""

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Configuration"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"There are currently no options for this plugin in particular.  See \\f[B]rpm-"
"plugins\\f[R](8) on how to control plugins in general.\\fR"
msgstr ""

#. type: SH
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "\\f[B]fapolicyd\\f[R](8), \\f[B]rpm-plugins\\f[R](8)\\fR"
msgstr ""
