# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:36+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "BLOCKDEV"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "blockdev - call block device ioctls from the command line"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<blockdev> [B<-q>] [B<-v>] I<command> [I<command>...] I<device> "
"[I<device>...]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<blockdev> B<--report> [I<device>...]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<blockdev> B<-h>|B<-V>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The utility B<blockdev> allows one to call block device ioctls from the "
"command line."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-q>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Be quiet."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-v>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Be verbose."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--report>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Print a report for the specified device. It is possible to give multiple "
"devices. If none is given, all devices which appear in I</proc/partitions> "
"are shown. Note that the partition StartSec is in 512-byte sectors."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "COMMANDS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "It is possible to give multiple devices and multiple commands."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--flushbufs>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Flush buffers."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getalignoff>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Get alignment offset."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getbsz>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Print the blocksize in bytes. This size does not describe device topology. "
"It\\(cqs the size used internally by the kernel and it may be modified (for "
"example) by filesystem driver on mount."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getdiscardzeroes>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Get discard zeroes support status."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getfra>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Get filesystem readahead in 512-byte sectors."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getiomin>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Get minimum I/O size."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getioopt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Get optimal I/O size."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getmaxsect>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Get max sectors per request."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getpbsz>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Get physical block (sector) size."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getra>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print readahead (in 512-byte sectors)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getro>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Get read-only. Print 1 if the device is read-only, 0 otherwise."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getsize64>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print device size in bytes."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getsize>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Print device size (32-bit!) in sectors. Deprecated in favor of the B<--"
"getsz> option."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getss>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print logical sector size in bytes - usually 512."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--getsz>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Get size in 512-byte sectors."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--rereadpt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Reread partition table"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--setbsz> I<bytes>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Set blocksize. Note that the block size is specific to the current file "
"descriptor opening the block device, so the change of block size only "
"persists for as long as B<blockdev> has the device open, and is lost once "
"B<blockdev> exits."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--setfra> I<sectors>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Set filesystem readahead (same as B<--setra> on 2.6 kernels)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--setra> I<sectors>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Set readahead (in 512-byte sectors)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--setro>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Set read-only. The currently active access to the device may not be affected "
"by the change. For example, a filesystem already mounted in read-write mode "
"will not be affected. The change applies after remount."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--setrw>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Set read-write."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<blockdev> was written by Andries E. Brouwer and rewritten by Karel Zak."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<blockdev> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
