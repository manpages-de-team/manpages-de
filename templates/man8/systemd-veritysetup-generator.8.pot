# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:54+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYSTEMD-VERITYSETUP-GENERATOR"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "systemd 257.3"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "systemd-veritysetup-generator"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-veritysetup-generator - Unit generator for verity protected block "
"devices"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "/usr/lib/systemd/system-generators/systemd-veritysetup-generator"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd-veritysetup-generator> is a generator that translates kernel "
"command line options configuring verity protected block devices into native "
"systemd units early at boot and when configuration of the system manager is "
"reloaded\\&. This will create B<systemd-veritysetup@.service>(8)  units as "
"necessary\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Currently, only two verity devices may be set up with this generator, "
"backing the root and /usr file systems of the OS\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "B<systemd-veritysetup-generator> implements B<systemd.generator>(7)\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "KERNEL COMMAND LINE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd-veritysetup-generator> understands the following kernel command "
"line parameters:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<systemd\\&.verity=>, I<rd\\&.systemd\\&.verity=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"Takes a boolean argument\\&. Defaults to \"yes\"\\&. If \"no\", disables the "
"generator entirely\\&.  I<rd\\&.systemd\\&.verity=> is honored only in the "
"initrd while I<systemd\\&.verity=> is honored by both the main system and in "
"the initrd\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Added in version 233\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid "I<veritytab=>, I<rd\\&.veritytab=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide
msgid ""
"Takes a boolean argument\\&. Defaults to \"yes\"\\&. If \"no\", causes the "
"generator to ignore any devices configured in /etc/veritytab\\&.  "
"I<rd\\&.veritytab=> is honored only in the initrd while I<veritytab=> is "
"honored by both the main system and in the initrd\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Added in version 248\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<roothash=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Takes a root hash value for the root file system\\&. Expects a hash value "
"formatted in hexadecimal characters of the appropriate length (i\\&.e\\&. "
"most likely 256 bit/64 characters, or longer)\\&. If not specified via "
"I<systemd\\&.verity_root_data=> and I<systemd\\&.verity_root_hash=>, the "
"hash and data devices to use are automatically derived from the specified "
"hash value\\&. Specifically, the data partition device is looked for under a "
"GPT partition UUID derived from the first 128-bit of the root hash, the hash "
"partition device is looked for under a GPT partition UUID derived from the "
"last 128-bit of the root hash\\&. Hence it is usually sufficient to specify "
"the root hash to boot from a verity protected root file system, as device "
"paths are automatically determined from it \\(em as long as the partition "
"table is properly set up\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<systemd\\&.verity_root_data=>, I<systemd\\&.verity_root_hash=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"These two settings take block device paths as arguments and may be used to "
"explicitly configure the data partition and hash partition to use for "
"setting up the verity protection for the root file system\\&. If not "
"specified, these paths are automatically derived from the I<roothash=> "
"argument (see above)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "I<systemd\\&.verity_root_options=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Takes a comma-separated list of dm-verity options\\&. Expects the following "
"options B<superblock=>I<BOOLEAN>, B<format=>I<NUMBER>, B<data-block-"
"size=>I<BYTES>, B<hash-block-size=>I<BYTES>, B<data-blocks=>I<BLOCKS>, "
"B<hash-offset=>I<BYTES>, B<salt=>I<HEX>, B<uuid=>I<UUID>, B<ignore-"
"corruption>, B<restart-on-corruption>, B<ignore-zero-blocks>, B<check-at-"
"most-once>, B<panic-on-corruption>, B<hash=>I<HASH>, B<fec-device=>I<PATH>, "
"B<fec-offset=>I<BYTES>, B<fec-roots=>I<NUM> and B<root-hash-"
"signature=>I<PATH>B<|base64:>I<HEX>\\&. See B<veritysetup>(8)  for more "
"details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"I<usrhash=>, I<systemd\\&.verity_usr_data=>, I<systemd\\&.verity_usr_hash=>, "
"I<systemd\\&.verity_usr_options=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Equivalent to their counterparts for the root file system as described "
"above, but apply to the /usr/ file system instead\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Added in version 250\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd>(1), B<systemd-veritysetup@.service>(8), B<veritysetup>(8), "
"B<systemd-fstab-generator>(8)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/system-generators/systemd-veritysetup-generator"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Takes a boolean argument\\&. Defaults to \"yes\"\\&. If \"no\", disables the "
"generator entirely\\&.  I<rd\\&.systemd\\&.verity=> is honored only by the "
"initrd while I<systemd\\&.verity=> is honored by both the host system and "
"the initrd\\&."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""
