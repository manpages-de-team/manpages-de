# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-01-31 18:00+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "RFKILL"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2024-11-21"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "rfkill - tool for enabling and disabling wireless devices"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<rfkill> [options] [I<command>] [I<ID>|I<type> ...]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<rfkill> lists, enabling and disabling wireless devices."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The command \"list\" output format is deprecated and maintained for backward "
"compatibility only. The new output format is the default when no command is "
"specified or when the option B<--output> is used."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default output is subject to change. So whenever possible, you should "
"avoid using default outputs in your scripts. Always explicitly define "
"expected columns by using the B<--output> option together with a columns "
"list in environments where a stable output is required."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-J>, B<--json>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use JSON output format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do not print a header line."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--output>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of "
"available columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--output-all>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Output all available columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use the raw output format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "COMMANDS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<event>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Listen for rfkill events and display them on stdout."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<list> [I<id>|I<type> ...]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"List the current state of all available devices. The command output format "
"is deprecated, see the B<DESCRIPTION> section. It is a good idea to check "
"with B<list> command I<id> or I<type> scope is appropriate before setting "
"B<block> or B<unblock>. Special I<all> type string will match everything. "
"Use of multiple I<ID> or I<type> arguments is supported. Possible types are "
"all, {wlan | wifi}, bluetooth, {uwb | ultrawideband}, wimax, wwan, gps, fm, "
"nfc."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<block> I<id>|I<type> [...]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Disable the corresponding device."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<unblock> I<id>|I<type> [...]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Enable the corresponding device. If the device is hard-blocked, for example "
"via a hardware switch, it will remain unavailable though it is now soft-"
"unblocked."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<toggle> I<id>|I<type> [...]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Enable or disable the corresponding device."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"  rfkill --output ID,TYPE\n"
"  rfkill block all\n"
"  rfkill unblock wlan\n"
"  rfkill block bluetooth uwb wimax wwan gps fm nfc\n"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<rfkill> was originally written by"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "and"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The code has been later modified by"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "for the util-linux project."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This manual page was written by"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "for the Debian project (and may be used by others)."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<powertop>(8), B<systemd-rfkill>(8),"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<rfkill> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
