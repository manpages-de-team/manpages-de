# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:31+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DNF4-REPODIFF"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "Feb 12, 2025"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "4.10.0"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "dnf-plugins-core"
msgstr ""

#. type: SH
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "dnf4-repodiff - DNF repodiff Plugin"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Display a list of differences between two or more repositories"
msgstr ""

#. type: SH
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "B<dnf repodiff [E<lt>optionsE<gt>]>"
msgstr ""

#. type: SH
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"I<repodiff> is a program which will list differences between two sets of "
"repositories.  Note that by default only source packages are compared."
msgstr ""

#. type: SS
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Options"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"All general DNF options are accepted, see I<Options> in B<dnf(8)> for "
"details."
msgstr ""

#. type: TP
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--repo-old=E<lt>repoidE<gt>, -o E<lt>repoidE<gt>>"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"Add a B<E<lt>repoidE<gt>> as an old repository. It is possible to be used in "
"conjunction with B<--repofrompath> option. Can be specified multiple times."
msgstr ""

#. type: TP
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--repo-new=E<lt>repoidE<gt>, -n E<lt>repoidE<gt>>"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"Add a B<E<lt>repoidE<gt>> as a new repository. Can be specified multiple "
"times."
msgstr ""

#. type: TP
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--archlist=E<lt>archE<gt>, -a E<lt>archE<gt>>"
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"Add architectures to change the default from just comparing source packages. "
"Note that you can use a wildcard “*” for all architectures. Can be specified "
"multiple times."
msgstr ""

#. type: TP
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--size, -s>"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Output additional data about the size of the changes."
msgstr ""

#. type: TP
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--compare-arch>"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"Normally packages are just compared based on their name, this flag makes the "
"comparison also use the arch. So foo.noarch and foo.x86_64 are considered to "
"be a different packages."
msgstr ""

#. type: TP
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--simple>"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Output a simple one line message for modified packages."
msgstr ""

#. type: TP
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--downgrade>"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"Split the data for modified packages between upgraded and downgraded "
"packages."
msgstr ""

#. type: SH
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Compare source pkgs in two local repos:"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "dnf repodiff --repofrompath=o,/tmp/repo-old --repofrompath=n,/tmp/repo-new --repo-old=o --repo-new=n\n"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"Compare x86_64 compat. binary pkgs in two remote repos, and two local one:"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "dnf repodiff --repofrompath=o,http://example.com/repo-old --repofrompath=n,http://example.com/repo-new --repo-old=o --repo-new=n --archlist=x86_64\n"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "Compare x86_64 compat. binary pkgs, but also compare architecture:"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "dnf repodiff --repofrompath=o,http://example.com/repo-old --repofrompath=n,http://example.com/repo-new --repo-old=o --repo-new=n --archlist=x86_64 --compare-arch\n"
msgstr ""

#. type: SH
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "See AUTHORS in your Core DNF Plugins distribution"
msgstr ""

#. type: SH
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid "2014, Red Hat, Licensed under GPLv2+"
msgstr ""

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "Jan 16, 2025"
msgstr ""

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"Add architectures to change the default from just comparing source packages. "
"Note that you can use a wildcard \"*\" for all architectures. Can be "
"specified multiple times."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Nov 16, 2024"
msgstr ""
