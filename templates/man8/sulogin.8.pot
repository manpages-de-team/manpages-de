# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:53+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "SULOGIN"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "sulogin - single-user login"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<sulogin> [options] [I<tty>]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<sulogin> is invoked by B<init> when the system goes into single-user mode."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The user is prompted:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Give root password for system maintenance (or type Control-D for normal "
"startup):"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If the root account is locked and B<--force> is specified, no password is "
"required."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<sulogin> will be connected to the current terminal, or to the optional "
"I<tty> device that can be specified on the command line (typically I</dev/"
"console>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When the user exits from the single-user shell, or presses control-D at the "
"prompt, the system will continue to boot."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-e>, B<--force>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If the default method of obtaining the root password from the system via "
"B<getpwnam>(3) fails, then examine I</etc/passwd> and I</etc/shadow> to get "
"the password. If these files are damaged or nonexistent, or when root "
"account is locked by \\(aq!\\(aq or \\(aq*\\(aq at the begin of the password "
"then B<sulogin> will B<start a root shell without asking for a password>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Only use the B<-e> option if you are sure the console is physically "
"protected against unauthorized access."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--login-shell>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specifying this option causes B<sulogin> to start the shell process as a "
"login shell."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-t>, B<--timeout> I<seconds>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify the maximum amount of time to wait for user input. By default, "
"B<sulogin> will wait forever."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<sulogin> looks for the environment variable B<SUSHELL> or B<sushell> to "
"determine what shell to start. If the environment variable is not set, it "
"will try to execute root\\(cqs shell from I</etc/passwd>. If that fails, it "
"will fall back to I</bin/sh>."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<sulogin> was written by Miquel van Smoorenburg for sysvinit and later "
"ported to util-linux by Dave Reisner and Karel Zak."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<sulogin> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
