# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-22 07:23+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "FSCK"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "fsck - check and repair a Linux filesystem"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fsck> [B<-lsAVRTMNP>] [B<-r> [I<fd>]] [B<-C> [I<fd>]] [B<-t> I<fstype>] "
"[I<filesystem>...] [B<-->] [I<fs-specific-options>]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fsck> is used to check and optionally repair one or more Linux "
"filesystems. I<filesystem> can be a device name (e.g., I</dev/hdc1>, I</dev/"
"sdb2>), a mount point (e.g., I</>, I</usr>, I</home>), or an filesystem "
"label or UUID specifier (e.g., UUID=8868abf6-88c5-4a83-98b8-bfc24057f7bd or "
"LABEL=root). Normally, the B<fsck> program will try to handle filesystems on "
"different physical disk drives in parallel to reduce the total amount of "
"time needed to check all of them."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If no filesystems are specified on the command line, and the B<-A> option is "
"not specified, B<fsck> will default to checking filesystems in I</etc/fstab> "
"serially. This is equivalent to the B<-As> options."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The exit status returned by B<fsck> is the sum of the following conditions:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<0>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "No errors"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<1>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Filesystem errors corrected"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<2>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "System should be rebooted"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<4>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Filesystem errors left uncorrected"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<8>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Operational error"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<16>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Usage or syntax error"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<32>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Checking canceled by user request"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<128>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Shared-library error"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The exit status returned when multiple filesystems are checked is the bit-"
"wise OR of the exit statuses for each filesystem that is checked."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"In actuality, B<fsck> is simply a front-end for the various filesystem "
"checkers (B<fsck>.I<fstype>) available under Linux. The filesystem-specific "
"checker is searched for in the B<PATH> environment variable. If the B<PATH> "
"is undefined then fallback to I</sbin>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Please see the filesystem-specific checker manual pages for further details."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-l>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Create an exclusive B<flock>(2) lock file (I</run/fsck/"
"E<lt>disknameE<gt>.lock>) for whole-disk device. This option can be used "
"with one device only (this means that B<-A> and B<-l> are mutually "
"exclusive). This option is recommended when more B<fsck> instances are "
"executed in the same time. The option is ignored when used for multiple "
"devices or for non-rotating disks. B<fsck> does not lock underlying devices "
"when executed to check stacked devices (e.g. MD or DM) - this feature is not "
"implemented yet."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-r> [I<fd>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Report certain statistics for each fsck when it completes. These statistics "
"include the exit status, the maximum run set size (in kilobytes), the "
"elapsed all-clock time and the user and system CPU time used by the fsck "
"run. For example:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B</dev/sda1: status 0, rss 92828, real 4.002804, user 2.677592, sys 0.86186>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"GUI front-ends may specify a file descriptor I<fd>, in which case the "
"progress bar information will be sent to that file descriptor in a machine "
"parsable format. For example:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B</dev/sda1 0 92828 4.002804 2.677592 0.86186>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Serialize B<fsck> operations. This is a good idea if you are checking "
"multiple filesystems and the checkers are in an interactive mode. (Note: "
"B<e2fsck>(8) runs in an interactive mode by default. To make B<e2fsck>(8) "
"run in a non-interactive mode, you must either specify the B<-p> or B<-a> "
"option, if you wish for errors to be corrected automatically, or the B<-n> "
"option if you do not.)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-t> I<fslist>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specifies the type(s) of filesystem to be checked. When the B<-A> flag is "
"specified, only filesystems that match I<fslist> are checked. The I<fslist> "
"parameter is a comma-separated list of filesystems and options specifiers. "
"All of the filesystems in this comma-separated list may be prefixed by a "
"negation operator \\(aqB<no>\\(aq or \\(aqB<!>\\(aq, which requests that "
"only those filesystems not listed in I<fslist> will be checked. If none of "
"the filesystems in I<fslist> is prefixed by a negation operator, then only "
"those listed filesystems will be checked."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Options specifiers may be included in the comma-separated I<fslist>. They "
"must have the format B<opts=>I<fs-option>. If an options specifier is "
"present, then only filesystems which contain I<fs-option> in their mount "
"options field of I</etc/fstab> will be checked. If the options specifier is "
"prefixed by a negation operator, then only those filesystems that do not "
"have I<fs-option> in their mount options field of I</etc/fstab> will be "
"checked."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"For example, if B<opts=ro> appears in I<fslist>, then only filesystems "
"listed in I</etc/fstab> with the B<ro> option will be checked."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"For compatibility with Mandrake distributions whose boot scripts depend upon "
"an unauthorized UI change to the B<fsck> program, if a filesystem type of "
"B<loop> is found in I<fslist>, it is treated as if B<opts=loop> were "
"specified as an argument to the B<-t> option."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Normally, the filesystem type is deduced by searching for I<filesys> in the "
"I</etc/fstab> file and using the corresponding entry. If the type cannot be "
"deduced, and there is only a single filesystem given as an argument to the "
"B<-t> option, B<fsck> will use the specified filesystem type. If this type "
"is not available, then the default filesystem type (currently ext2) is used."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-A>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Walk through the I</etc/fstab> file and try to check all filesystems in one "
"run. This option is typically used from the I</etc/rc> system initialization "
"file, instead of multiple commands for checking a single filesystem."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The root filesystem will be checked first unless the B<-P> option is "
"specified (see below). After that, filesystems will be checked in the order "
"specified by the I<fs_passno> (the sixth) field in the I</etc/fstab> file. "
"Filesystems with a I<fs_passno> value of 0 are skipped and are not checked "
"at all. Filesystems with a I<fs_passno> value of greater than zero will be "
"checked in order, with filesystems with the lowest I<fs_passno> number being "
"checked first. If there are multiple filesystems with the same pass number, "
"B<fsck> will attempt to check them in parallel, although it will avoid "
"running multiple filesystem checks on the same physical disk."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fsck> does not check stacked devices (RAIDs, dm-crypt, ...) in parallel "
"with any other device. See below for B<FSCK_FORCE_ALL_PARALLEL> setting. The "
"I</sys> filesystem is used to determine dependencies between devices."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Hence, a very common configuration in I</etc/fstab> files is to set the root "
"filesystem to have a I<fs_passno> value of 1 and to set all other "
"filesystems to have a I<fs_passno> value of 2. This will allow B<fsck> to "
"automatically run filesystem checkers in parallel if it is advantageous to "
"do so. System administrators might choose not to use this configuration if "
"they need to avoid multiple filesystem checks running in parallel for some "
"reason - for example, if the machine in question is short on memory so that "
"excessive paging is a concern."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fsck> normally does not check whether the device actually exists before "
"calling a filesystem specific checker. Therefore non-existing devices may "
"cause the system to enter filesystem repair mode during boot if the "
"filesystem specific checker returns a fatal error. The I</etc/fstab> mount "
"option B<nofail> may be used to have B<fsck> skip non-existing devices. "
"B<fsck> also skips non-existing devices that have the special filesystem "
"type B<auto>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-C> [I<fd>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display completion/progress bars for those filesystem checkers (currently "
"only for ext[234]) which support them. B<fsck> will manage the filesystem "
"checkers so that only one of them will display a progress bar at a time. GUI "
"front-ends may specify a file descriptor I<fd>, in which case the progress "
"bar information will be sent to that file descriptor."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-M>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Do not check mounted filesystems and return an exit status of 0 for mounted "
"filesystems."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-N>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Don\\(cqt execute, just show what would be done."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-P>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When the B<-A> flag is set, check the root filesystem in parallel with the "
"other filesystems. This is not the safest thing in the world to do, since if "
"the root filesystem is in doubt things like the B<e2fsck>(8) executable "
"might be corrupted! This option is mainly provided for those sysadmins who "
"don\\(cqt want to repartition the root filesystem to be small and compact "
"(which is really the right solution)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-R>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When checking all filesystems with the B<-A> flag, skip the root filesystem. "
"(This is useful in case the root filesystem has already been mounted read-"
"write.)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-T>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Don\\(cqt show the title on startup."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Produce verbose output, including all filesystem-specific commands that are "
"executed."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-?>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display version information and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILESYSTEM SPECIFIC OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<Options which are not understood by fsck are passed to the filesystem-"
"specific checker!>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"These options B<must> not take arguments, as there is no way for B<fsck> to "
"be able to properly guess which options take arguments and which don\\(cqt."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Options and arguments which follow the B<--> are treated as filesystem-"
"specific options to be passed to the filesystem-specific checker."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Please note that B<fsck> is not designed to pass arbitrarily complicated "
"options to filesystem-specific checkers. If you\\(cqre doing something "
"complicated, please just execute the filesystem-specific checker directly. "
"If you pass B<fsck> some horribly complicated options and arguments, and it "
"doesn\\(cqt do what you expect, B<don\\(cqt bother reporting it as a bug.> "
"You\\(cqre almost certainly doing something that you shouldn\\(cqt be doing "
"with B<fsck>. Options to different filesystem-specific fsck\\(cqs are not "
"standardized."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<fsck> program\\(cqs behavior is affected by the following environment "
"variables:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<FSCK_FORCE_ALL_PARALLEL>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If this environment variable is set, B<fsck> will attempt to check all of "
"the specified filesystems in parallel, regardless of whether the filesystems "
"appear to be on the same device. (This is useful for RAID systems or high-"
"end storage systems such as those sold by companies such as IBM or EMC.) "
"Note that the I<fs_passno> value is still used."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<FSCK_MAX_INST>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"This environment variable will limit the maximum number of filesystem "
"checkers that can be running at one time. This allows configurations which "
"have a large number of disks to avoid B<fsck> starting too many filesystem "
"checkers at once, which might overload CPU and memory resources available on "
"the system. If this value is zero, then an unlimited number of processes can "
"be spawned. This is currently the default, but future versions of B<fsck> "
"may attempt to automatically determine how many filesystem checks can be run "
"based on gathering accounting data from the operating system."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<PATH>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The B<PATH> environment variable is used to find filesystem checkers."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<FSTAB_FILE>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"This environment variable allows the system administrator to override the "
"standard location of the I</etc/fstab> file. It is also useful for "
"developers who are testing B<fsck>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<LIBBLKID_DEBUG=all>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "enables libblkid debug output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<LIBMOUNT_DEBUG=all>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "enables libmount debug output."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I</etc/fstab>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fstab>(5), B<mkfs>(8), B<fsck.ext2>(8) or B<fsck.ext3>(8) or B<e2fsck>(8), "
"B<fsck.cramfs>(8), B<fsck.jfs>(8), B<fsck.nfs>(8), B<fsck.minix>(8), "
"B<fsck.msdos>(8), B<fsck.vfat>(8), B<fsck.xfs>(8), B<reiserfsck>(8)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<fsck> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
