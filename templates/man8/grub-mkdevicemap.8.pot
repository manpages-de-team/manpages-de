# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-09-21 09:02+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-MKDEVICEMAP"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2024"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.12-1~bpo12+1"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "grub-mkdevicemap - make a device map file automatically"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<grub-mkdevicemap> [I<\\,OPTION\\/>]..."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Generate a device map file automatically."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-n>, B<--no-floppy>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "do not probe any floppy drive"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<-s>, B<--probe-second-floppy> probe the second floppy drive"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "use FILE as the device map [default=/boot/grub/device.map]"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "display this message and exit"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print version information and exit"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "print verbose messages"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<grub-probe>(8)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-mkdevicemap> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkdevicemap> programs are properly "
"installed at your site, the command"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<info grub-mkdevicemap>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "July 2024"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-5"
msgstr ""
