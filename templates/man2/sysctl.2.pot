# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:51+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sysctl"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-11-17"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sysctl - read/write system parameters"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>unistd.hE<gt>>\n"
"B<#include E<lt>linux/sysctl.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] int _sysctl(struct __sysctl_args *>I<args>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<This system call no longer exists on current kernels!> See NOTES."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<_sysctl>()  call reads and/or writes kernel parameters.  For example, "
"the hostname, or the maximum number of open files.  The argument has the form"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"struct __sysctl_args {\n"
"    int    *name;    /* integer vector describing variable */\n"
"    int     nlen;    /* number of elements of this vector */\n"
"    void   *oldval;  /* 0 or address where to store old value */\n"
"    size_t *oldlenp; /* available room for old value,\n"
"                        overwritten by actual size of old value */\n"
"    void   *newval;  /* 0 or address of new value */\n"
"    size_t  newlen;  /* size of new value */\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This call does a search in a tree structure, possibly resembling a directory "
"tree under I</proc/sys>, and if the requested item is found calls some "
"appropriate routine to read or modify the value."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Upon successful completion, B<_sysctl>()  returns 0.  Otherwise, a value of "
"-1 is returned and I<errno> is set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"No search permission for one of the encountered \"directories\", or no read "
"permission where I<oldval> was nonzero, or no write permission where "
"I<newval> was nonzero."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The invocation asked for the previous value by setting I<oldval> non-NULL, "
"but allowed zero room in I<oldlenp>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<name> was not found."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux 1.3.57.  Removed in Linux 5.5, glibc 2.32."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"It originated in 4.4BSD.  Only Linux has the I</proc/sys> mirror, and the "
"object naming schemes differ between Linux and 4.4BSD, but the declaration "
"of the B<sysctl>()  function is the same in both."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Use of this system call was long discouraged: since Linux 2.6.24, uses of "
"this system call result in warnings in the kernel log, and in Linux 5.5, the "
"system call was finally removed.  Use the I</proc/sys> interface instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that on older kernels where this system call still exists, it is "
"available only if the kernel was configured with the "
"B<CONFIG_SYSCTL_SYSCALL> option.  Furthermore, glibc does not provide a "
"wrapper for this system call, necessitating the use of B<syscall>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The object names vary between kernel versions, making this system call "
"worthless for applications."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Not all available objects are properly documented."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"It is not yet possible to change operating system by writing to I</proc/sys/"
"kernel/ostype>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#include E<lt>linux/sysctl.hE<gt>\n"
"\\&\n"
"#define ARRAY_SIZE(arr)  (sizeof(arr) / sizeof((arr)[0]))\n"
"\\&\n"
"int _sysctl(struct __sysctl_args *args);\n"
"\\&\n"
"#define OSNAMESZ 100\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    int                   name[] = { CTL_KERN, KERN_OSTYPE };\n"
"    char                  osname[OSNAMESZ];\n"
"    size_t                osnamelth;\n"
"    struct __sysctl_args  args;\n"
"\\&\n"
"    memset(&args, 0, sizeof(args));\n"
"    args.name = name;\n"
"    args.nlen = ARRAY_SIZE(name);\n"
"    args.oldval = osname;\n"
"    args.oldlenp = &osnamelth;\n"
"\\&\n"
"    osnamelth = sizeof(osname);\n"
"\\&\n"
"    if (syscall(SYS__sysctl, &args) == -1) {\n"
"        perror(\"_sysctl\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    printf(\"This machine is running %*s\\[rs]n\", (int) osnamelth, osname);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"struct __sysctl_args {\n"
"    int    *name;    /* integer vector describing variable */\n"
"    int     nlen;    /* length of this vector */\n"
"    void   *oldval;  /* 0 or address where to store old value */\n"
"    size_t *oldlenp; /* available room for old value,\n"
"                        overwritten by actual size of old value */\n"
"    void   *newval;  /* 0 or address of new value */\n"
"    size_t  newlen;  /* size of new value */\n"
"};\n"
msgstr ""

#. type: TP
#: debian-bookworm
#, no-wrap
msgid "B<EACCES>, B<EPERM>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"This system call first appeared in Linux 1.3.57.  It was removed in Linux "
"5.5; glibc support was removed in glibc 2.32."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"This call is Linux-specific, and should not be used in programs intended to "
"be portable.  It originated in 4.4BSD.  Only Linux has the I</proc/sys> "
"mirror, and the object naming schemes differ between Linux and 4.4BSD, but "
"the declaration of the B<sysctl>()  function is the same in both."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "#include E<lt>linux/sysctl.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "#define ARRAY_SIZE(arr)  (sizeof(arr) / sizeof((arr)[0]))\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "int _sysctl(struct __sysctl_args *args);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "#define OSNAMESZ 100\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    int                   name[] = { CTL_KERN, KERN_OSTYPE };\n"
"    char                  osname[OSNAMESZ];\n"
"    size_t                osnamelth;\n"
"    struct __sysctl_args  args;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    memset(&args, 0, sizeof(args));\n"
"    args.name = name;\n"
"    args.nlen = ARRAY_SIZE(name);\n"
"    args.oldval = osname;\n"
"    args.oldlenp = &osnamelth;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    osnamelth = sizeof(osname);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (syscall(SYS__sysctl, &args) == -1) {\n"
"        perror(\"_sysctl\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    printf(\"This machine is running %*s\\en\", (int) osnamelth, osname);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#include E<lt>linux/sysctl.hE<gt>\n"
"\\&\n"
"#define ARRAY_SIZE(arr)  (sizeof(arr) / sizeof((arr)[0]))\n"
"\\&\n"
"int _sysctl(struct __sysctl_args *args);\n"
"\\&\n"
"#define OSNAMESZ 100\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    int                   name[] = { CTL_KERN, KERN_OSTYPE };\n"
"    char                  osname[OSNAMESZ];\n"
"    size_t                osnamelth;\n"
"    struct __sysctl_args  args;\n"
"\\&\n"
"    memset(&args, 0, sizeof(args));\n"
"    args.name = name;\n"
"    args.nlen = ARRAY_SIZE(name);\n"
"    args.oldval = osname;\n"
"    args.oldlenp = &osnamelth;\n"
"\\&\n"
"    osnamelth = sizeof(osname);\n"
"\\&\n"
"    if (syscall(SYS__sysctl, &args) == -1) {\n"
"        perror(\"_sysctl\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"    printf(\"This machine is running %*s\\en\", (int) osnamelth, osname);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
