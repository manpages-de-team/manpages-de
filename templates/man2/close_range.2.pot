# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "close_range"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "close_range - close all file descriptors in a given range"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>         /* See feature_test_macros(7) */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/close_range.hE<gt>> /* Definition of B<CLOSE_RANGE_*>\n"
"                                  constants */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<int close_range(unsigned int >I<first>B<, unsigned int >I<last>B<, int >I<flags>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<close_range>()  system call closes all open file descriptors from "
"I<first> to I<last> (included)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Errors closing a given file descriptor are currently ignored."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<flags> is a bit mask containing 0 or more of the following:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<CLOSE_RANGE_CLOEXEC> (since Linux 5.11)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Set the close-on-exec flag on the specified file descriptors, rather than "
"immediately closing them."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<CLOSE_RANGE_UNSHARE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Unshare the specified file descriptors from any other processes before "
"closing them, avoiding races with other threads sharing the file descriptor "
"table."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<close_range>()  returns 0.  On error, -1 is returned and "
"I<errno> is set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<flags> is not valid, or I<first> is greater than I<last>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following can occur with B<CLOSE_RANGE_UNSHARE> (when constructing the "
"new descriptor table):"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The number of open file descriptors exceeds the limit specified in I</proc/"
"sys/fs/nr_open> (see B<proc>(5)).  This error can occur in situations where "
"that limit was lowered before a call to B<close_range>()  where the "
"B<CLOSE_RANGE_UNSHARE> flag is specified."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "FreeBSD.  Linux 5.9, glibc 2.34."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Closing all open file descriptors"
msgstr ""

#.  278a5fbaed89dacd04e9d052f4594ffd0e0585de
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To avoid blindly closing file descriptors in the range of possible file "
"descriptors, this is sometimes implemented (on Linux)  by listing open file "
"descriptors in I</proc/self/fd/> and calling B<close>(2)  on each one.  "
"B<close_range>()  can take care of this without requiring I</proc> and "
"within a single system call, which provides significant performance benefits."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Closing file descriptors before exec"
msgstr ""

#.  60997c3d45d9a67daf01c56d805ae4fec37e0bd8
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "File descriptors can be closed safely using"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"/* we don't want anything past stderr here */\n"
"close_range(3, \\[ti]0U, CLOSE_RANGE_UNSHARE);\n"
"execve(....);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<CLOSE_RANGE_UNSHARE> is conceptually equivalent to"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"unshare(CLONE_FILES);\n"
"close_range(first, last, 0);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"but can be more efficient: if the unshared range extends past the current "
"maximum number of file descriptors allocated in the caller's file descriptor "
"table (the common case when I<last> is \\[ti]0U), the kernel will unshare a "
"new file descriptor table for the caller up to I<first>, copying as few file "
"descriptors as possible.  This avoids subsequent B<close>(2)  calls "
"entirely; the whole operation is complete once the table is unshared."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Closing files on exec"
msgstr ""

#.  582f1fb6b721facf04848d2ca57f34468da1813e
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is particularly useful in cases where multiple pre-B<exec> setup steps "
"risk conflicting with each other.  For example, setting up a B<seccomp>(2)  "
"profile can conflict with a B<close_range>()  call: if the file descriptors "
"are closed before the B<seccomp>(2)  profile is set up, the profile setup "
"can't use them itself, or control their closure; if the file descriptors are "
"closed afterwards, the seccomp profile can't block the B<close_range>()  "
"call or any fallbacks.  Using B<CLOSE_RANGE_CLOEXEC> avoids this: the "
"descriptors can be marked before the B<seccomp>(2)  profile is set up, and "
"the profile can control access to B<close_range>()  without affecting the "
"calling process."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The program shown below opens the files named in its command-line arguments, "
"displays the list of files that it has opened (by iterating through the "
"entries in I</proc/PID/fd>), uses B<close_range>()  to close all file "
"descriptors greater than or equal to 3, and then once more displays the "
"process's list of open files.  The following example demonstrates the use of "
"the program:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<touch /tmp/a /tmp/b /tmp/c>\n"
"$ B<./a.out /tmp/a /tmp/b /tmp/c>\n"
"/tmp/a opened as FD 3\n"
"/tmp/b opened as FD 4\n"
"/tmp/c opened as FD 5\n"
"/proc/self/fd/0 ==E<gt> /dev/pts/1\n"
"/proc/self/fd/1 ==E<gt> /dev/pts/1\n"
"/proc/self/fd/2 ==E<gt> /dev/pts/1\n"
"/proc/self/fd/3 ==E<gt> /tmp/a\n"
"/proc/self/fd/4 ==E<gt> /tmp/b\n"
"/proc/self/fd/5 ==E<gt> /tmp/b\n"
"/proc/self/fd/6 ==E<gt> /proc/9005/fd\n"
"========= About to call close_range() =======\n"
"/proc/self/fd/0 ==E<gt> /dev/pts/1\n"
"/proc/self/fd/1 ==E<gt> /dev/pts/1\n"
"/proc/self/fd/2 ==E<gt> /dev/pts/1\n"
"/proc/self/fd/3 ==E<gt> /proc/9005/fd\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Note that the lines showing the pathname I</proc/9005/fd> result from the "
"calls to B<opendir>(3)."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>dirent.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"/* Show the contents of the symbolic links in /proc/self/fd */\n"
"\\&\n"
"static void\n"
"show_fds(void)\n"
"{\n"
"    DIR            *dirp;\n"
"    char           path[PATH_MAX], target[PATH_MAX];\n"
"    ssize_t        len;\n"
"    struct dirent  *dp;\n"
"\\&\n"
"    dirp = opendir(\"/proc/self/fd\");\n"
"    if (dirp  == NULL) {\n"
"        perror(\"opendir\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    for (;;) {\n"
"        dp = readdir(dirp);\n"
"        if (dp == NULL)\n"
"            break;\n"
"\\&\n"
"        if (dp-E<gt>d_type == DT_LNK) {\n"
"            snprintf(path, sizeof(path), \"/proc/self/fd/%s\",\n"
"                     dp-E<gt>d_name);\n"
"\\&\n"
"            len = readlink(path, target, sizeof(target));\n"
"            printf(\"%s ==E<gt> %.*s\\[rs]n\", path, (int) len, target);\n"
"        }\n"
"    }\n"
"\\&\n"
"    closedir(dirp);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int  fd;\n"
"\\&\n"
"    for (size_t j = 1; j E<lt> argc; j++) {\n"
"        fd = open(argv[j], O_RDONLY);\n"
"        if (fd == -1) {\n"
"            perror(argv[j]);\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"        printf(\"%s opened as FD %d\\[rs]n\", argv[j], fd);\n"
"    }\n"
"\\&\n"
"    show_fds();\n"
"\\&\n"
"    printf(\"========= About to call close_range() =======\\[rs]n\");\n"
"\\&\n"
"    if (close_range(3, \\[ti]0U, 0) == -1) {\n"
"        perror(\"close_range\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    show_fds();\n"
"    exit(EXIT_FAILURE);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<close>(2)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-10"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "B<#include E<lt>linux/close_range.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid ""
"B<int close_range(unsigned int >I<first>B<, unsigned int >I<last>B<,>\n"
"B<                unsigned int >I<flags>B<);>\n"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<close_range>()  first appeared in Linux 5.9.  Library support was added in "
"glibc 2.34."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<close_range>()  is a nonstandard function that is also present on FreeBSD."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>dirent.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "/* Show the contents of the symbolic links in /proc/self/fd */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void\n"
"show_fds(void)\n"
"{\n"
"    DIR            *dirp;\n"
"    char           path[PATH_MAX], target[PATH_MAX];\n"
"    ssize_t        len;\n"
"    struct dirent  *dp;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    dirp = opendir(\"/proc/self/fd\");\n"
"    if (dirp  == NULL) {\n"
"        perror(\"opendir\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    for (;;) {\n"
"        dp = readdir(dirp);\n"
"        if (dp == NULL)\n"
"            break;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"        if (dp-E<gt>d_type == DT_LNK) {\n"
"            snprintf(path, sizeof(path), \"/proc/self/fd/%s\",\n"
"                     dp-E<gt>d_name);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"            len = readlink(path, target, sizeof(target));\n"
"            printf(\"%s ==E<gt> %.*s\\en\", path, (int) len, target);\n"
"        }\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    closedir(dirp);\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int  fd;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    for (size_t j = 1; j E<lt> argc; j++) {\n"
"        fd = open(argv[j], O_RDONLY);\n"
"        if (fd == -1) {\n"
"            perror(argv[j]);\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"        printf(\"%s opened as FD %d\\en\", argv[j], fd);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    show_fds();\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    printf(\"========= About to call close_range() =======\\en\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (syscall(SYS_close_range, 3, \\[ti]0U, 0) == -1) {\n"
"        perror(\"close_range\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    show_fds();\n"
"    exit(EXIT_FAILURE);\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>dirent.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"/* Show the contents of the symbolic links in /proc/self/fd */\n"
"\\&\n"
"static void\n"
"show_fds(void)\n"
"{\n"
"    DIR            *dirp;\n"
"    char           path[PATH_MAX], target[PATH_MAX];\n"
"    ssize_t        len;\n"
"    struct dirent  *dp;\n"
"\\&\n"
"    dirp = opendir(\"/proc/self/fd\");\n"
"    if (dirp  == NULL) {\n"
"        perror(\"opendir\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    for (;;) {\n"
"        dp = readdir(dirp);\n"
"        if (dp == NULL)\n"
"            break;\n"
"\\&\n"
"        if (dp-E<gt>d_type == DT_LNK) {\n"
"            snprintf(path, sizeof(path), \"/proc/self/fd/%s\",\n"
"                     dp-E<gt>d_name);\n"
"\\&\n"
"            len = readlink(path, target, sizeof(target));\n"
"            printf(\"%s ==E<gt> %.*s\\en\", path, (int) len, target);\n"
"        }\n"
"    }\n"
"\\&\n"
"    closedir(dirp);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int  fd;\n"
"\\&\n"
"    for (size_t j = 1; j E<lt> argc; j++) {\n"
"        fd = open(argv[j], O_RDONLY);\n"
"        if (fd == -1) {\n"
"            perror(argv[j]);\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"        printf(\"%s opened as FD %d\\en\", argv[j], fd);\n"
"    }\n"
"\\&\n"
"    show_fds();\n"
"\\&\n"
"    printf(\"========= About to call close_range() =======\\en\");\n"
"\\&\n"
"    if (syscall(SYS_close_range, 3, \\[ti]0U, 0) == -1) {\n"
"        perror(\"close_range\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    show_fds();\n"
"    exit(EXIT_FAILURE);\n"
"}\n"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
