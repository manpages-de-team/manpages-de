# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:29+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "bdflush"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-08-21"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "bdflush - start, flush, or tune buffer-dirty-flush daemon"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/kdaemon.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "B<int bdflush(int >I<func>B<, long >I<data>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"This system call used to turn the calling process into the I<bdflush> "
"daemon, or tune it, or flush the \"old buffers\".  It then progressively "
"lost all of that functionality."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"See I<fs/buffer.c> in the kernel version you're interested in to see what it "
"actually does there."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "B<ENOSYS> (this system call is unimplemented)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"This system call was introduced in Linux 1.1.3, became effectively obsolete "
"in Linux 1.3.50, mostly useless in Linux 2.3.23, entirely useless in Linux "
"2.5.12, officially deprecated in Linux 2.5.52, and removed outright in Linux "
"5.15."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Sometimes, if I<func> was even, I<data> actually represented a pointer."
msgstr ""

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "The header and prototype were removed in glibc 2.23."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<sync>(1), B<fsync>(2), B<sync>(2)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid ""
"B<[[deprecated]] int bdflush(int >I<func>B<, long *>I<address>B<);>\n"
"B<[[deprecated]] int bdflush(int >I<func>B<, long >I<data>B<);>\n"
msgstr ""

#.  As noted in changes in the 2.5.12 source
#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"I<Note>: Since Linux 2.6, this system call is deprecated and does nothing.  "
"It is likely to disappear altogether in a future kernel release.  Nowadays, "
"the task performed by B<bdflush>()  is handled by the kernel I<pdflush> "
"thread."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"B<bdflush>()  starts, flushes, or tunes the buffer-dirty-flush daemon.  Only "
"a privileged process (one with the B<CAP_SYS_ADMIN> capability) may call "
"B<bdflush>()."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"If I<func> is negative or 0, and no daemon has been started, then "
"B<bdflush>()  enters the daemon code and never returns."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "If I<func> is 1, some dirty buffers are written to disk."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"If I<func> is 2 or more and is even (low bit is 0), then I<address> is the "
"address of a long word, and the tuning parameter numbered (I<func>-2)/2 is "
"returned to the caller in that address."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"If I<func> is 3 or more and is odd (low bit is 1), then I<data> is a long "
"word, and the kernel sets tuning parameter numbered (I<func>-3)/2 to that "
"value."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"The set of parameters, their values, and their valid ranges are defined in "
"the Linux kernel source file I<fs/buffer.c>."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"If I<func> is negative or 0 and the daemon successfully starts, "
"B<bdflush>()  never returns.  Otherwise, the return value is 0 on success "
"and -1 on failure, with I<errno> set to indicate the error."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<EBUSY>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"An attempt was made to enter the daemon code after another process has "
"already entered."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "I<address> points outside your accessible address space."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"An attempt was made to read or write an invalid parameter number, or to "
"write an invalid value to a parameter."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<EPERM>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Caller does not have the B<CAP_SYS_ADMIN> capability."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Since glibc 2.23, glibc no longer supports this obsolete system call."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<bdflush>()  is Linux-specific and should not be used in programs intended "
"to be portable."
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr ""

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""
