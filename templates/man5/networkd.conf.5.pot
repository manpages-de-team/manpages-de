# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-28 16:41+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NETWORKD\\&.CONF"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd 257.3"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "networkd.conf"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "networkd.conf, networkd.conf.d - Global Network configuration files"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/etc/systemd/networkd\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "/run/systemd/networkd\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "/usr/local/lib/systemd/networkd\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "/usr/lib/systemd/networkd\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "/etc/systemd/networkd\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "/run/systemd/networkd\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "/usr/local/lib/systemd/networkd\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/usr/lib/systemd/networkd\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "These configuration files control global network parameters\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. The main "
"configuration file is loaded from one of the listed directories in order of "
"priority, only the first file found is used: /etc/systemd/, /run/systemd/, /"
"usr/local/lib/systemd/ \\&\\s-2\\u[1]\\d\\s+2, /usr/lib/systemd/\\&. The "
"vendor version of the file contains commented out entries showing the "
"defaults as a guide to the administrator\\&. Local overrides can also be "
"created by creating drop-ins, as described below\\&. The main configuration "
"file can also be edited for this purpose (or a copy in /etc/ if it\\*(Aqs "
"shipped under /usr/), however using drop-ins for local configuration is "
"recommended over modifications to the main configuration file\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"In addition to the main configuration file, drop-in configuration snippets "
"are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/systemd/"
"*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins have "
"higher precedence and override the main configuration file\\&. Files in the "
"*\\&.conf\\&.d/ configuration subdirectories are sorted by their filename in "
"lexicographic order, regardless of in which of the subdirectories they "
"reside\\&. When multiple files specify the same option, for options which "
"accept just a single value, the entry in the file sorted last takes "
"precedence, and for options which accept a list of values, entries are "
"collected as they occur in the sorted files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering\\&. This also defines a concept of drop-in "
"priorities to allow OS vendors to ship drop-ins within a specific range "
"lower than the range used by users\\&. This should lower the risk of package "
"drop-ins overriding accidentally drop-ins defined by users\\&. It is "
"recommended to use the range 10-40 for drop-ins in /usr/ and the range 60-90 "
"for drop-ins in /etc/ and /run/, to make sure that local and transient drop-"
"ins take priority over drop-ins shipped by the OS vendor\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To disable a configuration file supplied by the vendor, the recommended way "
"is to place a symlink to /dev/null in the configuration directory in /etc/, "
"with the same filename as the vendor configuration file\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "[NETWORK] SECTION OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following options are available in the [Network] section:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<SpeedMeter=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Takes a boolean\\&. If set to yes, then B<systemd-networkd> measures the "
"traffic of each interface, and B<networkctl status >I<INTERFACE> shows the "
"measured speed\\&. Defaults to no\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 244\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<SpeedMeterIntervalSec=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specifies the time interval to calculate the traffic speed of each "
"interface\\&. If I<SpeedMeter=no>, the value is ignored\\&. Defaults to "
"10sec\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<ManageForeignRoutingPolicyRules=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A boolean\\&. When true, B<systemd-networkd> will remove rules that are not "
"configured in \\&.network files (except for rules with protocol \"kernel\")"
"\\&. When false, it will not remove any foreign rules, keeping them even if "
"they are not configured in a \\&.network file\\&. Defaults to yes\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 249\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<ManageForeignRoutes=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A boolean\\&. When true, B<systemd-networkd> will remove routes that are not "
"configured in \\&.network files (except for routes with protocol \"kernel\", "
"\"dhcp\" when I<KeepConfiguration=> is true or \"dhcp\", and \"static\" when "
"I<KeepConfiguration=> is true or \"static\")\\&. When false, it will not "
"remove any foreign routes, keeping them even if they are not configured in a "
"\\&.network file\\&. Defaults to yes\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 246\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<ManageForeignNextHops=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"A boolean\\&. When true, B<systemd-networkd> will remove nexthops that are "
"not configured in \\&.network files (except for routes with protocol "
"\"kernel\")\\&. When false, it will not remove any foreign nexthops, keeping "
"them even if they are not configured in a \\&.network file\\&. Defaults to "
"yes\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Added in version 256\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<RouteTable=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Defines the route table name\\&. Takes a whitespace-separated list of the "
"pairs of route table name and number\\&. The route table name and number in "
"each pair are separated with a colon, i\\&.e\\&., \"I<name>:I<number>\"\\&. "
"The route table name must not be \"default\", \"main\", or \"local\", as "
"these route table names are predefined with route table number 253, 254, and "
"255, respectively\\&. The route table number must be an integer in the range "
"1\\&...4294967295, except for predefined numbers 253, 254, and 255\\&. This "
"setting can be specified multiple times\\&. If an empty string is specified, "
"then the list specified earlier are cleared\\&. Defaults to unset\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 248\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<IPv4Forwarding=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Configures IPv4 packet forwarding for the system\\&. Takes a boolean "
"value\\&. This controls the net\\&.ipv4\\&.conf\\&.default\\&.forwarding and "
"net\\&.ipv4\\&.conf\\&.all\\&.forwardingsysctl options\\&. See \\m[blue]B<IP "
"Sysctl>\\m[]\\&\\s-2\\u[2]\\d\\s+2 for more details about the sysctl "
"options\\&. Defaults to unset and the sysctl options will not be changed\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If an interface is configured with a \\&.network file that enables "
"I<IPMasquerade=> for IPv4 (that is, \"ipv4\" or \"both\"), this setting is "
"implied unless explicitly specified\\&. See I<IPMasquerade=> in "
"B<systemd.network>(5)  for more details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<IPv6Forwarding=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Configures IPv6 packet forwarding for the system\\&. Takes a boolean "
"value\\&. This controls the net\\&.ipv6\\&.conf\\&.default\\&.forwarding and "
"net\\&.ipv6\\&.conf\\&.all\\&.forwarding sysctl options\\&. See "
"\\m[blue]B<IP Sysctl>\\m[]\\&\\s-2\\u[2]\\d\\s+2 for more details about the "
"sysctl options\\&. Defaults to unset and the sysctl options will not be "
"changed\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"If an interface is configured with a \\&.network file that enables "
"I<IPMasquerade=> for IPv6 (that is, \"ipv6\" or \"both\"), this setting is "
"implied unless explicitly specified\\&. See I<IPMasquerade=> in "
"B<systemd.network>(5)  for more details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<IPv6PrivacyExtensions=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specifies the default value for per-network I<IPv6PrivacyExtensions=>\\&. "
"Takes a boolean or the special values \"prefer-public\" and \"kernel\"\\&. "
"See for details in B<systemd.network>(5)\\&. Defaults to \"no\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 254\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<UseDomains=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Specifies the network- and protocol-independent default value for the same "
"settings in [IPv6AcceptRA], [DHCPv4], and [DHCPv6] sections below\\&. Takes "
"a boolean, or the special value B<route>\\&. See the same setting in "
"B<systemd.network>(5)\\&. Defaults to \"no\"\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "[IPV6ACCEPTRA] SECTION OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This section configures the default setting of the Neighbor Discovery\\&. "
"The following options are available in the [IPv6AcceptRA] section:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Specifies the network-independent default value for the same setting in the "
"[IPv6AcceptRA] section in B<systemd.network>(5)\\&. Takes a boolean, or the "
"special value B<route>\\&. When unspecified, the value specified in the "
"[Network] section in B<networkd.conf>(5), which defaults to \"no\", will be "
"used\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "[IPV6ADDRESSLABEL] SECTION OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"An [IPv6AddressLabel] section accepts the following keys\\&. Specify "
"multiple [IPv6AddressLabel] sections to configure multiple address "
"labels\\&. IPv6 address labels are used for address selection\\&. See "
"\\m[blue]B<RFC 3484>\\m[]\\&\\s-2\\u[3]\\d\\s+2\\&. Precedence is managed by "
"userspace, and only the label itself is stored in the kernel\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<Label=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The label for the prefix, an unsigned integer in the range "
"0\\&...4294967294\\&. 0xffffffff is reserved\\&. This setting is "
"mandatory\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Added in version 257\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "I<Prefix=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"IPv6 prefix is an address with a prefix length, separated by a slash \"/\" "
"character\\&. This setting is mandatory\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "[DHCPV4] SECTION OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This section configures the DHCP Unique Identifier (DUID) value used by DHCP "
"protocol\\&. DHCPv4 client protocol sends IAID and DUID to the DHCP server "
"when acquiring a dynamic IPv4 address if B<ClientIdentifier=duid>\\&. IAID "
"and DUID allows a DHCP server to uniquely identify the machine and the "
"interface requesting a DHCP IP address\\&. To configure IAID and "
"ClientIdentifier, see B<systemd.network>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The following options are understood:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<DUIDType=>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Specifies how the DUID should be generated\\&. See \\m[blue]B<RFC 3315>\\m[]"
"\\&\\s-2\\u[4]\\d\\s+2 for a description of all the options\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This takes an integer in the range 0\\&...65535, or one of the following "
"string values:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<vendor>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If \"DUIDType=vendor\", then the DUID value will be generated using "
"\"43793\" as the vendor identifier (systemd) and hashed contents of "
"B<machine-id>(5)\\&. This is the default if I<DUIDType=> is not specified\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 230\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<uuid>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If \"DUIDType=uuid\", and I<DUIDRawData=> is not set, then the product UUID "
"is used as a DUID value\\&. If a system does not have valid product UUID, "
"then an application-specific B<machine-id>(5)  is used as a DUID value\\&. "
"About the application-specific machine ID, see "
"B<sd_id128_get_machine_app_specific>(3)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<link-layer-time[:>I<TIME>B<]>, B<link-layer>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If \"link-layer-time\" or \"link-layer\" is specified, then the MAC address "
"of the interface is used as a DUID value\\&. The value \"link-layer-time\" "
"can take additional time value after a colon, e\\&.g\\&.  \"link-layer-"
"time:2018-01-23 12:34:56 UTC\"\\&. The default time value is \"2000-01-01 "
"00:00:00 UTC\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 240\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In all cases, I<DUIDRawData=> can be used to override the actual DUID value "
"that is used\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<DUIDRawData=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specifies the DHCP DUID value as a single newline-terminated, hexadecimal "
"string, with each byte separated by \":\"\\&. The DUID that is sent is "
"composed of the DUID type specified by I<DUIDType=> and the value configured "
"here\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The DUID value specified here overrides the DUID that B<systemd-"
"networkd.service>(8)  generates from the machine ID\\&. To configure DUID "
"per-network, see B<systemd.network>(5)\\&. The configured DHCP DUID should "
"conform to the specification in \\m[blue]B<RFC 3315>\\m[]"
"\\&\\s-2\\u[5]\\d\\s+2, \\m[blue]B<RFC 6355>\\m[]\\&\\s-2\\u[6]\\d\\s+2\\&. "
"To configure IAID, see B<systemd.network>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<Example\\ \\&1.\\ \\&A DUIDType=vendor with a custom value>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"DUIDType=vendor\n"
"DUIDRawData=00:00:ab:11:f9:2a:c2:77:29:f9:5c:00\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This specifies a 14 byte DUID, with the type DUID-EN (\"00:02\"), enterprise "
"number 43793 (\"00:00:ab:11\"), and identifier value "
"\"f9:2a:c2:77:29:f9:5c:00\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Same as the one in the [IPv6AcceptRA] section, but applied for DHCPv4 "
"protocol\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "[DHCPV6] SECTION OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This section configures the DHCP Unique Identifier (DUID) value used by "
"DHCPv6 protocol\\&. DHCPv6 client protocol sends the DHCP Unique Identifier "
"and the interface Identity Association Identifier (IAID) to a DHCPv6 server "
"when acquiring a dynamic IPv6 address\\&. IAID and DUID allows a DHCPv6 "
"server to uniquely identify the machine and the interface requesting a DHCP "
"IP address\\&. To configure IAID, see B<systemd.network>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<DUIDType=>, I<DUIDRawData=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "As in the [DHCPv4] section\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "[DHCPSERVER] SECTION OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This section configures the default setting of the DHCP server\\&. The "
"following options are available in the [DHCPServer] section:"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemd.network>(5), B<systemd-networkd.service>(8), "
"B<machine-id>(5), B<sd_id128_get_machine_app_specific>(3)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"💣💥🧨💥💥💣 Please note that those configuration files must be available at "
"all times. If /usr/local/ is a separate partition, it may not be available "
"during early boot, and must not be used for configuration."
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "IP Sysctl"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "\\%https://docs.kernel.org/networking/ip-sysctl.html"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 3."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "RFC 3484"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "\\%https://tools.ietf.org/html/rfc3484"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid " 4."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "RFC 3315"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://tools.ietf.org/html/rfc3315#section-9"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid " 5."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%http://tools.ietf.org/html/rfc3315#section-9"
msgstr ""

#. type: IP
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid " 6."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "RFC 6355"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%http://tools.ietf.org/html/rfc6355"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/networkd\\&.conf\\&.d/*\\&.conf"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"These configuration files control global network parameters\\&. Currently "
"the DHCP Unique Identifier (DUID)\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. "
"Initially, the main configuration file in /etc/systemd/ contains commented "
"out entries showing the defaults as a guide to the administrator\\&. Local "
"overrides can be created by editing this file or by creating drop-ins, as "
"described below\\&. Using drop-ins for local configuration is recommended "
"over modifications to the main configuration file\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"In addition to the \"main\" configuration file, drop-in configuration "
"snippets are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/"
"systemd/*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins "
"have higher precedence and override the main configuration file\\&. Files in "
"the *\\&.conf\\&.d/ configuration subdirectories are sorted by their "
"filename in lexicographic order, regardless of in which of the "
"subdirectories they reside\\&. When multiple files specify the same option, "
"for options which accept just a single value, the entry in the file sorted "
"last takes precedence, and for options which accept a list of values, "
"entries are collected as they occur in the sorted files\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Specifies how the DUID should be generated\\&. See \\m[blue]B<RFC 3315>\\m[]"
"\\&\\s-2\\u[1]\\d\\s+2 for a description of all the options\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The following values are understood:"
msgstr ""

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"The DUID value specified here overrides the DUID that B<systemd-"
"networkd.service>(8)  generates from the machine ID\\&. To configure DUID "
"per-network, see B<systemd.network>(5)\\&. The configured DHCP DUID should "
"conform to the specification in \\m[blue]B<RFC 3315>\\m[]"
"\\&\\s-2\\u[2]\\d\\s+2, \\m[blue]B<RFC 6355>\\m[]\\&\\s-2\\u[3]\\d\\s+2\\&. "
"To configure IAID, see B<systemd.network>(5)\\&."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. The main "
"configuration file is either in /usr/lib/systemd/ or /etc/systemd/ and "
"contains commented out entries showing the defaults as a guide to the "
"administrator\\&. Local overrides can be created by creating drop-ins, as "
"described below\\&. The main configuration file can also be edited for this "
"purpose (or a copy in /etc/ if it\\*(Aqs shipped in /usr/) however using "
"drop-ins for local configuration is recommended over modifications to the "
"main configuration file\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&. This also defined a concept "
"of drop-in priority to allow distributions to ship drop-ins within a "
"specific range lower than the range used by users\\&. This should lower the "
"risk of package drop-ins overriding accidentally drop-ins defined by "
"users\\&."
msgstr ""

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "systemd 257.2"
msgstr ""
