.\"***************************************************************************
.\" Copyright 2018-2022,2023 Thomas E. Dickey                                *
.\" Copyright 2002-2016,2017 Free Software Foundation, Inc.                  *
.\"                                                                          *
.\" Permission is hereby granted, free of charge, to any person obtaining a  *
.\" copy of this software and associated documentation files (the            *
.\" "Software"), to deal in the Software without restriction, including      *
.\" without limitation the rights to use, copy, modify, merge, publish,      *
.\" distribute, distribute with modifications, sublicense, and/or sell       *
.\" copies of the Software, and to permit persons to whom the Software is    *
.\" furnished to do so, subject to the following conditions:                 *
.\"                                                                          *
.\" The above copyright notice and this permission notice shall be included  *
.\" in all copies or substantial portions of the Software.                   *
.\"                                                                          *
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS  *
.\" OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF               *
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   *
.\" IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,   *
.\" DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR    *
.\" OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR    *
.\" THE USE OR OTHER DEALINGS IN THE SOFTWARE.                               *
.\"                                                                          *
.\" Except as contained in this notice, the name(s) of the above copyright   *
.\" holders shall not be used in advertising or otherwise to promote the     *
.\" sale, use or other dealings in this Software without prior written       *
.\" authorization.                                                           *
.\"***************************************************************************
.\"
.\" $Id: curs_get_wch.3x,v 1.31 2023/12/23 16:35:10 tom Exp $
.TH get_wch 3NCURSES 2023-12-23 "ncurses 6.4" "Library calls"
.ie \n(.g \{\
.ds `` \(lq
.ds '' \(rq
.\}
.el \{\
.ie t .ds `` ``
.el   .ds `` ""
.ie t .ds '' ''
.el   .ds '' ""
.\}
.
.de bP
.ie n  .IP \(bu 4
.el    .IP \(bu 2
..
.SH NAME
\fB\%get_wch\fP,
\fB\%wget_wch\fP,
\fB\%mvget_wch\fP,
\fB\%mvwget_wch\fP,
\fB\%unget_wch\fP \-
get (or push back) a wide character from \fIcurses\fR terminal keyboard
.SH SYNOPSIS
.nf
\fB#include <ncursesw/curses.h>
.PP
\fBint get_wch(wint_t *\fIwch\fP);
\fBint wget_wch(WINDOW *\fIwin\fP, wint_t *\fIwch\fP);
\fBint mvget_wch(int \fIy\fP, int \fIx\fP, wint_t *\fIwch\fP);
\fBint mvwget_wch(WINDOW *\fIwin\fP, int \fIy\fP, int \fIx\fP, wint_t *\fIwch\fP);
.PP
\fBint unget_wch(const wchar_t \fIwch\fP);
.fi
.SH DESCRIPTION
.SS wget_wch
The
\fBget_wch\fP,
\fBwget_wch\fP,
\fBmvget_wch\fP, and
\fBmvwget_wch\fP
functions read a character
from the terminal associated with the current or specified window.
In no-delay mode,
if no input is waiting, the value \fBERR\fP is returned.
In delay mode,
the program waits until the system passes text through to the program.
Depending on the setting of \fBcbreak\fP,
this is after one character (cbreak mode),
or after the first newline (nocbreak mode).
In half-delay mode,
the program waits until the user types a character or the specified
timeout interval has elapsed.
.PP
Unless \fBnoecho\fP has been set,
these routines echo the character into the designated window.
.PP
If the window is not a pad and has been moved or modified since the
last call to \fBwrefresh\fP,
\fBwrefresh\fP will be called before another character is read.
.PP
If \fBkeypad\fP is enabled,
these functions respond to
the pressing of a function key by setting the object pointed to by
\fIwch\fP
to the keycode assigned to the function key,
and returning \fBKEY_CODE_YES\fP.
If a character (such as escape) that could be the
beginning of a function key is received, curses sets a timer.
If the remainder
of the sequence does arrive within the designated time, curses passes through
the character; otherwise, curses returns the function key value.
For this
reason, many terminals experience a delay between the time a user presses
the escape key and the time the escape is returned to the program.
.PP
The keycodes returned by these functions are the same as those
returned by \fBwgetch\fP:
.bP
The predefined function
keys are listed in \fB<curses.h>\fP as macros with values outside the range
of 8-bit characters.
Their names begin with \fBKEY_\fP.
.bP
Other (user-defined) function keys
which may be defined using \fB\%define_key\fP(3NCURSES) have no names,
but also are expected to have values outside the range of 8-bit characters.
.SS unget_wch
The
\fBunget_wch\fP
function pushes the wide character
\fIwch\fP
back onto the head of the input queue, so the wide character
is returned by the next call to
\fBget_wch\fP.
The pushback of
one character is guaranteed.
If the program calls
\fBunget_wch\fP
too many times without an intervening call to
\fBget_wch\fP,
the operation may fail.
.PP
Unlike \fBungetch\fP and \fBwgetch\fP,
\fBunget_wch\fP cannot distinguish special characters
returned by \fBwget_wch\fP from ordinary characters.
An application can push special keys
which it may read via \fBwget_wch\fP
by checking for the \fBKEY_CODE_YES\fP result,
and using \fBungetch\fP for those special keys.
.SH RETURN VALUE
When
\fBget_wch\fP,
\fBwget_wch\fP,
\fBmvget_wch\fP, and
\fBmvwget_wch\fP
functions successfully
report the pressing of a function key, they return
\fBKEY_CODE_YES\fP.
When they successfully report a wide character, they return
\fBOK\fP.
Otherwise, they return
\fBERR\fP.
.PP
Upon successful completion,
\fBunget_wch\fP
returns
\fBOK\fP.
Otherwise, the function returns
\fBERR\fP.
.PP
Functions with a \*(``mv\*('' prefix first perform a cursor movement using
\fBwmove\fP, and return an error if the position is outside the window,
or if the window pointer is null.
.SH NOTES
Applications should not define the escape key by itself as a single-character
function.
.PP
When using
\fBget_wch\fP,
\fBwget_wch\fP,
\fBmvget_wch\fP, or
\fBmvwget_wch\fP, applications should
not use
\fBnocbreak\fP
mode and
\fBecho\fP
mode
at the same time.
Depending on the state of the tty driver when each character
is typed, the program may produce undesirable results.
.PP
All of these functions except
\fB\%wget_wch\fP and
\fB\%unget_wch\fP
may be implemented as macros.
.SH PORTABILITY
These functions are described in the XSI Curses standard, Issue 4.
.SH SEE ALSO
\fB\%ncurses\fP(3NCURSES),
\fB\%getch\fP(3NCURSES),
\fB\%inopts\fP(3NCURSES),
\fB\%ins_wch\fP(3NCURSES),
\fB\%move\fP(3NCURSES),
\fB\%refresh\fP(3NCURSES)
