# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexey, 2016.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014-2017.
# kogamatranslator49 <r.podarov@yandex.ru>, 2015.
# Darima Kogan <silverdk99@gmail.com>, 2014.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:45+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_setconcurrency"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 июля 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"pthread_setconcurrency, pthread_getconcurrency - set/get the concurrency "
"level"
msgstr ""
"pthread_setconcurrency, pthread_getconcurrency - изменяет/возвращает уровень "
"распараллеливания"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgid "POSIX threads library (I<libpthread>,\\ I<-lpthread>)"
msgstr "Библиотека потоков POSIX (I<libpthread>, I<-lpthread>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr "B<#include E<lt>pthread.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int pthread_setconcurrency(int >I<new_level>B<);>\n"
#| "B<int pthread_getconcurrency(void);>\n"
msgid ""
"B<int pthread_setconcurrency(int >I<new_level>B<);>\n"
"B<int pthread_getconcurrency(>I<void>B<);>\n"
msgstr ""
"B<int pthread_setconcurrency(int >I<new_level>B<);>\n"
"B<int pthread_getconcurrency(void);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pthread_setconcurrency>()  function informs the implementation of the "
"application's desired concurrency level, specified in I<new_level>.  The "
"implementation takes this only as a hint: POSIX.1 does not specify the level "
"of concurrency that should be provided as a result of calling "
"B<pthread_setconcurrency>()."
msgstr ""
"Функция B<pthread_setconcurrency>() информирует реализацию приложения о "
"желаемом уровне распараллеливания (concurrency level), задаваемом в "
"I<new_level>. Реализация принимает это только как рекомендацию: в POSIX.1 не "
"указан уровень распараллеливания, который должен получиться в результате "
"вызова B<pthread_setconcurrency>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specifying I<new_level> as 0 instructs the implementation to manage the "
"concurrency level as it deems appropriate."
msgstr ""
"Значение I<new_level> равное 0 указывает реализации использовать уровень "
"распараллеливания по своему усмотрению."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pthread_getconcurrency>()  returns the current value of the concurrency "
"level for this process."
msgstr ""
"Функция B<pthread_getconcurrency>() возвращает текущее значение уровня "
"распараллеливания для этого процесса."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<pthread_setconcurrency>()  returns 0; on error, it returns a "
"nonzero error number."
msgstr ""
"При успешном выполнении B<pthread_setconcurrency>() возвращается 0; при "
"ошибке возвращается ненулевой номер ошибки."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<pthread_getconcurrency>()  always succeeds, returning the concurrency "
"level set by a previous call to B<pthread_setconcurrency>(), or 0, if "
"B<pthread_setconcurrency>()  has not previously been called."
msgstr ""
"Функция B<pthread_getconcurrency>() всегда завершается успешно, возвращая "
"уровень распараллеливания, заданный предыдущим вызовом "
"B<pthread_setconcurrency>(), или 0, если функция B<pthread_setconcurrency>() "
"не вызывалась."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pthread_setconcurrency>()  can fail with the following error:"
msgstr ""
"Функция B<pthread_setconcurrency>() может завершиться со следующей ошибкой:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<new_level> is negative."
msgstr "Значение I<new_level> отрицательно."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1 also documents an B<EAGAIN> error (\"the value specified by "
"I<new_level> would cause a system resource to be exceeded\")."
msgstr ""
"В POSIX.1 также описана ошибка B<EAGAIN> («значение, указанное в "
"I<new_level>, привело бы к превышению системного ограничения»)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pthread_setconcurrency>(),\n"
"B<pthread_getconcurrency>()"
msgstr ""
"B<pthread_setconcurrency>(),\n"
"B<pthread_getconcurrency>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.1.  POSIX.1-2001."
msgstr "glibc 2.1.  POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The default concurrency level is 0."
msgstr "Значение уровня распараллеливания по умолчанию равно 0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Concurrency levels are meaningful only for M:N threading implementations, "
"where at any moment a subset of a process's set of user-level threads may be "
"bound to a smaller number of kernel-scheduling entities.  Setting the "
"concurrency level allows the application to give the system a hint as to the "
"number of kernel-scheduling entities that should be provided for efficient "
"execution of the application."
msgstr ""
"Уровни распараллеливания имеют смысл только в реализациях нитей M:N, где в "
"любой момент времени поднабор из набора пользовательских нитей процесса "
"может быть ограничен меньшим количеством единиц планирования ядра. "
"Назначение уровня распараллеливания позволяет приложению дать системе "
"подсказку о количестве единиц планирования ядра для эффективного выполнения "
"приложения."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Both LinuxThreads and NPTL are 1:1 threading implementations, so setting the "
"concurrency level has no meaning.  In other words, on Linux these functions "
"merely exist for compatibility with other systems, and they have no effect "
"on the execution of a program."
msgstr ""
"В реализациях нитей LinuxThreads и NPTL используется отношение 1:1, поэтому "
"назначение уровня распараллеливания не имеет смысла. Другими словами, в "
"Linux эти функции существуют только лишь для совместимости с другими "
"системами и никак не влияют на выполнение программы."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<pthread_attr_setscope>(3), B<pthreads>(7)"
msgstr "B<pthread_attr_setscope>(3), B<pthreads>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr "Библиотека потоков POSIX (I<libpthread>, I<-lpthread>)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
msgid "These functions are available since glibc 2.1."
msgstr "Эти функции доступны начиная с glibc 2.1."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
