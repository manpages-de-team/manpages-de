# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexey, 2016.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014-2017.
# kogamatranslator49 <r.podarov@yandex.ru>, 2015.
# Darima Kogan <silverdk99@gmail.com>, 2014.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:45+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "pthread_attr_setguardsize"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 июля 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"pthread_attr_setguardsize, pthread_attr_getguardsize - set/get guard size "
"attribute in thread attributes object"
msgstr ""
"pthread_attr_setguardsize, pthread_attr_getguardsize - изменяет/возвращает "
"атрибут размера защиты у объекта атрибутов нити"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgid "POSIX threads library (I<libpthread>,\\ I<-lpthread>)"
msgstr "Библиотека потоков POSIX (I<libpthread>, I<-lpthread>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr "B<#include E<lt>pthread.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int pthread_attr_setguardsize(pthread_attr_t *>I<attr>B<, size_t >I<guardsize>B<);>\n"
#| "B<int pthread_attr_getguardsize(const pthread_attr_t *>I<attr>B<, size_t *>I<guardsize>B<);>\n"
msgid ""
"B<int pthread_attr_setguardsize(pthread_attr_t *>I<attr>B<, size_t >I<guardsize>B<);>\n"
"B<int pthread_attr_getguardsize(const pthread_attr_t *restrict >I<attr>B<,>\n"
"B<                              size_t *restrict >I<guardsize>B<);>\n"
msgstr ""
"B<int pthread_attr_setguardsize(pthread_attr_t *>I<attr>B<, size_t >I<guardsize>B<);>\n"
"B<int pthread_attr_getguardsize(const pthread_attr_t *>I<attr>B<, size_t *>I<guardsize>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pthread_attr_setguardsize>()  function sets the guard size attribute "
"of the thread attributes object referred to by I<attr> to the value "
"specified in I<guardsize>."
msgstr ""
"Функция B<pthread_attr_setguardsize>() изменяет атрибут размера защиты у "
"объекта атрибутов нити, на который указывает I<attr>, в значение "
"I<guardsize>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<guardsize> is greater than 0, then for each new thread created using "
"I<attr> the system allocates an additional region of at least I<guardsize> "
"bytes at the end of the thread's stack to act as the guard area for the "
"stack (but see BUGS)."
msgstr ""
"Если I<guardsize> больше 0, то для каждой новой нити, созданной с "
"использованием атрибута I<attr>, система выделяет дополнительную область не "
"менее I<guardsize> байт в конце стека нити, которая служит как защитная "
"область стека (но смотрите ДЕФЕКТЫ)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<guardsize> is 0, then new threads created with I<attr> will not have a "
"guard area."
msgstr ""
"Если I<guardsize> равно 0, то новые нити, создаваемые с использованием "
"I<attr>, не будут иметь защитной области."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The default guard size is the same as the system page size."
msgstr "Размер защиты по умолчанию совпадает с размером системной страницы."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the stack address attribute has been set in I<attr> (using "
"B<pthread_attr_setstack>(3)  or B<pthread_attr_setstackaddr>(3)), meaning "
"that the caller is allocating the thread's stack, then the guard size "
"attribute is ignored (i.e., no guard area is created by the system): it is "
"the application's responsibility to handle stack overflow (perhaps by using "
"B<mprotect>(2)  to manually define a guard area at the end of the stack that "
"it has allocated)."
msgstr ""
"Если в I<attr> установлен атрибут адреса стека (с помощью "
"B<pthread_attr_setstack>(3) или B<pthread_attr_setstackaddr>(3)), "
"означающий, что вызывающий выделяет стек для нити, то атрибут размера защиты "
"игнорируется (т. е., система не создаёт защитную область): само приложение "
"должно обрабатывать переполнение стека (возможно, с помощью B<mprotect>(2), "
"для задания защитной области в конце выделенного стека)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<pthread_attr_getguardsize>()  function returns the guard size "
"attribute of the thread attributes object referred to by I<attr> in the "
"buffer pointed to by I<guardsize>."
msgstr ""
"Функция B<pthread_attr_getguardsize>() возвращает атрибут размера защиты у "
"объекта атрибутов нити, на который указывает I<attr>, в буфер на который "
"указывает I<guardsize>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, these functions return 0; on error, they return a nonzero error "
"number."
msgstr ""
"При успешном выполнении эти функции возвращают 0; при ошибке возвращается "
"ненулевой номер ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"POSIX.1 documents an B<EINVAL> error if I<attr> or I<guardsize> is invalid.  "
"On Linux these functions always succeed (but portable and future-proof "
"applications should nevertheless handle a possible error return)."
msgstr ""
"В POSIX.1 описана ошибка B<EINVAL> при некорректном значении I<attr> или "
"I<guardsize>. В Linux эти функции всегда выполняются успешно (тем не менее, "
"в переносимых приложениях нужно учитывать возможность возврата ошибки)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pthread_attr_setguardsize>(),\n"
"B<pthread_attr_getguardsize>()"
msgstr ""
"B<pthread_attr_setguardsize>(),\n"
"B<pthread_attr_getguardsize>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.1.  POSIX.1-2001."
msgstr "glibc 2.1.  POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A guard area consists of virtual memory pages that are protected to prevent "
"read and write access.  If a thread overflows its stack into the guard area, "
"then, on most hard architectures, it receives a B<SIGSEGV> signal, thus "
"notifying it of the overflow.  Guard areas start on page boundaries, and the "
"guard size is internally rounded up to the system page size when creating a "
"thread.  (Nevertheless, B<pthread_attr_getguardsize>()  returns the guard "
"size that was set by B<pthread_attr_setguardsize>().)"
msgstr ""
"Защитная область состоит из страниц виртуальной памяти, которые защищены от "
"чтения и записи. Если нить переполнит свой стек и попадёт в защитную "
"область, то на большинстве аппаратных архитектур она получит сигнал "
"B<SIGSEGV> и таким образом узнает о переполнении. Защитные области "
"начинаются на границах страниц, а размер защиты внутри округляется до "
"размера системной страницы при создании нити (тем не менее, функция "
"B<pthread_attr_getguardsize>() возвращает размер защиты, установленный  "
"B<pthread_attr_setguardsize>())."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Setting a guard size of 0 may be useful to save memory in an application "
"that creates many threads and knows that stack overflow can never occur."
msgstr ""
"Установка размера защиты в нулевое значение полезно для экономии памяти в "
"приложениях, которые создают много нитей и знают, что переполнение стека "
"никогда не произойдёт."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Choosing a guard size larger than the default size may be necessary for "
"detecting stack overflows if a thread allocates large data structures on the "
"stack."
msgstr ""
"Указание размера защиты больше чем размер по умолчанию может потребоваться "
"для обнаружения переполнений стека при выделении в нити больших структур "
"данных в стеке."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"As at glibc 2.8, the NPTL threading implementation includes the guard area "
"within the stack size allocation, rather than allocating extra space at the "
"end of the stack, as POSIX.1 requires.  (This can result in an B<EINVAL> "
"error from B<pthread_create>(3)  if the guard size value is too large, "
"leaving no space for the actual stack.)"
msgstr ""
"В glibc 2.8 реализация нитей NPTL добавляет защитную область к размеру "
"выделяемого стека, а не выделяет дополнительное пространство в конце стека "
"как требуется POSIX.1 (это может приводить к ошибке B<EINVAL> в "
"B<pthread_create>(3), если значение размера защиты слишком большое и не "
"оставляет место именно под стек)."

#. #-#-#-#-#  archlinux: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  debian-unstable: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  fedora-42: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The obsolete LinuxThreads implementation did the right thing, allocating "
"extra space at the end of the stack for the guard area."
msgstr ""
"Устаревшая реализация LinuxThreads делает это правильно, выделяя "
"дополнительное пространство в конце стека под защитную область."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "See B<pthread_getattr_np>(3)."
msgstr "См. B<pthread_getattr_np>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<mmap>(2), B<mprotect>(2), B<pthread_attr_init>(3), "
"B<pthread_attr_setstack>(3), B<pthread_attr_setstacksize>(3), "
"B<pthread_create>(3), B<pthreads>(7)"
msgstr ""
"B<mmap>(2), B<mprotect>(2), B<pthread_attr_init>(3), "
"B<pthread_attr_setstack>(3), B<pthread_attr_setstacksize>(3), "
"B<pthread_create>(3), B<pthreads>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr "Библиотека потоков POSIX (I<libpthread>, I<-lpthread>)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
msgid "These functions are provided since glibc 2.1."
msgstr "Эти функции доступны начиная с glibc 2.1."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
