# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:51+0100\n"
"PO-Revision-Date: 2024-12-21 19:50+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "strchr"
msgstr ""

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 июля 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "strchr, strrchr, strchrnul - locate character in string"
msgstr ""
"strchr, strrchr, strchrnul - определение местонахождения символа в строке"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Standard C library (I<libc>, I<-lc>)"
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<char *strndup(const char *>I<s>B<, size_t >I<n>B<);>\n"
#| "B<char *strdupa(const char *>I<s>B<);>\n"
#| "B<char *strndupa(const char *>I<s>B<, size_t >I<n>B<);>\n"
msgid ""
"B<char *strchr(const char *>I<s>B<, int >I<c>B<);>\n"
"B<char *strrchr(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr ""
"B<char *strndup(const char *>I<s>B<, size_t >I<n>B<);>\n"
"B<char *strdupa(const char *>I<s>B<);>\n"
"B<char *strndupa(const char *>I<s>B<, size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>         /* See feature_test_macros(7) */\n"
"B<#include E<lt>string.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>         /* см. feature_test_macros(7) */\n"
"B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strchrnul(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr "B<char *strchrnul(const char *>I<s>B<, int >I<c>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strchr>()  function returns a pointer to the first occurrence of the "
"character I<c> in the string I<s>."
msgstr ""
"Функция B<strchr>() возвращает указатель на местонахождение первого "
"совпадения с символом I<c> в строке I<s>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strrchr>()  function returns a pointer to the last occurrence of the "
"character I<c> in the string I<s>."
msgstr ""
"Функция B<strrchr>() возвращает указатель на местонахождение последнего "
"совпадения с символом I<c> в строке I<s>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strchrnul>()  function is like B<strchr>()  except that if I<c> is not "
"found in I<s>, then it returns a pointer to the null byte at the end of "
"I<s>, rather than NULL."
msgstr ""
"Функция B<strchrnul>() подобна B<strchr>(), за исключением того, что если "
"символ I<c> не найден в строке I<s>, то возвращается указатель на байт null "
"в конце I<s>, а не NULL."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Here \"character\" means \"byte\"; these functions do not work with wide or "
"multibyte characters."
msgstr ""
"Здесь под «символом» подразумевается «байт» — данные функции не работают с "
"широкими и многобайтными символами."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strchr>()  and B<strrchr>()  functions return a pointer to the "
#| "matched character or NULL if the character is not found.  The terminating "
#| "null byte is considered part of the string, so that if I<c> is specified "
#| "as \\(aq\\e0\\(aq, these functions return a pointer to the terminator."
msgid ""
"The B<strchr>()  and B<strrchr>()  functions return a pointer to the matched "
"character or NULL if the character is not found.  The terminating null byte "
"is considered part of the string, so that if I<c> is specified as \\[aq]\\"
"[rs]0\\[aq], these functions return a pointer to the terminator."
msgstr ""
"Функции B<strchr>() и B<strrchr>() возвращает указатель на местонахождение "
"первого совпадения с символом или NULL, если символ не найден. Завершающий "
"байт null считается частью строки, и поэтому, если символ I<c> задан как \\"
"(aq\\e0\\(aq, то эти функции возвращают указатель на завершающий символ."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<strchrnul>()  function returns a pointer to the matched character, or "
"a pointer to the null byte at the end of I<s> (i.e., I<s+strlen(s)>)  if the "
"character is not found."
msgstr ""
"Функция B<strchrnul>() возвращает указатель на совпавший символ, или "
"указатель на байт null в конце строки I<s> (т.е., I<s+strlen(s)>), если "
"символ не найден."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strchr>(),\n"
"B<strrchr>(),\n"
"B<strchrnul>()"
msgstr ""
"B<strchr>(),\n"
"B<strrchr>(),\n"
"B<strchrnul>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strncpy>(3)"
msgid "B<strchr>()"
msgstr "B<strncpy>(3)"

#. type: TQ
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strncpy>(3)"
msgid "B<strrchr>()"
msgstr "B<strncpy>(3)"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strlen>(3)"
msgid "B<strchrnul>()"
msgstr "B<strlen>(3)"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C89, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, C89, SVr4, 4.3BSD."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "glibc 2.1.1, FreeBSD 10, NetBSD 8."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<index>(3), B<memchr>(3), B<rindex>(3), B<string>(3), B<strlen>(3), "
#| "B<strpbrk>(3), B<strsep>(3), B<strspn>(3), B<strstr>(3), B<strtok>(3), "
#| "B<wcschr>(3), B<wcsrchr>(3)"
msgid ""
"B<memchr>(3), B<string>(3), B<strlen>(3), B<strpbrk>(3), B<strsep>(3), "
"B<strspn>(3), B<strstr>(3), B<strtok>(3), B<wcschr>(3), B<wcsrchr>(3)"
msgstr ""
"B<index>(3), B<memchr>(3), B<rindex>(3), B<string>(3), B<strlen>(3), "
"B<strpbrk>(3), B<strsep>(3), B<strspn>(3), B<strstr>(3), B<strtok>(3), "
"B<wcschr>(3), B<wcsrchr>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "The B<strchr>()  and B<strrchr>()  functions return a pointer to the "
#| "matched character or NULL if the character is not found.  The terminating "
#| "null byte is considered part of the string, so that if I<c> is specified "
#| "as \\(aq\\e0\\(aq, these functions return a pointer to the terminator."
msgid ""
"The B<strchr>()  and B<strrchr>()  functions return a pointer to the matched "
"character or NULL if the character is not found.  The terminating null byte "
"is considered part of the string, so that if I<c> is specified as \\[aq]"
"\\e0\\[aq], these functions return a pointer to the terminator."
msgstr ""
"Функции B<strchr>() и B<strrchr>() возвращает указатель на местонахождение "
"первого совпадения с символом или NULL, если символ не найден. Завершающий "
"байт null считается частью строки, и поэтому, если символ I<c> задан как \\"
"(aq\\e0\\(aq, то эти функции возвращают указатель на завершающий символ."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<strchrnul>()  first appeared in glibc in version 2.1.1."
msgid "B<strchrnul>()  was added in glibc 2.1.1."
msgstr "Функция B<strchrnul>() впервые появилась в glibc версии 2.1.1."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<strchr>(), B<strrchr>(): POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, "
#| "4.3BSD."
msgid ""
"B<strchr>(), B<strrchr>(): POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr ""
"B<strchr>(), B<strrchr>(): POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, "
"4.3BSD."

#. type: Plain text
#: debian-bookworm
msgid "B<strchrnul>()  is a GNU extension."
msgstr "Функция B<strchrnul>() является расширением GNU."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr "15 июня 2024 г."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, fuzzy
#| msgid "Since glibc 2.19:"
msgid "glibc 2.1.1."
msgstr "Начиная с glibc 2.19:"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
