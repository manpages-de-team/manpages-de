# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2013, 2015-2017, 2019.
# Kirill Rekhov <krekhov.dev@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:43+0100\n"
"PO-Revision-Date: 2024-12-21 19:49+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<__ppc_set_ppr_med>(),\n"
msgid "__ppc_set_ppr_med"
msgstr "B<__ppc_set_ppr_med>(),\n"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 июля 2024 г."

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "LinuxProgrammer's Manual"
msgid "Programmer's Manual\""
msgstr "Руководство пользователя Linux"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАИМЕНОВАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"__ppc_set_ppr_med, __ppc_set_ppr_very_low, __ppc_set_ppr_low, "
"__ppc_set_ppr_med_low, __ppc_set_ppr_med_high - Set the Program Priority "
"Register"
msgstr ""
"__ppc_set_ppr_med, __ppc_set_ppr_very_low, __ppc_set_ppr_low, "
"__ppc_set_ppr_med_low, __ppc_set_ppr_med_high - изменяет регистр приоритета "
"программы"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "БИБЛИОТЕКА"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Standard C library (I<libc>, I<-lc>)"
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ОБЗОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/platform/ppc.hE<gt>>\n"
msgstr "B<#include E<lt>sys/platform/ppc.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "__ppc_set_ppr_med, __ppc_set_ppr_very_low, __ppc_set_ppr_low, __ppc_set_ppr_med_low, __ppc_set_ppr_med_high - Set the Program Priority Register"
msgid ""
"B<void __ppc_set_ppr_med(void);>\n"
"B<void __ppc_set_ppr_very_low(void);>\n"
"B<void __ppc_set_ppr_low(void);>\n"
"B<void __ppc_set_ppr_med_low(void);>\n"
"B<void __ppc_set_ppr_med_high(void);>\n"
msgstr "__ppc_set_ppr_med, __ppc_set_ppr_very_low, __ppc_set_ppr_low, __ppc_set_ppr_med_low, __ppc_set_ppr_med_high - изменяет регистр приоритета программы"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions provide access to the I<Program Priority Register> (PPR) on "
"the Power architecture."
msgstr ""
"Эти функции предоставляют доступ к I<Program Priority Register> (PPR, "
"регистру приоритета программы) архитектуры Power."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The PPR is a 64-bit register that controls the program's priority.  By "
"adjusting the PPR value the programmer may improve system throughput by "
"causing system resources to be used more efficiently, especially in "
"contention situations.  The available unprivileged states are covered by the "
"following functions:"
msgstr ""
"PPR — это 64-битный регистр, которой управляет приоритетом программы. "
"Изменяя значение PPR, программист может улучшить системную "
"производительность, сделав использование системных ресурсов более "
"эффективным, особенно в ситуациях конкурентного доступа. Доступные "
"непривилегированные состояния достигаются следующими функциями:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<__ppc_set_ppr_med>(),\n"
msgid "B<__ppc_set_ppr_med>()"
msgstr "B<__ppc_set_ppr_med>(),\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<__ppc_set_ppr_med>()  sets the Program Priority Register value to "
#| "I<medium> (default)."
msgid "sets the Program Priority Register value to I<medium> (default)."
msgstr ""
"Функция B<__ppc_set_ppr_med>() устанавливает регистр приоритета программы "
"равным I<среднему> значению (по умолчанию)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<__ppc_set_ppr_very_low>(),\n"
msgid "B<__ppc_set_ppr_very_low>()"
msgstr "B<__ppc_set_ppr_very_low>(),\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<__ppc_set_ppr_very_low>()  sets the Program Priority Register value to "
#| "I<very low>."
msgid "sets the Program Priority Register value to I<very low>."
msgstr ""
"Функция B<__ppc_set_ppr_very_low>() устанавливает регистр приоритета "
"программы равным I<очень низкому> значению."

#. #-#-#-#-#  archlinux: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-42: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  mageia-cauldron: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-16-0: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<__ppc_set_ppr_low>(),\n"
msgid "B<__ppc_set_ppr_low>()"
msgstr "B<__ppc_set_ppr_low>(),\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<__ppc_set_ppr_low>()  sets the Program Priority Register value to "
#| "I<low>."
msgid "sets the Program Priority Register value to I<low>."
msgstr ""
"Функция B<__ppc_set_ppr_low>() устанавливает регистр приоритета программы "
"равным I<низкому> значению."

#. #-#-#-#-#  archlinux: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-42: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  mageia-cauldron: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-16-0: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<__ppc_set_ppr_med_low>(),\n"
msgid "B<__ppc_set_ppr_med_low>()"
msgstr "B<__ppc_set_ppr_med_low>(),\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<__ppc_set_ppr_med_low>()  sets the Program Priority Register value to "
#| "I<medium low>."
msgid "sets the Program Priority Register value to I<medium low>."
msgstr ""
"Функция B<__ppc_set_ppr_med_low>() устанавливает регистр приоритета "
"программы равным значению I<ниже среднего>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The privileged state I<medium high> may also be set during certain time "
"intervals by problem-state (unprivileged)  programs, with the following "
"function:"
msgstr ""
"Привилегированное состояние I<выше среднего> также может быть установлено на "
"определённые временные интервалы (непривилегированными) программами в режиме "
"задачи (problem-state) следующей функцией:"

#. #-#-#-#-#  archlinux: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-42: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  mageia-cauldron: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-16-0: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: __ppc_set_ppr_med.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<__ppc_set_ppr_med_high>()"
msgstr "B<__ppc_set_ppr_med_high>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<__ppc_set_ppr_med_high>()  sets the Program Priority to I<medium high>."
msgid "sets the Program Priority to I<medium high>."
msgstr ""
"Функция B<__ppc_set_ppr_med_high>() устанавливает регистр приоритета "
"программы равным значению I<выше среднего>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the program priority is medium high when the time interval expires or if "
"an attempt is made to set the priority to medium high when it is not "
"allowed, the priority is set to medium."
msgstr ""
"Если приоритет программы выше среднего и истёк временной интервал или если "
"выполняется попытка назначить приоритет выше среднего в неразрешённый "
"момент, то приоритет устанавливается в среднее значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "__ppc_set_ppr_med, __ppc_set_ppr_very_low, __ppc_set_ppr_low, __ppc_set_ppr_med_low, __ppc_set_ppr_med_high - Set the Program Priority Register"
msgid ""
"B<__ppc_set_ppr_med>(),\n"
"B<__ppc_set_ppr_very_low>(),\n"
"B<__ppc_set_ppr_low>(),\n"
"B<__ppc_set_ppr_med_low>(),\n"
"B<__ppc_set_ppr_med_high>()"
msgstr "__ppc_set_ppr_med, __ppc_set_ppr_very_low, __ppc_set_ppr_low, __ppc_set_ppr_med_low, __ppc_set_ppr_med_high - изменяет регистр приоритета программы"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.18."
msgstr "glibc 2.18."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "Since glibc 2.2.2:"
msgid "glibc 2.23."
msgstr "Начиная с glibc 2.2.2:"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ПРИМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The functions B<__ppc_set_ppr_very_low>()  and B<__ppc_set_ppr_med_high>()  "
"will be defined by I<E<lt>sys/platform/ppc.hE<gt>> if B<_ARCH_PWR8> is "
"defined.  Availability of these functions can be tested using B<#ifdef "
"_ARCH_PWR8>."
msgstr ""
"Функции B<__ppc_set_ppr_very_low>() и B<__ppc_set_ppr_med_high>() будут "
"определены из I<E<lt>sys/platform/ppc.hE<gt>>, если определён B<_ARCH_PWR8>. "
"Доступность этих функций можно проверить с помощью B<#ifdef _ARCH_PWR8>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<__ppc_yield>(3)"
msgstr "B<__ppc_yield>(3)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid "I<Power ISA, Book\\ II - Section\\ 3.1 (Program Priority Registers)>"
msgid "I<Power ISA, Book\\ II - Section\\ 3.1 (Program Priority Registers)>"
msgstr "I<Power ISA, Book\\ II - Section\\ 3.1 (Program Priority Registers)>"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Справочные страницы Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Стандартная библиотека языка C (I<libc>, I<-lc>)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "The functions B<__ppc_set_ppr_med>(), B<__ppc_set_ppr_low>()  and "
#| "B<__ppc_set_ppr_med_low>()  are provided by glibc since version 2.18.  "
#| "The functions B<__ppc_set_ppr_very_low>()  and "
#| "B<__ppc_set_ppr_med_high>()  first appeared in glibc in version 2.23."
msgid ""
"The functions B<__ppc_set_ppr_med>(), B<__ppc_set_ppr_low>(), and "
"B<__ppc_set_ppr_med_low>()  are provided since glibc 2.18.  The functions "
"B<__ppc_set_ppr_very_low>()  and B<__ppc_set_ppr_med_high>()  first appeared "
"in glibc 2.23."
msgstr ""
"Функции B<__ppc_set_ppr_med>(), B<__ppc_set_ppr_low>() и "
"B<__ppc_set_ppr_med_low>() имеются в glibc начиная с версии 2.18. Функции "
"B<__ppc_set_ppr_very_low>() и B<__ppc_set_ppr_med_high>() впервые появились "
"в glibc в версии 2.23."

#. type: Plain text
#: debian-bookworm
msgid "These functions are nonstandard GNU extensions."
msgstr "Эти функции являются расширениями GNU."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 мая 2024 г."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Справочные страницы Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Справочные страницы Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Справочные страницы Linux (невыпущенные)"
