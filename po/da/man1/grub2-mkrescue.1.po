# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: 2024-05-13 15:41+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKRESCUE"
msgstr "GRUB-MKRESCUE"

#. type: TH
#: fedora-42 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "February 2023"
msgid "February 2025"
msgstr "februar 2023"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.12"
msgstr "GRUB 2.12"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brugerkommandoer"

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "grub-mkrescue - make a GRUB rescue image"
msgstr "grub-mkresuce - lav et GRUB-redningsaftryk"

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<grub-mkrescue> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>] I<\\,SOURCE\\/>..."
msgstr ""
"B<grub-mkrescue> [I<\\,TILVALG\\/>...] [I<\\,TILVALG\\/>] I<\\,KILDE\\/>..."

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Make GRUB CD-ROM, disk, pendrive and floppy bootable image."
msgstr "Opret GRUB-opstartsaftryk til cd-rom, disk, USB-nøgle eller diskette."

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--appended-signature-size>=I<\\,SIZE\\/>"
msgstr "B<--appended-signature-size>=I<\\,STR\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Add a note segment reserving SIZE bytes for an appended signature"
msgstr ""
"Tilføj et notesegment der reserverer STØRRELSE byte for en tilføjet signatur"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--compress>=I<\\,no\\/>|xz|gz|lzo"
msgstr "B<--compress>=I<\\,no\\/>|xz|gz|lzo"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "compress GRUB files [optional]"
msgstr "komprimer GRUB-filer [valgfri]"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--disable-shim-lock>"
msgstr "B<--disable-shim-lock>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "disable shim_lock verifier"
msgstr "deaktiver shim_lock-verifikator"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--dtb>=I<\\,FILE\\/>"
msgstr "B<--dtb>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "embed a specific DTB"
msgstr "indlejr en specifik DTB"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,MAPPE\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"
msgstr ""
"brug billeder og moduler under KAT [standard=/usr/lib/grub/"
"E<lt>platformE<gt>]"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--fonts>=I<\\,FONTS\\/>"
msgstr "B<--fonts>=I<\\,SKRIFTTYPER\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install FONTS [default=unicode]"
msgstr "installer SKRIFTTYPER [standard=unicode]"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--install-modules>=I<\\,MODULES\\/>"
msgstr "B<--install-modules>=I<\\,MODULER\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install only MODULES and their dependencies [default=all]"
msgstr "installer kun MODULER og deres afhængigheder [standard=alle]"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--pubkey>=I<\\,FILE\\/>"
msgstr "B<-k>, B<--pubkey>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "embed FILE as public key for signature checking"
msgstr "indlejr FIL som offentlig nøgle for signaturkontrol"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--locale-directory>=I<\\,DIR\\/> use translations under DIR"
msgstr "B<--locale-directory>=I<\\,MAPPE\\/> brug oversættelser under MAPPE"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "[default=/usr/share/locale]"
msgstr "[standard=/usr/share/locale]"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--locales>=I<\\,LOCALES\\/>"
msgstr "B<--locales>=I<\\,REGIONER\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install only LOCALES [default=all]"
msgstr "installer kun REGIONER [standard=alle]"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--modules>=I<\\,MODULES\\/>"
msgstr "B<--modules>=I<\\,MODULER\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "pre-load specified modules MODULES"
msgstr "forudindlæs angivne moduler MODULER"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--sbat>=I<\\,FILE\\/>"
msgstr "B<--sbat>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "SBAT metadata"
msgstr "SBAT-metadata"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--themes>=I<\\,THEMES\\/>"
msgstr "B<--themes>=I<\\,TEMAER\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "install THEMES [default=starfield]"
msgstr "installer TEMAER [standard=starfield]"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "print verbose messages."
msgstr "udskriv uddybende meddelelser."

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--x509key>=I<\\,FILE\\/>"
msgstr "B<-x>, B<--x509key>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "embed FILE as an x509 certificate for signature checking"
msgstr "indlejr FIL som et x509-certifikat for signaturkontrol"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--arcs-boot>"
msgstr "B<--arcs-boot>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"enable ARCS (big-endian mips machines, mostly SGI) boot. Disables HFS+, APM, "
"sparc64 and boot as disk image for i386-pc"
msgstr ""
"så ARCS-boot til (big-endian mips-maskiner, oftest SGI). Deaktiverer HFS+, "
"APM og sparc64 samt boot som diskaftryk for i386-pc"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--core-compress>=I<\\,xz\\/>|none|auto"
msgstr "B<--core-compress>=I<\\,xz\\/>|none|auto"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "choose the compression to use for core image"
msgstr "vælg komprimeringen, der skal bruges til kerneaftryk"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--label-bgcolor>=I<\\,COLOR\\/>"
msgstr "B<--label-bgcolor>=I<\\,FARVE\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "use COLOR for label background"
msgstr "brug FARVE til etiketbaggrund"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--label-color>=I<\\,COLOR\\/>"
msgstr "B<--label-color>=I<\\,FARVE\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "use COLOR for label"
msgstr "brug FARVE til etiket"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--label-font>=I<\\,FILE\\/>"
msgstr "B<--label-font>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "use FILE as font for label"
msgstr "brug FIL som skrifttype for etiket"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "save output in FILE [required]"
msgstr "gem uddata i FIL [påkrævet]"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--product-name>=I<\\,STRING\\/>"
msgstr "B<--product-name>=I<\\,STRENG\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "use STRING as product name"
msgstr "brug STRENG som produktnavn"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--product-version>=I<\\,STRING\\/>"
msgstr "B<--product-version>=I<\\,STRENG\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "use STRING as product version"
msgstr "brug STRENG som produktversion"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--rom-directory>=I<\\,DIR\\/>"
msgstr "B<--rom-directory>=I<\\,MAPPE\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "save ROM images in DIR [optional]"
msgstr "gem ROM-aftryk i MAPPE [valgfri]"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--sparc-boot>"
msgstr "B<--sparc-boot>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"enable sparc boot. Disables HFS+, APM, ARCS and boot as disk image for i386-"
"pc"
msgstr ""
"aktiver sparc-boot. Deaktiverer HFS+, APM og ARCS samt boot som diskaftryk "
"for i386-pc"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--xorriso>=I<\\,FILE\\/>"
msgstr "B<--xorriso>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "use FILE as xorriso [optional]"
msgstr "brug FIL som xorriso [valgfri]"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "vis denne hjælpeliste"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "vis en kort besked om brug af programmet"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "print program version"
msgstr "udskriv programversion"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Obligatoriske eller valgfri argumenter til lange tilvalg er også "
"obligatoriske henholdsvis valgfri til de tilsvarende korte."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Generates a bootable CD/USB/floppy image.  Arguments other than options to "
"this program are passed to xorriso, and indicate source files, source "
"directories, or any of the mkisofs options listed by the output of `xorriso "
"B<-as> mkisofs B<-help>'."
msgstr ""
"Opret et cd/usb/diskette-opstartsaftryk. Argumenter udover indstillingerne "
"til dette program sendes til xorriso, og indikerer kildefiler, kildemapper, "
"eller enhver af mkisofts-tilvalg via resultatet for »xorriso B<-as> mkisofs "
"B<-help>«."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Option B<--> switches to native xorriso command mode."
msgstr "Tilvalget B<--> skifter til systemspecifik xorriso-kommandotilstand."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Mail xorriso support requests to E<lt>bug-xorriso@gnu.orgE<gt>."
msgstr "Send forespørgsler om xorriso til E<lt>bug-xorriso@gnu.orgE<gt>."

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEJLRAPPORTER"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapporter programfejl på engelsk til E<lt>bug-grub@gnu.orgE<gt>.\n"
"Oversættelsesfejl rapporteres til E<lt>dansk@dansk-gruppen.dkE<gt>."

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<grub-mkimage>(1)"
msgstr "B<grub-mkimage>(1)"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mkrescue> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkrescue> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fulde dokumentation for B<grub-mkrescue> vedligeholdes som Texinfo-"
"manual. Hvis B<info> og B<grub-mkrescue> programmerne er korrekt installeret "
"på din side, bør kommandoen"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<info grub-mkrescue>"
msgstr "B<info grub-mkrescue>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "give dig adgang til den fulde manual."

#. type: TP
#: fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--disable-cli>"
msgstr "B<--disable-cli>"

#. type: Plain text
#: fedora-rawhide opensuse-leap-16-0 opensuse-tumbleweed
msgid "disabled command line interface access"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2024"
msgstr "oktober 2024"

#. type: TH
#: opensuse-leap-16-0
#, fuzzy, no-wrap
#| msgid "December 2021"
msgid "December 2024"
msgstr "december 2021"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "GRUB2 2.12"

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"use images and modules under DIR [default=/usr/share/grub2/"
"E<lt>platformE<gt>]"
msgstr ""
"brug billeder og moduler under MAPPE [standard=/usr/share/grub2/"
"E<lt>platformE<gt>]"
