# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2025-02-28 16:49+0100\n"
"PO-Revision-Date: 2024-11-27 21:42+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sem_wait"
msgstr "sem_wait"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 iulie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "sem_wait, sem_timedwait, sem_trywait - lock a semaphore"
msgstr "sem_wait, sem_timedwait, sem_trywait - blochează un semafor"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>,\\ I<-lpthread>)"
msgstr "Biblioteca de fire de execuție POSIX (I<libpthread>, I<-lpthread>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>semaphore.hE<gt>>\n"
msgstr "B<#include E<lt>semaphore.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int sem_wait(sem_t *>I<sem>B<);>\n"
"B<int sem_trywait(sem_t *>I<sem>B<);>\n"
"B<int sem_timedwait(sem_t *restrict >I<sem>B<,>\n"
"B<                  const struct timespec *restrict >I<abs_timeout>B<);>\n"
msgstr ""
"B<int sem_wait(sem_t *>I<sem>B<);>\n"
"B<int sem_trywait(sem_t *>I<sem>B<);>\n"
"B<int sem_timedwait(sem_t *restrict >I<sem>B<,>\n"
"B<                  const struct timespec *restrict >I<abs_timeout>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<sem_timedwait>():"
msgstr "B<sem_timedwait>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<sem_wait>()  decrements (locks) the semaphore pointed to by I<sem>.  If "
"the semaphore's value is greater than zero, then the decrement proceeds, and "
"the function returns, immediately.  If the semaphore currently has the value "
"zero, then the call blocks until either it becomes possible to perform the "
"decrement (i.e., the semaphore value rises above zero), or a signal handler "
"interrupts the call."
msgstr ""
"B<sem_wait>() decrementează (blochează) semaforul indicat de I<sem>. În "
"cazul în care valoarea semaforului este mai mare decât zero, atunci "
"decrementarea are loc, iar funcția returnează, imediat. În cazul în care "
"semaforul are în prezent valoarea zero, apelul se blochează până când fie "
"devine posibilă efectuarea decrementării (de exemplu, valoarea semaforului "
"crește peste zero), fie un gestionar de semnal întrerupe apelul."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<sem_trywait>()  is the same as B<sem_wait>(), except that if the decrement "
"cannot be immediately performed, then call returns an error (I<errno> set to "
"B<EAGAIN>)  instead of blocking."
msgstr ""
"B<sem_trywait>() este la fel ca B<sem_wait>(), cu excepția faptului că, dacă "
"decrementarea nu poate fi efectuată imediat, apelul returnează o eroare "
"(I<errno> este configurată la B<EAGAIN>) în loc să blocheze."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<sem_timedwait>()  is the same as B<sem_wait>(), except that I<abs_timeout> "
"specifies a limit on the amount of time that the call should block if the "
"decrement cannot be immediately performed.  The I<abs_timeout> argument "
"points to a B<timespec>(3)  structure that specifies an absolute timeout in "
"seconds and nanoseconds since the Epoch, 1970-01-01 00:00:00 +0000 (UTC)."
msgstr ""
"B<sem_timedwait>() este la fel ca B<sem_wait>(), cu excepția faptului că "
"I<abs_timeout> specifică o limită de timp în care apelul trebuie blocat dacă "
"decrementarea nu poate fi efectuată imediat.  Argumentul I<abs_timeout> "
"indică o structură B<timespec>(3) care specifică un timp de așteptare "
"absolut în secunde și nanosecunde de la Epoch, 1970-01-01 00:00:00 +0000 "
"(UTC)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the timeout has already expired by the time of the call, and the "
"semaphore could not be locked immediately, then B<sem_timedwait>()  fails "
"with a timeout error (I<errno> set to B<ETIMEDOUT>)."
msgstr ""
"Dacă timpul de așteptare a expirat deja în momentul apelului, iar semaforul "
"nu a putut fi blocat imediat, atunci B<sem_timedwait>() eșuează cu o eroare "
"de timp (I<errno> este configurată la B<ETIMEDOUT>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the operation can be performed immediately, then B<sem_timedwait>()  "
"never fails with a timeout error, regardless of the value of "
"I<abs_timeout>.  Furthermore, the validity of I<abs_timeout> is not checked "
"in this case."
msgstr ""
"Dacă operația poate fi efectuată imediat, atunci B<sem_timedwait>() nu "
"eșuează niciodată cu o eroare de timp de așteptare, indiferent de valoarea "
"lui I<abs_timeout>. În plus, validitatea lui I<abs_timeout> nu este "
"verificată în acest caz."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"All of these functions return 0 on success; on error, the value of the "
"semaphore is left unchanged, -1 is returned, and I<errno> is set to indicate "
"the error."
msgstr ""
"Toate aceste funcții returnează 0 în caz de succes; în caz de eroare, "
"valoarea semaforului rămâne neschimbată, se returnează -1, iar I<errno> este "
"configurată pentru a indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"(B<sem_trywait>())  The operation could not be performed without blocking "
"(i.e., the semaphore currently has the value zero)."
msgstr ""
"(B<sem_trywait>()) Operația nu a putut fi efectuată fără blocare (de "
"exemplu, semaforul are în prezent valoarea zero)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The call was interrupted by a signal handler; see B<signal>(7)."
msgstr ""
"Apelul a fost întrerupt de un gestionar de semnal; a se vedea B<signal>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<sem> is not a valid semaphore."
msgstr "I<semafor> nu este un semafor valid."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"(B<sem_timedwait>())  The value of I<abs_timeout.tv_nsecs> is less than 0, "
"or greater than or equal to 1000 million."
msgstr ""
"(B<sem_timedwait>()) Valoarea I<abs_timeout.tv_nsecs> este mai mică decât 0 "
"sau mai mare sau egală cu 1000 de milioane."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ETIMEDOUT>"
msgstr "B<ETIMEDOUT>"

#.  POSIX.1-2001 also allows EDEADLK -- "A deadlock condition
#.  was detected", but this does not occur on Linux(?).
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"(B<sem_timedwait>())  The call timed out before the semaphore could be "
"locked."
msgstr ""
"(B<sem_timedwait>()) Apelul a expirat înainte ca semaforul să poată fi "
"blocat."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<sem_wait>(),\n"
"B<sem_trywait>(),\n"
"B<sem_timedwait>()"
msgstr ""
"B<sem_wait>(),\n"
"B<sem_trywait>(),\n"
"B<sem_timedwait>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The (somewhat trivial) program shown below operates on an unnamed "
"semaphore.  The program expects two command-line arguments.  The first "
"argument specifies a seconds value that is used to set an alarm timer to "
"generate a B<SIGALRM> signal.  This handler performs a B<sem_post>(3)  to "
"increment the semaphore that is being waited on in I<main()> using "
"B<sem_timedwait>().  The second command-line argument specifies the length "
"of the timeout, in seconds, for B<sem_timedwait>().  The following shows "
"what happens on two different runs of the program:"
msgstr ""
"Programul (oarecum trivial) prezentat mai jos operează pe un semafor fără "
"nume. Programul așteaptă două argumente în linia de comandă. Primul argument "
"specifică o valoare în secunde care este utilizată pentru a configura un "
"temporizator de alarmă pentru a genera un semnal B<SIGALRM>. Acest gestionar "
"efectuează un B<sem_post>(3) pentru a incrementa semaforul care este "
"așteptat în I<main()> utilizând B<sem_timedwait>(). Al doilea argument din "
"linia de comandă specifică lungimea timpului de așteptare, în secunde, "
"pentru B<sem_timedwait>(). Ceea ce urmează arată ce se întâmplă la două "
"execuții diferite ale programului:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out 2 3>\n"
"About to call sem_timedwait()\n"
"sem_post() from handler\n"
"sem_timedwait() succeeded\n"
"$B< ./a.out 2 1>\n"
"About to call sem_timedwait()\n"
"sem_timedwait() timed out\n"
msgstr ""
"$B< ./a.out 2 3>\n"
"În curs de apelare a sem_timedwait()\n"
"sem_post() de la gestionar\n"
"sem_timedwait() a reușit\n"
"$B< ./a.out 2 1>\n"
"În curs de apelare a sem_timedwait()\n"
"ssem_timedwait() a expirat\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Sursa programului"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>semaphore.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#include E<lt>assert.hE<gt>\n"
"\\&\n"
"sem_t sem;\n"
"\\&\n"
"#define handle_error(msg) \\[rs]\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void\n"
"handler(int sig)\n"
"{\n"
"    write(STDOUT_FILENO, \"sem_post() from handler\\[rs]n\", 24);\n"
"    if (sem_post(&sem) == -1) {\n"
"        write(STDERR_FILENO, \"sem_post() failed\\[rs]n\", 18);\n"
"        _exit(EXIT_FAILURE);\n"
"    }\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct sigaction sa;\n"
"    struct timespec ts;\n"
"    int s;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>alarm-secsE<gt> E<lt>wait-secsE<gt>\\[rs]n\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (sem_init(&sem, 0, 0) == -1)\n"
"        handle_error(\"sem_init\");\n"
"\\&\n"
"    /* Establish SIGALRM handler; set alarm timer using argv[1]. */\n"
"\\&\n"
"    sa.sa_handler = handler;\n"
"    sigemptyset(&sa.sa_mask);\n"
"    sa.sa_flags = 0;\n"
"    if (sigaction(SIGALRM, &sa, NULL) == -1)\n"
"        handle_error(\"sigaction\");\n"
"\\&\n"
"    alarm(atoi(argv[1]));\n"
"\\&\n"
"    /* Calculate relative interval as current time plus\n"
"       number of seconds given argv[2]. */\n"
"\\&\n"
"    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"
"\\&\n"
"    ts.tv_sec += atoi(argv[2]);\n"
"\\&\n"
"    printf(\"%s() about to call sem_timedwait()\\[rs]n\", __func__);\n"
"    while ((s = sem_timedwait(&sem, &ts)) == -1 && errno == EINTR)\n"
"        continue;       /* Restart if interrupted by handler. */\n"
"\\&\n"
"    /* Check what happened. */\n"
"\\&\n"
"    if (s == -1) {\n"
"        if (errno == ETIMEDOUT)\n"
"            printf(\"sem_timedwait() timed out\\[rs]n\");\n"
"        else\n"
"            perror(\"sem_timedwait\");\n"
"    } else\n"
"        printf(\"sem_timedwait() succeeded\\[rs]n\");\n"
"\\&\n"
"    exit((s == 0) ? EXIT_SUCCESS : EXIT_FAILURE);\n"
"}\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>semaphore.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#include E<lt>assert.hE<gt>\n"
"\\&\n"
"sem_t sem;\n"
"\\&\n"
"#define handle_error(msg) \\[rs]\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void\n"
"handler(int sig)\n"
"{\n"
"    write(STDOUT_FILENO, \"sem_post() de la gestionar\\[rs]n\", 24);\n"
"    if (sem_post(&sem) == -1) {\n"
"        write(STDERR_FILENO, \"sem_post() a eșuat\\[rs]n\", 18);\n"
"        _exit(EXIT_FAILURE);\n"
"    }\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct sigaction sa;\n"
"    struct timespec ts;\n"
"    int s;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Utilizare: %s E<lt>secunde-alarmăE<gt> E<lt>secunde-așteptareE<gt>\\[rs]n\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (sem_init(&sem, 0, 0) == -1)\n"
"        handle_error(\"sem_init\");\n"
"\\&\n"
"    /* Stabilește gestionarul SIGALRM; fixează temporizatorul de alarmă utilizând argv[1]. */\n"
"\\&\n"
"    sa.sa_handler = handler;\n"
"    sigemptyset(&sa.sa_mask);\n"
"    sa.sa_flags = 0;\n"
"    if (sigaction(SIGALRM, &sa, NULL) == -1)\n"
"        handle_error(\"sigaction\");\n"
"\\&\n"
"    alarm(atoi(argv[1]));\n"
"\\&\n"
"    /* Calculează intervalul relativ ca timp curent plus\n"
"       numărul de secunde dat de argv[2]. */\n"
"\\&\n"
"    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"
"\\&\n"
"    ts.tv_sec += atoi(argv[2]);\n"
"\\&\n"
"    printf(\"%s() pe cale să apeleze sem_timedwait()\\[rs]n\", __func__);\n"
"    while ((s = sem_timedwait(&sem, &ts)) == -1 && errno == EINTR)\n"
"        continue;       /* Repornește dacă este întrerupt de gestionar. */\n"
"\\&\n"
"    /* Verifică ce s-a întâmplat. */\n"
"\\&\n"
"    if (s == -1) {\n"
"        if (errno == ETIMEDOUT)\n"
"            printf(\"sem_timedwait() timed out\\[rs]n\");\n"
"        else\n"
"            perror(\"sem_timedwait\");\n"
"    } else\n"
"        printf(\"sem_timedwait() a reușit\\[rs]n\");\n"
"\\&\n"
"    exit((s == 0) ? EXIT_SUCCESS : EXIT_FAILURE);\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<clock_gettime>(2), B<sem_getvalue>(3), B<sem_post>(3), B<timespec>(3), "
"B<sem_overview>(7), B<time>(7)"
msgstr ""
"B<clock_gettime>(2), B<sem_getvalue>(3), B<sem_post>(3), B<timespec>(3), "
"B<sem_overview>(7), B<time>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 decembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr "Biblioteca de fire de execuție POSIX (I<libpthread>, I<-lpthread>)"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>semaphore.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>semaphore.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "#include E<lt>assert.hE<gt>\n"
msgstr "#include E<lt>assert.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "sem_t sem;\n"
msgstr "sem_t sem;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
msgstr ""
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"static void\n"
"handler(int sig)\n"
"{\n"
"    write(STDOUT_FILENO, \"sem_post() from handler\\en\", 24);\n"
"    if (sem_post(&sem) == -1) {\n"
"        write(STDERR_FILENO, \"sem_post() failed\\en\", 18);\n"
"        _exit(EXIT_FAILURE);\n"
"    }\n"
"}\n"
msgstr ""
"static void\n"
"handler(int sig)\n"
"{\n"
"    write(STDOUT_FILENO, \"sem_post() de la gestionar\\en\", 24);\n"
"    if (sem_post(&sem) == -1) {\n"
"        write(STDERR_FILENO, \"sem_post() a eșuat\\en\", 18);\n"
"        _exit(EXIT_FAILURE);\n"
"    }\n"
"}\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct sigaction sa;\n"
"    struct timespec ts;\n"
"    int s;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct sigaction sa;\n"
"    struct timespec ts;\n"
"    int s;\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>alarm-secsE<gt> E<lt>wait-secsE<gt>\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Utilizare: %s E<lt>secunde-alarmăE<gt> E<lt>secunde-așteptareE<gt>\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (sem_init(&sem, 0, 0) == -1)\n"
"        handle_error(\"sem_init\");\n"
msgstr ""
"    if (sem_init(&sem, 0, 0) == -1)\n"
"        handle_error(\"sem_init\");\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* Establish SIGALRM handler; set alarm timer using argv[1]. */\n"
msgstr "    /* Stabilește gestionarul SIGALRM; fixează temporizatorul de alarmă utilizând argv[1]. */\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    sa.sa_handler = handler;\n"
"    sigemptyset(&sa.sa_mask);\n"
"    sa.sa_flags = 0;\n"
"    if (sigaction(SIGALRM, &sa, NULL) == -1)\n"
"        handle_error(\"sigaction\");\n"
msgstr ""
"    sa.sa_handler = handler;\n"
"    sigemptyset(&sa.sa_mask);\n"
"    sa.sa_flags = 0;\n"
"    if (sigaction(SIGALRM, &sa, NULL) == -1)\n"
"        handle_error(\"sigaction\");\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    alarm(atoi(argv[1]));\n"
msgstr "    alarm(atoi(argv[1]));\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    /* Calculate relative interval as current time plus\n"
"       number of seconds given argv[2]. */\n"
msgstr ""
"    /* Calculează intervalul relativ ca timp curent plus\n"
"       numărul de secunde dat de argv[2]. */\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"
msgstr ""
"    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    ts.tv_sec += atoi(argv[2]);\n"
msgstr "    ts.tv_sec += atoi(argv[2]);\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"%s() about to call sem_timedwait()\\en\", __func__);\n"
"    while ((s = sem_timedwait(&sem, &ts)) == -1 && errno == EINTR)\n"
"        continue;       /* Restart if interrupted by handler. */\n"
msgstr ""
"    printf(\"%s() pe cale să apeleze sem_timedwait()\\en\", __func__);\n"
"    while ((s = sem_timedwait(&sem, &ts)) == -1 && errno == EINTR)\n"
"        continue;       /* Repornește dacă este întrerupt de gestionar. */\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* Check what happened. */\n"
msgstr "    /* Verifică ce s-a întâmplat. */\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    if (s == -1) {\n"
"        if (errno == ETIMEDOUT)\n"
"            printf(\"sem_timedwait() timed out\\en\");\n"
"        else\n"
"            perror(\"sem_timedwait\");\n"
"    } else\n"
"        printf(\"sem_timedwait() succeeded\\en\");\n"
msgstr ""
"    if (s == -1) {\n"
"        if (errno == ETIMEDOUT)\n"
"            printf(\"sem_timedwait() a expirat\\en\");\n"
"        else\n"
"            perror(\"sem_timedwait\");\n"
"    } else\n"
"        printf(\"sem_timedwait() a reușit\\en\");\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    exit((s == 0) ? EXIT_SUCCESS : EXIT_FAILURE);\n"
"}\n"
msgstr ""
"    exit((s == 0) ? EXIT_SUCCESS : EXIT_FAILURE);\n"
"}\n"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr "15 iunie 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>semaphore.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#include E<lt>assert.hE<gt>\n"
"\\&\n"
"sem_t sem;\n"
"\\&\n"
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void\n"
"handler(int sig)\n"
"{\n"
"    write(STDOUT_FILENO, \"sem_post() from handler\\en\", 24);\n"
"    if (sem_post(&sem) == -1) {\n"
"        write(STDERR_FILENO, \"sem_post() failed\\en\", 18);\n"
"        _exit(EXIT_FAILURE);\n"
"    }\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct sigaction sa;\n"
"    struct timespec ts;\n"
"    int s;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>alarm-secsE<gt> E<lt>wait-secsE<gt>\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (sem_init(&sem, 0, 0) == -1)\n"
"        handle_error(\"sem_init\");\n"
"\\&\n"
"    /* Establish SIGALRM handler; set alarm timer using argv[1]. */\n"
"\\&\n"
"    sa.sa_handler = handler;\n"
"    sigemptyset(&sa.sa_mask);\n"
"    sa.sa_flags = 0;\n"
"    if (sigaction(SIGALRM, &sa, NULL) == -1)\n"
"        handle_error(\"sigaction\");\n"
"\\&\n"
"    alarm(atoi(argv[1]));\n"
"\\&\n"
"    /* Calculate relative interval as current time plus\n"
"       number of seconds given argv[2]. */\n"
"\\&\n"
"    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"
"\\&\n"
"    ts.tv_sec += atoi(argv[2]);\n"
"\\&\n"
"    printf(\"%s() about to call sem_timedwait()\\en\", __func__);\n"
"    while ((s = sem_timedwait(&sem, &ts)) == -1 && errno == EINTR)\n"
"        continue;       /* Restart if interrupted by handler. */\n"
"\\&\n"
"    /* Check what happened. */\n"
"\\&\n"
"    if (s == -1) {\n"
"        if (errno == ETIMEDOUT)\n"
"            printf(\"sem_timedwait() timed out\\en\");\n"
"        else\n"
"            perror(\"sem_timedwait\");\n"
"    } else\n"
"        printf(\"sem_timedwait() succeeded\\en\");\n"
"\\&\n"
"    exit((s == 0) ? EXIT_SUCCESS : EXIT_FAILURE);\n"
"}\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>semaphore.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#include E<lt>assert.hE<gt>\n"
"\\&\n"
"sem_t sem;\n"
"\\&\n"
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void\n"
"handler(int sig)\n"
"{\n"
"    write(STDOUT_FILENO, \"sem_post() de la gestionar\\en\", 24);\n"
"    if (sem_post(&sem) == -1) {\n"
"        write(STDERR_FILENO, \"sem_post() a eșuat\\en\", 18);\n"
"        _exit(EXIT_FAILURE);\n"
"    }\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct sigaction sa;\n"
"    struct timespec ts;\n"
"    int s;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Utilizare: %s E<lt>secunde-alarmăE<gt> E<lt>secunde-așteptareE<gt>\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (sem_init(&sem, 0, 0) == -1)\n"
"        handle_error(\"sem_init\");\n"
"\\&\n"
"    /* Stabilește gestionarul SIGALRM; fixează temporizatorul de alarmă utilizând argv[1]. */\n"
"\\&\n"
"    sa.sa_handler = handler;\n"
"    sigemptyset(&sa.sa_mask);\n"
"    sa.sa_flags = 0;\n"
"    if (sigaction(SIGALRM, &sa, NULL) == -1)\n"
"        handle_error(\"sigaction\");\n"
"\\&\n"
"    alarm(atoi(argv[1]));\n"
"\\&\n"
"    /* Calculează intervalul relativ ca timp curent plus\n"
"       numărul de secunde dat de argv[2]. */\n"
"\\&\n"
"    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"
"\\&\n"
"    ts.tv_sec += atoi(argv[2]);\n"
"\\&\n"
"    printf(\"%s() pe cale să apeleze sem_timedwait()\\en\", __func__);\n"
"    while ((s = sem_timedwait(&sem, &ts)) == -1 && errno == EINTR)\n"
"        continue;       /* Repornește dacă este întrerupt de gestionar. */\n"
"\\&\n"
"    /* Verifică ce s-a întâmplat. */\n"
"\\&\n"
"    if (s == -1) {\n"
"        if (errno == ETIMEDOUT)\n"
"            printf(\"sem_timedwait() a expirat\\en\");\n"
"        else\n"
"            perror(\"sem_timedwait\");\n"
"    } else\n"
"        printf(\"sem_timedwait() a reușit\\en\");\n"
"\\&\n"
"    exit((s == 0) ? EXIT_SUCCESS : EXIT_FAILURE);\n"
"}\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
