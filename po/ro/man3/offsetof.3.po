# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2025-02-28 16:42+0100\n"
"PO-Revision-Date: 2024-07-01 17:51+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "offsetof"
msgstr "offsetof"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 iulie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "offsetof - offset of a structure member"
msgstr "offsetof - decalajul unui membru al structurii"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stddef.hE<gt>>\n"
msgstr "B<#include E<lt>stddef.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<size_t offsetof(>I<type>B<, >I<member>B<);>\n"
msgstr "B<size_t offsetof(>I<type>B<, >I<member>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The macro B<offsetof>()  returns the offset of the field I<member> from the "
"start of the structure I<type>."
msgstr ""
"Macro B<offsetof>() returnează decalajul câmpului I<member> de la începutul "
"structurii I<type>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This macro is useful because the sizes of the fields that compose a "
"structure can vary across implementations, and compilers may insert "
"different numbers of padding bytes between fields.  Consequently, an "
"element's offset is not necessarily given by the sum of the sizes of the "
"previous elements."
msgstr ""
"Această macro este utilă deoarece dimensiunile câmpurilor care compun o "
"structură pot varia de la o implementare la alta, iar compilatorii pot "
"insera numere diferite de octeți de umplutură între câmpuri. În consecință, "
"decalajul unui element nu este neapărat dat de suma dimensiunilor "
"elementelor anterioare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A compiler error will result if I<member> is not aligned to a byte boundary "
"(i.e., it is a bit field)."
msgstr ""
"Se va produce o eroare de compilare dacă I<member> nu este aliniat la o "
"limită de octet (de exemplu, este un câmp de biți)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<offsetof>()  returns the offset of the given I<member> within the given "
"I<type>, in units of bytes."
msgstr ""
"B<offsetof>() returnează decalajul membrului I<member> dat în cadrul tipului "
"I<type> dat, în unități de octeți."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C89."
msgstr "POSIX.1-2001, C89."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On a Linux/i386 system, when compiled using the default B<gcc>(1)  options, "
"the program below produces the following output:"
msgstr ""
"Pe un sistem Linux/i386, atunci când este compilat folosind opțiunile "
"implicite B<gcc>(1), programul de mai jos produce următoarea ieșire:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out>\n"
"offsets: i=0; c=4; d=8 a=16\n"
"sizeof(struct s)=16\n"
msgstr ""
"$B< ./a.out>\n"
"offsets: i=0; c=4; d=8 a=16\n"
"sizeof(struct s)=16\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Sursa programului"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stddef.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct s {\n"
"        int i;\n"
"        char c;\n"
"        double d;\n"
"        char a[];\n"
"    };\n"
"\\&\n"
"    /* Output is compiler dependent */\n"
"\\&\n"
"    printf(\"offsets: i=%zu; c=%zu; d=%zu a=%zu\\[rs]n\",\n"
"           offsetof(struct s, i), offsetof(struct s, c),\n"
"           offsetof(struct s, d), offsetof(struct s, a));\n"
"    printf(\"sizeof(struct s)=%zu\\[rs]n\", sizeof(struct s));\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>stddef.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct s {\n"
"        int i;\n"
"        char c;\n"
"        double d;\n"
"        char a[];\n"
"    };\n"
"\\&\n"
"    /* Ieșirea depinde de compilator */\n"
"\\&\n"
"    printf(\"decalaje: i=%zu; c=%zu; d=%zu a=%zu\\[rs]n\",\n"
"           offsetof(struct s, i), offsetof(struct s, c),\n"
"           offsetof(struct s, d), offsetof(struct s, a));\n"
"    printf(\"sizeof(struct s)=%zu\\[rs]n\", sizeof(struct s));\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-29"
msgstr "29 decembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>stddef.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#include E<lt>stddef.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct s {\n"
"        int i;\n"
"        char c;\n"
"        double d;\n"
"        char a[];\n"
"    };\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    struct s {\n"
"        int i;\n"
"        char c;\n"
"        double d;\n"
"        char a[];\n"
"    };\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "    /* Output is compiler dependent */\n"
msgstr "    /* Ieșirea este dependentă de compilator */\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    printf(\"offsets: i=%zu; c=%zu; d=%zu a=%zu\\en\",\n"
"           offsetof(struct s, i), offsetof(struct s, c),\n"
"           offsetof(struct s, d), offsetof(struct s, a));\n"
"    printf(\"sizeof(struct s)=%zu\\en\", sizeof(struct s));\n"
msgstr ""
"    printf(\"decalaje: i=%zu; c=%zu; d=%zu a=%zu\\en\",\n"
"           offsetof(struct s, i), offsetof(struct s, c),\n"
"           offsetof(struct s, d), offsetof(struct s, a));\n"
"    printf(\"sizeof(struct s)=%zu\\en\", sizeof(struct s));\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr "15 iunie 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
"#include E<lt>stddef.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct s {\n"
"        int i;\n"
"        char c;\n"
"        double d;\n"
"        char a[];\n"
"    };\n"
"\\&\n"
"    /* Output is compiler dependent */\n"
"\\&\n"
"    printf(\"offsets: i=%zu; c=%zu; d=%zu a=%zu\\en\",\n"
"           offsetof(struct s, i), offsetof(struct s, c),\n"
"           offsetof(struct s, d), offsetof(struct s, a));\n"
"    printf(\"sizeof(struct s)=%zu\\en\", sizeof(struct s));\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>stddef.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    struct s {\n"
"        int i;\n"
"        char c;\n"
"        double d;\n"
"        char a[];\n"
"    };\n"
"\\&\n"
"    /* Ieșirea depinde de compilator */\n"
"\\&\n"
"    printf(\"decalaje: i=%zu; c=%zu; d=%zu a=%zu\\en\",\n"
"           offsetof(struct s, i), offsetof(struct s, c),\n"
"           offsetof(struct s, d), offsetof(struct s, a));\n"
"    printf(\"sizeof(struct s)=%zu\\en\", sizeof(struct s));\n"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
