# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2025-02-28 16:44+0100\n"
"PO-Revision-Date: 2024-11-25 02:20+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.5\n"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_io"
msgstr "proc_pid_io"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/io - I/O statistics"
msgstr "/proc/pid/io - statistici de In/Ieș"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</io> (since Linux 2.6.20)"
msgstr "I</proc/>pidI</io> (începând cu Linux 2.6.20)"

#.  commit 7c3ab7381e79dfc7db14a67c6f4f3285664e1ec2
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"This file contains I/O statistics for the process and its waited-for "
"children, for example:"
msgstr ""
"Acest fișier conține statistici de I/O pentru proces și copiii săi "
"așteptați, de exemplu:"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"#B< cat /proc/3828/io>\n"
"rchar: 323934931\n"
"wchar: 323929600\n"
"syscr: 632687\n"
"syscw: 632675\n"
"read_bytes: 0\n"
"write_bytes: 323932160\n"
"cancelled_write_bytes: 0\n"
msgstr ""
"#B< cat /proc/3828/io>\n"
"rchar: 323934931\n"
"wchar: 323929600\n"
"syscr: 632687\n"
"syscw: 632675\n"
"read_bytes: 0\n"
"write_bytes: 323932160\n"
"cancelled_write_bytes: 0\n"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The fields are as follows:"
msgstr "Câmpurile sunt următoarele:"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<rchar>: characters read"
msgstr "I<rchar>: caractere citite"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The number of bytes returned by successful B<read>(2)  and similar system "
"calls."
msgstr ""
"Numărul de octeți returnat de apelurile de sistem B<read>(2) și similare."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<wchar>: characters written"
msgstr "I<wchar>: caractere scrise"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The number of bytes returned by successful B<write>(2)  and similar system "
"calls."
msgstr ""
"Numărul de octeți returnat de apelurile de sistem B<write>(2) și similare."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<syscr>: read syscalls"
msgstr "I<syscr>: citește apelurile sistemului"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The number of \"file read\" system calls\\[em]those from the B<read>(2)  "
"family, B<sendfile>(2), B<copy_file_range>(2), and B<ioctl>(2)  "
"B<BTRFS_IOC_ENCODED_READ>[B<_32>] (including when invoked by the kernel as "
"part of other syscalls)."
msgstr ""
"Numărul de apeluri de sistem „citire fișier (file read)” - cele din familia "
"B<read>(2), B<sendfile>(2), B<copy_file_range>(2) și B<ioctl>(2) "
"B<BTRFS_IOC_ENCODED_READ>[B<_32>] (inclusiv atunci când sunt invocate de "
"nucleu ca parte a altor apeluri de sistem)."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<syscw>: write syscalls"
msgstr "I<syscw>: scrie apelurile sistemului"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The number of \"file write\" system calls\\[em]those from the B<write>(2)  "
"family, B<sendfile>(2), B<copy_file_range>(2), and B<ioctl>(2)  "
"B<BTRFS_IOC_ENCODED_WRITE>[B<_32>] (including when invoked by the kernel as "
"part of other syscalls)."
msgstr ""
"Numărul de apeluri de sistem „scriere fișier (file write)” - cele din "
"familia B<write>(2), B<sendfile>(2), B<copy_file_range>(2) și B<ioctl>(2) "
"B<BTRFS_IOC_ENCODED_WRITE>[B<_32>] (inclusiv atunci când sunt invocate de "
"nucleu ca parte a altor apeluri de sistem)."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<read_bytes>: bytes read"
msgstr "I<read_bytes>: octeți citiți"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The number of bytes really fetched from the storage layer.  This is accurate "
"for block-backed filesystems."
msgstr ""
"Numărul de octeți preluați efectiv din stratul de stocare. Acest lucru este "
"corect pentru sistemele de fișiere cu suport de blocuri."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<write_bytes>: bytes written"
msgstr "I<write_bytes>: octeți scriși"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "The number of bytes really sent to the storage layer."
msgstr "Numărul de octeți trimiși efectiv la stratul de stocare."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<cancelled_write_bytes>:"
msgstr "I<cancelled_write_bytes>:"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The above statistics fail to account for truncation: if a process writes 1 "
"MB to a regular file and then removes it, said 1 MB will not be written, but "
"I<will> have nevertheless been accounted as a 1 MB write.  This field "
"represents the number of bytes \"saved\" from I/O writeback.  This can yield "
"to having done negative I/O if caches dirtied by another process are "
"truncated.  I<cancelled_write_bytes> applies to I/O already accounted-for in "
"I<write_bytes>."
msgstr ""
"Statisticile de mai sus nu țin seama de trunchiere: dacă un proces scrie 1 "
"Mo într-un fișier obișnuit și apoi îl elimină, respectivul 1 Mo nu va fi "
"scris, dar I<will> va fi totuși contabilizat ca o scriere de 1 Mo.  Acest "
"câmp reprezintă numărul de octeți „salvați” de la rescrierea de In/Ieș. "
"Acest lucru poate ceda pentru a fi făcut In/Ieș negativă dacă cache-urile "
"murdărite de un alt proces sunt trunchiate. I<cancelled_write_bytes> se "
"aplică la In/Ieș deja contabilizate în I<write_bytes>."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Permission to access this file is governed by B<ptrace>(2)  access mode "
"B<PTRACE_MODE_READ_FSCREDS>."
msgstr ""
"Permisiunea de a accesa acest fișier este reglementată de modul de acces "
"B<ptrace>(2) B<PTRACE_MODE_READ_FSCREDS>."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr "AVERTISMENTE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"These counters are not atomic: on systems where 64-bit integer operations "
"may tear, a counter could be updated simultaneously with a read, yielding an "
"incorrect intermediate value."
msgstr ""
"Aceste contoare nu sunt atomice: pe sistemele în care operațiile cu numere "
"întregi pe 64 de biți se pot întrerupe, un contor ar putea fi actualizat "
"simultan cu o citire, producând o valoare intermediară incorectă."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<getrusage>(2), B<proc>(5)"
msgstr "B<getrusage>(2), B<proc>(5)"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr "15 august 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#.  commit 7c3ab7381e79dfc7db14a67c6f4f3285664e1ec2
#. type: Plain text
#: mageia-cauldron
msgid "This file contains I/O statistics for the process, for example:"
msgstr "Acest fișier conține statistici de In/Ieș pentru proces, de exemplu:"

#. type: Plain text
#: mageia-cauldron
msgid ""
"The number of bytes which this task has caused to be read from storage.  "
"This is simply the sum of bytes which this process passed to B<read>(2)  and "
"similar system calls.  It includes things such as terminal I/O and is "
"unaffected by whether or not actual physical disk I/O was required (the read "
"might have been satisfied from pagecache)."
msgstr ""
"Numărul de octeți pe care această sarcină i-a făcut să fie citiți din "
"memorie. Aceasta este pur și simplu suma de octeți pe care acest proces i-a "
"transmis către B<read>(2) și alte apeluri de sistem similare. Aceasta "
"include lucruri precum In/Ieș de terminal și nu este afectată de faptul dacă "
"a fost sau nu necesară o In/Ieș reală de disc fizic (citirea ar fi putut fi "
"satisfăcută din pagecache)."

#. type: Plain text
#: mageia-cauldron
msgid ""
"The number of bytes which this task has caused, or shall cause to be written "
"to disk.  Similar caveats apply here as with I<rchar>."
msgstr ""
"Numărul de octeți pe care această sarcină i-a făcut sau îi va face să fie "
"scriși pe disc. Aici se aplică aceleași avertismente ca în cazul I<rchar>."

#. type: Plain text
#: mageia-cauldron
msgid ""
"Attempt to count the number of read I/O operations\\[em]that is, system "
"calls such as B<read>(2)  and B<pread>(2)."
msgstr ""
"Încearcă să numere numărul de operații de citire de In/Ieș - adică apeluri "
"de sistem precum B<read>(2) și B<pread>(2)."

#. type: Plain text
#: mageia-cauldron
msgid ""
"Attempt to count the number of write I/O operations\\[em]that is, system "
"calls such as B<write>(2)  and B<pwrite>(2)."
msgstr ""
"Încearcă să numere numărul de operații de scriere de In/Ieș - adică apeluri "
"de sistem precum B<write>(2) și B<pwrite>(2)."

#. type: Plain text
#: mageia-cauldron
msgid ""
"Attempt to count the number of bytes which this process really did cause to "
"be fetched from the storage layer.  This is accurate for block-backed "
"filesystems."
msgstr ""
"Încearcă să numere numărul de octeți pe care acest proces i-a făcut cu "
"adevărat să fie preluați din stratul de stocare.  Acest lucru este corect "
"pentru sistemele de fișiere cu suport de blocuri."

#. type: Plain text
#: mageia-cauldron
msgid ""
"Attempt to count the number of bytes which this process caused to be sent to "
"the storage layer."
msgstr ""
"Încearcă să numere numărul de octeți pe care acest proces i-a făcut să fie "
"trimiși la stratul de stocare."

#. type: Plain text
#: mageia-cauldron
msgid ""
"The big inaccuracy here is truncate.  If a process writes 1 MB to a file and "
"then deletes the file, it will in fact perform no writeout.  But it will "
"have been accounted as having caused 1 MB of write.  In other words: this "
"field represents the number of bytes which this process caused to not "
"happen, by truncating pagecache.  A task can cause \"negative\" I/O too.  If "
"this task truncates some dirty pagecache, some I/O which another task has "
"been accounted for (in its I<write_bytes>)  will not be happening."
msgstr ""
"Marea inexactitate aici este truncarea. Dacă un proces scrie 1 Mo într-un "
"fișier și apoi șterge fișierul, de fapt nu va efectua nicio scriere. Dar va "
"fi contabilizat ca și cum ar fi cauzat 1 Mo de scriere. Cu alte cuvinte: "
"acest câmp reprezintă numărul de octeți pe care acest proces i-a făcut să nu "
"se întâmple, truncând pagecache. O sarcină poate provoca și In/Ieș "
"„negative”. Dacă această sarcină trunchiază o anumită pagecache murdară, o "
"anumită In/Ieș pentru care o altă sarcină a fost contabilizată (în "
"I<write_bytes> a acesteia) nu va avea loc."

#. type: Plain text
#: mageia-cauldron
msgid ""
"I<Note>: In the current implementation, things are a bit racy on 32-bit "
"systems: if process A reads process B's I</proc/>pidI</io> while process B "
"is updating one of these 64-bit counters, process A could see an "
"intermediate result."
msgstr ""
"I<Notă>: În implementarea actuală, lucrurile sunt un pic mai complicate pe "
"sistemele pe 32 de biți: dacă procesul A citește I</proc/>pidI</io> al "
"procesului B în timp ce procesul B actualizează unul dintre aceste contoare "
"pe 64 de biți, procesul A ar putea vedea un rezultat intermediar."

#. type: Plain text
#: mageia-cauldron
msgid ""
"Permission to access this file is governed by a ptrace access mode "
"B<PTRACE_MODE_READ_FSCREDS> check; see B<ptrace>(2)."
msgstr ""
"Permisiunea de a accesa acest fișier este guvernată de o verificare a "
"modului de acces «ptrace» B<PTRACE_MODE_READ_FSCREDS>; a se vedea "
"B<ptrace>(2)."

#. type: Plain text
#: mageia-cauldron
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
