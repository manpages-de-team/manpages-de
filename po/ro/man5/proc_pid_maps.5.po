# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.24.0\n"
"POT-Creation-Date: 2025-02-28 16:44+0100\n"
"PO-Revision-Date: 2024-11-21 17:02+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 24.08.3\n"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_maps"
msgstr "proc_pid_maps"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-06-15"
msgstr "15 iunie 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pagini de manual de Linux 6.12"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/maps - mapped memory regions"
msgstr "/proc/pid/maps - regiuni de memorie cartografiate"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</maps>"
msgstr "I</proc/>pidI</maps>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A file containing the currently mapped memory regions and their access "
"permissions.  See B<mmap>(2)  for some further information about memory "
"mappings."
msgstr ""
"Un fișier care conține regiunile de memorie cartografiate în prezent și "
"permisiunile lor de acces. Consultați B<mmap>(2) pentru informații "
"suplimentare despre cartografierea memoriei."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Permission to access this file is governed by a ptrace access mode "
"B<PTRACE_MODE_READ_FSCREDS> check; see B<ptrace>(2)."
msgstr ""
"Permisiunea de a accesa acest fișier este guvernată de o verificare a "
"modului de acces «ptrace» B<PTRACE_MODE_READ_FSCREDS>; a se vedea "
"B<ptrace>(2)."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The format of the file is:"
msgstr "Formatul fișierului este următorul:"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"I<address           perms offset  dev   inode       pathname>\n"
"00400000-00452000 r-xp 00000000 08:02 173521      /usr/bin/dbus-daemon\n"
"00651000-00652000 r--p 00051000 08:02 173521      /usr/bin/dbus-daemon\n"
"00652000-00655000 rw-p 00052000 08:02 173521      /usr/bin/dbus-daemon\n"
"00e03000-00e24000 rw-p 00000000 00:00 0           [heap]\n"
"00e24000-011f7000 rw-p 00000000 00:00 0           [heap]\n"
"\\&...\n"
"35b1800000-35b1820000 r-xp 00000000 08:02 135522  /usr/lib64/ld-2.15.so\n"
"35b1a1f000-35b1a20000 r--p 0001f000 08:02 135522  /usr/lib64/ld-2.15.so\n"
"35b1a20000-35b1a21000 rw-p 00020000 08:02 135522  /usr/lib64/ld-2.15.so\n"
"35b1a21000-35b1a22000 rw-p 00000000 00:00 0\n"
"35b1c00000-35b1dac000 r-xp 00000000 08:02 135870  /usr/lib64/libc-2.15.so\n"
"35b1dac000-35b1fac000 ---p 001ac000 08:02 135870  /usr/lib64/libc-2.15.so\n"
"35b1fac000-35b1fb0000 r--p 001ac000 08:02 135870  /usr/lib64/libc-2.15.so\n"
"35b1fb0000-35b1fb2000 rw-p 001b0000 08:02 135870  /usr/lib64/libc-2.15.so\n"
"\\&...\n"
"f2c6ff8c000-7f2c7078c000 rw-p 00000000 00:00 0    [stack:986]\n"
"\\&...\n"
"7fffb2c0d000-7fffb2c2e000 rw-p 00000000 00:00 0   [stack]\n"
"7fffb2d48000-7fffb2d49000 r-xp 00000000 00:00 0   [vdso]\n"
msgstr ""
"I<adresă           permis poziție disp  nod-i       nume-rută>\n"
"00400000-00452000 r-xp 00000000 08:02 173521      /usr/bin/dbus-daemon\n"
"00651000-00652000 r--p 00051000 08:02 173521      /usr/bin/dbus-daemon\n"
"00652000-00655000 rw-p 00052000 08:02 173521      /usr/bin/dbus-daemon\n"
"00e03000-00e24000 rw-p 00000000 00:00 0           [heap]\n"
"00e24000-011f7000 rw-p 00000000 00:00 0           [heap]\n"
"\\&...\n"
"35b1800000-35b1820000 r-xp 00000000 08:02 135522  /usr/lib64/ld-2.15.so\n"
"35b1a1f000-35b1a20000 r--p 0001f000 08:02 135522  /usr/lib64/ld-2.15.so\n"
"35b1a20000-35b1a21000 rw-p 00020000 08:02 135522  /usr/lib64/ld-2.15.so\n"
"35b1a21000-35b1a22000 rw-p 00000000 00:00 0\n"
"35b1c00000-35b1dac000 r-xp 00000000 08:02 135870  /usr/lib64/libc-2.15.so\n"
"35b1dac000-35b1fac000 ---p 001ac000 08:02 135870  /usr/lib64/libc-2.15.so\n"
"35b1fac000-35b1fb0000 r--p 001ac000 08:02 135870  /usr/lib64/libc-2.15.so\n"
"35b1fb0000-35b1fb2000 rw-p 001b0000 08:02 135870  /usr/lib64/libc-2.15.so\n"
"\\&...\n"
"f2c6ff8c000-7f2c7078c000 rw-p 00000000 00:00 0    [stack:986]\n"
"\\&...\n"
"7fffb2c0d000-7fffb2c2e000 rw-p 00000000 00:00 0   [stack]\n"
"7fffb2d48000-7fffb2d49000 r-xp 00000000 00:00 0   [vdso]\n"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<address> field is the address space in the process that the mapping "
"occupies.  The I<perms> field is a set of permissions:"
msgstr ""
"Câmpul I<adresă> este spațiul de adrese din proces ocupat de cartografiere. "
"Câmpul I<permis> este un set de permisiuni:"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"r = read\n"
"w = write\n"
"x = execute\n"
"s = shared\n"
"p = private (copy on write)\n"
msgstr ""
"r = citire\n"
"w = scriere\n"
"x = execuție\n"
"s = partajare\n"
"p = privată (copie la scriere)\n"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<offset> field is the offset into the file/whatever; I<dev> is the "
"device (major:minor); I<inode> is the inode on that device.  0 indicates "
"that no inode is associated with the memory region, as would be the case "
"with BSS (uninitialized data)."
msgstr ""
"Câmpul I<poziție> este poziția în fișier sau altceva; I<disp> este "
"dispozitivul (major:minor); I<nod-i> este nodul-i pe dispozitivul respectiv. "
"0 indică faptul că niciun nod-i nu este asociat cu regiunea de memorie, cum "
"ar fi cazul BSS (date neinițializate)."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<pathname> field will usually be the file that is backing the mapping.  "
"For ELF files, you can easily coordinate with the I<offset> field by looking "
"at the Offset field in the ELF program headers (I<readelf\\ -l>)."
msgstr ""
"Câmpul I<nume-rută> va fi de obicei fișierul care conține cartografierea. "
"Pentru fișierele ELF, vă puteți coordona cu ușurință cu câmpul I<poziție> "
"uitându-vă la câmpul Offset din anteturile programului ELF (I<readelf -l>)."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "There are additional helpful pseudo-paths:"
msgstr "Există pseudo-rute suplimentare utile:"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<[stack]>"
msgstr "I<[stack]>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The initial process's (also known as the main thread's) stack."
msgstr "Stiva procesului inițial (cunoscută și ca firul principal)."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<[stack:>tidI<]> (from Linux 3.4 to Linux 4.4)"
msgstr "I<[stack:>tidI<]> (de la Linux 3.4 la Linux 4.4)"

#.  commit b76437579d1344b612cf1851ae610c636cec7db0 (added)
#.  commit 65376df582174ffcec9e6471bf5b0dd79ba05e4a (removed)
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A thread's stack (where the I<tid> is a thread ID).  It corresponds to the "
"I</proc/>pidI</task/>tidI</> path.  This field was removed in Linux 4.5, "
"since providing this information for a process with large numbers of threads "
"is expensive."
msgstr ""
"Stiva unui fir (unde I<tid> este un ID al firului). Acesta corespunde rutei "
"I</proc/>pidI</task/>tidI</>. Acest câmp a fost eliminat în Linux 4.5, "
"deoarece furnizarea acestor informații pentru un proces cu un număr mare de "
"fire este costisitoare."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<[vdso]>"
msgstr "I<[vdso]>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The virtual dynamically linked shared object.  See B<vdso>(7)."
msgstr "Obiectul partajat virtual legat dinamic. A se vedea B<vdso>(7)."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<[heap]>"
msgstr "I<[heap]>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The process's heap."
msgstr "Grămada procesului."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<[anon:>nameI<]> (since Linux 5.17)"
msgstr "I<[anon:>numeI<]> (începând cu Linux 5.17)"

#.  Commit 9a10064f5625d5572c3626c1516e0bebc6c9fe9b
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A named private anonymous mapping.  Set with B<prctl>(2)  "
"B<PR_SET_VMA_ANON_NAME>."
msgstr ""
"O cartografiere anonimă privată cu nume.  Definită cu B<prctl>(2)  "
"B<PR_SET_VMA_ANON_NAME>."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<[anon_shmem:>nameI<]> (since Linux 6.2)"
msgstr "I<[anon_shmem:>numeI<]> (începând cu Linux 6.2)"

#.  Commit d09e8ca6cb93bb4b97517a18fbbf7eccb0e9ff43
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A named shared anonymous mapping.  Set with B<prctl>(2)  "
"B<PR_SET_VMA_ANON_NAME>."
msgstr ""
"O cartografiere anonimă partajată cu nume.  Definită cu B<prctl>(2)  "
"B<PR_SET_VMA_ANON_NAME>."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the I<pathname> field is blank, this is an anonymous mapping as obtained "
"via B<mmap>(2).  There is no easy way to coordinate this back to a process's "
"source, short of running it through B<gdb>(1), B<strace>(1), or similar."
msgstr ""
"Dacă câmpul I<nume-rută> este gol, aceasta este o cartografiere anonimă "
"obținută prin B<mmap>(2). Nu există o modalitate ușoară de a corela aceasta "
"cu sursa unui proces, cu excepția rulării acesteia prin B<gdb>(1), "
"B<strace>(1) sau similar."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"I<pathname> is shown unescaped except for newline characters, which are "
"replaced with an octal escape sequence.  As a result, it is not possible to "
"determine whether the original pathname contained a newline character or the "
"literal I<\\[rs]012> character sequence."
msgstr ""
"I<nume-rută> este afișat neeludat, cu excepția caracterelor de linie nouă, "
"care sunt înlocuite cu o secvență octală de eludare. Ca urmare, nu este "
"posibil să se determine dacă numele de rută original conținea un caracter de "
"linie nouă sau secvența literală de caractere I<\\[rs]012>."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the mapping is file-backed and the file has been deleted, the string \" "
"(deleted)\" is appended to the pathname.  Note that this is ambiguous too."
msgstr ""
"Dacă cartografierea este stocată în fișier și fișierul a fost șters, șirul "
"„(deleted)” este adăugat la numele rutei. Rețineți că acest lucru este, de "
"asemenea, ambiguu."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Under Linux 2.0, there is no field giving pathname."
msgstr "Sub Linux 2.0, nu există niciun câmp care să indice numele rutei."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pagini de manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-09-07"
msgstr "7 septembrie 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: Plain text
#: mageia-cauldron
msgid ""
"I<pathname> is shown unescaped except for newline characters, which are "
"replaced with an octal escape sequence.  As a result, it is not possible to "
"determine whether the original pathname contained a newline character or the "
"literal I<\\e012> character sequence."
msgstr ""
"I<pathname> este afișat neeludat, cu excepția caracterelor de linie nouă, "
"care sunt înlocuite cu o secvență de eludare octală. Ca urmare, nu este "
"posibil să se determine dacă numele de rută original conținea un caracter de "
"linie nouă sau secvența literală de caractere I<\\e012>."

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pagini de manual Linux (nepublicate)"
