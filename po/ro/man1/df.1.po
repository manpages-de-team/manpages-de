# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Laurențiu Buzdugan <lbuz@rolix.org>, 2004.
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.16\n"
"POT-Creation-Date: 2025-02-28 16:31+0100\n"
"PO-Revision-Date: 2025-02-01 10:13+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DF"
msgstr "DF"

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "February 2025"
msgstr "februarie 2025"

#. type: TH
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.6"
msgstr "GNU coreutils 9.6"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "df - report file system space usage"
msgstr "df - raportează utilizarea spațiului de către sistemul de fișiere"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<df> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<df> [I<\\,OPȚIUNE\\/>]... [I<\\,FIȘIER\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manual page documents the GNU version of B<df>.  B<df> displays the "
"amount of space available on the file system containing each file name "
"argument.  If no file name is given, the space available on all currently "
"mounted file systems is shown.  Space is shown in 1K blocks by default, "
"unless the environment variable POSIXLY_CORRECT is set, in which case 512-"
"byte blocks are used."
msgstr ""
"Această pagină de manual documentează versiunea GNU a B<df>.  B<df> afișează "
"cantitatea de spațiu disponibil pe sistemul de fișiere care conține fiecare "
"nume de fișier dat ca argument.  Dacă nu se indică niciun nume de fișier, se "
"afișează spațiul disponibil pe toate sistemele de fișiere montate în "
"prezent.  În mod implicit, spațiul este afișat în blocuri de 1K, cu excepția "
"cazului în care este definită variabila de mediu „POSIXLY_CORRECT”, caz în "
"care se utilizează blocuri de 512 octeți."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If an argument is the absolute file name of a device node containing a "
"mounted file system, B<df> shows the space available on that file system "
"rather than on the file system containing the device node.  This version of "
"B<df> cannot show the space available on unmounted file systems, because on "
"most kinds of systems doing so requires non-portable intimate knowledge of "
"file system structures."
msgstr ""
"Dacă un argument este numele absolut de fișier al unui nod de dispozitiv "
"care conține un sistem de fișiere montat, B<df> afișează spațiul disponibil "
"pe acel sistem de fișiere, mai degrabă decât pe sistemul de fișiere care "
"conține nodul de dispozitiv. Această versiune de B<df> nu poate afișa "
"spațiul disponibil pe sistemele de fișiere nemontate, deoarece pe "
"majoritatea tipurilor de sisteme, a face acest lucru necesită cunoștințe "
"intime foarte neportabile despre structurile sistemurilor de fișiere."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Show information about the file system on which each FILE resides, or all "
"file systems by default."
msgstr ""
"Arată informații despre sistemul de fișiere pe care rezidă fiecare FIȘIER "
"sau, implicit, informații despre toate sistemele de fișiere."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumentele obligatorii pentru opțiunile lungi sunt de asemenea obligatorii "
"pentru opțiunile scurte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "include pseudo, duplicate, inaccessible file systems"
msgstr ""
"include pseudo sisteme de fișiere și sisteme de fișiere duplicate sau "
"inaccesibile"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-B>, B<--block-size>=I<\\,SIZE\\/>"
msgstr "B<-B>, B<--block-size>=I<\\,DIMENS\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"scale sizes by SIZE before printing them; e.g., \\&'-BM' prints sizes in "
"units of 1,048,576 bytes; see SIZE format below"
msgstr ""
"scalează dimensiunile în funcție de DIM înainte de a le afișa; de exp: „-BM” "
"afișează dimensiunile în unități de 1.048.576 de octeți; vedeți mai jos "
"formatul de DIMENSIUNE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--human-readable>"
msgstr "B<-h>, B<--human-readable>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print sizes in powers of 1024 (e.g., 1023M)"
msgstr "afișează dimensiunile în puteri de 1024 (de exp.: 1023M)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>, B<--si>"
msgstr "B<-H>, B<--si>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print sizes in powers of 1000 (e.g., 1.1G)"
msgstr "afișează dimensiunile în puteri de 1000 (de exp.: 1.1G)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--inodes>"
msgstr "B<-i>, B<--inodes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "list inode information instead of block usage"
msgstr ""
"listează informațiile despre utilizarea nodurilor-i, în loc de cea a "
"utilizării blocurilor"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "like B<--block-size>=I<\\,1K\\/>"
msgstr "precum B<--block-size>=I<\\,1K\\/>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--local>"
msgstr "B<-l>, B<--local>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "limit listing to local file systems"
msgstr "limitează listarea numai la sistemele de fișiere locale"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-sync>"
msgstr "B<--no-sync>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "do not invoke sync before getting usage info (default)"
msgstr ""
"nu invocă sync() înainte de a obține informațiile de utilizare (implicit)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--output>[=I<\\,FIELD_LIST\\/>]"
msgstr "B<--output>[=I<\\,LISTĂ_CÂMP\\/>]"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"use the output format defined by FIELD_LIST, or print all fields if "
"FIELD_LIST is omitted"
msgstr ""
"utilizează formatul de ieșire definit de LISTĂ_CÂMP, sau afișează toate "
"câmpurile dacă LISTĂ_CÂMP este omisă"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--portability>"
msgstr "B<-P>, B<--portability>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "use the POSIX output format"
msgstr "folosește formatul de ieșire POSIX"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--sync>"
msgstr "B<--sync>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "invoke sync before getting usage info"
msgstr "invocă sync() înainte de a obține informațiile de utilizare"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--total>"
msgstr "B<--total>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"elide all entries insignificant to available space, and produce a grand total"
msgstr ""
"elimină toate intrările nesemnificative pentru spațiul disponibil, și "
"prezintă un total general"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--type>=I<\\,TYPE\\/>"
msgstr "B<-t>, B<--type>=I<\\,TIP\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "limit listing to file systems of type TYPE"
msgstr "limitează listarea la sisteme de fișiere de tipul TIP"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--print-type>"
msgstr "B<-T>, B<--print-type>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print file system type"
msgstr "afișează tipul sistemului de fișiere"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--exclude-type>=I<\\,TYPE\\/>"
msgstr "B<-x>, B<--exclude-type>=I<\\,TIP\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "limit listing to file systems not of type TYPE"
msgstr "limitează listarea la sisteme de fișiere care nu sunt de tipul TIP"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "(ignored)"
msgstr "(ignorată)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afișează acest mesaj de ajutor și iese"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "afișează informațiile despre versiune și iese"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Display values are in units of the first available SIZE from B<--block-"
"size>, and the DF_BLOCK_SIZE, BLOCK_SIZE and BLOCKSIZE environment "
"variables.  Otherwise, units default to 1024 bytes (or 512 if "
"POSIXLY_CORRECT is set)."
msgstr ""
"Valorile afișate sunt în unități ale primei DIM disponibile din B<--block-"
"size>, și ale variabilelor de mediu DF_BLOCK_SIZE, BLOCK_SIZE și BLOCKSIZE. "
"În caz contrar, unitățile sunt implicit la 1024 de octeți (sau 512 dacă este "
"definită variabila POSIXLY_CORRECT)."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y,R,Q (powers of 1024) or KB,MB,... "
"(powers of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"Argumentul DIMENSIUNE este compus dintr-un număr întreg și o unitate "
"opțională (exemplu: 10K este 10*1024). Unitățile sunt K,M,G,T,P,E,Z,Y,R,Q "
"(puteri de 1024) sau KB,MB,... (puteri de 1000). Prefixele binare pot fi "
"folosite, de asemenea: KiB=K, MiB=M ș.a.m.d."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"FIELD_LIST is a comma-separated list of columns to be included.  Valid field "
"names are: 'source', 'fstype', 'itotal', 'iused', 'iavail', 'ipcent', "
"\\&'size', 'used', 'avail', 'pcent', 'file' and 'target' (see info page)."
msgstr ""
"LISTĂ_CÂMP este o listă de nume de coloane, separate prin virgulă, care "
"trebuie incluse. Numele de câmpuri valide sunt: „sursă”, „fstype”, „itotal”, "
"„iused”, „iavail”, „ipcent”, „size”, „used”, „avail”, „pcent”, „file” și "
"„target” (consultați pagina de informații cu «info df»)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Torbjorn Granlund, David MacKenzie, and Paul Eggert."
msgstr "Scris de Torbjorn Granlund, David MacKenzie și Paul Eggert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Ajutor online GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Raportați orice erori de traducere la: E<lt>https://translationproject.org/"
"team/ro.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2025 Free Software Foundation, Inc. Licența GPLv3+: "
"GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți. Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/dfE<gt>"
msgstr ""
"Documentația completă este disponibilă la E<lt>https://www.gnu.org/software/"
"coreutils/dfE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) df invocation\\(aq"
msgstr "sau local rulând comanda: «info \\(aq(coreutils) df invocation\\(aq»"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "septembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm
msgid ""
"If an argument is the absolute file name of a device node containing a "
"mounted file system, B<df> shows the space available on that file system "
"rather than on the file system containing the device node.  This version of "
"B<df> cannot show the space available on unmounted file systems, because on "
"most kinds of systems doing so requires very nonportable intimate knowledge "
"of file system structures."
msgstr ""
"Dacă un argument este numele absolut de fișier al unui nod de dispozitiv "
"care conține un sistem de fișiere montat, B<df> arată spațiul disponibil pe "
"acel sistem de fișiere, mai degrabă decât pe sistemul de fișiere care "
"conține nodul dispozitiv. Această versiune de B<df> nu poate afișa spațiul "
"disponibil pe sistemele de fișiere nemontate, deoarece pe majoritatea "
"tipurilor de sisteme, a face acest lucru necesită cunoștințe intime foarte "
"neportabile despre structurile sistemului de fișiere."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
msgid ""
"use the output format defined by FIELD_LIST, or print all fields if "
"FIELD_LIST is omitted."
msgstr ""
"utilizează formatul de ieșire definit de LISTĂ_CÂMP, sau afișează toate "
"câmpurile dacă LISTĂ_CÂMP este omisă."

#. type: Plain text
#: debian-bookworm
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers "
"of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"Argumentul DIMensiune este compus dintr-un număr întreg și o unitate "
"opțională (exemplu: 10K este 10*1024). Unitățile sunt K,M,G,T,P,E,Z,Y "
"(puteri de 1024) sau KB,MB,... (puteri de 1000). Prefixele binare pot fi "
"folosite, de asemenea: KiB=K, MiB=M ș.a.m.d."

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor © 2022 Free Software Foundation, Inc. Licența GPLv3+: GNU "
"GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "octombrie 2024"

#. type: TH
#: debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: Plain text
#: debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor © 2024 Free Software Foundation, Inc. Licența GPLv3+: GNU "
"GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-42 opensuse-tumbleweed
#, no-wrap
msgid "January 2025"
msgstr "ianuarie 2025"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--direct>"
msgstr "B<--direct>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid "show statistics for a file instead of mount point"
msgstr "afișează statisticile pentru un fișier în loc de punctul de montare"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2024"
msgstr "martie 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "ianuarie 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2023 Free Software Foundation, Inc. Licența GPLv3+: "
"GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."
