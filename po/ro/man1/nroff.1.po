# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-16 05:53+0100\n"
"PO-Revision-Date: 2024-06-30 23:10+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: SY
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\%nroff"
msgstr "\\%nroff"

#. type: TH
#: archlinux
#, no-wrap
msgid "28 August 2024"
msgstr "28 august 2024"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "groff 1.23.0"
msgstr "groff 1.23.0"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr "Nume"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%nroff - format documents with I<groff> for TTY (terminal) devices"
msgstr ""
"\\%nroff - formatează documentele cu I<groff> pentru dispozitive TTY "
"(terminale)"

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Synopsis"
msgstr "Rezumat"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"[B<-bcCEhikpRStUVz>] [B<-d\\ >I<ctext>] [B<-d\\ >I<string>B<=>I<text>] [B<-"
"K\\ >I<fallback-encoding>] [B<-m\\ >I<macro-package>] [B<-M\\ >I<macro-"
"directory>] [B<-n\\ >I<page-number>] [B<-o\\ >I<page-list>] [B<-P\\ "
">I<postprocessor-argument>] [B<-r\\ >I<cnumeric-expression>] [B<-r\\ "
">I<register>B<=>I<numeric-expression>] [B<-T\\ >I<output-device>] [B<-w\\ "
">I<warning-category>] [B<-W\\ >I<warning-category>] [I<file\\ >.\\|.\\|.]"
msgstr ""
"[B<-bcCEhikpRStUVz>] [B<-d\\ >I<ctext>] [B<-d\\ >I<șir>B<=>I<text>] [B<-K\\ "
">I<codificarea-de-rezervă>] [B<-m\\ >I<macro-pachet>] [B<-M\\>I<macro-"
"director>] [B<-n\\ >I<număr-pagină>] [B<-o\\ >I<listă-pagini>] [B<-P\\ "
">I<argument-postprocesare>] [B<-r\\ >I<expresie-cnumerică>] [B<-r\\ "
">I<registru>B<=>I<expresie-numerică>] [B<-T\\>I<dispozitiv-ieșire>] [B<-w\\ "
">I<categorie-avertisment>] [B<-W\\ >I<categorie-avertisment>] [I<fișier\\ >."
"\\|.\\|.]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--version>"
msgstr "B<--version>"

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Descriere"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<\\%nroff> formats documents written in the E<.MR groff 7> language for "
"typewriter-like devices such as terminal emulators."
msgstr ""
"I<\\%nroff> formatează documentele scrise în limbajul E<.MR groff 7> pentru "
"dispozitive de tip mașină de scris, cum ar fi emulatoarele de terminale."

#.  GNU
#.  AT&T
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU I<nroff> emulates the AT&T I<nroff> command using E<.MR groff 1 .>"
msgstr "GNU I<nroff> emulează comanda AT&T I<nroff> folosind E<.MR groff 1>."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<\\%nroff> generates output via E<.MR grotty 1 ,> I<groff>'s terminal "
"output driver, which needs to know the character encoding scheme used by the "
"device."
msgstr ""
"I<nroff> generează ieșiri prin E<.MR grotty 1>, dispozitivul de ieșire TTY "
"al lui I<groff>, care trebuie să cunoască schema de codificare a "
"caracterelor utilizată de terminal."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Consequently, acceptable arguments to the B<-T> option are B<ascii>, "
"B<latin1>, B<utf8>, and B<cp1047>; any others are ignored."
msgstr ""
"În consecință, argumentele acceptabile pentru opțiunea B<-T> sunt B<ascii>, "
"B<latin1>, B<utf8> și B<cp1047>; orice alte argumente sunt ignorate."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If neither the I<\\%GROFF_TYPESETTER> environment variable nor the B<-T> "
"command-line option (which overrides the environment variable)  specifies a "
"(valid) device, I<\\%nroff> consults the locale to select an appropriate "
"output device."
msgstr ""
"În cazul în care nici variabila de mediu I<\\%GROFF_TYPESETTER>, nici "
"opțiunea de linie de comandă B<-T> (care înlocuiește variabila de mediu) nu "
"specifică un dispozitiv (valid), I<\\%nroff> consultă «locale» pentru a "
"selecta un dispozitiv de ieșire adecvat."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"It first tries the E<.MR locale 1> program, then checks several locale-"
"related environment variables; see section \\[lq]Environment\\[rq] below."
msgstr ""
"Acesta încearcă mai întâi programul E<.MR locale 1>, apoi verifică mai multe "
"variabile de mediu legate de configurația regională; consultați secțiunea "
"„Mediu” de mai jos."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "If all of the foregoing fail, B<-Tascii> is implied."
msgstr ""
"În cazul în care toate cele de mai sus eșuează, B<-Tascii> este implicită."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<-b>, B<-c>, B<-C>, B<-d>, B<-E>, B<-i>, B<-m>, B<-M>, B<-n>, B<-o>, B<-"
"r>, B<-U>, B<-w>, B<-W>, and B<-z> options have the effects described in "
"E<.MR \\%troff 1 .>"
msgstr ""
"Opțiunile B<-b>, B<-c>, B<-C>, B<-d>, B<-E>, B<-i>, B<-m>, B<-M>, B<-n>, B<-"
"o>, B<-r>, B<-U>, B<-w>, B<-W>, B<-W> și B<-z> au efectele descrise în E<.MR "
"\\%troff 1>."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<-c> and B<-h> imply \\[lq]B<-P-c>\\[rq] and \\[lq]B<-P-h>\\[rq], "
"respectively; B<-c> is also interpreted directly by I<\\%troff>."
msgstr ""
"B<-c> și B<-h> implică „B<-P-c>” și, respectiv „B<-P-h>”; B<-c> este, de "
"asemenea, interpretat direct de I<\\%troff>."

#.  AT&T
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In addition, this implementation ignores the AT&T I<nroff> options B<-e>, B<-"
"q>, and B<-s> (which are not implemented in I<groff>)."
msgstr ""
"În plus, această implementare ignoră opțiunile AT&T I<nroff> B<-e>, B<-q> și "
"B<-s> (care nu sunt implementate în I<groff>)."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The options B<-k>, B<-K>, B<-p>, B<-P>, B<-R>, B<-t>, and B<-S> are "
"documented in E<.MR groff 1 .>"
msgstr ""
"Opțiunile B<-k>, B<-K>, B<-p>, B<-P>, B<-P>, B<-R>, B<-t> și B<-S> sunt "
"documentate în E<.MR groff 1>."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<-V> causes I<\\%nroff> to display the constructed I<groff> command on the "
"standard output stream, but does not execute it."
msgstr ""
"B<-V> face ca I<\\%nroff> să afișeze comanda I<groff> construită pe fluxul "
"de ieșire standard, dar nu o execută."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<-v> and B<--version> show version information about I<\\%nroff> and the "
"programs it runs, while B<--help> displays a usage message; all exit "
"afterward."
msgstr ""
"B<-v> și B<--version> afișează informații despre versiunea lui I<\\%nroff> "
"și a programelor pe care le rulează, în timp ce B<--help> afișează un mesaj "
"de utilizare; toate ies după aceea."

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Exit status"
msgstr "Starea de ieșire"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"I<\\%nroff> exits with error status\\ B<2> if there was a problem parsing "
"its arguments, with status\\ B<0> if any of the options B<-V>, B<-v>, B<--"
"version>, or B<--help> were specified, and with the status of I<groff> "
"otherwise."
msgstr ""
"I<\\%nroff> iese cu starea de eroare\\ B<2> dacă a existat o problemă la "
"analizarea argumentelor sale, cu starea\\ B<0> dacă a fost specificată "
"oricare dintre opțiunile B<-V>, B<-v>, B<--version> sau B<--help> și cu "
"starea I<groff> în caz contrar."

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Environment"
msgstr "Mediu"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Normally, the path separator in environment variables ending with I<PATH> is "
"the colon; this may vary depending on the operating system."
msgstr ""
"În mod normal, separatorul de rută în variabilele de mediu care se termină "
"cu I<PATH> este reprezentat de două puncte „:”; acest lucru poate varia în "
"funcție de sistemul de operare."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "For example, Windows uses a semicolon instead."
msgstr "De exemplu, Windows folosește în schimb un punct și virgulă „;”."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<GROFF_BIN_PATH>"
msgstr "I<GROFF_BIN_PATH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a colon-separated list of directories in which to search for the I<groff> "
"executable before searching in I<PATH>."
msgstr ""
"este o listă de directoare, separate prin două puncte, în care se caută "
"executabilul I<groff> înainte de a căuta în I<PATH>."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "If unset, I</usr/\\:\\%bin> is used."
msgstr "Dacă nu este definită, se utilizează I</usr/\\:\\%bin>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<GROFF_TYPESETTER>"
msgstr "I<GROFF_TYPESETTER>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "specifies the default output device for I<groff>."
msgstr "specifică dispozitivul de ieșire implicit pentru I<groff>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<LC_ALL>"
msgstr "I<LC_ALL>"

#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<LC_CTYPE>"
msgstr "I<LC_CTYPE>"

#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<LANG>"
msgstr "I<LANG>"

#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<LESSCHARSET>"
msgstr "I<LESSCHARSET>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"are pattern-matched in this order for contents matching standard character "
"encodings supported by I<groff> in the event no B<-T> option is given and "
"I<\\%GROFF_TYPESETTER> is unset, or the values specified are invalid."
msgstr ""
"sunt verificate în această ordine pentru conținuturi care corespund "
"codificărilor standard de caractere acceptate de I<groff> în cazul în care "
"nu este furnizată opțiunea B<-T> și I<\\%GROFF_TYPESETTER> nu este definită "
"sau valorile specificate nu sunt valide."

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Files"
msgstr "Fișiere"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%tmac/\\:\\%tty-char\\:.tmac>"
msgstr "I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%tmac/\\:\\%tty-char\\:.tmac>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "defines fallback definitions of I<roff> special characters."
msgstr "definește definițiile de rezervă ale caracterelor speciale I<roff>."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These definitions more poorly optically approximate typeset output than "
"those of I<tty.tmac> in favor of communicating semantic information."
msgstr ""
"Aceste definiții aproximează mai puțin optic ieșirile tipografice decât cele "
"din I<tty.tmac> în favoarea comunicării de informații semantice."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<nroff> loads it automatically."
msgstr "I<nroff> îl încarcă automat."

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Notes"
msgstr "Note"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Pager programs like E<.MR more 1> and E<.MR less 1> may require command-line "
"options to correctly handle some output sequences; see E<.MR grotty 1 .>"
msgstr ""
"Programele de paginare precum E<.MR more 1> și E<.MR less 1> pot necesita "
"opțiuni în linia de comandă pentru a gestiona corect anumite secvențe de "
"ieșire; consultați E<.MR grotty 1>."

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "See also"
msgstr "Consultați și"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"E<.MR groff 1 ,> E<.MR \\%troff 1 ,> E<.MR grotty 1 ,> E<.MR locale 1 ,> "
"E<.MR roff 7>"
msgstr ""
"E<.MR groff 1 ,> E<.MR \\%troff 1 ,> E<.MR grotty 1 ,> E<.MR locale 1 ,> "
"E<.MR roff 7>"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "NROFF"
msgstr "NROFF"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "7 March 2023"
msgstr "7 martie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "groff 1.22.4"
msgstr "groff 1.22.4"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm
msgid "nroff - use groff to format documents for TTY devices"
msgstr ""
"nroff - utilizează groff pentru a formata documente pentru dispozitive TTY"

#.  ====================================================================
#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: SY
#: debian-bookworm
#, no-wrap
msgid "nroff"
msgstr "nroff"

#. type: Plain text
#: debian-bookworm
msgid ""
"[B<-CchipStUv>] [B<-d>I<cs> ] [B<-M>I<dir> ] [B<-m>I<name> ] [B<-n>I<num> ] "
"[B<-o>I<list> ] [B<-r>I<cn> ] [B<-T>I<name> ] [B<-W>I<warning> ] [B<-"
"w>I<warning> ] [I<file> \\&.\\|.\\|.\\&]"
msgstr ""
"[B<-CchipStUv>] [B<-d>I<cs> ] [B<-M>I<director> ] [B<-m>I<nume> ] [B<-"
"n>I<număr> ] [B<-o>I<listă>] [B<-r>I<cn> ] [B<-T>I<nume> ] [B<-"
"W>I<avertisment> ] [B<-w>I<avertisment> ] [I<fișier> \\&.\\|.\\|.\\&]"

#.  ====================================================================
#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm
msgid ""
"I<nroff> formats documents written in the I<roff>(7)  language for "
"typewriter-like devices such as terminal emulators."
msgstr ""
"I<nroff> formatează documentele scrise în limbajul I<roff>(7) pentru "
"dispozitive de tip mașină de scris, cum ar fi emulatoarele de terminale."

#. type: Plain text
#: debian-bookworm
msgid ""
"GNU I<nroff> emulates the traditional Unix I<nroff> command using "
"I<groff>(1)."
msgstr ""
"GNU I<nroff> emulează comanda tradițională Unix I<nroff> folosind "
"I<groff>(1)."

#. type: Plain text
#: debian-bookworm
msgid ""
"I<nroff> generates output via I<grotty>(1), I<groff>'s TTY output device, "
"which needs to know the character encoding scheme used by the terminal."
msgstr ""
"I<nroff> generează ieșiri prin I<grotty>(1), dispozitivul de ieșire TTY al "
"lui I<groff>, care trebuie să cunoască schema de codificare a caracterelor "
"utilizată de terminal."

#. type: Plain text
#: debian-bookworm
msgid ""
"If neither the I<\\%GROFF_TYPESETTER> environment variable nor the B<-T> "
"command-line option (which overrides the environment variable)  specifies a "
"(valid) device, I<nroff> consults the locale to select an appropriate output "
"device."
msgstr ""
"În cazul în care nici variabila de mediu I<\\%GROFF_TYPESETTER>, nici "
"opțiunea de linie de comandă B<-T> (care înlocuiește variabila de mediu) nu "
"specifică un dispozitiv (valid), I<nroff> consultă «locale» pentru a selecta "
"un dispozitiv de ieșire adecvat."

#. type: Plain text
#: debian-bookworm
msgid ""
"It first tries the I<locale>(1)  program, then checks several locale-related "
"environment variables; see \\(lqENVIRONMENT\\(rq, below."
msgstr ""
"Acesta încearcă mai întâi programul I<locale>(1), apoi verifică mai multe "
"variabile de mediu legate de configurația regională; a se vedea secțiunea "
"„MEDIU”, de mai jos."

#. type: Plain text
#: debian-bookworm
msgid "Whitespace is not permitted between an option and its argument."
msgstr ""
"Spațiul alb este permis între o opțiune de linie de comandă și argumentul "
"acesteia."

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<-h> and B<-c> options are equivalent to I<grotty>'s options B<-h> "
"(using tabs in the output) and B<-c> (using the old output scheme instead of "
"SGR escape sequences)."
msgstr ""
"Opțiunile B<-h> și B<-c> sunt echivalente cu opțiunile B<-h> de la I<grotty> "
"( care utilizează tabulatoare în ieșire) și B<-c> (care utilizează vechea "
"schemă de ieșire în loc de secvențe de control SGR „Select Graphic "
"Rendition”)."

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<-d>, B<-C>, B<-i>, B<-M>, B<-m>, B<-n>, B<-o>, B<-r>, B<-w>, and B<-W> "
"options have the effect described in I<troff>(1)."
msgstr ""
"Opțiunile B<-d>, B<-C>, B<-i>, B<-M>, B<-m>, B<-m>, B<-n>, B<-o>, B<-r>, B<-"
"w> și B<-W> au efectul descris în I<troff>(1)."

#. type: Plain text
#: debian-bookworm
msgid ""
"In addition, I<nroff> ignores B<-e>, B<-q>, and B<-s> (which are not "
"implemented in I<troff>)."
msgstr ""
"În plus, I<nroff> ignoră B<-e>, B<-q> și B<-s> (care nu sunt implementate în "
"I<troff>)."

#. type: Plain text
#: debian-bookworm
msgid ""
"The options B<-p> (pic), B<-t> (tbl), B<-S> (safer), and B<-U> (unsafe) are "
"passed to I<groff>."
msgstr ""
"Opțiunile B<-p> (pic), B<-t> (tbl), B<-S> (safer) și B<-U> (unsafe) sunt "
"transmise către I<groff>."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<-v> and B<--version> show version information, while B<--help> displays a "
"usage message; all exit afterward."
msgstr ""
"B<-v> și B<--version> afișează informații despre versiune, în timp ce B<--"
"help> afișează un mesaj de utilizare; toate acestea ies după aceea."

#.  ====================================================================
#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MEDIU"

#. type: Plain text
#: debian-bookworm
msgid "If unset, I</usr/\\:bin> is used."
msgstr "Dacă nu este definită, se utilizează I</usr/\\:bin>."

#. type: Plain text
#: debian-bookworm
msgid ""
"are pattern-matched in this order for standard character encodings supported "
"by I<groff> in the event no B<-T> option is given and I<GROFF_TYPESETTER> is "
"unset."
msgstr ""
"se potrivesc în această ordine pentru codificările standard de caractere "
"acceptate de I<groff> în cazul în care nu este furnizată opțiunea B<-T> și "
"I<GROFF_TYPESETTER> nu este definită."

#.  ====================================================================
#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: debian-bookworm
msgid ""
"Character definitions in the file I</usr/\\:share/\\:groff/\\:1.22.4/\\:tmac/"
"\\:tty-char.tmac> are loaded to replace unrepresentable glyphs."
msgstr ""
"Definițiile caracterelor din fișierul I</usr/\\\\:share/\\:groff/"
"\\:1.22.4/\\:tmac/\\:tty-char.tmac> sunt încărcate pentru a înlocui glifele "
"nereprezentabile."

#.  ====================================================================
#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm
msgid "I<groff>(1), I<troff>(1), I<grotty>(1), I<locale>(1), I<roff>(7)"
msgstr "I<groff>(1), I<troff>(1), I<grotty>(1), I<locale>(1), I<roff>(7)"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "26 December 2024"
msgstr "26 decembrie 2024"

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "17 January 2025"
msgstr "17 ianuarie 2025"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2 July 2023"
msgstr "2 iulie 2021"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "13 September 2024"
msgstr "13 septembrie 2024"
