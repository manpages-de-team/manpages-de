# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-16 06:08+0100\n"
"PO-Revision-Date: 2024-02-13 17:30+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "THINKFAN"
msgstr "THINKFAN"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "December 2021"
msgstr "decembrie 2021"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "thinkfan 1.3.1"
msgstr "thinkfan 1.3.1"

#. type: SY
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "thinkfan"
msgstr "thinkfan"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "thinkfan - A simple fan control program"
msgstr "thinkfan -un program simplu de control al ventilatorului"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: OP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "-hnqDd"
msgstr "-hnqDd"

#. type: OP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "-b"
msgstr "-b"

#. type: OP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "BIAS"
msgstr "DEVIAȚIE (balans)"

#. type: OP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "-c"
msgstr "-c"

#. type: OP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "CONFIG"
msgstr "CONFIGURARE"

#. type: OP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "-s"
msgstr "-s"

#. type: OP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SECONDS"
msgstr "SECUNDE"

#. type: OP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "-p"
msgstr "-p"

#. type: OP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "[I<DELAY>]"
msgstr "[I<ÎNTÂRZIERE>]"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Thinkfan sets the fan speed according to temperature limits set in the "
"config file.  It can read temperatures from a number of sources:"
msgstr ""
"«thinkfan» reglează viteza ventilatorului în funcție de limitele de "
"temperatură stabilite în fișierul de configurare. Acesta poate citi "
"temperaturi din mai multe surse:"

#. type: IP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "/proc/acpi/ibm/thermal"
msgstr "/proc/acpi/ibm/thermal"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Which is provided by the thinkpad_acpi kernel module on older Thinkpads,"
msgstr ""
"Care este furnizat de modulul de nucleu „thinkpad_acpi” pe laptopurile "
"Thinkpad mai vechi,"

#. type: IP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "temp*_input files in sysfs"
msgstr "fișiere temp*_input în sysfs"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Which may be provided by any hwmon drivers, including thinkpad_acpi on "
"modern Thinkpads,"
msgstr ""
"Care poate fi furnizat de orice controlor „hwmon”, inclusiv „thinkpad_acpi” "
"pe laptopurile Thinkpad moderne,"

#. type: IP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Hard disks with S.M.A.R.T. support"
msgstr "Discuri dure cu suport S.M.A.R.T."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"With the help of libatasmart, if thinkfan was compiled with B<-"
"DUSE_ATASMART=ON>"
msgstr ""
"Cu ajutorul bibliotecii „libatasmart”, dacă «thinkfan» a fost compilat cu "
"opțiunea B<-DUSE_ATASMART=ON>"

#. type: IP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "From the proprietary nVidia driver"
msgstr "De la controlorul proprietar nVidia"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"When the proprietary nVidia driver is used, no hwmon for the card will be "
"available. In this situation, thinkfan can use the proprietary NVML API to "
"get temperatures."
msgstr ""
"Atunci când este utilizat controlorul proprietar nVidia, nu va fi disponibil "
"niciun controlor „hwmon” pentru această placă. În această situație, "
"«thinkfan« poate utiliza API-ul NVML proprietar pentru a obține "
"temperaturile."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"The fan can be /proc/acpi/ibm/fan or some PWM file in /sys/class/hwmon. See "
"B<thinkfan.conf>(5)  for a detailed explanation of the config syntax."
msgstr ""
"Ventilatorul poate fi „/proc/acpi/ibm/fan” sau un fișier PWM în „/sys/class/"
"hwmon”. Consultați B<thinkfan.conf>(5) pentru o explicație detaliată a "
"sintaxei de configurare."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"B<WARNING>: This program does only very basic sanity checking on the "
"configuration. That means that you can set your temperature limits as insane "
"as you like."
msgstr ""
"B<Atenție>: Acest program face doar o verificare de bază a corectitudinii "
"configurației. Aceasta înseamnă că puteți stabili limitele de temperatură "
"oricât de nebunești doriți."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "There are two general modes of operation:"
msgstr "Există două moduri generale de funcționare:"

#. type: SS
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "COMPLEX MODE"
msgstr "MODUL COMPLEX"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"In complex mode, temperature limits are defined for each sensor thinkfan "
"knows about. Setting suitable limits for each sensor in your system will "
"probably require a bit of experimentation and good knowledge about your "
"hardware, but it's the safest way of keeping each component within its "
"specified temperature range. See http://www.thinkwiki.org/wiki/"
"Thermal_Sensors for details on which sensor measures what temperature in a "
"Thinkpad. On other systems you'll have to find out on your own. See the "
"example configs to learn about the syntax."
msgstr ""
"În modul complex, limitele de temperatură sunt definite pentru fiecare "
"senzor pe care «thinkfan» îl cunoaște. Stabilirea unor limite adecvate "
"pentru fiecare senzor din sistemul dumneavoastră va necesita probabil un pic "
"de experimentare și o bună cunoaștere a echipamentului dumneavoastră, dar "
"este cel mai sigur mod de a menține fiecare componentă în intervalul de "
"temperatură specificat. Consultați pagina http://www.thinkwiki.org/wiki/"
"Thermal_Sensors pentru detalii despre ce senzor măsoară ce temperatură într-"
"un Thinkpad. La alte sisteme va trebui să aflați pe cont propriu. Consultați "
"exemplele de configurare pentru a afla mai multe despre sintaxă."

#. type: SS
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SIMPLE MODE"
msgstr "MODUL SIMPLU"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"In simple mode, Thinkfan uses only the highest temperature found in the "
"system. That may be dangerous, e.g. for hard disks.  That's why you should "
"provide a correction value (i.e. add 10-15 \\[char176]C) for the sensor that "
"has the temperature of your hard disk (or battery...). See the example "
"config files for details about that."
msgstr ""
"În modul simplu, «thinkfan» utilizează doar cea mai ridicată temperatură "
"găsită în sistem. Acest lucru poate fi periculos, de exemplu, pentru "
"discurile dure. De aceea, ar trebui să furnizați o valoare de corecție (de "
"exemplu, adăugați 10-15 \\[char176]C) pentru senzorul care are temperatura "
"discului dur (sau a bateriei...). Consultați fișierele de configurare de "
"exemplu pentru detalii în acest sens."

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "CONFIGURATION"
msgstr "CONFIGURARE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Some example configurations are provided with the source package. For a "
"detailed see the config man page B<thinkfan.conf>(5)."
msgstr ""
"Unele exemple de configurații sunt furnizate împreună cu pachetul sursă. "
"Pentru mai multe detalii, consultați pagina de manual de configurare "
"B<thinkfan.conf>(5)."

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Show a short help message"
msgstr "Afișează un scurt mesaj de ajutor."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-s>I< SECONDS>"
msgstr "B<-s>I< SECUNDE>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Maximum seconds between temperature updates (default: 5)"
msgstr ""
"Numărul maxim de secunde între actualizările de temperatură (implicit: 5)"

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-b>I< BIAS>"
msgstr "B<-b>I< DEVIAȚIE>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Floating point number (-10 to 30) to control rising temperature "
"exaggeration.  If the temperature increases by more than 2 \\[char176]C "
"during one cycle, this number is used to calculate a bias, which is added to "
"the current highest temperature seen in the system:"
msgstr ""
"Număr în virgulă mobilă (de la -10 la 30) pentru a controla creșterea "
"exagerată a temperaturii. Dacă temperatura crește cu mai mult de 2 \\"
"[char176]C în timpul unui ciclu, acest număr este utilizat pentru a calcula "
"o deviație, care se adaugă la cea mai ridicată temperatură actuală observată "
"în sistem:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid " current_tmax = current_tmax + delta_t * I<BIAS> / 10\n"
msgstr " current_tmax = current_tmax + delta_t * I<DEVIAȚIE> / 10\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"This means that negative numbers can be used to even out short and sudden "
"temperature spikes like those seen on some on-DIE sensors. Use DANGEROUS "
"mode to remove the -10 to +30 limit. Note that you can't have a space "
"between -b and a negative argument, because otherwise getopt will interpret "
"things like -10 as an option and fail (i.e. write B<-b-10> instead of B<-b "
"-10>)."
msgstr ""
"Acest lucru înseamnă că numerele negative pot fi utilizate pentru a "
"echilibra vârfurile de temperatură scurte și bruște, cum ar fi cele "
"observate la unii senzori on-DIE. Folosiți modul „DANGEROUS” (periculos) "
"pentru a elimina limita de la -10 la +30. Rețineți că nu puteți avea un "
"spațiu între -b și un argument negativ, pentru că altfel getopt() va "
"interpreta lucruri precum -10 ca o opțiune și va da greș (adică scrieți B<-"
"b-10> în loc de B<-b -10>)."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Default is 15.0"
msgstr "Valoarea implicită este 15.0"

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-c>I< FILE>"
msgstr "B<-c>I< FIȘIER>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Load a different configuration file.  By default, thinkfan first tries to "
"load /etc/thinkfan.yaml, and /etc/thinkfan.conf after that.  The former must "
"be in YAML format, while the latter can be either YAML or the old legacy "
"syntax."
msgstr ""
"Încarcă un alt fișier de configurare. În mod implicit, «thinkfan» încearcă "
"mai întâi să încarce „/etc/thinkfan.yaml”, iar apoi „/etc/thinkfan.conf”. "
"Primul trebuie să fie în format YAML, în timp ce al doilea poate fi fie "
"YAML, fie sintaxa veche anterioară."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"If this option is specified, thinkfan attempts to load the config only from "
"I<FILE>.  If its name ends in ``.yaml'', it must be in YAML format.  "
"Otherwise, it can be either YAML or legacy syntax.  See B<thinkfan.conf>(5)  "
"and B<thinkfan.conf.legacy>(5)  for details."
msgstr ""
"Dacă este specificată această opțiune, «thinkfan» încearcă să încarce "
"configurația numai din I<FIȘIER>. Dacă numele său se termină în „.yaml”, "
"trebuie să fie în format YAML. În caz contrar, poate fi fie în format YAML, "
"fie în sintaxa dinainte. Consultați B<thinkfan.conf>(5) și "
"B<thinkfan.conf.legacy>(5) pentru detalii."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-n>"
msgstr "B<-n>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Do not become a daemon and log to terminal instead of syslog"
msgstr "Nu devine un demon și jurnalizează în terminal în loc de syslog"

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-q>"
msgstr "B<-q>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Be quiet, i.e. reduce logging level from the default. Can be specified "
"multiple times until only errors are displayed/logged."
msgstr ""
"Îi indică lui «thinkfan» să opereze silențios, adică să reducă nivelul de "
"jurnalizare față de cel implicit. Poate fi specificată de mai multe ori până "
"când sunt afișate/înregistrate doar erorile."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Be more verbose. Can be specified multiple times until every message is "
"displayed/logged."
msgstr ""
"Îi indică lui «thinkfan» să emită informații detaliate despre operațiile "
"realizate. Poate fi specificată de mai multe ori până când fiecare mesaj "
"este afișat/înregistrat."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-p >[I<SECONDS>]"
msgstr "B<-p >[I<SECUNDE>]"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Use the pulsing-fan workaround (for older Thinkpads). Takes an optional "
"floating-point argument (0-10s) as depulsing duration. Default 0.5s."
msgstr ""
"Folosește soluția „pulsing-fan workaround” pentru rezolvarea problemelor "
"ventilatorului de pulsații (pentru laptopurile Thinkpad mai vechi). Acceptă "
"un argument opțional în virgulă mobilă (0-10s) ca durată a aplicării. "
"Valoarea implicită este 0,5 s."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Do not read temperature from sleeping disks. Instead, 0 \\[char176]C is used "
"as that disk's temperature. This is needed if reading the temperature causes "
"your disk to wake up unnecessarily.  NOTE: This option is only available if "
"thinkfan was built with -D USE_ATASMART."
msgstr ""
"Nu citește temperatura de la discurile care dorm. În schimb, se utilizează 0 "
"\\[char176]C ca temperatură a discului respectiv. Acest lucru este necesar "
"în cazul în care citirea temperaturii face ca discul să se trezească inutil. "
"NOTĂ: Această opțiune este disponibilă numai dacă «thinkfan» a fost "
"construit cu opțiunea „-D USE_ATASMART”."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-D>"
msgstr "B<-D>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "DANGEROUS mode: Disable all sanity checks. May damage your hardware!!"
msgstr ""
"Modul PERICULOS „DANGEROUS”: Dezactivează toate verificările de "
"corectitudine a valorilor alese. Vă poate deteriora echipamentul!!!"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SIGNALS"
msgstr "SEMNALE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"SIGINT and SIGTERM simply interrupt operation and should cause thinkfan to "
"terminate cleanly."
msgstr ""
"SIGINT și SIGTERM întrerup pur și simplu operațiile în curs și ar trebui să "
"facă ca «thinkfan» să se încheie în mod curat."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"SIGHUP makes thinkfan reload its config. If there's any problem with the new "
"config, we keep the old one."
msgstr ""
"SIGHUP face ca «thinkfan» să-și reîncarce configurația. Dacă există vreo "
"problemă cu noua configurație, o păstrează pe cea veche."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"SIGUSR1 causes thinkfan to dump all currently known temperatures either to "
"syslog, or to the console (if running with the -n option)."
msgstr ""
"SIGUSR1 determină «thinkfan» să descarce toate temperaturile cunoscute în "
"prezent fie în «syslog», fie în consolă (dacă rulează cu opțiunea „-n”)."

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Normal exit"
msgstr "Ieșire normală"

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Runtime error"
msgstr "Eroare în timpul execuției"

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<2>"
msgstr "B<2>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Unexpected runtime error"
msgstr "Eroare neașteptată în timpul execuției"

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<3>"
msgstr "B<3>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Invalid commandline option"
msgstr "Opțiune în linia de comandă nevalidă"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid ""
"The thinkfan config manpage:\n"
"B<thinkfan.conf>(5)\n"
msgstr ""
"Pagina de manual de configurare a thinkfan:\n"
"B<thinkfan.conf>(5)\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Example configs shipped with the source distribution, also available at:\n"
msgstr "Exemple de configurații livrate împreună cu distribuția sursă, disponibile și la:\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "https://github.com/vmatare/thinkfan/tree/master/examples\n"
msgstr "https://github.com/vmatare/thinkfan/tree/master/examples\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid ""
"The Linux hwmon user interface documentation:\n"
"https://www.kernel.org/doc/html/latest/hwmon/sysfs-interface.html\n"
msgstr ""
"Documentația privind interfața de utilizator hwmon pentru Linux:\n"
"https://www.kernel.org/doc/html/latest/hwmon/sysfs-interface.html\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid ""
"The thinkpad_acpi interface documentation:\n"
"https://www.kernel.org/doc/html/latest/admin-guide/laptops/thinkpad-acpi.html\n"
msgstr ""
"Documentația privind interfața thinkpad_acpi:\n"
"https://www.kernel.org/doc/html/latest/admin-guide/laptops/thinkpad-acpi.html\n"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"If thinkfan tells you to, or if you feel like it, report issues at the "
"Github issue tracker:"
msgstr ""
"Dacă thinkfan vă spune să o faceți, sau dacă aveți chef, raportați "
"problemele la sistemul de urmărire a problemelor din Github:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "https://github.com/vmatare/thinkfan/issues"
msgstr "https://github.com/vmatare/thinkfan/issues"
