# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2025-02-28 16:36+0100\n"
"PO-Revision-Date: 2024-03-31 13:10+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "PSUTILS"
msgstr "PSUTILS"

#. type: TH
#: archlinux
#, no-wrap
msgid "2025-01-09"
msgstr "9 ianuarie 2025"

#. type: TH
#: archlinux
#, no-wrap
msgid "psutils 3.3.9"
msgstr "psutils 3.3.9"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-unstable
msgid "psutils"
msgstr "psutils"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-unstable
msgid "B<psutils> [OPTION...] [INFILE [OUTFILE]]"
msgstr "B<psutils> [OPȚIUNE...] [FIȘIER_INTRARE [FIȘIER_IEȘIRE]]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<includeres> includes resources (fonts, procsets, patterns, files, etc.) in "
"place of B<%%IncludeResource> comments in a PostScript document.  The "
"resources are searched for under the resource name, and with an appropriate "
"extension.  The pipeline"
msgstr ""
"B<includeres> include resurse (fonturi, seturi de procese, modele, fișiere "
"etc.) în locul comentariilor B<%%IncludeResource> într-un document "
"PostScript.  Resursele sunt căutate după numele resursei și cu o extensie "
"corespunzătoare.  Conducta:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "extractres file.ps | includeres E<gt>out.ps"
msgstr "extractres fișier.ps | includeres E<gt>ieșire.ps"

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"will move all resources appearing in a document to the document prologue, "
"removing redundant copies.  The output file can then be put through page re-"
"arrangement filters such as B<psnup> or B<pstops> safely.  Include resources "
"in a PostScript document."
msgstr ""
"va muta toate resursele care apar într-un document în prologul documentului, "
"eliminând copiile redundante. Fișierul de ieșire poate fi apoi trecut prin "
"filtre de rearanjare a paginilor, cum ar fi B<psnup> sau B<pstops>, în "
"condiții de siguranță. Include resurse într-un document PostScript."

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "B<INFILE>"
msgstr "B<INFILE>"

#. type: Plain text
#: archlinux debian-unstable
msgid "`-' or no INFILE argument means standard input"
msgstr "„-”sau fără argumentul FIȘIER_INTRARE înseamnă intrarea standard"

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "B<OUTFILE>"
msgstr "B<OUTFILE>"

#. type: Plain text
#: archlinux debian-unstable
msgid "`-' or no OUTFILE argument means standard output"
msgstr "„-”sau fără argumentul FIȘIER_IEȘIRE înseamnă ieșirea standard"

#. type: SH
#: archlinux debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: archlinux debian-unstable
msgid "show program's version number and exit"
msgstr "afișează numărul versiunii programului și iese"

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: archlinux debian-unstable
msgid "don't show progress"
msgstr "nu afișează progresul"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<includeres> does not alter the B<%%DocumentNeededResources> comments."
msgstr ""
"B<includeres> nu modifică comentariile din B<%%%DocumentNeedededResources>."

#. type: TH
#: debian-bookworm fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "INCLUDERES"
msgstr "INCLUDERES"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr "PSUtils Release 1 Patchlevel 17"

#. type: Plain text
#: debian-bookworm
msgid "includeres - filter to include resources in a PostScript document"
msgstr ""
"includeres - filtru pentru a include resurse într-un document PostScript"

#. type: Plain text
#: debian-bookworm
msgid "B<includeres> E<lt> I<document.ps> E<gt> I<output.ps>"
msgstr "B<includeres> E<lt> I<document.ps> E<gt> I<ieșire.ps>"

#. type: Plain text
#: debian-bookworm
msgid ""
"I<Includeres> includes resources (fonts, procsets, patterns, files, etc) in "
"place of I<%%IncludeResource> comments in a PostScript document. The "
"resources are searched for in the current directory and the system default "
"directory under the resource name, and with an appropriate extension.  The "
"pipeline"
msgstr ""
"I<includeres> include resurse (fonturi, seturi de procese, modele, fișiere "
"etc.) în locul comentariilor I<%%%IncludeResource> într-un document "
"PostScript. Resursele sunt căutate în directorul curent și în directorul "
"implicit al sistemului pe baza numelui resursei și cu o extensie "
"corespunzătoare.  Conducta:"

#. type: Plain text
#: debian-bookworm
msgid ""
"will move all resources appearing in a document to the document prologue, "
"removing redundant copies. The output file can then be put through page re-"
"arrangement filters such as I<psnup> or I<pstops> safely."
msgstr ""
"va muta toate resursele care apar într-un document în prologul documentului, "
"eliminând copiile redundante. Fișierul de ieșire poate fi apoi trecut prin "
"filtre de rearanjare a paginilor, cum ar fi I<psnup> sau I<pstops>, în "
"condiții de siguranță."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Plain text
#: debian-bookworm
msgid "/usr/lib/psutils - system resource directory."
msgstr "/usr/lib/psutils - directorul de resurse al sistemului."

#. type: Plain text
#: debian-bookworm
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr "Drepturi de autor © Angus J. C. Duggan 1991-1995"

#. type: SH
#: debian-bookworm fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"

#. type: SH
#: debian-bookworm fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "TRADEMARKS"
msgstr "MĂRCI ÎNREGISTRATE"

#. type: Plain text
#: debian-bookworm fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr "B<PostScript> este o marcă înregistrată a Adobe Systems Incorporated."

#. type: Plain text
#: debian-bookworm
msgid "I<includeres> does not alter the I<%%DocumentNeededResources> comments."
msgstr ""
"I<includeres> nu modifică comentariile din I<%%DocumentNeedededResources>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2025-02-15"
msgstr "15 februarie 2025"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "psutils 3.3.8"
msgstr "psutils 3.3.8"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "February 2023"
msgstr "februarie 2023"

#. type: TH
#: fedora-42 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "includeres 2.10"
msgstr "includeres 2.10"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "includeres - include resources in a PostScript document"
msgstr "includeres - include resurse într-un document PostScript"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<includeres> [I<\\,OPTION\\/>...] [I<\\,INFILE \\/>[I<\\,OUTFILE\\/>]]"
msgstr ""
"B<includeres> [I<\\,OPȚIUNE\\/>...] [I<\\,FIȘIER-INTRARE \\/>[I<\\,FIȘIER-"
"IEȘIRE\\/>]]"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Include resources in a PostScript document."
msgstr "Include resurse într-un document PostScript."

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afișează acest mesaj de ajutor și iese"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "display version information and exit"
msgstr "afișează informațiile despre versiune și iese"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"will move all resources appearing in a document to the document prologue, "
"removing redundant copies.  The output file can then be put through page re-"
"arrangement filters such as B<psnup> or B<pstops> safely."
msgstr ""
"va muta toate resursele care apar într-un document în prologul documentului, "
"eliminând copiile redundante. Fișierul de ieșire poate fi apoi trecut prin "
"filtre de rearanjare a paginilor, cum ar fi B<psnup> sau B<pstops>, în "
"condiții de siguranță."

#. type: SS
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr "Starea de ieșire:"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "if OK,"
msgstr "dacă totul este OK,"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "1"
msgstr "1"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"if arguments or options are incorrect, or there is some other problem "
"starting up,"
msgstr ""
"dacă argumentele sau opțiunile sunt incorecte sau dacă există o altă "
"problemă la pornire,"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2"
msgstr "2"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"if there is some problem during processing, typically an error reading or "
"writing an input or output file."
msgstr ""
"dacă există o problemă în timpul procesării, de obicei o eroare de citire "
"sau de scriere a unui fișier de intrare sau de ieșire."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Written by Angus J. C. Duggan and Reuben Thomas."
msgstr "Scris de Angus J. C. Duggan și Reuben Thomas."

#. type: SH
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron
msgid "Copyright \\(co Reuben Thomas 2012-2022."
msgstr "Drepturi de autor © Reuben Thomas 2012-2022."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Copyright \\(co Angus J. C. Duggan 1991-1997."
msgstr "Drepturi de autor © Angus J. C. Duggan 1991-1997."

#. type: Plain text
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "B<psutils>(1), B<paper>(1)"
msgstr "B<psutils>(1), B<paper>(1)"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "December 2021"
msgstr "decembrie 2021"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "includeres 2.08"
msgstr "includeres 2.08"

#. type: TP
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Copyright \\(co Reuben Thomas 2012-2019."
msgstr "Drepturi de autor © Reuben Thomas 2012-2019."
