# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2025-02-16 05:58+0100\n"
"PO-Revision-Date: 2023-07-12 10:11+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "RECINS"
msgstr "RECINS"

#. type: TH
#: debian-bookworm fedora-42 fedora-rawhide
#, no-wrap
msgid "April 2022"
msgstr "aprilie 2022"

#. type: TH
#: debian-bookworm fedora-42 fedora-rawhide
#, no-wrap
msgid "recins 1.9"
msgstr "recins 1.9"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "recins - insert records in a recfile"
msgstr "recins - inserează înregistrări într-un fișier „rec”"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"B<recins> [I<\\,OPTION\\/>]... [I<\\,-t TYPE\\/>] [I<\\,-n NUM | -e "
"RECORD_EXPR | -q STR | -m NUM\\/>] [I<\\,(-f NAME -v STR) | -r RECDATA\\/"
">]... [I<\\,FILE\\/>]"
msgstr ""
"B<recins> [I<\\,OPȚIUNE\\/>]... [I<\\,-t TIP\\/>] [I<\\,-n NUM | -e "
"EXPR_ÎNREGISTRARE | -q ȘIR | -m NUM\\/>] [I<\\,(-f NUME -v ȘIR) | -r "
"DATA_ÎNREGISTRĂRII\\/>]... [I<\\,FIȘIER\\/>]"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Insert new records in a rec database."
msgstr "Inserează noi înregistrări într-o bază de date „rec”."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-f>, B<--field>=I<\\,STR\\/>"
msgstr "B<-f>, B<--field>=I<\\,ȘIR\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "field name; should be followed by a B<-v>."
msgstr "numele câmpului; trebuie să fie urmat de opțiunea B<-v>."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-v>, B<--value>=I<\\,STR\\/>"
msgstr "B<-v>, B<--value>=I<\\,ȘIR\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "field value; should be preceded by an B<-f>."
msgstr "valoarea câmpului; trebuie să fie precedată de opțiunea B<-f>."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-r>, B<--record>=I<\\,STR\\/>"
msgstr "B<-r>, B<--record>=I<\\,ȘIR\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "record that will be inserted in the file."
msgstr "înregistrarea care va fi inserată în fișier."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--force>"
msgstr "B<--force>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "insert the record even if it is violating record restrictions."
msgstr ""
"inserează înregistrarea chiar dacă aceasta încalcă restricțiile de "
"înregistrare."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--no-external>"
msgstr "B<--no-external>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "don't use external descriptors."
msgstr "nu utilizează descriptori externi."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--no-auto>"
msgstr "B<--no-auto>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "don't insert auto generated fields."
msgstr "nu inserează câmpuri generate automat."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--verbose>"
msgstr "B<--verbose>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "give a detailed report if the integrity check fails."
msgstr ""
"oferă un raport detaliat în cazul în care verificarea integrității eșuează."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-s>, B<--password>=I<\\,STR\\/>"
msgstr "B<-s>, B<--password>=I<\\,ȘIR\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "encrypt confidential fields with the given password."
msgstr "criptează câmpurile confidențiale cu parola dată."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "print a help message and exit."
msgstr "imprimă un mesaj de ajutor și iese."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "show version and exit."
msgstr "afișează versiunea și iese."

#. type: SS
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Record selection options:"
msgstr "Opțiuni de selectare a înregistrărilor:"

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-i>, B<--case-insensitive>"
msgstr "B<-i>, B<--case-insensitive>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "make strings case-insensitive in selection expressions."
msgstr ""
"face ca șirurile de caractere să nu țină cont de majuscule și minuscule în "
"expresiile de selecție."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-t>, B<--type>=I<\\,TYPE\\/>"
msgstr "B<-t>, B<--type>=I<\\,TIP\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "operate on records of the specified type only."
msgstr "operează numai asupra înregistrărilor de tipul specificat."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-e>, B<--expression>=I<\\,RECORD_EXPR\\/>"
msgstr "B<-e>, B<--expression>=I<\\,EXPR_ÎNREGISTRARE\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "selection expression."
msgstr "expresia de selecție."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-q>, B<--quick>=I<\\,STR\\/>"
msgstr "B<-q>, B<--quick>=I<\\,ȘIR\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "select records with fields containing a string."
msgstr "selectează înregistrările cu câmpuri care conțin un șir de caractere."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-n>, B<--number>=I<\\,NUM\\/>,..."
msgstr "B<-n>, B<--number>=I<\\,NUMĂR\\/>,..."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "select specific records by position, with ranges."
msgstr "selectează înregistrări specifice în funcție de poziție, cu intervale."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-m>, B<--random>=I<\\,NUM\\/>"
msgstr "B<-m>, B<--random>=I<\\,NUMĂR\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "select a given number of random records."
msgstr "selectează un număr dat de înregistrări aleatorii."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"If no FILE is specified then the command acts like a filter, getting the "
"data from standard input and writing the result to standard output."
msgstr ""
"Dacă nu se specifică FIȘIER, comanda acționează ca un filtru, obținând "
"datele de la intrarea standard și scriind rezultatul la ieșirea standard."

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Written by Jose E. Marchesi."
msgstr "Scris de Jose E. Marchesi."

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Report bugs to: bug-recutils@gnu.org"
msgstr "Raportați erorile la: E<.MT bug-recutils@gnu.org>E<.ME>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"GNU recutils home page: E<lt>https://www.gnu.org/software/recutils/E<gt>"
msgstr ""
"Pagina principală a GNU recutils: E<lt>https://www.gnu.org/software/recutils/"
"E<gt>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Ajutor general pentru utilizarea software-ului GNU: E<lt>http://www.gnu.org/"
"gethelp/E<gt>"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Copyright \\(co 2010-2020 Jose E. Marchesi.  License GPLv3+: GNU GPL version "
"3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2010-2020 Jose E. Marchesi. Licența GPLv3+: GNU GPL "
"versiunea 3 sau ulterioară E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți. Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"The full documentation for B<recins> is maintained as a Texinfo manual.  If "
"the B<info> and B<recins> programs are properly installed at your site, the "
"command"
msgstr ""
"Documentația completă pentru B<recins> este menținută ca un manual Texinfo.  "
"Dacă programele B<info> și B<recins> sunt instalate corect în sistemul dvs., "
"comanda"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "B<info recutils>"
msgstr "B<info recutils>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "March 2024"
msgstr "martie 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GNU recutils 1.9"
msgstr "GNU recutils 1.9"
