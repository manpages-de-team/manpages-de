# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2025-01-10 10:18+0100\n"
"PO-Revision-Date: 2024-10-06 21:00+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.3\n"

#. type: ds C+
#: debian-bookworm
#, no-wrap
msgid "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"
msgstr "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"

#.  ========================================================================
#. type: IX
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Title"
msgstr "Titlu"

#.  ========================================================================
#. type: IX
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DEB-SYSTEMD-INVOKE 1p"
msgstr "DEB-SYSTEMD-INVOKE 1p"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DEB-SYSTEMD-INVOKE"
msgstr "DEB-SYSTEMD-INVOKE"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-09-18"
msgstr "18 septembrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "1.65.2"
msgstr "1.65.2"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "init-system-helpers"
msgstr "init-system-helpers"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "deb-systemd-invoke - wrapper around systemctl, respecting policy-rc.d"
msgstr ""
"deb-systemd-invoke - script învăluitor în jurul systemctl, respectând policy-"
"rc.d"

#. type: IX
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: IX
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Header"
msgstr "Antet"

#. type: Plain text
#: debian-bookworm
msgid ""
"\\&B<deb-systemd-invoke> [B<--user>] start|stop|restart I<unit file> ..."
msgstr ""
"\\&B<deb-systemd-invoke> [B<--user>] start|stop|restart I<fișier-unitate> ..."

#. type: IX
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"\\&B<deb-systemd-invoke> is a Debian-specific helper script which asks /usr/"
"sbin/policy-rc.d before performing a systemctl call."
msgstr ""
"\\&B<deb-systemd-invoke> este un script de ajutor specific Debian care "
"solicită «/usr/sbin/policy-rc.d» înainte de a efectua un apel «systemctl»."

#. type: Plain text
#: debian-bookworm
msgid ""
"\\&B<deb-systemd-invoke> is intended to be used from maintscripts to start "
"systemd unit files. It is specifically \\s-1NOT\\s0 intended to be used "
"interactively by users. Instead, users should run systemd and use systemctl, "
"or not bother about the systemd enabled state in case they are not running "
"systemd."
msgstr ""
"\\&B<deb-systemd-invoke> este destinat a fi utilizat din scripturile de "
"întreținere pentru a porni fișierele de unitate „systemd”. Acesta este "
"destinat în mod special pentru a \\s-1NU\\s0 fi utilizat în mod interactiv "
"de către utilizatori. În schimb, utilizatorii ar trebui să ruleze „systemd” "
"și să folosească «systemctl», sau să nu-și bată capul cu starea de activare "
"a „systemd” în cazul în care nu rulează „systemd”."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2024-12-29"
msgstr "29 decembrie 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "1.68"
msgstr "1.68"

#. type: Plain text
#: debian-unstable
msgid ""
"\\&B<deb-systemd-invoke> [B<--user>] start|stop|restart I<unit\\ "
"file>\\ ...  \\&B<deb-systemd-invoke> [B<--user>] [B<--no-dbus>] daemon-"
"reload|daemon-reexec"
msgstr ""
"\\&B<deb-systemd-invoke> [B<--user>] start|stop|restart I<fișier-"
"unitate> ....  \\&B<deb-systemd-invoke> [B<--user>] [B<--no-dbus>] daemon-"
"reload|daemon-reexec"

#. type: Plain text
#: debian-unstable
msgid ""
"\\&B<deb-systemd-invoke> is intended to be used from maintscripts to manage "
"systemd unit files. It is specifically NOT intended to be used interactively "
"by users. Instead, users should run systemd and use systemctl, or not bother "
"about the systemd enabled state in case they are not running systemd."
msgstr ""
"\\&B<deb-systemd-invoke> este destinat a fi utilizat din scripturile de "
"întreținere pentru a gestiona fișierele de unitate „systemd”. Acesta este "
"destinat în mod special pentru a NU fi utilizat în mod interactiv de către "
"utilizatori. În schimb, utilizatorii ar trebui să ruleze „systemd” și să "
"folosească «systemctl», sau să nu-și bată capul cu starea de activare a "
"„systemd” în cazul în care nu rulează „systemd”."
