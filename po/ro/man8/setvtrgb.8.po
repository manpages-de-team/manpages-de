# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2025-02-16 06:02+0100\n"
"PO-Revision-Date: 2023-10-31 12:52+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SETVTRGB"
msgstr "SETVTRGB"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "3 Mar 2011"
msgstr "3 martie 2011"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "kbd"
msgstr "kbd"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "setvtrgb - set the virtual terminal RGB colors"
msgstr "setvtrgb - stabilește culorile RGB ale terminalului virtual"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<setvtrgb> [I<\\,options\\/>] I<vga|FILE|->"
msgstr "B<setvtrgb> [I<\\,opțiuni\\/>] I<vga|FIȘIER|->"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The I<setvtrgb> command takes a single argument, either the string B<vga> , "
"or a path to a file containing the colors to be used by the Linux virtual "
"terminals."
msgstr ""
"Comanda I<setvtrgb> primește un singur argument, fie șirul de caractere "
"B<vga> , fie o rută către un fișier care să conțină culorile care urmează să "
"fie utilizate de terminalele virtuale Linux."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"You can choose to write the colors in decimal or hexadecimal format, it will "
"be detected on runtime."
msgstr ""
"Puteți alege să scrieți culorile în format zecimal sau hexazecimal, care va "
"fi detectat în timpul execuției."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Decimal B<FILE> format should be exactly 3 lines of 16 comma-separated "
"decimal values for RED, GREEN, and BLUE."
msgstr ""
"Formatul zecimal al B<FIȘIERULUI> trebuie să fie format din exact 3 linii de "
"16 valori zecimale separate prin virgulă pentru ROȘU, VERDE și ALBASTRU."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "To seed a valid B<FILE> :"
msgstr "Pentru a crea un B<FIȘIER> valid :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<cat /sys/module/vt/parameters/default_{red,grn,blu} E<gt> FILE>"
msgstr "B<cat /sys/module/vt/parameters/default_{red,grn,blu} E<gt> FIȘIER>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "And then edit the values in B<FILE>"
msgstr "Și apoi editați valorile din B<FIȘIER>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Hexadecimal B<FILE> format should be exactly 16 lines of hex triplets for "
"RED, GREEN and BLUE, prefixed with a number sign (#). For example:"
msgstr ""
"Formatul hexazecimal al B<FIȘIERULUI> trebuie să fie format din exact 16 "
"linii de triplete hexazecimale pentru ROȘU, VERDE și ALBASTRU, prefixate cu "
"un semn numeric (#). De exemplu:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#000000>\n"
"B<#AA0000>\n"
"B<#00AA00>\n"
"B<#AA5500>\n"
"B<#0000AA>\n"
"B<#AA00AA>\n"
"B<#00AAAA>\n"
"B<#AAAAAA>\n"
"B<#555555>\n"
"B<#FF5555>\n"
"B<#55FF55>\n"
"B<#FFFF55>\n"
"B<#5555FF>\n"
"B<#FF55FF>\n"
"B<#55FFFF>\n"
"B<#FFFFFF>\n"
msgstr ""
"B<#000000>\n"
"B<#AA0000>\n"
"B<#00AA00>\n"
"B<#AA5500>\n"
"B<#0000AA>\n"
"B<#AA00AA>\n"
"B<#00AAAA>\n"
"B<#AAAAAA>\n"
"B<#555555>\n"
"B<#FF5555>\n"
"B<#55FF55>\n"
"B<#FFFF55>\n"
"B<#5555FF>\n"
"B<#FF55FF>\n"
"B<#55FFFF>\n"
"B<#FFFFFF>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-C>, B<--console>=I<\\,DEV\\/>"
msgstr "B<-C>, B<--console>=I<\\,DEV\\/>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "the console device to be used."
msgstr "dispozitivul consolă care urmează să fie utilizat."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Prints usage message and exits."
msgstr "Afișează un scurt mesaj de utilizare și iese."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Prints version number and exists."
msgstr "Afișează numărul de versiune și iese."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The utility is written by Alexey Gladkov, Seth Forshee, Dustin Kirkland."
msgstr ""
"Utilitatea este scrisă de Alexey Gladkov, Seth Forshee, Dustin Kirkland."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENTATION"
msgstr "DOCUMENTAȚIE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Documentation by Dustin Kirkland."
msgstr "Documentație realizată de Dustin Kirkland."

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "the console device to be used;"
msgstr "dispozitivul consolă care urmează să fie utilizat;"

#. type: SH
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"
