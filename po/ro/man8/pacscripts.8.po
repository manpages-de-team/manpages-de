# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-28 16:42+0100\n"
"PO-Revision-Date: 2024-02-09 14:50+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "PACSCRIPTS"
msgstr "PACSCRIPTS"

#. type: TH
#: archlinux
#, no-wrap
msgid "2025-02-01"
msgstr "1 februarie 2025"

#. type: TH
#: archlinux
#, no-wrap
msgid "Pacman-contrib 1\\&.11\\&.0"
msgstr "Pacman-contrib 1\\&.11\\&.0"

#. type: TH
#: archlinux
#, no-wrap
msgid "Pacman-contrib Manual"
msgstr "Manualul Pacman-contrib"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux
msgid ""
"pacscripts - print out the {pre,post}_{install,remove,upgrade} scripts of a "
"given package\\&."
msgstr ""
"pacscripts - afișează scripturile {pre, post}_{install,remove,upgrade} ale "
"unui anumit pachet"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux
msgid "I<pacscripts> E<lt>pkgname | pkgfileE<gt>"
msgstr "I<pacscripts> E<lt>nume-pachet | fișier-pachetE<gt>"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux
msgid ""
"I<pacscripts> will print the {pre,post}_{install,remove,upgrade} scripts of "
"a given package Example: pacscripts gconf-editor Example: pacscripts gconf-"
"editor-3\\&.0\\&.1-3-x86_64\\&.pkg\\&.tar\\&.xz"
msgstr ""
"I<pacscripts> va afișa scripturile {pre,post}_{install,remove,upgrade} ale "
"unui pachet dat Exemplu: «pacscripts gconf-editor» Exemplu: «pacscripts "
"gconf-editor-3\\&.0\\&.1-3-x86_64\\&.pkg\\&.tar\\&.xz»"

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr "B<-h, --help>"

#. type: Plain text
#: archlinux
msgid "Display syntax and command-line options\\&."
msgstr "Afișează sintaxa și opțiunile liniei de comandă\\&."

#. type: Plain text
#: archlinux
msgid "B<-V, --version>"
msgstr "B<-V, --version>"

#. type: Plain text
#: archlinux
msgid "Display version information\\&."
msgstr "Afișează informațiile despre versiune.\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux
msgid "B<pacman>(8)"
msgstr "B<pacman>(8)"

#. type: SH
#: archlinux
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux
msgid ""
"Bugs? You must be kidding; there are no bugs in this software\\&. But if we "
"happen to be wrong, file an issue with as much detail as possible at https://"
"gitlab\\&.archlinux\\&.org/pacman/pacman-contrib/-/issues/new\\&."
msgstr ""
"Hibe? Probabil că glumiți; nu există nicio hibă în acest software&. Dar dacă "
"se întâmplă să fie ceva greșit, depuneți o cerere de rezolvare a problemei "
"cu cât mai multe detalii posibile la: https://gitlab\\&.archlinux\\&.org/"
"pacman/pacman-contrib/-/issues/new\\&."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

# R-GB, scrie:
# cred că la fel de bine, mesajul ar putea fi
# tradus ca:
# „Menținătorii actuali:”
#. type: Plain text
#: archlinux
msgid "Current maintainers:"
msgstr "Responsabilii actuali:"

#. type: Plain text
#: archlinux
msgid "Johannes Löthberg E<lt>johannes@kyriasis\\&.comE<gt>"
msgstr "Johannes Löthberg E<lt>johannes@kyriasis\\&.comE<gt>"

#. type: Plain text
#: archlinux
msgid "Daniel M\\&. Capella E<lt>polyzen@archlinux\\&.orgE<gt>"
msgstr "Daniel M\\&. Capella E<lt>polyzen@archlinux\\&.orgE<gt>"

#. type: Plain text
#: archlinux
msgid ""
"For additional contributors, use git shortlog -s on the pacman-"
"contrib\\&.git repository\\&."
msgstr ""
"Pentru contribuitori suplimentari, folosiți «git shortlog -s» în depozitul "
"\\&.git pacman-contrib\\&."
