# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-28 16:54+0100\n"
"PO-Revision-Date: 2025-03-03 19:12+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.5\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "TALKD"
msgstr "TALKD"

#. type: TH
#: archlinux
#, no-wrap
msgid "December 2023"
msgstr "decembrie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GNU inetutils 2.5"
msgstr "GNU inetutils 2.5"

#. type: TH
#: archlinux
#, no-wrap
msgid "System Administration Utilities"
msgstr "Utilitare de administrare a sistemului"

#. type: SH
#. type: Sh
#. type: Sh
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux
msgid "talkd - Talk server"
msgstr "talkd - server de discuții"

#. type: SH
#. type: Sh
#. type: Sh
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux
msgid "B<talkd> [I<\\,OPTION\\/>...]"
msgstr "B<talkd> [I<\\,OPȚIUNE\\/>...]"

#. type: SH
#. type: Sh
#. type: Sh
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux
msgid "Talk daemon, using service `ntalk'."
msgstr "Demon de discuții, folosind serviciul „ntalk”."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-a>, B<--acl>=I<\\,FILE\\/>"
msgstr "B<-a>, B<--acl>=I<\\,FIȘIER\\/>"

#. type: Plain text
#: archlinux
msgid "read site-wide ACLs from FILE"
msgstr "citește ACL-uri la nivel de site din FIȘIER"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-d>, B<--debug>"
msgstr "B<-d>, B<--debug>"

#. type: Plain text
#: archlinux
msgid "enable debugging"
msgstr "activează depanarea"

#. type: Plain text
#: archlinux
msgid ""
"B<-i>, B<--idle-timeout>=I<\\,SECONDS\\/> set idle timeout value to SECONDS"
msgstr ""
"B<-i>, B<--idle-timeout>=I<\\,SECUNDE\\/> stabilește valoarea timpului de "
"inactivitate la SECUNDE"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-l>, B<--logging>"
msgstr "B<-l>, B<--logging>"

#. type: Plain text
#: archlinux
msgid "enable more syslog reporting"
msgstr "activează mai multe raportări pentru «syslog»"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-r>, B<--request-ttl>=I<\\,SECONDS\\/>"
msgstr "B<-r>, B<--request-ttl>=I<\\,SECUNDE\\/>"

#. type: Plain text
#: archlinux
msgid "set request time-to-live value to SECONDS"
msgstr "stabilește valoarea cererii „time-to-live” la SECUNDE"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-S>, B<--strict-policy>"
msgstr "B<-S>, B<--strict-policy>"

#. type: Plain text
#: archlinux
msgid "apply strict ACL policy"
msgstr "aplică o politică ACL strictă"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-t>, B<--timeout>=I<\\,SECONDS\\/>"
msgstr "B<-t>, B<--timeout>=I<\\,SECUNDE\\/>"

#. type: Plain text
#: archlinux
msgid "set timeout value to SECONDS"
msgstr "stabilește valoarea timpului de așteptare la SECUNDE"

#. type: TP
#: archlinux
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux
msgid "give this help list"
msgstr "oferă această listă de ajutor"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux
msgid "give a short usage message"
msgstr "oferă un mesaj de utilizare scurt"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux
msgid "print program version"
msgstr "afișează versiunea programului"

#. type: Plain text
#: archlinux
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Argumentele obligatorii sau opționale pentru opțiunile lungi sunt "
"obligatorii sau opționale și pentru opțiunile corespunzătoare scurte."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux
msgid "Written by Sergey Poznyakoff."
msgstr "Scris de Sergey Poznyakoff."

#. type: SH
#: archlinux
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux
msgid "Report bugs to E<lt>bug-inetutils@gnu.orgE<gt>."
msgstr "Raportați erorile la: E<lt>bug-inetutils@gnu.orgE<gt>."

#. type: SH
#: archlinux
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: archlinux
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2023 Free Software Foundation, Inc. Licența GPLv3+: "
"GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți. Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#. type: Sh
#. type: Sh
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux
msgid "talk(1)"
msgstr "talk(1)"

#. type: Plain text
#: archlinux
msgid ""
"The full documentation for B<talkd> is maintained as a Texinfo manual.  If "
"the B<info> and B<talkd> programs are properly installed at your site, the "
"command"
msgstr ""
"Documentația completă pentru B<talkd> este menținută ca un manual Texinfo. "
"Dacă programele B<info> și B<talkd> sunt instalate corect pe sistemul "
"dumneavoastră, comanda"

#. type: Plain text
#: archlinux
msgid "B<info talkd>"
msgstr "B<info talkd>"

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: Dd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "February 9, 2019"
msgstr "9 februarie 2019"

#. type: Dt
#: debian-bookworm
#, no-wrap
msgid "TALKD 8 SMM"
msgstr "TALKD 8 SMM"

#. type: Os
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GNU Network Utilities"
msgstr "Utilități GNU de rețea"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm talkd>"
msgstr "E<.Nm talkd>"

#. type: Nd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "remote user communication server"
msgstr "server de comunicare cu utilizatorul la distanță"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm talkd> E<.Op Ar options>"
msgstr "E<.Nm talkd> E<.Op Ar opțiuni>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm talkd> is the server that notifies a user that someone else wants to "
"initiate a conversation.  It acts as a repository of invitations, responding "
"to requests by clients wishing to rendezvous to hold a conversation.  In "
"normal operation, a client, the caller, initiates a rendezvous by sending a "
"E<.Tn CTL_MSG> to the server of type E<.Tn LOOK_UP> (see E<.Aq Pa protocols/"
"talkd.h>).  This causes the server to search its invitation tables to check "
"if an invitation currently exists for the caller (to speak to the callee "
"specified in the message).  If the lookup fails, the caller then sends an "
"E<.Tn ANNOUNCE> message causing the server to broadcast an announcement on "
"the callee's login ports requesting contact.  When the callee responds, the "
"local server uses the recorded invitation to respond with the appropriate "
"rendezvous address and the caller and callee client programs establish a "
"stream connection through which the conversation takes place."
msgstr ""
"E<.Nm talkd> este serverul care notifică un utilizator că altcineva dorește "
"să inițieze o conversație. Acesta acționează ca un depozitar de invitații, "
"răspunzând la cererile clienților care doresc să se întâlnească pentru a "
"purta o conversație. În condiții normale de funcționare, un client, "
"apelantul, inițiază o întâlnire prin trimiterea unui E<.Tn CTL_MSG> către "
"server de tip E<.Tn LOOK_UP> (a se vedea E<.Aq Pa protocols/talkd.h>). Acest "
"lucru determină serverul să caute în tabelele sale de invitații pentru a "
"verifica dacă există în prezent o invitație pentru apelant (pentru a vorbi "
"cu destinatarul specificat în mesaj). În cazul în care căutarea eșuează, "
"apelantul trimite un mesaj E<.Tn ANNOUNCE> care determină serverul să "
"difuzeze un anunț pe porturile de conectare ale persoanei apelate, "
"solicitând contactul. Atunci când persoana apelată răspunde, serverul local "
"utilizează invitația înregistrată pentru a răspunde cu adresa de întâlnire "
"corespunzătoare, iar programele client ale apelantului și ale persoanei "
"apelate stabilesc o conexiune de flux prin care are loc conversația."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl l , -logging"
msgstr "Fl l , -logging"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Enable more verbose logging to syslog."
msgstr "Activează mai multe raportări pentru «syslog»."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl d , -debug"
msgstr "Fl d , -debug"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Enable debug mode."
msgstr "Activează modul de depanare."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl t , -timeout Ar seconds"
msgstr "Fl t , -timeout Ar secunde"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Set timeout value to E<.Ar seconds>."
msgstr "Stabilește valoarea timpului de așteptare la E<.Ar secunde>."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl i , -idle-timeout Ar seconds"
msgstr "Fl i , -idle-timeout Ar secunde"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Set idle timeout value to E<.Ar seconds>."
msgstr "Stabilește valoarea timpului de inactivitate la E<.Ar secunde>."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl r , -request-ttl Ar seconds"
msgstr "Fl r , -request-ttl Ar secunde"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Set request time-to-live value to E<.Ar seconds>."
msgstr "Stabilește valoarea cererii „time-to-live” la E<.Ar secunde>."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl a , -acl Ar filename"
msgstr "Fl a , -acl Ar nume-fișier"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Read the site-wide ACLs from E<.Ar filename>."
msgstr "Citește ACL-urile la nivel de sit din E<.Ar nume-fișier>."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl S , -strict-policy"
msgstr "Fl S , -strict-policy"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Apply a strict ACL policy."
msgstr "Aplică o politică ACL strictă."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl ? , -help"
msgstr "Fl ? , -help"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Display a help list."
msgstr "Afișează acest mesaj de ajutor."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl -usage"
msgstr "Fl -usage"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Display a short usage message."
msgstr "Afișează un scurt mesaj de utilizare."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl V , -version"
msgstr "Fl V , -version"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Display program version."
msgstr "Afișează versiunea programului, apoi iese."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Xr talk 1>, E<.Xr write 1>"
msgstr "E<.Xr talk 1>, E<.Xr write 1>"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "The E<.Nm> command appeared in E<.Bx 4.3>."
msgstr "Comanda E<.Nm> a apărut în E<.Bx 4.3>."

#. type: Dt
#: debian-unstable
#, no-wrap
msgid "talkd 8 SMM"
msgstr "talkd 8 SMM"
