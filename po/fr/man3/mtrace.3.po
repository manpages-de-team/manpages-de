# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999,2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2012-2014.
# Denis Barbier <barbier@debian.org>, 2006,2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2025-02-28 16:41+0100\n"
"PO-Revision-Date: 2024-04-20 11:28+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "mtrace"
msgstr "mtrace"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 juillet 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pages du manuel de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "mtrace, muntrace - malloc tracing"
msgstr "mtrace, muntrace - Suivi de malloc"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>,\\ I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>mcheck.hE<gt>>\n"
msgstr "B<#include E<lt>mcheck.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void mtrace(void);>\n"
"B<void muntrace(void);>\n"
msgstr ""
"B<void mtrace(void);>\n"
"B<void muntrace(void);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<mtrace>()  function installs hook functions for the memory-allocation "
"functions (B<malloc>(3), B<realloc>(3)  B<memalign>(3), B<free>(3)).  These "
"hook functions record tracing information about memory allocation and "
"deallocation.  The tracing information can be used to discover memory leaks "
"and attempts to free nonallocated memory in a program."
msgstr ""
"La fonction B<mtrace>() installe des fonctions de rappel (« hook ») pour les "
"fonctions d'allocation mémoire B<malloc>(3), B<realloc>(3)  B<memalign>(3), "
"et B<free>(3). Ces fonctions de rappel enregistrent l'historique des "
"allocations et désallocations de mémoire. Ces informations permettent de "
"découvrir des fuites mémoire ou des libérations de mémoire non allouée dans "
"un programme."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<muntrace>()  function disables the hook functions installed by "
"B<mtrace>(), so that tracing information is no longer recorded for the "
"memory-allocation functions.  If no hook functions were successfully "
"installed by B<mtrace>(), B<muntrace>()  does nothing."
msgstr ""
"La fonction B<muntrace>() désactive les fonctions de rappel (« hook ») "
"installées par B<mtrace>() afin d'arrêter l'enregistrement des appels aux "
"fonction d'allocation mémoire. Si aucune fonction de rappel n'était "
"installée par B<mtrace>(), B<muntrace>() ne fait rien."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When B<mtrace>()  is called, it checks the value of the environment variable "
"B<MALLOC_TRACE>, which should contain the pathname of a file in which the "
"tracing information is to be recorded.  If the pathname is successfully "
"opened, it is truncated to zero length."
msgstr ""
"Lors de l'appel à B<mtrace>(), la valeur de la variable d'environnement "
"B<MALLOC_TRACE> est lue pour y trouver le chemin du fichier où enregistrer "
"l'historique des appels. Si ce fichier est correctement ouvert, il est remis "
"à zéro."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If B<MALLOC_TRACE> is not set, or the pathname it specifies is invalid or "
"not writable, then no hook functions are installed, and B<mtrace>()  has no "
"effect.  In set-user-ID and set-group-ID programs, B<MALLOC_TRACE> is "
"ignored, and B<mtrace>()  has no effect."
msgstr ""
"Si B<MALLOC_TRACE> n'est pas définie, ou si le chemin passé en paramètre est "
"invalide ou ne peut pas être écrit, alors aucune fonction de rappel "
"(« hook ») n'est installé et B<mtrace>() n'aura pas d'effet. Pour les "
"programmes I<setuid> ou I<setgid>, B<MALLOC_TRACE> est ignoré et B<mtrace>() "
"n'est pas d'effet."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<mtrace>(),\n"
"B<muntrace>()"
msgstr ""
"B<mtrace>(),\n"
"B<muntrace>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe"
msgstr "MT-Unsafe"

#.  FIXME: The marking is different from that in the glibc manual,
#.  markings in glibc manual are more detailed:
#.       mtrace: MT-Unsafe env race:mtrace const:malloc_hooks init
#.       muntrace: MT-Unsafe race:mtrace const:malloc_hooks locale
#.  But there is something wrong in glibc manual, for example:
#.  glibc manual says muntrace should have marking locale because it calls
#.  fprintf(), but muntrace does not execute area which cause locale problem.
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In normal usage, B<mtrace>()  is called once at the start of execution of a "
"program, and B<muntrace>()  is never called."
msgstr ""
"En utilisation habituelle, B<mtrace>() est appelé une fois au début de "
"l'exécution du programme, et B<muntrace>() n'est jamais appelé."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The tracing output produced after a call to B<mtrace>()  is textual, but not "
"designed to be human readable.  The GNU C library provides a Perl script, "
"B<mtrace>(1), that interprets the trace log and produces human-readable "
"output.  For best results, the traced program should be compiled with "
"debugging enabled, so that line-number information is recorded in the "
"executable."
msgstr ""
"La sortie produite lors d'un appel à B<mtrace>() est sous forme de texte, "
"mais n'a pas été prévue pour être lisible par un humain. La bibliothèque GNU "
"C fournit un script Perl, B<mtrace>(1), qui interprète l'enregistrement des "
"appels mémoire et affiche une sortie lisible. Pour de meilleurs résultats, "
"le programme instrumenté devrait être compilé avec les symboles de débogage, "
"afin que les numéros de ligne soient inscrits dans l'exécutable."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The tracing performed by B<mtrace>()  incurs a performance penalty (if "
"B<MALLOC_TRACE> points to a valid, writable pathname)."
msgstr ""
"Le suivi des appels d'allocation mémoire effectué par B<mtrace>() pénalise "
"les performances."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The line-number information produced by B<mtrace>(1)  is not always precise: "
"the line number references may refer to the previous or following "
"(nonblank)  line of the source code."
msgstr ""
"Le numéro de ligne indiqué par B<mtrace>(1) n'est pas toujours précis : il "
"peut faire référence à la ligne de code (non vide) précédente ou suivante."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The shell session below demonstrates the use of the B<mtrace>()  function "
"and the B<mtrace>(1)  command in a program that has memory leaks at two "
"different locations.  The demonstration uses the following program:"
msgstr ""
"L'exemple de session suivant montre comment utiliser la fonction B<mtrace>() "
"et la commande B<mtrace>(1) dans un programme qui renferme des fuites "
"mémoire à deux endroits différents."

#. #-#-#-#-#  archlinux: mtrace.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC BEGIN (t_mtrace.c)
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: mtrace.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC BEGIN (t_mtrace.c)
#. type: Plain text
#. #-#-#-#-#  debian-unstable: mtrace.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC BEGIN (t_mtrace.c)
#. type: Plain text
#. #-#-#-#-#  fedora-42: mtrace.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC BEGIN (t_mtrace.c)
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: mtrace.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC BEGIN (t_mtrace.c)
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: mtrace.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  [[memory leak]] SRC BEGIN (t_mtrace.c)
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-16-0: mtrace.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC BEGIN (t_mtrace.c)
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: mtrace.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC BEGIN (t_mtrace.c)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "$ B<cat t_mtrace.c>"
msgstr "$ B<cat t_mtrace.c>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>mcheck.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    mtrace();\n"
"\\&\n"
"    for (unsigned int j = 0; j E<lt> 2; j++)\n"
"        malloc(100);            /* Never freed--a memory leak */\n"
"\\&\n"
"    calloc(16, 16);             /* Never freed--a memory leak */\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>mcheck.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    mtrace();\n"
"\\&\n"
"    for (unsigned int j = 0; j E<lt> 2; j++)\n"
"        malloc(100);            /* Jamais libéré -- une fuite mémoire */\n"
"\\&\n"
"    calloc(16, 16);             /* Jamais libéré -- une fuite mémoire */\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When we run the program as follows, we see that B<mtrace>()  diagnosed "
"memory leaks at two different locations in the program:"
msgstr ""
"Lorsque ce programme est exécuté comme ci-dessous, B<mtrace>() diagnostique "
"des fuites mémoire à deux endroits différents."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<cc -g t_mtrace.c -o t_mtrace>\n"
"$ B<export MALLOC_TRACE=/tmp/t>\n"
"$ B<./t_mtrace>\n"
"$ B<mtrace ./t_mtrace $MALLOC_TRACE>\n"
"Memory not freed:\n"
"-----------------\n"
"   Address     Size     Caller\n"
"0x084c9378     0x64  at /home/cecilia/t_mtrace.c:12\n"
"0x084c93e0     0x64  at /home/cecilia/t_mtrace.c:12\n"
"0x084c9448    0x100  at /home/cecilia/t_mtrace.c:16\n"
msgstr ""
"$ B<cc -g t_mtrace.c -o t_mtrace>\n"
"$ B<export MALLOC_TRACE=/tmp/t>\n"
"$ B<./t_mtrace>\n"
"$ B<mtrace ./t_mtrace $MALLOC_TRACE>\n"
"Memory not freed:\n"
"-----------------\n"
"   Address     Size     Caller\n"
"0x084c9378     0x64  at /home/cecilia/t_mtrace.c:12\n"
"0x084c93e0     0x64  at /home/cecilia/t_mtrace.c:12\n"
"0x084c9448    0x100  at /home/cecilia/t_mtrace.c:16\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The first two messages about unfreed memory correspond to the two "
"B<malloc>(3)  calls inside the I<for> loop.  The final message corresponds "
"to the call to B<calloc>(3)  (which in turn calls B<malloc>(3))."
msgstr ""
"Les deux premiers messages liés à la mémoire non libérée correspondent aux "
"deux appels à B<malloc>(3) dans la boucle I<for>. Le dernier message "
"correspond à l'appel B<calloc>(3) (qui à son tour appelle B<malloc>(3)). "

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<mtrace>(1), B<malloc>(3), B<malloc_hook>(3), B<mcheck>(3)"
msgstr "B<mtrace>(1), B<malloc>(3), B<malloc_hook>(3), B<mcheck>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
msgid "These functions are GNU extensions."
msgstr "Ces fonctions sont des extensions GNU."

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"#include E<lt>mcheck.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#include E<lt>mcheck.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    mtrace();\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    mtrace();\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    for (unsigned int j = 0; j E<lt> 2; j++)\n"
"        malloc(100);            /* Never freed--a memory leak */\n"
msgstr ""
"    for (unsigned int j = 0; j E<lt> 2; j++)\n"
"        malloc(100);            /* Jamais libéré -- une fuite mémoire */\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"    calloc(16, 16);             /* Never freed--a memory leak */\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    calloc(16, 16);             /* Jamais libéré -- une fuite mémoire */\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
