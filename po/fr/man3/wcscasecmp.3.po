# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Baptiste Holcroft <jean-baptiste@holcroft.fr>, 2018
# Grégoire Scano <gregoire.scano@malloc.fr>, 2019
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2025-02-28 16:56+0100\n"
"PO-Revision-Date: 2023-03-08 09:09+0100\n"
"Last-Translator: Grégoire Scano <gregoire.scano@malloc.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "wcscasecmp"
msgstr "wcscasecmp"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 juillet 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pages du manuel de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "wcscasecmp - compare two wide-character strings, ignoring case"
msgid ""
"wcscasecmp, wcsncasecmp - compare two wide-character strings, ignoring case"
msgstr ""
"wcscasecmp - Comparer deux chaînes de caractères larges en ignorant la casse"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>,\\ I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr "B<#include E<lt>wchar.hE<gt>>\n"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int wcscasecmp(const wchar_t *>I<s1>B<, const wchar_t *>I<s2>B<);>\n"
"B<int wcsncasecmp(const wchar_t >I<s1>B<[.>I<n>B<], const wchar_t >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "B<strcasecmp>(3), B<wcscmp>(3)"
msgid "B<wcscasecmp>(), B<wcsncasecmp>():"
msgstr "B<strcasecmp>(3), B<wcscmp>(3)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.10 :\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Avant la glibc 2.10 :\n"
"        _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<wcscasecmp>()  function is the wide-character equivalent of the "
"B<strcasecmp>(3)  function.  It compares the wide-character string pointed "
"to by I<s1> and the wide-character string pointed to by I<s2>, ignoring case "
"differences (B<towupper>(3), B<towlower>(3))."
msgstr ""
"La fonction B<wcscasecmp>() est l'équivalent pour caractères larges de la "
"fonction B<strcasecmp>(3). Elle compare la chaîne de caractères larges "
"pointée par I<s1> avec celle pointée par I<s2>, en ignorant les différences "
"de casse (B<towupper>(3), B<towlower>(3))."

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<wcscasecmp>()  function is the wide-character equivalent of the "
#| "B<strcasecmp>(3)  function.  It compares the wide-character string "
#| "pointed to by I<s1> and the wide-character string pointed to by I<s2>, "
#| "ignoring case differences (B<towupper>(3), B<towlower>(3))."
msgid ""
"The B<wcsncasecmp>()  function is similar (the wide-character equivalent of "
"B<strncasecmp>(3)), except that it compares no more than I<n> wide "
"characters of I<s1> and I<s2>."
msgstr ""
"La fonction B<wcscasecmp>() est l'équivalent pour caractères larges de la "
"fonction B<strcasecmp>(3). Elle compare la chaîne de caractères larges "
"pointée par I<s1> avec celle pointée par I<s2>, en ignorant les différences "
"de casse (B<towupper>(3), B<towlower>(3))."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid ""
"The B<wcscasecmp>()  and B<wcsncasecmp>()  functions return an integer less "
"than, equal to, or greater than zero if I<s1> is, after ignoring case, found "
"to be less than, to match, or be greater than I<s2>, respectively."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strcasecmp>(3), B<wcscmp>(3)"
msgid ""
"B<wcscasecmp>(),\n"
"B<wcsncasecmp>()"
msgstr "B<strcasecmp>(3), B<wcscmp>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "glibc 2.1."
msgstr "glibc 2.1."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The behavior of B<wcscasecmp>()  depends on the B<LC_CTYPE> category of the "
"current locale."
msgstr ""
"Le comportement de B<wcscasecmp>() dépend de la catégorie B<LC_CTYPE> de la "
"locale utilisée."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<strcasecmp>(3), B<wcscmp>(3)"
msgstr "B<strcasecmp>(3), B<wcscmp>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "wcscasecmp - compare two wide-character strings, ignoring case"
msgstr ""
"wcscasecmp - Comparer deux chaînes de caractères larges en ignorant la casse"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<int wcscasecmp(const wchar_t *>I<s1>B<, const wchar_t *>I<s2>B<);>\n"
msgstr "B<int wcscasecmp(const wchar_t *>I<s1>B<, const wchar_t *>I<s2>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "B<wcscasecmp>():"
msgstr "B<wcscasecmp>() :"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"The B<wcscasecmp>()  function returns zero if the wide-character strings at "
"I<s1> and I<s2> are equal except for case distinctions.  It returns a "
"positive integer if I<s1> is greater than I<s2>, ignoring case.  It returns "
"a negative integer if I<s1> is smaller than I<s2>, ignoring case."
msgstr ""
"La fonction B<wcscasecmp>() renvoie zéro si les chaînes de caractères larges "
"I<s1> et I<s2> sont égales, excepté pour des distinctions de casse. Elle "
"renvoie un entier positif si I<s1> est supérieur à I<s2>, en ignorant la "
"casse. Elle renvoie un entier négatif si I<s1> est inférieur à I<s2>, en "
"ignorant la casse."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm
msgid "The B<wcscasecmp>()  function is provided since glibc 2.1."
msgstr "La fonction B<wcscasecmp>() est fournie depuis la glibc 2.1."

#. type: tbl table
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, no-wrap
msgid "B<wcscasecmp>()"
msgstr "B<wcscasecmp>()"

#. type: Plain text
#: debian-bookworm
msgid ""
"POSIX.1-2008.  This function is not specified in POSIX.1-2001, and is not "
"widely available on other systems."
msgstr ""
"POSIX.1-2008. Cette fonction n'est pas spécifiée dans POSIX.1-2001 et n'est "
"que rarement disponible sur d'autres systèmes."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
