# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Sylvain Cherrier <sylvain.cherrier@free.fr>, 2006, 2007, 2008, 2009.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2007.
# Dominique Simen <dominiquesimen@hotmail.com>, 2009.
# Nicolas Sauzède <nsauzede@free.fr>, 2009.
# Romain Doumenc <rd6137@gmail.com>, 2010, 2011.
# David Prévot <david@tilapin.org>, 2011, 2012, 2014.
# Denis Mugnier <myou72@orange.fr>, 2011.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2012, 2013.
msgid ""
msgstr ""
"Project-Id-Version: nfs-utils\n"
"POT-Creation-Date: 2025-02-16 06:09+0100\n"
"PO-Revision-Date: 2013-05-30 17:56+0200\n"
"Last-Translator: Cédric Boutillier <boutil@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "UMOUNT.NFS"
msgstr "UMOUNT.NFS"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "6 Jun 2006"
msgstr "6 juin 2006"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "umount.nfs, umount.nfs4 - unmount a Network File System"
msgstr "umount.nfs, umount.nfs4 - Démonter un système de fichiers réseau (NFS)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<umount.nfs>I< dir>B< [-fvnrlh ]>"
msgstr "B<umount.nfs>I< répertoire> [B<-fvnrlh>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<umount.nfs> and B<umount.nfs4> are a part of B<nfs>(5)  utilities package, "
"which provides NFS client functionality."
msgstr ""
"B<umount.nfs> et B<umount.nfs4> font partie du paquet des utilitaires "
"B<nfs>(5), qui offre les fonctionnalités de client NFS."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<umount.nfs4> and B<umount.nfs> are meant to be used by the B<umount>(8)  "
"command for unmounting NFS shares. This subcommand, however, can also be "
"used as a standalone command with limited functionality."
msgstr ""
"B<umount.nfs4> et B<umount.nfs>sont destinés à l'usage de la commande "
"B<umount>(8) pour le démontage des partages NFS. Toutefois, cette sous-"
"commande peut aussi servir en tant que commande autonome, avec des "
"fonctionnalités restreintes."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<dir> is the directory on which the file system is mounted."
msgstr ""
"I<répertoire> est le répertoire dans lequel le système de fichiers est monté."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Force unmount the file system in case of unreachable NFS system."
msgstr ""
"Forcer le démontage du système de fichiers dans le cas où le partage NFS est "
"inaccessible."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Be verbose."
msgstr "Sortie détaillée."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>"
msgstr "B<-n>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Do not update I</etc/mtab.> By default, an entry is created in I</etc/mtab> "
"for every mounted file system. Use this option to skip deleting an entry."
msgstr ""
"Ne pas mettre à jour I</etc/mtab.> Une entrée est créée par défaut dans I</"
"etc/mtab> pour chaque système de fichiers monté. On utilise cette option "
"pour empêcher l'effacement de cette entrée."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>"
msgstr "B<-r>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "In case unmounting fails, try to mount read-only."
msgstr "En cas d'échec du démontage, essayer de monter en lecture seule."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Lazy unmount. Detach the file system from the file system hierarchy now, and "
"cleanup all references to the file system as soon as it is not busy anymore."
msgstr ""
"Démonter à retardement (« lazy »). Détacher immédiatement le système de "
"fichiers de l'arborescence du système, puis supprimer progressivement toutes "
"les références à ce système de fichiers dès qu'elles ne sont plus utilisées."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print help message."
msgstr "Afficher un message d'aide."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTE"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For further information please refer B<nfs>(5)  and B<umount>(8)  manual "
"pages."
msgstr ""
"Pour plus d'informations, veuillez consulter les pages de manuel de "
"B<nfs>(5) et de B<umount>(8)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/fstab>"
msgstr "I</etc/fstab>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "file system table"
msgstr "Table des systèmes de fichiers"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/mtab>"
msgstr "I</etc/mtab>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "table of mounted file systems"
msgstr "Table des systèmes de fichiers montés"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<nfs>(5), B<umount>(8),"
msgstr "B<nfs>(5), B<umount>(8),"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Amit Gud E<lt>agud@redhat.comE<gt>"
msgstr "Amit Gud E<lt>agud@redhat.comE<gt>"
