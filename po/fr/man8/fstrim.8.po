# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"POT-Creation-Date: 2024-03-29 09:39+0100\n"
"PO-Revision-Date: 2022-05-12 23:08+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: vim\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "FSTRIM"
msgstr "FSTRIM"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11 mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr "Administration Système"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm
msgid "fstrim - discard unused blocks on a mounted filesystem"
msgstr ""
"fstrim – Abandonner les blocs non utilisés d'un système de fichiers monté"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fstrim> [B<-Aa>] [B<-o> I<offset>] [B<-l> I<length>] [B<-m> I<minimum-"
"size>] [B<-v> I<mountpoint>]"
msgstr ""
"B<fstrim> [B<-Aa>] [B<-o> I<position>] [B<-l> I<taille>] [B<-m> "
"I<taille_mini> [B<-v>] I<point_montage>]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fstrim> is used on a mounted filesystem to discard (or \"trim\") blocks "
"which are not in use by the filesystem. This is useful for solid-state "
"drives (SSDs) and thinly-provisioned storage."
msgstr ""
"B<fstrim> est utilisé sur un système de fichiers monté pour abandonner (ou "
"« rogner ») les blocs qui ne sont pas utilisés par le système de fichiers. "
"C'est pratique pour les SSD (« solid-state drive ») et l'allocation fine et "
"dynamique (« thinly-provisioned storage »)."

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, B<fstrim> will discard all unused blocks in the filesystem. "
"Options may be used to modify this behavior based on range or size, as "
"explained below."
msgstr ""
"Par défaut, B<fstrim> abandonnera tous les blocs non utilisés du système de "
"fichiers. Les options permettent de modifier ce comportement en fonction "
"d'intervalle ou de taille, conformément aux explications suivantes."

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<mountpoint> argument is the pathname of the directory where the "
"filesystem is mounted."
msgstr ""
"Le paramètre I<point_montage> est le chemin du répertoire où le système de "
"fichiers est monté."

#. type: Plain text
#: debian-bookworm
msgid ""
"Running B<fstrim> frequently, or even using B<mount -o discard>, might "
"negatively affect the lifetime of poor-quality SSD devices. For most desktop "
"and server systems a sufficient trimming frequency is once a week. Note that "
"not all devices support a queued trim, so each trim command incurs a "
"performance penalty on whatever else might be trying to use the disk at the "
"time."
msgstr ""
"Exécuter B<fstrim> fréquemment, ou même utiliser B<mount -o discard>, "
"pourrait affecter négativement la durée de vie des périphériques SSD de "
"mauvaise qualité. Pour la plupart des systèmes de bureau ou de serveur, la "
"fréquence d’abandon suffisante est une fois par semaine. Remarquez que tous "
"les périphériques ne permettent pas de mettre en attente les abandons, donc "
"chaque commande d’abandon pénalise les performances de tout ce qui pourrait "
"être en train d’essayer d’utiliser le disque en même temps."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<offset>, I<length>, and I<minimum-size> arguments may be followed by "
"the multiplicative suffixes KiB (=1024), MiB (=1024*1024), and so on for "
"GiB, TiB, PiB, EiB, ZiB and YiB (the \"iB\" is optional, e.g., \"K\" has the "
"same meaning as \"KiB\") or the suffixes KB (=1000), MB (=1000*1000), and so "
"on for GB, TB, PB, EB, ZB and YB."
msgstr ""
"Les arguments I<position>, I<taille> et I<taille_mini> peuvent être suivis "
"des suffixes multiplicatifs KiB=1024, MiB=1024*1024, etc., pour GiB, TiB, "
"PiB, EiB, ZiB et YiB (la partie « iB » est facultative, par exemple « K » "
"est identique à « KiB ») ou des suffixes KB=1000, MB=1000*1000, etc., pour "
"GB, TB, PB, EB, ZB et YB."

#. type: Plain text
#: debian-bookworm
msgid "B<-A, --fstab>"
msgstr "B<-A>, B<--fstab>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Trim all mounted filesystems mentioned in I</etc/fstab> on devices that "
"support the discard operation. The root filesystem is determined from kernel "
"command line if missing in the file. The other supplied options, like B<--"
"offset>, B<--length> and B<--minimum>, are applied to all these devices. "
"Errors from filesystems that do not support the discard operation, read-only "
"devices, autofs and read-only filesystems are silently ignored. Filesystems "
"with \"X-fstrim.notrim\" mount option are skipped."
msgstr ""
"Rogner tous les systèmes de fichiers mentionnés dans I</etc/fstab> montés "
"sur les périphériques qui permettent l’opération d’abandon. Le système de "
"fichiers racine est déterminé à partir de la ligne de commande du noyau s’il "
"n’est pas indiqué dans le fichier. Les autres options fournies, comme B<--"
"offset>, B<--length> et B<--minimum> sont appliquées à tous ces "
"périphériques. Les erreurs des systèmes de fichiers qui ne permettent pas "
"l’opération d’abandon sont ignorées silencieusement. Les systèmes de "
"fichiers avec l'option de montage « X-fstrim.notrim » sont sautés."

#. type: Plain text
#: debian-bookworm
msgid "B<-a, --all>"
msgstr "B<-a, --all>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Trim all mounted filesystems on devices that support the discard operation. "
"The other supplied options, like B<--offset>, B<--length> and B<--minimum>, "
"are applied to all these devices. Errors from filesystems that do not "
"support the discard operation, read-only devices and read-only filesystems "
"are silently ignored."
msgstr ""
"Rogner tous les systèmes de fichiers montés sur les périphériques qui "
"permettent l’opération d’abandon. Les autres options fournies, comme B<--"
"offset>, B<--length> et B<--minimum> sont appliquées à tous ces "
"périphériques. Les erreurs des systèmes de fichiers qui ne permettent pas "
"l’opération d’abandon, les périphériques en lecture seule et les systèmes de "
"fichiers en lecture seule sont ignorées silencieusement."

#. type: Plain text
#: debian-bookworm
msgid "B<-n, --dry-run>"
msgstr "B<-n>, B<--dry-run>"

#. type: Plain text
#: debian-bookworm
msgid "This option does everything apart from actually call B<FITRIM> ioctl."
msgstr ""
"Cette option permet de tout faire, sauf d’appeler réellement l’ioctl "
"B<FITRIM>."

#. type: Plain text
#: debian-bookworm
msgid "B<-o, --offset> I<offset>"
msgstr "B<-o>, B<--offset> I<position>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Byte offset in the filesystem from which to begin searching for free blocks "
"to discard. The default value is zero, starting at the beginning of the "
"filesystem."
msgstr ""
"La I<position> en octet dans le système de fichiers à partir de laquelle "
"commencer à chercher les blocs libres à abandonner. La valeur par défaut est "
"zéro, désignant le début du système de fichiers."

#. type: Plain text
#: debian-bookworm
msgid "B<-l, --length> I<length>"
msgstr "B<-l>, B<--length> I<taille>"

#. type: Plain text
#: debian-bookworm
msgid ""
"The number of bytes (after the starting point) to search for free blocks to "
"discard. If the specified value extends past the end of the filesystem, "
"B<fstrim> will stop at the filesystem size boundary. The default value "
"extends to the end of the filesystem."
msgstr ""
"Le nombre d'octets (après le point de départ) où chercher des blocs libres à "
"abandonner. Si la valeur indiquée va au-delà de la fin du système de "
"fichiers, B<fstrim> s'arrêtera à la frontière de taille du système de "
"fichiers. La valeur par défaut prolonge la recherche jusqu'à la fin du "
"système de fichiers."

#. type: Plain text
#: debian-bookworm
msgid "B<-I, --listed-in> I<list>"
msgstr "B<-I>, B<--listed-in> I<liste>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specifies a colon-separated list of files in fstab or kernel mountinfo "
"format. All missing or empty files are silently ignored. The evaluation of "
"the I<list> stops after first non-empty file. For example:"
msgstr ""
"Indiquer une liste de fichiers séparés par des deux-points dans le format "
"mountinfo du noyau ou de fstab. Tous les fichiers manquants ou vides sont "
"ignorés silencieusement. L’évaluation de la I<liste> s’arrête après le "
"premier fichier non vide. Par exemple :"

#. type: Plain text
#: debian-bookworm
msgid "B<--listed-in /etc/fstab:/proc/self/mountinfo>."
msgstr "B<--listed-in /etc/fstab:/proc/self/mountinfo>."

#. type: Plain text
#: debian-bookworm
msgid "Filesystems with \"X-fstrim.notrim\" mount option in fstab are skipped."
msgstr ""
"Les systèmes de fichiers avec l'option de montage « X-fstrim.notrim » sont "
"sautés."

#. type: Plain text
#: debian-bookworm
msgid "B<-m, --minimum> I<minimum-size>"
msgstr "B<-m>, B<--minimum> I<taille_mini>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Minimum contiguous free range to discard, in bytes. (This value is "
"internally rounded up to a multiple of the filesystem block size.) Free "
"ranges smaller than this will be ignored and B<fstrim> will adjust the "
"minimum if it\\(cqs smaller than the device\\(cqs minimum, and report that "
"(fstrim_range.minlen) back to userspace. By increasing this value, the "
"B<fstrim> operation will complete more quickly for filesystems with badly "
"fragmented freespace, although not all blocks will be discarded. The default "
"value is zero, discarding every free block."
msgstr ""
"L'intervalle libre contigu minimal à abandonner, en octet (cette valeur est "
"arrondie en interne à un multiple de la taille de bloc du système de "
"fichiers). Les intervalles libres plus petits que cela seront ignorés et "
"B<fstrim> ajustera la valeur minimale si elle est plus petite que le minimum "
"du périphérique et le rapportera (fstrim_range.minlen) dans l’espace "
"utilisateur. En augmentant cette valeur, l'opération B<fstrim> se terminera "
"plus rapidement pour les systèmes de fichiers avec des espaces libres mal "
"fragmentés, même si tous les blocs libres ne sont pas abandonnés. La valeur "
"par défaut est zéro, pour abandonner tous les blocs libres."

#. type: Plain text
#: debian-bookworm
msgid "B<-v, --verbose>"
msgstr "B<-v, --verbose>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Verbose execution. With this option B<fstrim> will output the number of "
"bytes passed from the filesystem down the block stack to the device for "
"potential discard. This number is a maximum discard amount from the storage "
"device\\(cqs perspective, because I<FITRIM> ioctl called repeated will keep "
"sending the same sectors for discard repeatedly."
msgstr ""
"Exécution détaillée. Avec cette option, B<fstrim> affichera le nombre "
"d'octets transmis du système de fichiers par la pile de bloc vers le "
"périphérique pour abandon éventuel. Ce nombre est un montant maximal "
"d'abandon d'un point de vue du périphérique de stockage, parce que l'ioctl "
"I<FITRIM> appelé plusieurs fois continuera à répéter l'envoi des mêmes "
"secteurs pour abandon."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fstrim> will report the same potential discard bytes each time, but only "
"sectors which had been written to between the discards would actually be "
"discarded by the storage device. Further, the kernel block layer reserves "
"the right to adjust the discard ranges to fit raid stripe geometry, non-trim "
"capable devices in a LVM setup, etc. These reductions would not be reflected "
"in fstrim_range.len (the B<--length> option)."
msgstr ""
"B<fstrim> signalera à chaque fois les mêmes octets à abandonner, mais seuls "
"les secteurs sur lesquels une écriture a eu lieu entre les abandons seront "
"vraiment abandonnés par le périphérique de stockage. De plus, la couche bloc "
"du noyau se réserve le droit d'ajuster les intervalles d'abandon pour "
"s'ajuster à la géométrie des bandes RAID, aux périphériques ne pouvant pas "
"être rognés dans une configuration LVM, etc. Ces réductions ne seront pas "
"reflétées par fstrim_range.len (l'option B<--length>)."

#. type: Plain text
#: debian-bookworm
msgid "B<--quiet-unsupported>"
msgstr "B<--quiet-unsupported>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Suppress error messages if trim operation (ioctl) is unsupported. This "
"option is meant to be used in B<systemd> service file or in B<cron>(8) "
"scripts to hide warnings that are result of known problems, such as NTFS "
"driver reporting I<Bad file descriptor> when device is mounted read-only, or "
"lack of file system support for ioctl I<FITRIM> call. This option also "
"cleans exit status when unsupported filesystem specified on B<fstrim> "
"command line."
msgstr ""
"Supprimer les messages d’erreur si l’opération de rognage (ioctl) n’est pas "
"gérée. Cette option est destinée à une utilisation dans un fichier de "
"service systemd ou dans des scripts cron pour cacher les avertissements qui "
"sont le résultat de problèmes connus, tels que le pilote NTFS signalant "
"I<Mauvais descripteur de fichier> lorsque le périphérique est monté en "
"lecture seule ou l’absence de prise en charge du système de fichiers de "
"l’appel de l’ioctl FITRIM. Cette option nettoie aussi le code de retour "
"quand un système de fichiers non pris en charge est spécifié sur la ligne de "
"commande de B<fstrim>."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Afficher la version puis quitter."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr "CODE DE RETOUR"

#. type: Plain text
#: debian-bookworm
msgid "0"
msgstr "0"

#. type: Plain text
#: debian-bookworm
msgid "success"
msgstr "Succès."

#. type: Plain text
#: debian-bookworm
msgid "1"
msgstr "1"

#. type: Plain text
#: debian-bookworm
msgid "failure"
msgstr "échec"

#. type: Plain text
#: debian-bookworm
msgid "32"
msgstr "32"

#. type: Plain text
#: debian-bookworm
msgid "all failed"
msgstr "Tout a échoué."

#. type: Plain text
#: debian-bookworm
msgid "64"
msgstr "64"

#. type: Plain text
#: debian-bookworm
msgid "some filesystem discards have succeeded, some failed"
msgstr ""
"Certains abandons sur le système de fichiers ont réussi, d’autres ont "
"échoués."

#. type: Plain text
#: debian-bookworm
msgid ""
"The command B<fstrim --all> returns 0 (all succeeded), 32 (all failed) or 64 "
"(some failed, some succeeded)."
msgstr ""
"La commande B<fstrim --all> renvoie B<0> (tous ont réussi), B<32> (tous ont "
"échoué) ou B<64> (certains ont réussi, d’autres ont échoué)."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr "AUTEURS"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm
msgid "B<blkdiscard>(8), B<mount>(8)"
msgstr "B<blkdiscard>(8), B<mount>(8)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pour envoyer un rapport de bogue, utilisez le système de gestion des "
"problèmes à l'adresse"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<fstrim> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"La commande B<fstrim> fait partie du paquet util-linux, qui peut être "
"téléchargé de"
