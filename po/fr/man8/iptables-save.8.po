# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Lucien Gentis <lucien.gentis@waika9.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr\n"
"POT-Creation-Date: 2024-12-06 18:02+0100\n"
"PO-Revision-Date: 2024-01-30 08:46+0100\n"
"Last-Translator: Lucien Gentis <lucien.gentis@waika9.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: vim-gtk3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "IPTABLES-SAVE"
msgstr "IPTABLES-SAVE"

#. type: TH
#: archlinux
#, no-wrap
msgid "iptables 1.8.10"
msgstr "iptables 1.8.10"

#.  Man page written by Harald Welte <laforge@gnumonks.org>
#.  It is based on the iptables man page.
#. 	This program is free software; you can redistribute it and/or modify
#. 	it under the terms of the GNU General Public License as published by
#. 	the Free Software Foundation; either version 2 of the License, or
#. 	(at your option) any later version.
#. 	This program is distributed in the hope that it will be useful,
#. 	but WITHOUT ANY WARRANTY; without even the implied warranty of
#. 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. 	GNU General Public License for more details.
#. 	You should have received a copy of the GNU General Public License
#. 	along with this program; if not, write to the Free Software
#. 	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "iptables-save \\(em dump iptables rules"
msgstr "iptables-save — Extraire les règles iptables"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "ip6tables-save \\(em dump iptables rules"
msgstr "ip6tables-save — Extraire les règles iptables"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<iptables-save> [B<-M> I<modprobe>] [B<-c>] [B<-t> I<table>] [B<-f> "
"I<filename>]"
msgstr ""
"B<iptables-save> [B<-M> I<chemin_modprobe>] [B<-c>] [B<-t> I<table>] [B<-f> "
"I<nom_fichier>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<ip6tables-save> [B<-M> I<modprobe>] [B<-c>] [B<-t> I<table>] [B<-f> "
"I<filename>]"
msgstr ""
"B<ip6tables-save> [B<-M> I<chemin_modprobe>] [B<-c>] [B<-t> I<table>] [B<-f> "
"I<nom_fichier>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"B<iptables-save> and B<ip6tables-save> are used to dump the contents of IP "
"or IPv6 Table in easily parseable format either to STDOUT or to a specified "
"file."
msgstr ""
"B<iptables-save> et B<ip6tables-save> permettent d'extraire le contenu des "
"tables IP ou IPv6 dans un format facilement analysable vers la sortie "
"standard ou vers le fichier indiqué."

#. type: TP
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-M>, B<--modprobe> I<modprobe>"
msgstr "B<-M>, B<--modprobe> I<chemin_modprobe>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"Specify the path to the modprobe(8) program. By default, iptables-save will "
"inspect I</proc/sys/kernel/modprobe> to determine the executable's path."
msgstr ""
"Définir le chemin du programme B<modprobe>(8). Par défaut, iptables-save "
"consulte I</proc/sys/kernel/modprobe> pour déterminer le chemin de "
"l'exécutable."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--file> I<filename>"
msgstr "B<-f>, B<--file> I<nom_fichier>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Specify a filename to log the output to. If not specified, iptables-save "
"will log to STDOUT."
msgstr ""
"Indiquer le nom du fichier dans lequel sera enregistrée la sortie. S'il "
"n'est pas indiqué, iptables-save redirigera la sortie vers la sortie "
"standard."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--counters>"
msgstr "B<-c>, B<--counters>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"Include the current values of all packet and byte counters in the output."
msgstr ""
"Inclure les valeurs actuelles de tous les compteurs de paquets et d'octets "
"dans la sortie."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--table> I<tablename>"
msgstr "B<-t>, B<--table> I<nom_table>"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"Restrict output to only one table. If the kernel is configured with "
"automatic module loading, an attempt will be made to load the appropriate "
"module for that table if it is not already there."
msgstr ""
"Restreindre la sortie aux règles concernant la table indiquée. Si le noyau "
"est configuré pour charger automatiquement les modules, il tentera de "
"charger le module approprié à cette table, s'il ne l'a pas déjà été."

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"If not specified, output includes all available tables. No module loading "
"takes place, so in order to include a specific table in the output, the "
"respective module (something like B<iptable_mangle> or B<ip6table_raw>) must "
"be loaded first."
msgstr ""
"Si cette option n'est pas spécifiée, la sortie inclut toutes les tables "
"disponibles. Aucun chargement de module n'a lieu et, pour inclure une table "
"particulière dans la sortie, il faut donc charger au préalable le module "
"correspondant (quelque chose comme B<iptable_mangle> ou B<ip6table_raw>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "None known as of iptables-1.2.1 release"
msgstr "Aucun bogue connu au moment de la distribution iptables-1.2.1."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTEURS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Harald Welte E<lt>laforge@gnumonks.orgE<gt>"
msgstr "Harald Welte E<lt>laforge@gnumonks.orgE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Rusty Russell E<lt>rusty@rustcorp.com.auE<gt>"
msgstr "Rusty Russell E<lt>rusty@rustcorp.com.auE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "Andras Kis-Szabo E<lt>kisza@sch.bme.huE<gt> contributed ip6tables-save."
msgstr ""
"Andras Kis-Szabo E<lt>kisza@sch.bme.huE<gt> a contribué à ip6tables-save."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "B<iptables-apply>(8), B<iptables-restore>(8), B<iptables>(8)"
msgstr "B<iptables-apply>(8), B<iptables-restore>(8), B<iptables>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"The iptables-HOWTO, which details more iptables usage, the NAT-HOWTO, which "
"details NAT, and the netfilter-hacking-HOWTO which details the internals."
msgstr ""
"Le iptables-HOWTO qui entre plus en détails dans l'utilisation d'iptables, "
"le NAT-HOWTO qui détaille le NAT et le netfilter-hacking-HOWTO qui détaille "
"le fonctionnement interne."

#. type: TH
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "iptables 1.8.9"
msgstr "iptables 1.8.9"

#. type: TP
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "B<-M>, B<--modprobe> I<modprobe_program>"
msgstr "B<-M>, B<--modprobe> I<chemin_modprobe>"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Specify the path to the modprobe program. By default, iptables-save will "
"inspect /proc/sys/kernel/modprobe to determine the executable's path."
msgstr ""
"Définir le chemin du programme modprobe. Par défaut, iptables-save "
"consultera /proc/sys/kernel/modprobe pour déterminer le chemin de "
"l'exécutable."

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"include the current values of all packet and byte counters in the output"
msgstr ""
"Inclure les valeurs actuelles de tous les compteurs de paquets et d'octets "
"dans la sortie."

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"restrict output to only one table. If the kernel is configured with "
"automatic module loading, an attempt will be made to load the appropriate "
"module for that table if it is not already there."
msgstr ""
"Restreindre la sortie aux règles concernant la table indiquée. Si le noyau "
"est configuré pour charger automatiquement les modules, il tentera de "
"charger le module approprié à cette table, s'il ne l'a pas déjà été."

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "If not specified, output includes all available tables."
msgstr ""
"Si la table n'est pas indiquée, la sortie inclut les règles concernant "
"toutes les tables."

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "B<iptables-apply>(8),B<iptables-restore>(8), B<iptables>(8)"
msgstr "B<iptables-apply>(8), B<iptables-restore>(8), B<iptables>(8)"

#. type: TH
#: debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "iptables 1.8.11"
msgstr "iptables 1.8.11"
