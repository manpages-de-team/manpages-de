# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: 2023-10-07 06:51+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-FSTEST"
msgstr "GRUB-FSTEST"

#. type: TH
#: archlinux
#, no-wrap
msgid "February 2025"
msgstr "Février 2025"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12.r212.g4dc616657-2"
msgstr "GRUB 2:2.12.r212.g4dc616657-2"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-fstest - debug tool for GRUB filesystem drivers"
msgstr ""
"grub-fstest – Outil de débogage pour les pilotes du système de fichiers"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-fstest> [I<\\,OPTION\\/>...] I<\\,IMAGE_PATH COMMANDS\\/>"
msgstr "B<grub-fstest> [I<\\,OPTION\\/>...] I<\\,CHEMIN_IMAGE COMMANDES\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Debug tool for filesystem driver."
msgstr "Outil de débogage pour le pilote du système de fichiers."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Commands:"
msgstr "Commandes :"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "blocklist FILE"
msgstr "blocklist FICHIER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Display blocklist of FILE."
msgstr "Afficher la liste de blocages de FICHIER."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "cat FILE"
msgstr "cat FICHIER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Copy FILE to standard output."
msgstr "Copier FICHIER sur la sortie standard."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "cmp FILE LOCAL"
msgstr "cmp FICHIER LOCAL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Compare FILE with local file LOCAL."
msgstr "Comparer FICHIER avec le fichier local LOCAL."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "cp FILE LOCAL"
msgstr "cp FICHIER LOCAL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Copy FILE to local file LOCAL."
msgstr "Copier FILE dans le fichier local LOCAL."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "crc FILE"
msgstr "crc FICHIER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Get crc32 checksum of FILE."
msgstr "Déterminer la somme de contrôle crc32 de FICHIER."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "hex FILE"
msgstr "hex FICHIER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Show contents of FILE in hex."
msgstr "Afficher le contenu du FICHIER en hexadécimal."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "ls PATH"
msgstr "ls CHEMIN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "List files in PATH."
msgstr "Lister les fichiers de CHEMIN."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "xnu_uuid DEVICE"
msgstr "xnu_uuid PÉRIPHÉRIQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Compute XNU UUID of the device."
msgstr "Calculer l'UUID XNU du périphérique."

#. type: TP
#: archlinux
#, no-wrap
msgid "zfs-bootfs ZFS_DATASET"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Compute ZFS dataset bootpath."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-c>, B<--diskcount>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--diskcount>=I<\\,NOMBRE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Specify the number of input files."
msgstr "Spécifier le nombre des fichiers d'entrée."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-C>, B<--crypto>"
msgstr "B<-C>, B<--crypto>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Mount crypto devices."
msgstr "Monter les périphériques chiffrés."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--debug>=I<\\,STRING\\/>"
msgstr "B<-d>, B<--debug>=I<\\,CHAÎNE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Set debug environment variable."
msgstr "Définir la variable d'environnement de débogage."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-K>, B<--zfs-key>=I<\\,FILE\\/>|prompt"
msgstr "B<-K>, B<--zfs-key>=I<\\,FICHIER\\/>|prompt"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Load zfs crypto key."
msgstr "Charger la clé de chiffrement zfs."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-n>, B<--length>=I<\\,NUM\\/>"
msgstr "B<-n>, B<--length>=I<\\,NOMBRE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Handle N bytes in output file."
msgstr "Traiter I<N> octets dans le fichier de sortie."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-r>, B<--root>=I<\\,DEVICE_NAME\\/>"
msgstr "B<-r>, B<--root>=I<\\,NOM_PÉRIPHÉRIQUE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Set root device."
msgstr "Établir un périphérique racine."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-s>, B<--skip>=I<\\,NUM\\/>"
msgstr "B<-s>, B<--skip>=I<\\,NOMBRE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Skip N bytes from output file."
msgstr "Sauter I<N> octets du fichier de sortie."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-u>, B<--uncompress>"
msgstr "B<-u>, B<--uncompress>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Uncompress data."
msgstr "Décompresser les données."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "Afficher des messages détaillés."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "Afficher l’aide-mémoire."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "Afficher un court message pour l’utilisation."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "Afficher la version du programme."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Les paramètres obligatoires ou facultatifs pour les options de forme longue "
"le sont aussi pour les options correspondantes de forme courte."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-probe>(8)"
msgstr "B<grub-probe>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-fstest> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-fstest> programs are properly installed "
"at your site, the command"
msgstr ""
"La documentation complète de B<grub-fstest> est disponible dans un manuel "
"Texinfo. Si les programmes B<info> et B<grub-fstest> sont correctement "
"installés, la commande"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-fstest>"
msgstr "B<info grub-fstest>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "devrait vous donner accès au manuel complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2024"
msgstr "Février 2024"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.12-1~bpo12+1"
msgstr "GRUB 2.12-1~bpo12+1"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "July 2024"
msgstr "Juillet 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-5"
msgstr "GRUB 2.12-5"
