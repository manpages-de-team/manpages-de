# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-28 16:33+0100\n"
"PO-Revision-Date: 2024-06-11 12:31+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

# Pas sûr de l'emplacement de la chaîne, vient probablement de Netpbm
#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "filesystems"
msgstr "filesystems"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pages du manuel de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"filesystems - Linux filesystem types: ext, ext2, ext3, ext4, hpfs, iso9660, "
"JFS, minix, msdos, ncpfs nfs, ntfs, proc, Reiserfs, smb, sysv, umsdos, vfat, "
"XFS, xiafs"
msgstr ""
"filesystems – Types des systèmes de fichiers sous Linux : ext, ext2, ext3, "
"ext4, hpfs, iso9660, JFS, minix, msdos, ncpfs nfs, ntfs, proc, Reiserfs, "
"smb, sysv, umsdos, vfat, XFS, xiafs"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#.  commit: 6af9f7bf3c399e0ab1eee048e13572c6d4e15fe9
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"When, as is customary, the B<proc> filesystem is mounted on I</proc>, you "
"can find in the file I</proc/filesystems> which filesystems your kernel "
"currently supports; see B<proc>(5)  for more details.  There is also a "
"legacy B<sysfs>(2)  system call (whose availability is controlled by the "
"B<CONFIG_SYSFS_SYSCALL> kernel build configuration option since Linux 3.15)  "
"that enables enumeration of the currently available filesystem types "
"regardless of I</proc> availability and/or sanity."
msgstr ""
"Lorsque, comme il est d'usage, le système de fichiers B<proc> est monté sur "
"I</proc>, vous pouvez trouver dans le fichier I</proc/filesystems> la liste "
"des systèmes de fichiers gérés par le noyau actuel ; consultez B<proc>(5) "
"pour plus de précisions. Il existe aussi un appel système ancien B<sysfs>(2) "
"(dont la disponibilité est contrôlée depuis Linux 3.15 par l'option de "
"configuration de construction du noyau B<CONFIG_SYSFS_SYSCALL>) qui permet "
"l'inventaire des types de système de fichiers actuellement disponibles quels "
"que soit la disponibilité et l'état de I</proc>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you need a currently unsupported filesystem, insert the corresponding "
"kernel module or recompile the kernel."
msgstr ""
"Si vous avez besoin d'utiliser un système de fichiers actuellement non géré, "
"vous pouvez recompiler le noyau ou y insérer le module correspondant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In order to use a filesystem, you have to I<mount> it; see B<mount>(2)  and "
"B<mount>(8)."
msgstr ""
"Pour pouvoir utiliser un système de fichiers, vous devez le I<monter> ; "
"consultez B<mount>(2) et B<mount>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The following list provides a short description of the available or "
"historically available filesystems in the Linux kernel.  See the kernel "
"documentation for a comprehensive description of all options and limitations."
msgstr ""
"La liste suivante fournit une courte description des systèmes de fichiers "
"disponibles ou anciennement disponibles dans le noyau Linux. Pour une "
"description complète de toutes leurs options et de leurs limitations, "
"consultez la documentation du noyau."

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<erofs>"
msgstr "B<erofs>"

#.  commit 47e4937a4a7ca4184fd282791dfee76c6799966a moves it out of staging
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the Enhanced Read-Only File System, stable since Linux 5.4.  See "
"B<erofs>(5)."
msgstr ""
"est le système de fichiers avancé en lecture seule (Enhanced Read-Only File "
"System), stable depuis Linux 5.4. Voir B<erofs>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ext>"
msgstr "B<ext>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is an elaborate extension of the B<minix> filesystem.  It has been "
"completely superseded by the second version of the extended filesystem "
"(B<ext2>)  and has been removed from the kernel (in Linux 2.1.21)."
msgstr ""
"est une extension perfectionnée du système de fichiers B<minix>. Il a été "
"complètement remplacé par la seconde version du système de fichiers étendu "
"(B<ext2>) et supprimé du noyau (dans Linux 2.1.21)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ext2>"
msgstr "B<ext2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a disk filesystem that was used by Linux for fixed disks as well as "
"removable media.  The second extended filesystem was designed as an "
"extension of the extended filesystem (B<ext>).  See B<ext2>(5)."
msgstr ""
"est un système de fichiers pour disque, utilisé par Linux pour les disques "
"fixes et pour les supports amovibles. Le second système de fichiers étendu a "
"été conçu comme une extension du système de fichiers étendu (B<ext>). Voir "
"B<ext2>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ext3>"
msgstr "B<ext3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a journaling version of the B<ext2> filesystem.  It is easy to switch "
"back and forth between B<ext2> and B<ext3>.  See B<ext3>(5)."
msgstr ""
"est une version du système de fichiers B<ext2> gérant la journalisation. On "
"peut facilement basculer d'B<ext2> à B<ext3>, et inversement. Voir "
"B<ext3>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ext4>"
msgstr "B<ext4>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a set of upgrades to B<ext3> including substantial performance and "
"reliability enhancements, plus large increases in volume, file, and "
"directory size limits.  See B<ext4>(5)."
msgstr ""
"est un ensemble de mises à jour d'B<ext3> qui apporte des améliorations "
"notables en terme de performance et de fiabilité, ainsi qu'une augmentation "
"importante des limites des tailles de volume, fichier et répertoire. Voir "
"B<ext4>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<hpfs>"
msgstr "B<hpfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the High Performance Filesystem, used in OS/2.  This filesystem is read-"
"only under Linux due to the lack of available documentation."
msgstr ""
"est le système de fichiers « High Performance Filesystem » utilisé par OS/2. "
"Ce système de fichiers est en lecture seule sous Linux en raison du manque "
"de documentation disponible."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<iso9660>"
msgstr "B<iso9660>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "is a CD-ROM filesystem type conforming to the ISO/IEC\\ 9660 standard."
msgstr ""
"est un type de système de fichiers pour CD-ROM, conforme à la norme ISO/"
"IEC\\ 9660."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<High Sierra>"
msgstr "B<High Sierra>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Linux supports High Sierra, the precursor to the ISO/IEC\\ 9660 standard for "
"CD-ROM filesystems.  It is automatically recognized within the B<iso9660> "
"filesystem support under Linux."
msgstr ""
"Linux gère le format « High Sierra », précurseur de la norme ISO/IEC\\ 9660 "
"pour systèmes de fichiers des CD-ROM. Ce format est automatiquement reconnu "
"par le gestionnaire du système de fichiers B<iso9660> de Linux."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<Rock Ridge>"
msgstr "B<Rock Ridge>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Linux also supports the System Use Sharing Protocol records specified by the "
"Rock Ridge Interchange Protocol.  They are used to further describe the "
"files in the B<iso9660> filesystem to a UNIX host, and provide information "
"such as long filenames, UID/GID, POSIX permissions, and devices.  It is "
"automatically recognized within the B<iso9660> filesystem support under "
"Linux."
msgstr ""
"Linux gère également les enregistrements au format « System Use Sharing "
"Protocol » spécifié par le « Rock Ridge Interchange Protocol ». Ils sont "
"utilisés pour décrire plus en profondeur les fichiers contenus dans un "
"système de fichiers B<iso9660> à destination d'un hôte UNIX, et pour fournir "
"des informations telles que les noms de fichiers longs, les UID/GID, les "
"autorisations POSIX et les fichiers de périphériques. Ce format est "
"automatiquement reconnu par le gestionnaire du système de fichiers "
"B<iso9660> de Linux."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<JFS>"
msgstr "B<JFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a journaling filesystem, developed by IBM, that was integrated into Linux "
"2.4.24."
msgstr ""
"est un système de fichiers journalisé, développé par IBM, qui a été intégré "
"dans Linux 2.4.24."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<minix>"
msgstr "B<minix>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the filesystem used in the Minix operating system, the first to run under "
"Linux.  It has a number of shortcomings, including a 64\\ MB partition size "
"limit, short filenames, and a single timestamp.  It remains useful for "
"floppies and RAM disks."
msgstr ""
"est le système de fichiers utilisé par le système d'exploitation Minix, le "
"premier à avoir fonctionné sous Linux. Il a de nombreuses limitations : un "
"maximum de 64 Mo par partition, des noms de fichiers courts, un seul "
"horodatage, etc. Néanmoins, il reste très appréciable pour les disquettes et "
"les disques en mémoire vive."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<msdos>"
msgstr "B<msdos>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the filesystem used by DOS, Windows, and some OS/2 computers.  B<msdos> "
"filenames can be no longer than 8 characters, followed by an optional period "
"and 3 character extension."
msgstr ""
"est le système de fichiers utilisé sous DOS, Windows et certains ordinateurs "
"sous OS/2. Les noms de fichiers sous B<msdos> sont limités à 8\\ caractères "
"suivis éventuellement d'un point et jusqu'à 3 caractères d'extension."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ncpfs>"
msgstr "B<ncpfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a network filesystem that supports the NCP protocol, used by Novell "
"NetWare.  It was removed from the kernel in Linux 4.17."
msgstr ""
"est un système de fichiers par le réseau gérant le protocole NCP, utilisé "
"par NetWare de Novell. Il a été supprimé du noyau dans Linux 4.17."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To use B<ncpfs>, you need special programs, which can be found at E<.UR "
"ftp://ftp.gwdg.de\\:/pub\\:/linux\\:/misc\\:/ncpfs> E<.UE .>"
msgstr ""
"Pour utiliser le système B<ncpfs>, il vous faut des programmes spéciaux "
"disponibles à E<.UR ftp://ftp.gwdg.de\\:/pub\\:/linux\\:/misc\\:/ncpfs> E<."
"UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<nfs>"
msgstr "B<nfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the network filesystem used to access disks located on remote computers."
msgstr ""
"est le système de fichiers par le réseau utilisé pour accéder à des disques "
"situés sur des ordinateurs distants."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ntfs>"
msgstr "B<ntfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is the filesystem native to Microsoft Windows NT, supporting features like "
"ACLs, journaling, encryption, and so on."
msgstr ""
"est le système de fichiers natif de Windows NT de Microsoft, gérant des "
"fonctionnalités telles que les ACL, la journalisation, le chiffrement et "
"d'autres encore."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<proc>"
msgstr "B<proc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a pseudo filesystem which is used as an interface to kernel data "
"structures rather than reading and interpreting I</dev/kmem>.  In "
"particular, its files do not take disk space.  See B<proc>(5)."
msgstr ""
"est un pseudosystème de fichiers qui est utilisé comme interface avec les "
"structures de données du noyau plutôt que de lire et interpréter I</dev/"
"kmem>. En particulier, ses fichiers n'occupent aucun espace disque. "
"Consultez B<proc>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<Reiserfs>"
msgstr "B<Reiserfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a journaling filesystem, designed by Hans Reiser, that was integrated "
"into Linux 2.4.1."
msgstr ""
"est un système de fichiers journalisé, conçu par Hans Reiser, qui a été "
"intégré dans Linux 2.4.1."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<smb>"
msgstr "B<smb>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a network filesystem that supports the SMB protocol, used by Windows.  "
"See E<.UR https://www.samba.org\\:/samba\\:/smbfs/> E<.UE .>"
msgstr ""
"est un système de fichiers par le réseau gérant le protocole SMB, utilisé "
"par Windows. Voir E<.UR https://www.samba.org\\:/samba\\:/smbfs/> E<.UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<sysv>"
msgstr "B<sysv>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is an implementation of the System V/Coherent filesystem for Linux.  It "
"implements all of Xenix FS, System V/386 FS, and Coherent FS."
msgstr ""
"est une implémentation du système de fichiers SystemV/Coherent pour Linux. "
"Il implémente tous les systèmes de fichiers de Xenix, System V/386 et "
"Coherent."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<umsdos>"
msgstr "B<umsdos>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is an extended DOS filesystem used by Linux.  It adds capability for long "
"filenames, UID/GID, POSIX permissions, and special files (devices, named "
"pipes, etc.) under the DOS filesystem, without sacrificing compatibility "
"with DOS."
msgstr ""
"est un système de fichiers DOS étendu utilisé par Linux. Il ajoute la "
"possibilité d'utiliser des noms de fichier longs, les UID/GID, les "
"autorisations POSIX, les fichiers spéciaux (périphériques, tubes nommés,\\ "
"etc.) au système de fichiers DOS, sans sacrifier la compatibilité avec celui-"
"ci."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<tmpfs>"
msgstr "B<tmpfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a filesystem whose contents reside in virtual memory.  Since the files on "
"such filesystems typically reside in RAM, file access is extremely fast.  "
"See B<tmpfs>(5)."
msgstr ""
"est un système de fichiers dont le contenu réside en mémoire virtuelle. Dans "
"la mesure où les fichiers de ce type de système de fichiers résident en "
"mémoire vive, l'accès aux fichiers est extrêmement rapide. Voir B<tmpfs>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<vfat>"
msgstr "B<vfat>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is an extended FAT filesystem used by Microsoft Windows95 and Windows NT.  "
"B<vfat> adds the capability to use long filenames under the MSDOS filesystem."
msgstr ""
"est un système de fichiers FAT étendu utilisé par Windows95 et Windows NT de "
"Microsoft. B<vfat> ajoute la possibilité d'utiliser des noms de fichiers "
"longs au système de fichiers MSDOS."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<XFS>"
msgstr "B<XFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"is a journaling filesystem, developed by SGI, that was integrated into Linux "
"2.4.20."
msgstr ""
"est un système de fichiers journalisé, développé par SGI, qui a été intégré "
"dans Linux 2.4.20"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<xiafs>"
msgstr "B<xiafs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"was designed and implemented to be a stable, safe filesystem by extending "
"the Minix filesystem code.  It provides the basic most requested features "
"without undue complexity.  The B<xiafs> filesystem is no longer actively "
"developed or maintained.  It was removed from the kernel in Linux 2.1.21."
msgstr ""
"a été conçu et implémenté pour être un système de fichiers sûr et stable, en "
"étendant le code du système de fichiers Minix. Il procure les "
"fonctionnalités de base les plus recherchées sans complications excessives. "
"Le système de fichiers B<xiafs> n'est plus développé ni entretenu. Il a été "
"retiré du noyau dans Linux 2.1.21."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<fuse>(4), B<btrfs>(5), B<ext2>(5), B<ext3>(5), B<ext4>(5), B<nfs>(5), "
"B<proc>(5), B<sysfs>(5), B<tmpfs>(5), B<xfs>(5), B<fsck>(8), B<mkfs>(8), "
"B<mount>(8)"
msgstr ""
"B<fuse>(4), B<btrfs>(5), B<ext2>(5), B<ext3>(5), B<ext4>(5), B<nfs>(5), "
"B<proc>(5), B<sysfs>(5), B<tmpfs>(5), B<xfs>(5), B<fsck>(8), B<mkfs>(8), "
"B<mount>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-05"
msgstr "5 décembre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "is a CD-ROM filesystem type conforming to the ISO 9660 standard."
msgstr "est un système de fichiers pour CD-ROM, conforme à la norme ISO 9660."

#. type: Plain text
#: debian-bookworm
msgid ""
"Linux supports High Sierra, the precursor to the ISO 9660 standard for CD-"
"ROM filesystems.  It is automatically recognized within the B<iso9660> "
"filesystem support under Linux."
msgstr ""
"Linux gère le format « High Sierra », précurseur de la norme ISO 9660 pour "
"les CD-ROM. Ce format est automatiquement reconnu par le gestionnaire du "
"système de fichiers B<iso9660> de Linux."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2024-01-28"
msgstr "28 janvier 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
