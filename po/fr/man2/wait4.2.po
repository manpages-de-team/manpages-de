# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012, 2013.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011, 2012, 2013.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2022-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:56+0100\n"
"PO-Revision-Date: 2024-01-23 19:18+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "wait4"
msgstr "wait4"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 juillet 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pages du manuel de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "wait3, wait4 - wait for process to change state, BSD style"
msgstr "wait3, wait4 - Attendre la fin d'un processus - Style BSD"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>,\\ I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/wait.hE<gt>>\n"
msgstr "B<#include E<lt>sys/wait.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pid_t wait3(int *_Nullable >I<wstatus>B<, int >I<options>B<,>\n"
"B<            struct rusage *_Nullable >I<rusage>B<);>\n"
"B<pid_t wait4(pid_t >I<pid>B<, int *_Nullable >I<wstatus>B<, int >I<options>B<,>\n"
"B<            struct rusage *_Nullable >I<rusage>B<);>\n"
msgstr ""
"B<pid_t wait3(int *_Nullable >I<wstatus>B<, int >I<options>B<,>\n"
"B<            struct rusage *_Nullable >I<rusage>B<);>\n"
"B<pid_t wait4(pid_t >I<pid>B<, int *_Nullable >I<wstatus>B<, int >I<options>B<,>\n"
"B<            struct rusage *_Nullable >I<rusage>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<wait3>():"
msgstr "B<wait3>() :"

#.           || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.26:\n"
"        _DEFAULT_SOURCE\n"
"            || (_XOPEN_SOURCE E<gt>= 500 &&\n"
"                ! (_POSIX_C_SOURCE E<gt>= 200112L\n"
"                   || _XOPEN_SOURCE E<gt>= 600))\n"
"    From glibc 2.19 to glibc 2.25:\n"
"        _DEFAULT_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"    Depuis la glibc 2.26 :\n"
"        _DEFAULT_SOURCE\n"
"            || (_XOPEN_SOURCE E<gt>= 500 &&\n"
"                ! (_POSIX_C_SOURCE E<gt>= 200112L\n"
"                   || _XOPEN_SOURCE E<gt>= 600))\n"
"    Pour la glibc 2.19 à la glibc 2.25 :\n"
"        _DEFAULT_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
"    Pour la glibc antérieure et égale à 2.19 :\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<wait4>():"
msgstr "B<wait4>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.19 :\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 et antérieures :\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions are nonstandard; in new programs, the use of B<waitpid>(2)  "
"or B<waitid>(2)  is preferable."
msgstr ""
"Ces fonctions ne sont pas standard, les appels systèmes B<waitpid>(2) ou "
"B<waitid>(2) doivent être utilisés dans les nouveaux programmes."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<wait3>()  and B<wait4>()  system calls are similar to B<waitpid>(2), "
"but additionally return resource usage information about the child in the "
"structure pointed to by I<rusage>."
msgstr ""
"Les appels système B<wait3>() et B<wait4>() sont similaires à B<waitpid>(2), "
"mais retournent également des informations sur l'utilisation des ressources "
"concernant l'enfant dans la structure pointée par I<rusage>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Other than the use of the I<rusage> argument, the following B<wait3>()  call:"
msgstr ""
"À part l'utilisation de l'argument I<rusage>, l'appel B<wait3>() suivant\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "wait3(wstatus, options, rusage);\n"
msgstr "wait3(wstatus, options, rusage);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "is equivalent to:"
msgstr "est équivalent à\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "waitpid(-1, wstatus, options);\n"
msgstr "waitpid(-1, wstatus, options);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Similarly, the following B<wait4>()  call:"
msgstr "De même, l'appel B<wait4>() suivant\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "wait4(pid, wstatus, options, rusage);\n"
msgstr "wait4(pid, wstatus, options, rusage);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "waitpid(pid, wstatus, options);\n"
msgstr "waitpid(pid, wstatus, options);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In other words, B<wait3>()  waits of any child, while B<wait4>()  can be "
"used to select a specific child, or children, on which to wait.  See "
"B<wait>(2)  for further details."
msgstr ""
"En d'autres termes, B<wait3>() attend n'importe quel enfant et B<wait4>() "
"peut être utilisé pour sélectionner un ou des enfants spécifique(s) à "
"attendre. Consultez B<wait>(2) pour plus de détails."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If I<rusage> is not NULL, the I<struct rusage> to which it points will be "
"filled with accounting information about the child.  See B<getrusage>(2)  "
"for details."
msgstr ""
"Si I<rusage> n'est pas NULL, la I<struct rusage> sur laquelle il pointe sera "
"remplie avec les informations de comptabilité concernant l'enfant. Consultez "
"B<getrusage>(2) pour plus de détails."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "As for B<waitpid>(2)."
msgstr "Comme pour B<waitpid>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "None."
msgstr "Aucun."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "4.3BSD."
msgstr "4.3BSD."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"SUSv1 included a specification of B<wait3>(); SUSv2 included B<wait3>(), but "
"marked it LEGACY; SUSv3 removed it."
msgstr ""
"SUSv1 spécifiait B<wait3>() ; SUSv2 spécifiait B<wait3>() en la marquant "
"obsolète ; SUSv3 a supprimé cette fonction."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Including I<E<lt>sys/time.hE<gt>> is not required these days, but increases "
"portability.  (Indeed, I<E<lt>sys/resource.hE<gt>> defines the I<rusage> "
"structure with fields of type I<struct timeval> defined in I<E<lt>sys/time."
"hE<gt>>.)"
msgstr ""
"L'inclusion de I<E<lt>sys/time.hE<gt>> n'est plus obligatoire, mais améliore "
"la portabilité. (En fait, I<E<lt>sys/resource.hE<gt>> définit la structure "
"I<rusage> qui contient des champs de type I<struct timeval>, défini dans "
"I<E<lt>sys/time.hE<gt>>.)"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Différences entre bibliothèque C et noyau"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On Linux, B<wait3>()  is a library function implemented on top of the "
"B<wait4>()  system call."
msgstr ""
"Sous Linux, B<wait3>() est une fonction de bibliothèque implémentée à l'aide "
"de l'appel système B<wait4>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<fork>(2), B<getrusage>(2), B<sigaction>(2), B<signal>(2), B<wait>(2), "
"B<signal>(7)"
msgstr ""
"B<fork>(2), B<getrusage>(2), B<sigaction>(2), B<signal>(2), B<wait>(2), "
"B<signal>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
