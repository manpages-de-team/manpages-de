# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2025-02-28 16:36+0100\n"
"PO-Revision-Date: 2023-01-13 23:54+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "intro"
msgstr "intro"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2 mai 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Pages du manuel de Linux 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "intro - introduction to system calls"
msgstr "intro - Introduction à la section des appels système"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Section 2 of the manual describes the Linux system calls.  A system call is "
"an entry point into the Linux kernel.  Usually, system calls are not invoked "
"directly: instead, most system calls have corresponding C library wrapper "
"functions which perform the steps required (e.g., trapping to kernel mode) "
"in order to invoke the system call.  Thus, making a system call looks the "
"same as invoking a normal library function."
msgstr ""
"La section 2 des manuels décrit les appels système Linux. Un appel système "
"est un point d'entrée dans le noyau Linux. Généralement, les appels système "
"ne sont pas invoqués directement : à la place, la plupart des appels système "
"ont des fonctions enveloppes correspondantes dans la bibliothèque C qui "
"effectuent les étapes nécessaires (par exemple, déroutement (trap) en mode "
"noyau) afin d'invoquer l'appel système. Ainsi, faire un appel système "
"ressemble à la même chose que d'invoquer une fonction de la bibliothèque."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "In many cases, the C library wrapper function does nothing more than:"
msgstr ""
"Dans bien des cas, la fonction enveloppe fournie par la bibliothèque C ne "
"fait rien deplus que :"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "-"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"copying arguments and the unique system call number to the registers where "
"the kernel expects them;"
msgstr ""
"copier les paramètres et le numéro unique d'appel système dans les registres "
"où le noyau les attend ;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"trapping to kernel mode, at which point the kernel does the real work of the "
"system call;"
msgstr ""
"déroutement (« trap ») en mode noyau, à la suite de quoi le noyau effectue "
"le travail concret de l'appel système ;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"setting I<errno> if the system call returns an error number when the kernel "
"returns the CPU to user mode."
msgstr ""
"positionner I<errno> si l'appel système a renvoyé un numéro d'erreur au "
"moment où le noyau repasse le CPU en mode utilisateur."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"However, in a few cases, a wrapper function may do rather more than this, "
"for example, performing some preprocessing of the arguments before trapping "
"to kernel mode, or postprocessing of values returned by the system call.  "
"Where this is the case, the manual pages in Section 2 generally try to note "
"the details of both the (usually GNU) C library API interface and the raw "
"system call.  Most commonly, the main DESCRIPTION will focus on the C "
"library interface, and differences for the system call are covered in the "
"NOTES section."
msgstr ""
"Cependant, dans certains cas, une fonction enveloppe peut faire plus que "
"cela, comme prétraiter les paramètres avant de passer en mode noyau, ou post-"
"traiter les valeurs renvoyées par l'appel système. Lorsque c'est le cas, les "
"pages de manuel de la section 2 essaient de décrire les détails à la fois de "
"l'interface de programmation de la bibliothèque C (généralement GNU) et de "
"l'appel système brut. Plus généralement, la section B<DESCRIPTION> de la "
"page se concentre sur l'interface de la bibliothèque C, puis les différences "
"de l'appel système sont décrites dans la section B<NOTES>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "For a list of the Linux system calls, see B<syscalls>(2)."
msgstr ""
"Pour avoir une liste des appels système de Linux, consultez la page "
"B<syscalls>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On error, most system calls return a negative error number (i.e., the "
"negated value of one of the constants described in B<errno>(3)).  The C "
"library wrapper hides this detail from the caller: when a system call "
"returns a negative value, the wrapper copies the absolute value into the "
"I<errno> variable, and returns -1 as the return value of the wrapper."
msgstr ""
"En cas d'erreur, la plupart des appels système renvoient une valeur d'erreur "
"négative (c'est-à-dire, la valeur opposée de l'une des constantes décrites "
"dans B<errno>(3)). La fonction enveloppe de la bibliothèque C cache ce "
"détail à l'appelant : lorsqu'un appel système renvoie une valeur négative, "
"la fonction enveloppe met dans la variable I<errno> la valeur absolue de "
"cette valeur de retour et renvoie -1 comme sa valeur de retour."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The value returned by a successful system call depends on the call.  Many "
"system calls return 0 on success, but some can return nonzero values from a "
"successful call.  The details are described in the individual manual pages."
msgstr ""
"La valeur renvoyée par un appel système réussi dépend de l'appel. Beaucoup "
"d'appels système renvoient 0 lorsqu'ils réussissent, mais certains renvoient "
"parfois une valeur non nulle dans ce cas. Les détails sont décrits dans leur "
"page de manuel associée."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In some cases, the programmer must define a feature test macro in order to "
"obtain the declaration of a system call from the header file specified in "
"the man page SYNOPSIS section.  (Where required, these feature test macros "
"must be defined before including I<any> header files.)  In such cases, the "
"required macro is described in the man page.  For further information on "
"feature test macros, see B<feature_test_macros>(7)."
msgstr ""
"Dans certains cas, le programmeur doit définir une macro de test de "
"fonctionnalités afin d'obtenir la déclaration d'un appel système du fichier "
"d'en-tête spécifié dans la section SYNOPSIS de la page de manuel (si elle "
"est nécessaire, cette macro de test de fonctionnalités doit être définie "
"avant d'inclure I<tout> fichier d'en\\(hytête). Dans de tels cas, la macro "
"nécessaire est décrite dans la page de manuel. Pour plus d'informations sur "
"les macros de test de fonctionnalités, consultez B<feature_test_macros>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Certain terms and abbreviations are used to indicate UNIX variants and "
"standards to which calls in this section conform.  See B<standards>(7)."
msgstr ""
"Certains termes et abréviations sont utilisés pour indiquer les variantes "
"d'UNIX ou les normes auxquelles les appels de cette section se conforment. "
"Consultez B<standards>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Calling directly"
msgstr "Appel Direct"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In most cases, it is unnecessary to invoke a system call directly, but there "
"are times when the Standard C library does not implement a nice wrapper "
"function for you.  In this case, the programmer must manually invoke the "
"system call using B<syscall>(2).  Historically, this was also possible using "
"one of the _syscall macros described in B<_syscall>(2)."
msgstr ""
"Dans la plupart des cas, il n'est pas nécessaire d'invoquer un appel système "
"directement, mais il arrive parfois que la bibliothèque standard de C "
"n'implémente pas une fonction pourtant utile. Dans ce cas, le programmeur "
"doit invoquer l'appel système à la main, en utilisant B<syscall>(2). "
"Auparavant il était aussi possible d'utiliser les macros _syscall, qui sont "
"décrites dans B<_syscall>(2)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Authors and copyright conditions"
msgstr "Auteurs et termes de droit d'auteur"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Look at the header of the manual page source for the author(s) and copyright "
"conditions.  Note that these can be different from page to page!"
msgstr ""
"Consultez les en-têtes de la source de chaque page de manuel pour connaître "
"le(s) auteur(s) et conditions de droit d'auteur. Ils peuvent être différents "
"selon les pages."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<_syscall>(2), B<syscall>(2), B<syscalls>(2), B<errno>(3), B<intro>(3), "
"B<capabilities>(7), B<credentials>(7), B<feature_test_macros>(7), "
"B<mq_overview>(7), B<path_resolution>(7), B<pipe>(7), B<pty>(7), "
"B<sem_overview>(7), B<shm_overview>(7), B<signal>(7), B<socket>(7), "
"B<standards>(7), B<symlink>(7), B<system_data_types>(7), B<sysvipc>(7), "
"B<time>(7)"
msgstr ""
"B<_syscall>(2), B<syscall>(2), B<syscalls>(2), B<errno>(3), B<intro>(3), "
"B<capabilities>(7), B<credentials>(7), B<feature_test_macros>(7), "
"B<mq_overview>(7), B<path_resolution>(7), B<pipe>(7), B<pty>(7), "
"B<sem_overview>(7), B<shm_overview>(7), B<signal>(7), B<socket>(7), "
"B<standards>(7), B<symlink>(7), B<system_data_types>(7), B<sysvipc>(7), "
"B<time>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Pages du manuel de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (non publiées)"
