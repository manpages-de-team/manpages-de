# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
# Marcelo Pereira da Silva <marcelo@pereira.com>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:56+0100\n"
"PO-Revision-Date: 2000-06-02 19:20-0300\n"
"Last-Translator: Marcelo Pereira da Silva <marcelo@pereira.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-"
"portuguese@lists.debian.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "vm86"
msgstr "vm86"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23 julho 2024"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "vm86old, vm86 - enter virtual 8086 mode"
msgstr "vm86old, vm86 - entra no modo virtual 8086"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid "Standard C library (I<libc>, I<-lc>)"
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<#include E<lt>sys/vm86.hE<gt>>"
msgid "B<#include E<lt>sys/vm86.hE<gt>>\n"
msgstr "B<#include E<lt>sys/vm86.hE<gt>>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int vm86(unsigned long >I<fn>B<, struct vm86plus_struct *>I<v86>B<);>"
msgid ""
"B<int vm86old(struct vm86_struct *>I<info>B<);>\n"
"B<int vm86(unsigned long >I<fn>B<, struct vm86plus_struct *>I<v86>B<);>\n"
msgstr "B<int vm86(unsigned long >I<fn>B<, struct vm86plus_struct *>I<v86>B<);>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The system call B<vm86> was introduced in Linux 0.97p2. In Linux 2.1.15 "
#| "and 2.0.28 it was renamed to B<vm86old>, and a new B<vm86> was "
#| "introduced. The definition of `struct vm86_struct' was changed in 1.1.8 "
#| "and 1.1.9."
msgid ""
"The system call B<vm86>()  was introduced in Linux 0.97p2.  In Linux 2.1.15 "
"and 2.0.28, it was renamed to B<vm86old>(), and a new B<vm86>()  was "
"introduced.  The definition of I<struct vm86_struct> was changed in 1.1.8 "
"and 1.1.9."
msgstr ""
"A chamada de sistema B<vm86> foi introduzida no Linux 0.97p2. Nos Linux "
"2.1.15 e 2.0.28 ela foi renomeada para B<vm86old>, e uma nova B<vm86> foi "
"introduzida. A definição de `struct vm86_struct' foi alterada nos 1.1.8 e "
"1.1.9."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "These calls cause the process to enter VM86 mode, and are used by "
#| "B<dosemu>."
msgid ""
"These calls cause the process to enter VM86 mode (virtual-8086 in Intel "
"literature), and are used by B<dosemu>."
msgstr ""
"Estas chamadas provocam a entrada do processo no modo VM86, e são usadas "
"pelo B<dosemu>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "VM86 mode is an emulation of real mode within a protected mode task."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"Em caso de sucesso, zero é retornado. Em caso de erro, -1 é retornado, e "
"I<errno> é definido para indicar o erro."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This return value is specific to i386 and indicates a problem with getting "
"user-space data."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This return value indicates the call is not implemented on the present "
"architecture."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Saved kernel stack exists. (This is a kernel sanity check; the saved "
#| "stack should only exist within vm86 mode itself.)"
msgid ""
"Saved kernel stack exists.  (This is a kernel sanity check; the saved stack "
"should exist only within vm86 mode itself.)"
msgstr ""
"Pilha do Kernel já existe. (Isto é um teste de integridade do kernel; a "
"pilha armazenada deveria existir apenas no modo vm86."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Linux on 32-bit Intel processors."
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 outubro 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "This call is specific to Linux on Intel processors, and should not be "
#| "used in programs intended to be portable."
msgid ""
"This call is specific to Linux on 32-bit Intel processors, and should not be "
"used in programs intended to be portable."
msgstr ""
"Esta chamada é específica do Linux em processadores Intel, e não deve ser "
"usada em programas que pretendem ser portáveis."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2 maio 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 outubro 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (não lançado)"
