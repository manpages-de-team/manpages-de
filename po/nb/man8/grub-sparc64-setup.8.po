# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2025-02-28 16:35+0100\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB-SPARC64-SETUP"
msgstr "GRUB-SPARC64-SETUP"

#. type: TH
#: archlinux
#, no-wrap
msgid "February 2025"
msgstr "Februar 2025"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12.r212.g4dc616657-2"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemadministrasjonsverktøy"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux
msgid "grub-sparc64-setup - set up a device to boot using GRUB"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: archlinux
msgid "B<grub-sparc64-setup> [I<\\,OPTION\\/>...] I<\\,DEVICE\\/>"
msgstr "B<grub-sparc64-setup> [I<\\,VALG\\/>...] I<\\,ENHET\\/>"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux
msgid "Set up images to boot from DEVICE."
msgstr "Velg hvilken ENHET bilder skal startes opp fra."

#. type: Plain text
#: archlinux
msgid ""
"You should not normally run this program directly.  Use grub-install instead."
msgstr ""
"Du bør vanligvis ikke kjøre dette programmet direkte. Bruk heller B<grub-"
"install>(8)."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-a>, B<--allow-floppy>"
msgstr "B<-a>, B<--allow-floppy>"

#. type: Plain text
#: archlinux
msgid ""
"make the drive also bootable as floppy (default for fdX devices). May break "
"on some BIOSes."
msgstr ""
"la enheten starte opp i diskettmodus (standard for fdX-enheter). Dette "
"fungerer ikke med alle BIOS-er."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-b>, B<--boot-image>=I<\\,FILE\\/>"
msgstr "B<-b>, B<--boot-image>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux
msgid "use FILE as the boot image [default=boot.img]"
msgstr "bruk valgt FIL som oppstartsbilde (standard: boot.img)"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-c>, B<--core-image>=I<\\,FILE\\/>"
msgstr "B<-c>, B<--core-image>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux
msgid "use FILE as the core image [default=core.img]"
msgstr "bruk valgt FIL som kjernebilde (standard: core.img)"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,MAPPE\\/>"

#. type: Plain text
#: archlinux
msgid "use GRUB files in the directory DIR [default=//boot/grub]"
msgstr "bruk GRUB-filer i mappa DIR [standard=//boot/grub]"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: archlinux
msgid "install even if problems are detected"
msgstr "ikke stans installasjonen selv om problemer oppstår"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr "B<-m>, B<--device-map>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux
msgid "use FILE as the device map [default=//boot/grub/device.map]"
msgstr "bruk valgt FIL som enhetskart [standard=//boot/grub/device.map]"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--no-rs-codes>"
msgstr "B<--no-rs-codes>"

#. type: Plain text
#: archlinux
msgid ""
"Do not apply any reed-solomon codes when embedding core.img. This option is "
"only available on x86 BIOS targets."
msgstr ""
"Ikke bruk reed-solomon-koder ved innbygging av core.img. Dette valget er "
"bare tilgjengelig på x86-BIOS-mål."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-s>, B<--skip-fs-probe>"
msgstr "B<-s>, B<--skip-fs-probe>"

#. type: Plain text
#: archlinux
msgid "do not probe for filesystems in DEVICE"
msgstr "ikke undersøk filsystemer på valgt ENHET"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux
msgid "print verbose messages."
msgstr "skriv ut detaljerte meldinger"

#. type: TP
#: archlinux
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux
msgid "give this help list"
msgstr "vis denne hjelpelista"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux
msgid "give a short usage message"
msgstr "vis en kortfattet bruksanvisning"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux
msgid "print program version"
msgstr "skriv ut programversjon"

#. type: Plain text
#: archlinux
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Valg er enten obligatoriske både for fullstendige valg og tilsvarende "
"forkortede valg."

#. type: Plain text
#: archlinux
msgid "DEVICE must be an OS device (e.g. I<\\,/dev/sda\\/>)."
msgstr "ENHET må være en OS-enhet (f.eks. I<\\,/dev/sda\\/>)."

#. type: SH
#: archlinux
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: archlinux
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Rapporter feil til E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux
msgid "B<grub-install>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"
msgstr "B<grub-install>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"

#. type: Plain text
#: archlinux
msgid ""
"The full documentation for B<grub-sparc64-setup> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-sparc64-setup> programs are properly "
"installed at your site, the command"
msgstr ""
"Den fullstendige dokumentasjonen for B<grub-sparc64-setup> opprettholdes som "
"en Texinfo manual. Dersom B<info> og B<grub-sparc64-setup> programmene er "
"riktig installert på ditt sted burde kommandoen"

#. type: Plain text
#: archlinux
msgid "B<info grub-sparc64-setup>"
msgstr "B<info grub-sparc64-setup>"

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr "gi deg tilgang til hele manualen."
