# #-#-#-#-#  min-002-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021.
# #-#-#-#-#  min-003-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021.
# #-#-#-#-#  min-004-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021.
# #-#-#-#-#  min-010-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2025.
# #-#-#-#-#  min-020-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2025.
# #-#-#-#-#  min-100-occurences.po (manpages-l10n 2.15)  #-#-#-#-#
# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2025.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2025-02-28 16:46+0100\n"
"PO-Revision-Date: 2025-01-08 07:48+0100\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"#-#-#-#-#  min-002-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.0\n"
"#-#-#-#-#  min-003-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.0\n"
"#-#-#-#-#  min-004-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.0\n"
"#-#-#-#-#  min-010-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.12.0\n"
"#-#-#-#-#  min-020-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.12.0\n"
"#-#-#-#-#  min-100-occurences.po (manpages-l10n 2.15)  #-#-#-#-#\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REALPATH"
msgstr "REALPATH"

#. type: TH
#: archlinux fedora-rawhide
#, fuzzy, no-wrap
#| msgid "February 2023"
msgid "February 2025"
msgstr "Februari 2023"

#. type: TH
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "GNU coreutils 9.5"
msgid "GNU coreutils 9.6"
msgstr "GNU coreutils 9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Opdrachten voor gebruikers"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "realpath - print the resolved path"
msgstr "realpath - toon het herleide pad"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<realpath> [I<\\,OPTION\\/>]... I<\\,FILE\\/>..."
msgstr "B<realpath> [I<\\,OPTIE\\/>]... I<\\,BESTAND\\/>..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Print the resolved absolute file name; all but the last component must exist"
msgstr ""
"Toont de herleide absolute bestandsnaam; alleen de laatste component hoeft "
"niet te bestaan."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-e>, B<--canonicalize-existing>"
msgstr "B<-e>, B<--canonicalize-existing>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "all components of the path must exist"
msgstr "alle componenten van het pad moeten bestaan"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--canonicalize-missing>"
msgstr "B<-m>, B<--canonicalize-missing>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "no path components need exist or be a directory"
msgstr "geen van de pad componenten hoeft te bestaan noch een map te zijn"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-L>, B<--logical>"
msgstr "B<-L>, B<--logical>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "resolve '..' components before symlinks"
msgstr "'..'-componenten herleiden vr sym-koppelingen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--physical>"
msgstr "B<-P>, B<--physical>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "resolve symlinks as encountered (default)"
msgstr "symbolische koppelingen direct herleiden (standaard)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "suppress most error messages"
msgstr "onderdruk de meeste foutmeldingen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--relative-to>=I<\\,DIR\\/>"
msgstr "B<--relative-to>=I<\\,MAP\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print the resolved path relative to DIR"
msgstr "het herleide pad relatief tot MAP tonen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--relative-base>=I<\\,DIR\\/>"
msgstr "B<--relative-base>=I<\\,MAP\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "print absolute paths unless paths below DIR"
msgstr "absolute paden tonen behalve wanneer onder MAP"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--strip>, B<--no-symlinks>"
msgstr "B<-s>, B<--strip>, B<--no-symlinks>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "don't expand symlinks"
msgstr "symbolische koppelingen niet herleiden"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero>"
msgstr "B<-z>, B<--zero>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "end each output line with NUL, not newline"
msgstr "elke regel afsluiten met NUL-byte, niet met nieuwe regel"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "toon de helptekst en stop"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "toon programmaversie en stop"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Written by Padraig Brady."
msgstr "Geschreven door Padraig Brady."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTEREN VAN BUGS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Online hulp bij GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Meld alle vertaalfouten op E<lt>https://translationproject.org/team/"
"nl.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU "
#| "GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgid ""
"Copyright \\(co 2025 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dit is vrije software: u mag het vrijelijk wijzigen en verder verspreiden. "
"Deze software kent GEEN GARANTIE, voor zover de wet dit toestaat."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<readlink>(1), B<readlink>(2), B<realpath>(3)"
msgstr "B<readlink>(1), B<readlink>(2), B<realpath>(3)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/realpathE<gt>"
msgstr ""
"Volledige documentatie op: E<lt>https://www.gnu.org/software/coreutils/"
"realpathE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"or available locally via: info \\(aq(coreutils) realpath invocation\\(aq"
msgstr "of lokaal via: info \\(aq(coreutils) realpath invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "October 2024"
msgstr "Oktober 2024"

#. type: TH
#: debian-unstable mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: Plain text
#: debian-unstable mageia-cauldron
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-42 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "January 2024"
msgid "January 2025"
msgstr "Januari 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "March 2024"
msgstr "Maart 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "January 2024"
msgstr "Januari 2024"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: opensuse-leap-16-0
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
