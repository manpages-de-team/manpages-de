# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2025-02-16 05:52+0100\n"
"PO-Revision-Date: 2024-06-02 06:44+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MKLOST+FOUND"
msgstr "MKLOST+FOUND"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "January 2025"
msgstr "Januar 2025"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "E2fsprogs version 1.47.2"
msgstr "E2fsprogs Version 1.47.2"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"mklost+found - create a lost+found directory on a mounted ext2/ext3/ext4 "
"file system"
msgstr ""
"mklost+found - Erstellung eines Verzeichnisses »lost+found« auf einem "
"eingehängten Ext2/Ext3/Ext4-Dateisystem"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<mklost+found>"
msgstr "B<mklost+found>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<mklost+found> is used to create a I<lost+found> directory in the current "
"working directory on an ext2/ext3/ext4 file system.  There is normally a "
"I<lost+found> directory in the root directory of each file system."
msgstr ""
"B<mklost+found> wird dazu verwandt, ein Verzeichnis I<lost+found> in dem "
"aktuellen Arbeitsverzeichnis auf einem Ext2/Ext3/Ext4-Dateisystem zu "
"erstellen. Es gibt normalerweise ein Verzeichnis I<lost+found> im "
"Wurzelverzeichnis jedes Dateisystems."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<mklost+found> pre-allocates disk blocks to the I<lost+found> directory so "
"that when B<e2fsck>(8)  is being run to recover a file system, it does not "
"need to allocate blocks in the file system to store a large number of "
"unlinked files.  This ensures that B<e2fsck> will not have to allocate data "
"blocks in the file system during recovery."
msgstr ""
"B<mklost+found> reserviert Plattenblöcke für das Verzeichnis I<lost+found> "
"vor, so dass beim Aufruf von B<e2fsck>(8) zum Wiederherstellen eines "
"Dateisystems keine Blöcke im Dateisystem reserviert werden müssen, um eine "
"große Anzahl an nichtverlinkten Dateien zu speichern. Dies stellt sicher, "
"dass B<e2fsck> keine Datenblöcke in dem Dateisystem während der "
"Wiederherstellung reservieren muss."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "There are none."
msgstr "Es gibt keine."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<mklost+found> has been written by Remy Card "
"E<lt>Remy.Card@linux.orgE<gt>.  It is currently being maintained by Theodore "
"Ts'o E<lt>tytso@alum.mit.eduE<gt>."
msgstr ""
"B<mklost+found> wurde von Remy Card E<lt>Remy.Card@linux.orgE<gt> "
"geschrieben. Es wird derzeit von Theodore Ts'o E<lt>tytso@alum.mit.eduE<gt> "
"betreut."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "There are none :-)"
msgstr "Es gibt keine :-)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<mklost+found> is part of the e2fsprogs package and is available from "
"http://e2fsprogs.sourceforge.net."
msgstr ""
"B<mklost+found> ist Teil des Pakets »e2fsprogs« und ist von http://"
"e2fsprogs.sourceforge.net verfügbar."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<e2fsck>(8), B<mke2fs>(8)"
msgstr "B<e2fsck>(8), B<mke2fs>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "November 2024"
msgstr "November 2024"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "E2fsprogs version 1.47.2-rc1"
msgstr "E2fsprogs Version 1.47.2-rc1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "May 2024"
msgstr "Mai 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "E2fsprogs version 1.47.1"
msgstr "E2fsprogs Version 1.47.1"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "February 2023"
msgstr "Februar 2023"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "E2fsprogs version 1.47.0"
msgstr "E2fsprogs Version 1.47.0"

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"mklost+found - create a lost+found directory on a mounted Linux second "
"extended file system"
msgstr ""
"mklost+found - Erstellung eines Verzeichnisses »lost+found« auf einem "
"eingehängten Linux-Second-Extended-Dateisystem"

#. type: Plain text
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<mklost+found> is used to create a I<lost+found> directory in the current "
"working directory on a Linux second extended file system.  There is normally "
"a I<lost+found> directory in the root directory of each file system."
msgstr ""
"B<mklost+found> wird dazu verwandt, ein Verzeichnis I<lost+found> in dem "
"aktuellen Arbeitsverzeichnis auf einem Linux-Second-Extended-Dateisystem zu "
"erstellen. Es gibt normalerweise ein Verzeichnis I<lost+found> im "
"Wurzelverzeichnis jedes Dateisystems."
