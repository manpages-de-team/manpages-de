# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.13\n"
"POT-Creation-Date: 2025-02-16 05:48+0100\n"
"PO-Revision-Date: 2022-05-10 19:51+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Dd
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "March 16, 1991"
msgstr "16. März 1991"

#. type: Dt
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "TALKD 8"
msgstr "TALKD 8"

#. type: Os
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux NetKit (0.17)"
msgstr "Linux NetKit (0.17)"

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "E<.Nm talkd>"
msgstr "E<.Nm talkd>"

#. type: Nd
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "remote user communication server"
msgstr "Kommunikationsserver für ferne Benutzer"

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm /usr/sbin/in.talkd> E<.Op Fl dpq>"
msgstr "E<.Nm /usr/sbin/in.talkd> E<.Op Fl dpq>"

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME E<.Nm Talkd> → E<.Nm>
#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"E<.Nm Talkd> is the server that notifies a user that someone else wants to "
"initiate a conversation.  It acts a repository of invitations, responding to "
"requests by clients wishing to rendezvous to hold a conversation.  In normal "
"operation, a client, the caller, initiates a rendezvous by sending a E<.Tn "
"CTL_MSG> to the server of type E<.Tn LOOK_UP> (see E<.Aq Pa protocols/"
"talkd.h>).  This causes the server to search its invitation tables to check "
"if an invitation currently exists for the caller (to speak to the callee "
"specified in the message).  If the lookup fails, the caller then sends an "
"E<.Tn ANNOUNCE> message causing the server to broadcast an announcement on "
"the callee's login ports requesting contact.  When the callee responds, the "
"local server uses the recorded invitation to respond with the appropriate "
"rendezvous address and the caller and callee client programs establish a "
"stream connection through which the conversation takes place."
msgstr ""
"E<.Nm Talkd> ist der Server, der einen Benutzer benachrichtigt, dass jemand "
"anders ein Gespräch einleiten möchte. Er agiert als Depot für Einladungen "
"und antwortet Anfragen von Clients, die eine Verabredung zur Unterhaltung "
"einleiten möchten. Im Normalbetrieb leitet der Client, der Anrufende, eine "
"Verabredung ein, indem er eine E<.Tn CTL_MSG> vom Typ E<.Tn LOOK_UP> an den "
"Server sendet (siehe E<.Aq Pa protocols/talkd.h>). Dies führt dazu, dass der "
"Server seine Einladungstabellen durchsucht, um zu prüfen, ob für den "
"Anrufenden derzeit eine Einladung existiert (um mit dem in der Nachricht "
"angegebenen Angerufenen zu sprechen). Falls das Nachschlagen fehlschlägt, "
"schickt der Anrufende eine Meldung E<.Tn ANNOUNCE>, die dazu führt, dass der "
"Server an alle eine Ankündigung über die Anmelde-Ports des Angerufenen "
"verteilt und um Kontakt bittet. Wenn der Angerufene reagiert, verwendet der "
"lokale Server die aufgezeichnete Einladung, um mit der passenden "
"Verabredungsadresse zu antworten und die Programme des Anrufenden und des "
"Angerufenen etablieren eine Datenstromverbindung, über die dann der "
"Austausch stattfindet."

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"E<.Op Fl d> Debug mode; writes copious logging and debugging information to "
"E<.Pa /var/log/talkd.log>."
msgstr ""
"E<.Op Fl d> Fehlersuchmodus; schreibt umfangreiche Protokollier- und "
"Fehlersuchinformationen nach E<.Pa /var/log/talkd.log>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid ""
"E<.Op Fl p> Packet logging mode; writes copies of malformed packets to "
"E<.Pa /var/log/talkd.packets>.  This is useful for debugging "
"interoperability problems."
msgstr ""
"E<.Op Fl p> Paketprotokolliermodus; schreibt Kopien fehlerhafter Pakete nach "
"E<.Pa /var/log/talkd.packets>. Dies ist zur Fehlersuche bei "
"Interoperabilitätsproblemen hilfreich."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Op Fl q> Don't log successful connects."
msgstr "E<.Op Fl q> Protokolliert keine erfolgreichen Verbindungen."

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "E<.Xr talk 1>, E<.Xr write 1>"
msgstr "E<.Xr talk 1>, E<.Xr write 1>"

#. type: Sh
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "The E<.Nm> command appeared in E<.Bx 4.3>."
msgstr "Der Befehl E<.Nm> erschien in E<.Bx 4.3>."

#. type: Plain text
#: fedora-42 fedora-rawhide opensuse-tumbleweed
msgid "E<.Nm talkd> E<.Op Fl dp>"
msgstr "E<.Nm talkd> E<.Op Fl dp>"
