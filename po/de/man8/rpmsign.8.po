# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christoph Brinkhaus <c.brinkhaus@t-online.de>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.25.1\n"
"POT-Creation-Date: 2025-02-28 16:47+0100\n"
"PO-Revision-Date: 2025-03-01 10:42+0100\n"
"Last-Translator: Christoph Brinkhaus <c.brinkhaus@t-online.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "RPMSIGN"
msgstr "RPMSIGN"

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "Red Hat, Inc"
msgstr "Red Hat, Inc"

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "rpmsign - RPM Package Signing"
msgstr "rpmsign - RPM-Paket-Signierung"

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: SS
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SIGNING PACKAGES:"
msgstr "SIGNIERUNG VON PAKETEN:"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"\\f[B]rpmsign\\f[R] \\f[B]--addsign|--resign\\f[R] [\\f[B]rpmsign-"
"options\\f[R]] \\f[I]PACKAGE_FILE ...\\fR"
msgstr ""
"\\f[B]rpmsign\\f[R] \\f[B]--addsign|--resign\\f[R] [\\f[B]rpmsign-"
"options\\f[R]] \\f[I]PAKETDATEI …\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "\\f[B]rpmsign\\f[R] \\f[B]--delsign\\f[R] \\f[I]PACKAGE_FILE ...\\fR"
msgstr "\\f[B]rpmsign\\f[R] \\f[B]--delsign\\f[R] \\f[I]PAKETDATEI …\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"\\f[B]rpmsign\\f[R] \\f[B]--delfilesign\\f[R] \\f[I]PACKAGE_FILE ...\\fR"
msgstr "\\f[B]rpmsign\\f[R] \\f[B]--delfilesign\\f[R] \\f[I]PAKETDATEI …\\fR"

#. type: SS
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "rpmsign-options"
msgstr "Rpmsign-Optionen"

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"[\\f[B]--rpmv3\\f[R]] [\\f[B]--fskpath\\f[R] \\f[I]KEY\\f[R]] [\\f[B]--"
"signfiles\\f[R]]\\fR"
msgstr ""
"[\\f[B]--rpmv3\\f[R]] [\\f[B]--fskpath\\f[R] \\f[I]SCHLÜSSEL\\f[R]] [\\f[B]--"
"signfiles\\f[R]]\\fR"

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Both of the \\f[B]--addsign\\f[R] and \\f[B]--resign\\f[R] options generate "
"and insert new signatures for each package \\f[I]PACKAGE_FILE\\f[R] given, "
"replacing any existing signatures.  There are two options for historical "
"reasons, there is no difference in behavior currently.\\fR"
msgstr ""
"Sowohl die Option \\f[B]--addsign\\f[R] als auch \\f[B]--resign\\f[R] "
"erzeugen neue Signaturen für jedes angegebene Paket \\f[I]PAKETDATEI\\f[R] "
"und fügen sie ein. Sie ersetzten damit bereits vorhandene Signaturen. Aus "
"historischen Gründen gibt es zwei Optionen, die sich momentan identisch "
"verhalten.\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"To create a signature rpmsign needs to verify the package\\[aq]s checksum.  "
"As a result packages with a MD5/SHA1 checksums cannot be signed in FIPS mode."
msgstr ""
"Zur Generierung einer Signatur muss \\f[B]rpmsign\\f[R] die Prüfsumme des "
"Paketes verifizieren. Deshalb können Pakete mit MD5/SHA1-Prüfsummen nicht im "
"FIPS-Modus signiert werden.\\fR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Delete all signatures from each package \\f[I]PACKAGE_FILE\\f[R] given.\\fR"
msgstr ""
"Löscht alle Signaturen von jedem angegebenen Paket \\f[I]PAKETDATEI\\f[R]."
"\\fR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Delete all IMA and fsverity file signatures from each package "
"\\f[I]PACKAGE_FILE\\f[R] given.\\fR"
msgstr ""
"Löscht alle »IMA«- und »fsverity«-Datei-Signaturen von jedem angegebenen "
"Paket \\f[I]PAKETDATEI\\f[R].\\fR"

#. type: SS
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SIGN OPTIONS"
msgstr "OPTIONEN ZUM SIGNIEREN"

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "\\f[B]--rpmv3\\fR"
msgstr "\\f[B]--rpmv3\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"Force RPM V3 header+payload signature addition.  These are expensive and "
"redundant baggage on packages where a separate payload digest exists "
"(packages built with rpm E<gt>= 4.14).  Rpmsign will automatically detect "
"the need for V3 signatures, but this option can be used to force their "
"creation if the packages must be fully signature verifiable with rpm E<lt> "
"4.14 or other interoperability reasons."
msgstr ""
"Erzwingt das Hinzufügen von RPM-V3-Kopf- und RPM-V3-Nutzdaten-Signaturen. "
"Diese sind teuer und eine redundante Last für Pakete, wenn ein separater "
"Nutzdaten-Hash existiert (Pakete, die mit RPM E<gt>= 4.14 gebaut wurden). "
"Rpmsign erkennt eine Notwendigkeit von V3-Signaturen automatisch; diese "
"Option kann aber verwendet werden, um ihre Erzeugung zu erzwingen, falls die "
"Signatur der Pakete mit RPM E<lt> 4.14 voll überprüfbar sein muss oder aus "
"anderen Gründen der Interoperabilität."

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "\\f[B]--fskpath\\f[R] \\f[I]KEY\\fR"
msgstr "\\f[B]--fskpath\\f[R] \\f[I]SCHLÜSSEL\\fR"

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"Used with \\f[B]--signfiles\\f[R], use file signing key \\f[I]KEY\\f[R].\\fR"
msgstr ""
"Verwendet mit \\f[B]--signfiles\\f[R], verwendet Dateisignierungsschlüssel "
"\\f[I]SCHLÜSSEL\\f[R].\\fR"

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "\\f[B]--certpath\\f[R] \\f[I]CERT\\fR"
msgstr "\\f[B]--certpath\\f[R] \\f[I]ZERTIFIKAT\\fR"

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"Used with \\f[B]--signverity\\f[R], use file signing certificate "
"\\f[I]CERT\\f[R].\\fR"
msgstr ""
"Verwendet mit \\f[B]--signverity\\f[R], verwendet Dateisignierungszertifikat "
"\\f[I]ZERTIFIKAT\\f[R].\\fR"

#. type: TP
#: archlinux debian-unstable
#, no-wrap
msgid "\\f[B]--verityalgo\\f[R] \\f[I]ALG\\fR"
msgstr "\\f[B]--verityalgo\\f[R] \\f[I]ALG\\fR"

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"Used with \\f[B]--signverity\\f[R], to specify the signing algorithm.  "
"sha256 and sha512 are supported, with sha256 being the default if this "
"argument is not specified.  This can also be specified with the macro \\f[I]"
"%_verity_algorithm\\f[R].\\fR"
msgstr ""
"Mit \\f[B]--signverity\\f[R] verwendet, um den Algorithmus zur Signierung "
"vorzugeben. SHA256 und SHA512 werden unterstützt, wobei SHA256 die "
"Voreinstellung ist, wenn dieses Argument nicht angegeben ist. Er kann auch "
"mit dem Makro \\f[I]%_verity_algorithm\\f[R] angegeben werden.\\fR"

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "\\f[B]--signfiles\\fR"
msgstr "\\f[B]--signfiles\\fR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Sign package files.  The macro \\f[B]%_binary_filedigest_algorithm\\f[R] "
"must be set to a supported algorithm before building the package.  The "
"supported algorithms are SHA1, SHA256, SHA384, and SHA512, which are "
"represented as 2, 8, 9, and 10 respectively.  The file signing key (RSA "
"private key) must be set before signing the package, it can be configured on "
"the command line with \\f[B]--fskpath\\f[R] or the macro %_file_signing_key."
"\\fR"
msgstr ""
"Signiert Paketdateien. Das Makro \\f[B]%_binary_filedigest_algorithm\\f[R] "
"muss vor dem Bau des Paketes auf einen unterstützten Algorithmus gesetzt "
"werden. Die unterstützten Algorithmen sind SHA1, SHA256, SHA384 und SHA512, "
"die mit 2, 8, 9 und 10 entsprechend repräsentiert werden. Der "
"Dateisignierungsschlüssel (privater RSA-Schlüssel) muss vor dem Signieren "
"des Paketes gesetzt sein. Er kann auf der Befehlszeile mit \\f[B]--"
"fskpath\\f[R] oder dem Makro %_file_signing_key konfiguriert werden.\\fR"

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "\\f[B]--signverity\\fR"
msgstr "\\f[B]--signverity\\fR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"Sign package files with fsverity signatures.  The file signing key (RSA "
"private key) and the signing certificate must be set before signing the "
"package.  The key can be configured on the command line with \\f[B]--"
"fskpath\\f[R] or the macro %_file_signing_key, and the cert can be "
"configured on the command line with \\f[B]--certpath\\f[R] or the macro "
"%_file_signing_cert.\\fR"
msgstr ""
"Signiert Paketdateien mit »fsverify«-Signaturen. Der "
"Dateisignierungsschlüssel (privater RSA-Schlüssel) und das "
"Dateisignierungszertifikat müssen vor dem Signieren des Paketes gesetzt "
"sein. Der Schlüssel kann auf der Befehlszeile mit \\f[B]--fskpath\\f[R] oder "
"dem Makro %_file_signing_key und das Zertifikat auf der Befehlszeile mit "
"\\f[B]--certpath\\f[R] oder dem Makro %_file_signing_cert konfiguriert "
"werden.\\fR"

#. type: SS
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURING SIGNING KEYS"
msgstr "KONFIGURATION DER SIGNIERSCHLÜSSEL"

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"In order to sign packages, you need to create your own public and secret key "
"pair (see the GnuPG manual).  In addition, \\f[B]rpm\\f[R](8) must be "
"configured to find GnuPG and the appropriate keys with the following macros:"
"\\fR"
msgstr ""
"Um Pakete signieren zu können, müssen Sie Ihr eigenes Schlüsselpaar (aus "
"öffentlichem und privaten Schlüssel) erzeugen - schauen Sie dazu in das GPG-"
"Handbuch. Zusätzlich muss \\f[B]rpm\\f[R](8) so konfiguriert werden, dass es "
"GnuPG und die entsprechenden Schlüssel mit den folgenden Makros findet:\\fR"

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "\\f[B]%_gpg_name\\fR"
msgstr "\\f[B]%_gpg_name\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"The name of the \\[dq]user\\[dq] whose key you wish to use to sign your "
"packages.  Typically this is the only configuration needed."
msgstr ""
"Der Name des »Benutzers«, dessen Schlüssel Sie zur Signierung Ihrer Pakete "
"verwenden möchten. Dies ist normalerweise die einzige notwendige "
"Konfiguration."

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[B]%_gpg_path\\fR"
msgstr "\\f[B]%_gpg_path\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"The location of your GnuPG keyring if not the default \\f[B]$GNUPGHOME\\f[R]."
"\\fR"
msgstr ""
"Der Ort Ihres GnuPG-Schlüsselbunds, wenn er von der Voreinstellung \\f[B]"
"$GNUPGHOME\\f[R] abweicht.\\fR"

#. type: TP
#: archlinux debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "\\f[B]%__gpg\\fR"
msgstr "\\f[B]%__gpg\\fR"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid "The path of the GnuPG executable.  Normally pre-configured."
msgstr ""
"Der Pfad der ausführbaren Datei von GnuPG; normalerweise vorkonfiguriert."

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"For example, to be able to use GnuPG to sign packages as the user \\f[I]\\"
"[dq]John Doe E<lt>jdoe\\[at]foo.comE<gt>\\[dq]\\f[R] from the key rings "
"located in \\f[I]/etc/rpm/.gpg\\f[R] using the executable \\f[I]/opt/bin/"
"gpg\\f[R] you would include\\fR"
msgstr ""
"Um zum Beispiel GnuPG zur Signierung von Paketen als der Benutzer "
"\\f[I]»John Doe E<lt>jdoe\\[at]foo.comE<gt>«\\f[R] unter Verwendung der "
"ausführbaren Datei \\f[I]/opt/bin/gpg\\f[R] mit den Schlüsselbunden "
"verwenden zu können, die in \\f[I]/etc/rpm/.gpg\\f[R] liegen, würden Sie\\fR"

#. type: Plain text
#: archlinux debian-unstable
#, no-wrap
msgid ""
"%_gpg_path /etc/rpm/.gpg\n"
"%_gpg_name John Doe E<lt>jdoe\\[at]foo.comE<gt>\n"
"%__gpg /opt/bin/gpg\n"
msgstr ""
"%_gpg_path /etc/rpm/.gpg\n"
"%_gpg_name John Doe E<lt>jdoe\\[at]foo.comE<gt>\n"
"%__gpg /opt/bin/gpg\n"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"in a macro configuration file, typically \\[ti]/.config/rpm/macros.  See "
"\\f[B]Macro Configuration\\f[R] in \\f[B]rpm\\f[R](8) for more details.\\fR"
msgstr ""
"in einer Makro-Konfigurationsdatei, normalerweise \\[ti]/.config/rpm/macros. "
"Siehe unter \\f[B]Makro-Konfiguration\\f[R] in \\f[B]rpm\\f[R](8) für "
"weitere Details.\\fR"

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable opensuse-tumbleweed
msgid ""
"\\f[B]popt\\f[R](3), \\f[B]rpm\\f[R](8), \\f[B]rpmdb\\f[R](8), "
"\\f[B]rpmkeys\\f[R](8), \\f[B]rpm2cpio\\f[R](8), \\f[B]rpmbuild\\f[R](8), "
"\\f[B]rpmspec\\f[R](8)\\fR"
msgstr ""
"\\f[B]popt\\f[R](3), \\f[B]rpm\\f[R](8), \\f[B]rpmdb\\f[R](8), "
"\\f[B]rpmkeys\\f[R](8), \\f[B]rpm2cpio\\f[R](8), \\f[B]rpmbuild\\f[R](8), "
"\\f[B]rpmspec\\f[R](8)\\fR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid ""
"\\f[B]rpmsign --help\\f[R] - as rpm supports customizing the options via "
"popt aliases it\\[aq]s impossible to guarantee that what\\[aq]s described in "
"the manual matches what\\[aq]s available.\\fR"
msgstr ""
"\\f[B]rpmsign --help\\f[R] - wie RPM die Anpassung der Optionen mittels "
"»popt«-Aliase unterstützt, ist es unmöglich zu garantieren, dass alles, was "
"im Handbuch beschrieben ist, mit dem übereinstimmt, was verfügbar ist.\\fR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
msgid "\\f[B]http://www.rpm.org/ E<lt>URL:http://www.rpm.org/E<gt>\\fR"
msgstr "\\f[B]http://www.rpm.org/ E<lt>URL:http://www.rpm.org/E<gt>\\fR"

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: archlinux debian-unstable
#, no-wrap
msgid ""
"Marc Ewing E<lt>marc\\[at]redhat.comE<gt>\n"
"Jeff Johnson E<lt>jbj\\[at]redhat.comE<gt>\n"
"Erik Troan E<lt>ewt\\[at]redhat.comE<gt>\n"
"Panu Matilainen E<lt>pmatilai\\[at]redhat.comE<gt>\n"
"Fionnuala Gunter E<lt>fin\\[at]linux.vnet.ibm.comE<gt>\n"
"Jes Sorensen E<lt>jsorensen\\[at]fb.comE<gt>\n"
msgstr ""
"Marc Ewing E<lt>marc\\[at]redhat.comE<gt>\n"
"Jeff Johnson E<lt>jbj\\[at]redhat.comE<gt>\n"
"Erik Troan E<lt>ewt\\[at]redhat.comE<gt>\n"
"Panu Matilainen E<lt>pmatilai\\[at]redhat.comE<gt>\n"
"Fionnuala Gunter E<lt>fin\\[at]linux.vnet.ibm.comE<gt>\n"
"Jes Sorensen E<lt>jsorensen\\[at]fb.comE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"\\f[B]rpm\\f[R] \\f[B]--addsign|--resign\\f[R] [\\f[B]rpmsign-options\\f[R]] "
"\\f[I]PACKAGE_FILE ...\\fR"
msgstr ""
"\\f[B]rpm\\f[R] \\f[B]--addsign|--resign\\f[R] [\\f[B]Rpmsign-"
"Optionen\\f[R]] \\f[I]PAKETDATEI …\\fR"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "\\f[B]rpm\\f[R] \\f[B]--delsign\\f[R] \\f[I]PACKAGE_FILE ...\\fR"
msgstr "\\f[B]rpm\\f[R] \\f[B]--delsign\\f[R] \\f[I]PAKETDATEI …\\fR"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid "\\f[B]rpm\\f[R] \\f[B]--delfilesign\\f[R] \\f[I]PACKAGE_FILE ...\\fR"
msgstr "\\f[B]rpm\\f[R] \\f[B]--delfilesign\\f[R] \\f[I]PAKETDATEI …\\fR"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"[\\f[B]--rpmv3\\f[R]] [\\f[B]--fskpath \\f[I]KEY\\f[R]] [\\f[B]--"
"signfiles\\f[R]]\\fR"
msgstr ""
"[\\f[B]--rpmv3\\f[R]] [\\f[B]--fskpath \\f[I]SCHLÜSSEL\\f[R]] [\\f[B]--"
"signfiles\\f[R]]\\fR"

# FIXME rpm → \\f[B]rpm\\f[R](8)
#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"To create a signature rpm needs to verify the package\\[aq]s checksum.  As a "
"result packages with a MD5/SHA1 checksums cannot be signed in FIPS mode."
msgstr ""
"Zur Generierung einer Signatur muss \\f[B]rpm\\f[R](8) die Prüfsumme des "
"Paketes verifizieren. Deshalb können Pakete mit MD5/SHA1-Prüfsummen nicht im "
"FIPS-Modus signiert werden.\\fR"

# FIXME Rpm → RPM
#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"Force RPM V3 header+payload signature addition.  These are expensive and "
"redundant baggage on packages where a separate payload digest exists "
"(packages built with rpm E<gt>= 4.14).  Rpm will automatically detect the "
"need for V3 signatures, but this option can be used to force their creation "
"if the packages must be fully signature verifiable with rpm E<lt> 4.14 or "
"other interoperability reasons."
msgstr ""
"Erzwingt das Hinzufügen von RPM-V3-Kopf- und RPM-V3-Nutzdaten-Signaturen. "
"Diese sind teuer und eine redundante Last für Pakete, wenn ein separater "
"Nutzdaten-Hash existiert (Pakete, die mit RPM E<gt>= 4.14 gebaut wurden). "
"RPM erkennt eine Notwendigkeit von V3-Signaturen automatisch; diese Option "
"kann aber verwendet werden, um ihre Erzeugung zu erzwingen, falls die "
"Signatur der Pakete mit RPM E<lt> 4.14 voll überprüfbar sein muss oder aus "
"anderen Gründen der Interoperabilität."

#. type: TP
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\f[B]--fskpath \\f[I]KEY\\fR"
msgstr "\\f[B]--fskpath \\f[I]SCHLÜSSEL\\fR"

# FIXME \\f[I]Key\\f[R] → \\f[I]KEY\\f[R]
#. type: Plain text
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Used with \\f[B]--signfiles\\f[R], use file signing key \\f[I]Key\\f[R].\\fR"
msgstr ""
"Verwendet mit \\f[B]--signfiles\\f[R], verwendet Dateisignierungsschlüssel "
"\\f[I]SCHLÜSSEL\\f[R].\\fR"

#. type: TP
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\f[B]--certpath \\f[I]CERT\\fR"
msgstr "\\f[B]--certpath \\f[I]ZERTIFIKAT\\fR"

# FIXME \\f[I]Cert\\f[R] → \\f[I]CERT\\f[R]
#. type: Plain text
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Used with \\f[B]--signverity\\f[R], use file signing certificate "
"\\f[I]Cert\\f[R].\\fR"
msgstr ""
"Verwendet mit \\f[B]--signverity\\f[R], verwendet Dateisignierungszertifikat "
"\\f[I]ZERTIFIKAT\\f[R].\\fR"

#. type: TP
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\f[B]--verityalgo \\f[I]ALG\\fR"
msgstr "\\f[B]--verityalgo \\f[I]ALG\\fR"

# FIXME missing full stop
#. type: Plain text
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Used with \\f[B]--signverity\\f[R], to specify the signing algorithm.  "
"sha256 and sha512 are supported, with sha256 being the default if this "
"argument is not specified.  This can also be specified with the macro "
"%_verity_algorithm\\fR"
msgstr ""
"Mit \\f[B]--signverity\\f[R] verwendet, um den Algorithmus zur Signierung "
"vorzugeben. SHA256 und SHA512 werden unterstützt, wobei SHA256 die "
"Voreinstellung ist, wenn dieses Argument nicht angegeben ist. Er kann auch "
"mit dem Makro %_verity_algorithm angegeben werden.\\fR"

#. type: SS
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid "USING GPG TO SIGN PACKAGES"
msgstr "VERWENDUNG VON GPG ZUR SIGNIERUNG VON PAKETEN"

# FIXME \\f[B]rpm\\f[R] → \\f[B]rpm\\f[R](8)
#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"In order to sign packages using GPG, \\f[B]rpm\\f[R] must be configured to "
"run GPG and be able to find a key ring with the appropriate keys.  By "
"default, \\f[B]rpm\\f[R] uses the same conventions as GPG to find key rings, "
"namely the \\f[B]$GNUPGHOME\\f[R] environment variable.  If your key rings "
"are not located where GPG expects them to be, you will need to configure the "
"macro \\f[B]%_gpg_path\\f[R] to be the location of the GPG key rings to "
"use.  If you want to be able to sign packages you create yourself, you also "
"need to create your own public and secret key pair (see the GPG manual).  "
"You will also need to configure the \\f[B]rpm\\f[R] macros\\fR"
msgstr ""
"Um Pakete mit GPG signieren zu können, muss \\f[B]rpm\\f[R](8) konfiguriert "
"sein, GPG starten zu können und in der Lage sein, den richtigen "
"Schlüsselbund mit den passenden Schlüsseln zu finden. In der Voreinstellung "
"verwendet \\f[B]rpm\\f[R](8) die gleichen Konventionen wie GPG, um "
"Schlüsselbunde zu finden, und zwar die Umgebungsvariable \\f[B]"
"$GNUPGHOME\\f[R]. Wenn Ihre Schlüsselbunde nicht dort abgelegt sind, wo GPG "
"sie erwartet, dann müssen Sie das Makro \\f[B]%_gpg_path\\f[R] so "
"konfigurieren, dass es den Ort der zu verwendenden GPG-Schlüsselbunde "
"enthält. Wenn Sie in der Lage sein wollen, Pakete zu signieren, die Sie "
"selbst erzeugt haben, müssen Sie ebenso Ihr eigenes Schlüsselpaar (aus "
"öffentlichem und privaten Schlüssel) erzeugen - schauen Sie dazu in das GPG-"
"Handbuch. Sie werden auch die \\f[B]rpm\\f[R](8)-Makros konfigurieren müssen."
"\\fR"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"The name of the \\[dq]user\\[dq] whose key you wish to use to sign your "
"packages."
msgstr ""
"Der Name des »Benutzers«, dessen Schlüssel Sie zur Signierung Ihrer Pakete "
"verwenden möchten."

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"For example, to be able to use GPG to sign packages as the user \\f[I]\\"
"[dq]John Doe E<lt>jdoe\\[at]foo.comE<gt>\\[dq]\\f[R] from the key rings "
"located in \\f[I]/etc/rpm/.gpg\\f[R] using the executable \\f[I]/usr/bin/"
"gpg\\f[R] you would include\\fR"
msgstr ""
"Um zum Beispiel GPG zur Signierung von Paketen als der Benutzer \\f[I]»John "
"Doe E<lt>jdoe\\[at]foo.comE<gt>«\\f[R] unter Verwendung der ausführbaren "
"Datei \\f[I]/usr/bin/gpg\\f[R] mit den Schlüsselbunden verwenden zu können, "
"die in \\f[I]/etc/rpm/.gpg\\f[R] liegen, würden Sie\\fR"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid ""
"\\f[C]\n"
"%_gpg_path /etc/rpm/.gpg\n"
"%_gpg_name John Doe E<lt>jdoe\\[at]foo.comE<gt>\n"
"%__gpg /usr/bin/gpg\\fR\n"
"\n"
msgstr ""
"\\f[C]\n"
"%_gpg_path /etc/rpm/.gpg\n"
"%_gpg_name John Doe E<lt>jdoe\\[at]foo.comE<gt>\n"
"%__gpg /usr/bin/gpg\\fR\n"
"\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
msgid ""
"in a macro configuration file.  Use \\f[I]/etc/rpm/macros\\f[R] for per-"
"system configuration and \\f[I]\\[ti]/.rpmmacros\\f[R] for per-user "
"configuration.  Typically it\\[aq]s sufficient to set just %_gpg_name.\\fR"
msgstr ""
"in eine Makro-Konfigurationsdatei einbinden. Verwenden Sie \\f[I]/etc/rpm/"
"macros\\f[R] für systemweite Konfigurationen und \\f[I]\\"
"[ti]/.rpmmacros\\f[R] für benutzerspezifische Konfigurationen. Üblicherweise "
"ist es ausreichend, nur %_gpg_name zu setzen.\\fR"

# FIXME superflux comma at the end
# FIXME format
#. type: Plain text
#: debian-bookworm opensuse-leap-16-0
#, no-wrap
msgid ""
"\\f[C]\n"
"popt(3),\n"
"rpm(8),\n"
"rpmdb(8),\n"
"rpmkeys(8),\n"
"rpm2cpio(8),\n"
"rpmbuild(8),\n"
"rpmspec(8),\\fR\n"
"\n"
msgstr ""
"\\f[C]\n"
"\\f[B]popt\\f[R](3),\n"
"\\f[B]rpm\\f[R](8),\n"
"\\f[B]rpmdb\\f[R](8),\n"
"\\f[B]rpmkeys\\f[R](8),\n"
"\\f[B]rpm2cpio\\f[R](8),\n"
"\\f[B]rpmbuild\\f[R](8),\n"
"\\f[B]rpmspec\\f[R](8)\\fR\n"
"\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f[C]\n"
"Marc Ewing E<lt>marc\\[at]redhat.comE<gt>\n"
"Jeff Johnson E<lt>jbj\\[at]redhat.comE<gt>\n"
"Erik Troan E<lt>ewt\\[at]redhat.comE<gt>\n"
"Panu Matilainen E<lt>pmatilai\\[at]redhat.comE<gt>\n"
"Fionnuala Gunter E<lt>fin\\[at]linux.vnet.ibm.comE<gt>\n"
"Jes Sorensen E<lt>jsorensen\\[at]fb.comE<gt>\\fR\n"
"\n"
msgstr ""
"\\f[C]\n"
"Marc Ewing E<lt>marc\\[at]redhat.comE<gt>\n"
"Jeff Johnson E<lt>jbj\\[at]redhat.comE<gt>\n"
"Erik Troan E<lt>ewt\\[at]redhat.comE<gt>\n"
"Panu Matilainen E<lt>pmatilai\\[at]redhat.comE<gt>\n"
"Fionnuala Gunter E<lt>fin\\[at]linux.vnet.ibm.comE<gt>\n"
"Jes Sorensen E<lt>jsorensen\\[at]fb.comE<gt>\\fR\n"
"\n"

# FIXME \\f[B]rpm\\f[R] → \\f[B]rpm\\f[R](8)
#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"In order to sign packages, you need to create your own public and secret key "
"pair (see the GnuPG manual).  In addition, \\f[B]rpm\\f[R] must be "
"configured to find GnuPG and the appropriate keys with the following macros:"
"\\fR"
msgstr ""
"Um Pakete signieren zu können, müssen Sie Ihr eigenes Schlüsselpaar (aus "
"öffentlichem und privaten Schlüssel) erzeugen - schauen Sie dazu in das GPG-"
"Handbuch. Zusätzlich muss \\f[B]rpm\\f[R](8) so konfiguriert werden, dass es "
"GnuPG und die entsprechenden Schlüssel mit den folgenden Makros findet:\\fR"

#. type: Plain text
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f[C]\n"
"%_gpg_path /etc/rpm/.gpg\n"
"%_gpg_name John Doe E<lt>jdoe\\[at]foo.comE<gt>\n"
"%__gpg /opt/bin/gpg\\fR\n"
"\n"
msgstr ""
"\\f[C]\n"
"%_gpg_path /etc/rpm/.gpg\n"
"%_gpg_name John Doe E<lt>jdoe\\[at]foo.comE<gt>\n"
"%__gpg /opt/bin/gpg\\fR\n"
"\n"
