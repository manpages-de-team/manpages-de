# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.23.1\n"
"POT-Creation-Date: 2025-02-28 16:53+0100\n"
"PO-Revision-Date: 2024-08-03 11:07+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-PCRPHASE\\&.SERVICE"
msgstr "SYSTEMD-PCRPHASE\\&.SERVICE"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "systemd 257.3"
msgstr "systemd 257.3"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "systemd-pcrphase.service"
msgstr "systemd-pcrphase.service"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"systemd-pcrphase.service, systemd-pcrphase-sysinit.service, systemd-pcrphase-"
"initrd.service, systemd-pcrmachine.service, systemd-pcrfs-root.service, "
"systemd-pcrfs@.service, systemd-pcrextend - Measure boot phase into TPM2 PCR "
"11, machine ID and file system identity into PCR 15"
msgstr ""
"systemd-pcrphase.service, systemd-pcrphase-sysinit.service, systemd-pcrphase-"
"initrd.service, systemd-pcrmachine.service, systemd-pcrfs-root.service, "
"systemd-pcrfs@.service, systemd-pcrextend - Systemstartphase in TPM2 PCR 11 "
"einmessen, Maschinenkennung und Dateisystemidentität in PCR 15"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-pcrphase\\&.service"
msgstr "systemd-pcrphase\\&.service"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-pcrphase-sysinit\\&.service"
msgstr "systemd-pcrphase-sysinit\\&.service"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-pcrphase-initrd\\&.service"
msgstr "systemd-pcrphase-initrd\\&.service"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-pcrmachine\\&.service"
msgstr "systemd-pcrmachine\\&.service"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-pcrfs-root\\&.service"
msgstr "systemd-pcrfs-root\\&.service"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "systemd-pcrfs@\\&.service"
msgstr "systemd-pcrfs@\\&.service"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/usr/lib/systemd/systemd-pcrextend [I<STRING>]"
msgstr "/usr/lib/systemd/systemd-pcrextend [I<ZEICHENKETTE>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"systemd-pcrphase\\&.service, systemd-pcrphase-sysinit\\&.service, and "
"systemd-pcrphase-initrd\\&.service are system services that measure specific "
"strings into TPM2 PCR 11 during boot at various milestones of the boot "
"process\\&."
msgstr ""
"systemd-pcrphase\\&.service, systemd-pcrphase-sysinit\\&.service und systemd-"
"pcrphase-initrd\\&.service sind Systemdienste, die bestimmte Zeichenketten "
"in TPM2-PCR 11 zu bestimmten Meilensteinen während des Systemstartprozesses "
"einmessen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"systemd-pcrmachine\\&.service is a system service that measures the machine "
"ID (see B<machine-id>(5)) into PCR 15\\&."
msgstr ""
"systemd-pcrmachine\\&.service ist ein Systemdienst, der die Maschinenkennung "
"(siehe B<machine-id>(5)) in PCR 15 einmisst\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"systemd-pcrfs-root\\&.service and systemd-pcrfs@\\&.service are services "
"that measure file system identity information (i\\&.e\\&. mount point, file "
"system type, label and UUID, partition label and UUID) into PCR 15\\&.  "
"systemd-pcrfs-root\\&.service does so for the root file system, systemd-"
"pcrfs@\\&.service is a template unit that measures the file system indicated "
"by its instance identifier instead\\&."
msgstr ""
"systemd-pcrfs-root\\&.service und systemd-pcrfs@\\&.service sind Dienste, "
"die Dateisystem-Identitätsinformationen (z\\&.B\\&. Einhängepunkt, "
"Dateisystemtyp, Kennzeichnung und UUID, Partitionskennzeichnung und UUID) in "
"PCR 14 einmessen\\&. systemd-pcrfs-root\\&.service macht dies für das "
"Wurzeldateisystem, systemd-pcrfs@\\&.service ist eine Vorlagen-Unit, die "
"stattdessen das von seiner Instanzkennzeichner angezeigte Dateisystem "
"einmisst\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These services require B<systemd-stub>(7)  to be used in a unified kernel "
"image (UKI)\\&. They execute no operation when the stub has not been used to "
"invoke the kernel\\&. The stub will measure the invoked kernel and "
"associated vendor resources into PCR 11 before handing control to it; once "
"userspace is invoked these services then will extend TPM2 PCR 11 with "
"certain literal strings indicating phases of the boot process\\&. During a "
"regular boot process PCR 11 is extended with the following strings:"
msgstr ""
"Diese Dienste benötigen, dass B<systemd-stub>(7) in einem vereinigten "
"Kernelabbild (UKI) verwandt wird\\&. Sie führen keine Aktion aus, wenn der "
"Rumpf nicht zum Aufruf des Kernels verwandt wurde\\&. Der Rumpf wird "
"sicherstellen, dass der aufgerufene Kernel und die zugehörigen Ressourcen "
"des Lieferanten in PCR 11 eingemessen werden, bevor ihm die Steuerung "
"übergeben wird; sobald der Anwendungsraum aufgerufen wird, werden diese "
"Dienste TMP2 PCR 11 mit bestimmten wörtliche Zeichenketten erweitern und "
"damit Phasen des Systemstartprozesses andeuten\\&. Während des regulären "
"Systemstartprozesses wird PCR 11 mit den nachfolgenden Zeichenketten "
"erweitert\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\"enter-initrd\" \\(em early when the initrd initializes, before activating "
"system extension images for the initrd\\&. It acts as a barrier between the "
"time where the kernel initializes and where the initrd starts operating and "
"enables system extension images, i\\&.e\\&. code shipped outside of the "
"UKI\\&. (This extension happens when the B<systemd-pcrphase-"
"initrd.service>(8)  service is started\\&.)"
msgstr ""
"»enter-initrd« \\(en früh, wenn die Initrd sich initialisiert, bevor die "
"Systemerweiterungsabbilder für die Initrd aktiviert werden\\&. Sie agiert "
"als Barriere zwischen dem Zeitpunkt der Kernelinitialisierung und dem Start "
"der Initrd-Aktionen und der Aktivierung von Systemerweiterungsabbildern, "
"d\\&.h\\&. von Code, der außerhalb des UKI ausgeliefert wird\\&. (Diese "
"Erweiterung passiert, wenn der Dienst B<systemd-pcrphase-initrd.service>(8) "
"startet\\&.)"

# WONTFIX systemd-pcrphase-initrd\\&.service → B<systemd-pcrphase-initrd\\&.service>(8) // This is the same file, so I don't think we should use a ref.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\"leave-initrd\" \\(em when the initrd is about to transition into the host "
"file system\\&. It acts as barrier between initrd code and host OS code\\&. "
"(This extension happens when the systemd-pcrphase-initrd\\&.service service "
"is stopped\\&.)"
msgstr ""
"»leave-initrd« \\(en wenn die Initrd gerade dabei ist, in das Dateisystem "
"des Rechners überzugehen\\&. Sie agiert als Barriere zwischen dem Initrd-"
"Code und dem Code des Rechners\\&. (Diese Erweiterung passiert, wenn der "
"Dienst B<systemd-pcrphase-initrd\\&.service>(8) gestoppt wird\\&.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\"sysinit\" \\(em when basic system initialization is complete (which "
"includes local file systems having been mounted), and the system begins "
"starting regular system services\\&. (This extension happens when the "
"B<systemd-pcrphase-sysinit.service>(8)  service is started\\&.)"
msgstr ""
"»sysinit« \\(en wenn die grundlegende Initialisierung abgeschlossen ist "
"(dazu gehört, dass lokale Dateisysteme eingehängt wurden) und das System "
"anfängt, reguläre Dateisystemdienste zu starten\\&. (Diese Erweiterung "
"passiert, wenn der Dienst B<systemd-pcrphase-sysinit.service>(8) gestartet "
"wird\\&.)"

# FIXME systemd-pcrphase\\&.service → B<systemd-pcrphase\\&.service> // We don't use bold for service file names…
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\"ready\" \\(em during later boot-up, after remote file systems have been "
"activated (i\\&.e\\&. after remote-fs\\&.target), but before users are "
"permitted to log in (i\\&.e\\&. before systemd-user-sessions\\&.service)\\&. "
"It acts as barrier between the time where unprivileged regular users are "
"still prohibited to log in and where they are allowed to log in\\&. (This "
"extension happens when the systemd-pcrphase\\&.service service is "
"started\\&.)"
msgstr ""
"»ready« \\(en während des späten Hochfahrens, nachdem ferne Dateisystem "
"bereits aktiviert wurden (d\\&.h\\&. nach remote-fs\\&.target), aber bevor "
"Benutzer das Anmelden erlaubt wird (d\\&.h\\&. vor systemd-user-"
"sessions\\&.service)\\&. Sie agiert als Barriere zwischen dem Zeitpunkt, zu "
"dem nicht privilegierten Benutzer das Anmelden verwehrt wird und zu dem "
"Zeitpunkt, wo das möglich ist\\&. (Diese Erweiterung passiert, wenn der "
"Dienst B<systemd-pcrphase\\&.service> gestartet wird\\&.)"

# FIXME systemd-pcrphase\\&.service → B<systemd-pcrphase\\&.service>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\"shutdown\" \\(em when the system shutdown begins\\&. It acts as barrier "
"between the time the system is fully up and running and where it is about to "
"shut down\\&. (This extension happens when the systemd-pcrphase\\&.service "
"service is stopped\\&.)"
msgstr ""
"»shutdown« \\(en beim Beginn des System-Herunterfahrens\\&. Sie agiert als "
"Barriere zwischen dem Zeitpunkt, zu dem das System voll aktiv ist und zu "
"dem, wo es mit dem Herunterfahren beginnt\\&. (Diese Erweiterung passiert, "
"wenn der Dienst B<systemd-pcrphase\\&.service> gestoppt ist\\&.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\"final\" \\(em at the end of system shutdown\\&. It acts as barrier between "
"the time the service manager still runs and when it transitions into the "
"final shutdown phase where service management is not available anymore\\&. "
"(This extension happens when the B<systemd-pcrphase-sysinit.service>(8)  "
"service is stopped\\&.)"
msgstr ""
"»final« \\(en beim Ende des System-Herunterfahrens\\&. Sie agiert als "
"Barriere zwischen dem Zeitpunkt, zu dem der Diensteverwalter noch läuft und "
"zu dem Zeitpunkt, zu dem es in die abschließende Systemherunterfahrphase "
"übergeht, bei dem der Diensteverwalter nicht mehr verfügbar ist\\&. (Diese "
"Erweiterung passiert, wenn der Dienst B<systemd-pcrphase-sysinit.service>(8) "
"gestoppt ist\\&.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"During a regular system lifecycle, PCR 11 is extended with the strings "
"\"enter-initrd\", \"leave-initrd\", \"sysinit\", \"ready\", \"shutdown\", "
"and \"final\"\\&."
msgstr ""
"Während der regulären/normalen Nutzung(sdauer) eines Systems wird PCR 11 mit "
"den Zeichenketten »enter-initrd«, »leave-initrd«, »sysinit«, »ready«, "
"»shutdown« und »final« erweitert\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Specific phases of the boot process may be referenced via the series of "
"strings measured, separated by colons (the \"phase path\")\\&. For example, "
"the phase path for the regular system runtime is \"enter-initrd:leave-"
"initrd:sysinit:ready\", while the one for the initrd is just \"enter-"
"initrd\"\\&. The phase path for the boot phase before the initrd is an empty "
"string; because that\\*(Aqs hard to pass around a single colon (\":\") may "
"be used instead\\&. Note that the aforementioned six strings are just the "
"default strings and individual systems might measure other strings at other "
"times, and thus implement different and more fine-grained boot phases to "
"bind policy to\\&."
msgstr ""
"Bestimmte Phasen des Systemstartprozesses können über eine Reihe von "
"eingemessenen Zeichenketten (getrennt durch Doppelpunkte) referenziert "
"werden (den »Phasenpfad«)\\&. Der Phasenpfad für die reguläre System-"
"Laufzeitumgebung ist beispielsweise »enter-initrd:leave-"
"initrd:sysinit:ready«, während die für die Initrd nur »enter-initrd« ist\\&. "
"Der Phasenpfad für die Systemstartphase vor der Initrd ist die leere "
"Zeichenkette; da das schwer weiterzugeben ist, kann ein einzelner "
"Doppelpunkt (»:«) stattdessen verwandt werden\\&. Beachten Sie, dass die "
"vorgenannten sechs Zeichenketten nur die Vorgabezeichenketten sind und "
"individuelle Systeme andere Zeichenketten zu anderen Zeitpunkten einmessen "
"könnten, und daher andere und granularere Systemstartphasen implementieren "
"könnten, um Richtlinien daran zu binden\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"By binding policy of TPM2 objects to a specific phase path it is possible to "
"restrict access to them to specific phases of the boot process, for example "
"making it impossible to access the root file system\\*(Aqs encryption key "
"after the system transitioned from the initrd into the host root file "
"system\\&."
msgstr ""
"Durch Binden von Richtlinien von TPM2-Objekten an bestimmte Phasenpfade wird "
"es möglich, sie auf bestimmte Phasen des Systemstartprozesses zu "
"beschränken\\&. Damit wird es beispielsweise unmöglich, auf den "
"Verschlüsselungsschlüssel des Wurzeldateisystems zuzugreifen, nachdem das "
"System von der Initrd auf das Dateisystem des Systems gewechselt hat\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Use B<systemd-measure>(1)  to pre-calculate expected PCR 11 values for "
"specific boot phases (via the B<--phase=> switch)\\&."
msgstr ""
"Verwenden Sie B<systemd-measure>(1) (mit dem Schalter B<--phase=>), um die "
"erwarteten PCR-11-Werte für bestimmte Systemstartphasen vorzuberechnen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"systemd-pcrfs-root\\&.service and systemd-pcrfs@\\&.service are "
"automatically pulled into the initial transaction by B<systemd-gpt-auto-"
"generator>(8)  for the root and /var/ file systems\\&.  B<systemd-fstab-"
"generator>(8)  will do this for all mounts with the B<x-systemd\\&.pcrfs> "
"mount option in /etc/fstab\\&."
msgstr ""
"systemd-pcrfs-root\\&.service und systemd-pcrfs@\\&.service werden "
"automatisch durch B<systemd-gpt-auto-generator>(8) für die Wurzel- und /var/-"
"Dateisysteme in die anfängliche Transaktion hereingezogen\\&. B<systemd-"
"fstab-generator>(8) macht dies für alle Einhängepunkte, bei denen die "
"Einhängeoption B<x-systemd\\&.pcrfs> in /etc/fstab gesetzt ist\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The /usr/lib/systemd/system-pcrextend executable may also be invoked from "
"the command line, where it expects the word to extend into PCR 11, as well "
"as the following switches:"
msgstr ""
"Das Programm /usr/lib/systemd/system-pcrextend kann auch von der "
"Befehlszeile aufgerufen werden\\&. Dort erwartet es das Wort, das in PCR 11 "
"erweitert werden soll, sowie die folgenden Schalter:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--bank=>"
msgstr "B<--bank=>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Takes the PCR banks to extend the specified word into\\&. If not specified "
"the tool automatically determines all enabled PCR banks and measures the "
"word into all of them\\&."
msgstr ""
"Akzeptiert die PCR-Bänke, in die das angegeben Wort hinein erweitert werden "
"soll\\&. Falls nicht angegeben, wird das Werkzeug automatisch alle "
"aktivierten PCR-Bänke ermitteln und das Wort in alle von ihnen einmessen\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 252\\&."
msgstr "Hinzugefügt in Version 252\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--pcr=>"
msgstr "B<--pcr=>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Takes the index of the PCR to extend\\&. If B<--machine-id> or B<--file-"
"system=> are specified defaults to 15, otherwise defaults to 11\\&."
msgstr ""
"Akzeptiert den Index des zu erweiternden PCR\\&. Falls B<--machine-id> oder "
"B<--file-system=> angegeben sind, ist die Vorgabe 15, andernfalls ist die "
"Vorgabe 11\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 255\\&."
msgstr "Hinzugefügt in Version 255\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--tpm2-device=>I<PATH>"
msgstr "B<--tpm2-device=>I<PFAD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Controls which TPM2 device to use\\&. Expects a device node path referring "
"to the TPM2 chip (e\\&.g\\&.  /dev/tpmrm0)\\&. Alternatively the special "
"value \"auto\" may be specified, in order to automatically determine the "
"device node of a suitable TPM2 device (of which there must be exactly one)"
"\\&. The special value \"list\" may be used to enumerate all suitable TPM2 "
"devices currently discovered\\&."
msgstr ""
"Steuert das zu verwendende TPM2-Gerät\\&. Erwartet einen Geräteknotenpfad, "
"der sich auf einen TPM2-Chip bezieht (z\\&.B\\&. /dev/tpmrm0)\\&. Alternativ "
"kann der besondere Wert »auto« angegeben werden, um den Geräteknoten eines "
"geeigneten TPM2-Gerätes (von dem es genau einen geben darf) automatisch zu "
"bestimmen\\&. Der besondere Wert »list« kann verwandt werden, um alle "
"geeigneten, derzeit ermittelten TPM2-Geräte aufzuzählen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--graceful>"
msgstr "B<--graceful>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If no TPM2 firmware, kernel subsystem, kernel driver or device support is "
"found, exit with exit status 0 (i\\&.e\\&. indicate success)\\&. If this is "
"not specified any attempt to measure without a TPM2 device will cause the "
"invocation to fail\\&."
msgstr ""
"Falls keine Unterstützung für TPM2-Firmware, -Kernel-Subsystem, "
"-Kerneltreiber oder -Geräte gefunden wird, wird mit Exit-Status 0 beendet "
"(d\\&.h\\&. Erfolg angezeigt)\\&. Falls dies nicht angegeben ist, dann wird "
"jeder Versuch, ohne ein TPM2-Gerät zu messen, zu einem Fehlschlag des "
"Aufrufs führen\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "Added in version 253\\&."
msgstr "Hinzugefügt in Version 253\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--machine-id>"
msgstr "B<--machine-id>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Instead of measuring a word specified on the command line into PCR 11, "
"measure the host\\*(Aqs machine ID into PCR 15\\&."
msgstr ""
"Statt das auf der Befehlszeile angegebene Wort in PCR 11 einzumessen, wird "
"die Maschinenkennung des Rechners in PCR 15 eingemessen\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--file-system=>"
msgstr "B<--file-system=>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Instead of measuring a word specified on the command line into PCR 11, "
"measure identity information of the specified file system into PCR 15\\&. "
"The parameter must be the path to the established mount point of the file "
"system to measure\\&."
msgstr ""
"Statt das auf der Befehlszeile angegebene Wort in PCR 11 einzumessen, wird "
"die Maschinenidentitätsinformation des angegebenen Dateisystems in PCR 15 "
"eingemessen\\&. Dieser Parameter muss der Pfad zum aufgebauten Einhängepunkt "
"des zu messenden Dateisystems sein\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print a short help text and exit\\&."
msgstr "Zeigt einen kurzen Hilfetext an und beendet das Programm\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Print a short version string and exit\\&."
msgstr "Zeigt eine kurze Versionszeichenkette an und beendet das Programm\\&."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/run/log/systemd/tpm2-measure\\&.log"
msgstr "/run/log/systemd/tpm2-measure\\&.log"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Measurements are logged into an event log file maintained in /run/log/"
"systemd/tpm2-measure\\&.log, which contains a \\m[blue]B<JSON-SEQ>\\m[]"
"\\&\\s-2\\u[1]\\d\\s+2 series of objects that follow the general structure "
"of the \\m[blue]B<TCG Canonical Event Log Format (CEL-JSON)>\\m[]"
"\\&\\s-2\\u[2]\\d\\s+2 event objects (but lack the \"recnum\" field)\\&."
msgstr ""
"Messungen werden in ein Ereignisprotokoll protokolliert, das in /run/log/"
"systemd/tpm2-measure\\&.log verwaltet wird\\&. Es enthält eine "
"\\m[blue]B<JSON-SEQ>\\m[]\\&\\s-2\\u[1]\\d\\s+2-Serie von Objekten, die der "
"allgemeinen Struktur der \\m[blue]B<Kanonischen TCG-Ereignisprotokollformat- "
"(CEL-JSON-)>\\m[]\\&\\s-2\\u[2]\\d\\s+2-Ereignisobjekte folgt\\&. Es fehlen "
"ihm aber die Felder »recnum«\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A B<LOCK_EX> BSD file lock (B<flock>(2)) on the log file is acquired while "
"the measurement is made and the file is updated\\&. Thus, applications that "
"intend to acquire a consistent quote from the TPM with the associated "
"snapshot of the event log should acquire a B<LOCK_SH> lock while doing so\\&."
msgstr ""
"Eine B<LOCK_EX>-BSD-Dateisperre (B<flock>(2)) auf der Protokolldatei wird "
"während der Durchführung der Messung und der Aktualisierung der Datei "
"erlangt\\&. Daher sollten Anwendungen, die planen, eine konsistente Angabe "
"aus dem TPM mit dem zugehörigen Schnappschuss des Ereignisprotokolls zu "
"erlangen, eine B<LOCK_SH>-Sperre erlangen, während sie dies tun\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemd-stub>(7), B<systemd-measure>(1), B<systemd-gpt-auto-"
"generator>(8), B<systemd-fstab-generator>(8), \\m[blue]B<TPM2 PCR "
"Measurements Made by systemd>\\m[]\\&\\s-2\\u[3]\\d\\s+2"
msgstr ""
"B<systemd>(1), B<systemd-stub>(7), B<systemd-measure>(1), B<systemd-gpt-auto-"
"generator>(8), B<systemd-fstab-generator>(8), \\m[blue]B<Von Systemd "
"durchgeführte TPM2-PCR-Messungen>\\m[]\\&\\s-2\\u[3]\\d\\s+2"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: IP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr " 1."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "JSON-SEQ"
msgstr "JSON-SEQ"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://www.rfc-editor.org/rfc/rfc7464.html"
msgstr "\\%https://www.rfc-editor.org/rfc/rfc7464.html"

#. type: IP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 2."
msgstr " 2."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "TCG Canonical Event Log Format (CEL-JSON)"
msgstr "Kanonisches TCG-Ereignisprotokollformat (CEL-JSON)"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\%https://trustedcomputinggroup.org/resource/canonical-event-log-format/"
msgstr ""
"\\%https://trustedcomputinggroup.org/resource/canonical-event-log-format/"

#. type: IP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid " 3."
msgstr " 3."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "TPM2 PCR Measurements Made by systemd"
msgstr "Von Systemd durchgeführte TPM2-PCR-Messungen"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "\\%https://systemd.io/TPM2_PCR_MEASUREMENTS"
msgstr "\\%https://systemd.io/TPM2_PCR_MEASUREMENTS"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: Plain text
#: debian-bookworm
msgid ""
"systemd-pcrphase.service, systemd-pcrphase-sysinit.service, systemd-pcrphase-"
"initrd.service, systemd-pcrmachine.service, systemd-pcrfs-root.service, "
"systemd-pcrfs@.service, systemd-pcrphase - Measure boot phase into TPM2 PCR "
"11, machine ID and file system identity into PCR 15"
msgstr ""
"systemd-pcrphase.service, systemd-pcrphase-sysinit.service, systemd-pcrphase-"
"initrd.service, systemd-pcrmachine.service, systemd-pcrfs-root.service, "
"systemd-pcrfs@.service, systemd-pcrphase - Systemstartphase in TPM2 PCR 11 "
"einmessen, Maschinenkennung und Dateisystemidentität in PCR 15"

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/systemd-pcrphase [I<STRING>]"
msgstr "/lib/systemd/systemd-pcrphase [I<ZEICHENKETTE>]"

#. type: Plain text
#: debian-bookworm
msgid ""
"The /lib/systemd/system-pcrphase executable may also be invoked from the "
"command line, where it expects the word to extend into PCR 11, as well as "
"the following switches:"
msgstr ""
"Das Programm /lib/systemd/system-pcrphase kann auch von der Befehlszeile "
"aufgerufen werden\\&. Dort erwartet es das Wort, das in PCR 11 erweitert "
"werden soll, sowie die folgenden Schalter:"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<systemd>(1), B<systemd-stub>(7), B<systemd-measure>(1), B<systemd-gpt-auto-"
"generator>(8), B<systemd-fstab-generator>(8)"
msgstr ""
"B<systemd>(1), B<systemd-stub>(7), B<systemd-measure>(1), B<systemd-gpt-auto-"
"generator>(8), B<systemd-fstab-generator>(8)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr "systemd 255"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "systemd 257.2"
msgstr "systemd 257.2"
