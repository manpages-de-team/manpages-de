# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2025-02-28 16:39+0100\n"
"PO-Revision-Date: 2024-02-16 16:58+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "malloc_get_state"
msgstr "malloc_get_state"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-07-23"
msgstr "23. Juli 2024"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.12"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"malloc_get_state, malloc_set_state - record and restore state of malloc "
"implementation"
msgstr ""
"malloc_get_state, malloc_set_state - Den Zustand der Malloc-Implementierung "
"aufzeichnen und wiederherstellen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>,\\ I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>malloc.hE<gt>>\n"
msgstr "B<#include E<lt>malloc.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void *malloc_get_state(void);>\n"
"B<int malloc_set_state(void *>I<state>B<);>\n"
msgstr ""
"B<void *malloc_get_state(void);>\n"
"B<int malloc_set_state(void *>I<zustand>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "I<Note>: these functions are removed in glibc 2.25."
msgstr "I<Hinweis>: Diese Funktionen wurden in Glibc 2.25 entfernt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<malloc_get_state>()  function records the current state of all "
"B<malloc>(3)  internal bookkeeping variables (but not the actual contents of "
"the heap or the state of B<malloc_hook>(3)  functions pointers).  The state "
"is recorded in a system-dependent opaque data structure dynamically "
"allocated via B<malloc>(3), and a pointer to that data structure is returned "
"as the function result.  (It is the caller's responsibility to B<free>(3)  "
"this memory.)"
msgstr ""
"Die Funktion B<malloc_get_state>() zeichnet den aktuellen Zustand aller "
"internen Buchhaltungsvariablen von B<malloc>(3) auf (aber nicht die "
"tatsächlichen Inhalte des Heaps oder den Zustand der Funktionszeiger "
"B<malloc_hook>(3)). Der Zustand wird in einer systemabhängigen, "
"nichtransparenten Datenstruktur aufgezeichnet, die dynamisch mittels "
"B<malloc>(3) belegt wird, und ein Zeiger auf diese Datenstruktur wird als "
"Funktionsergebnis zurückgeliefert. (Es ist die Aufgabe des Aufrufenden, "
"diesen Speicher mit B<free>(3) freizugeben.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<malloc_set_state>()  function restores the state of all B<malloc>(3)  "
"internal bookkeeping variables to the values recorded in the opaque data "
"structure pointed to by I<state>."
msgstr ""
"Die Funktion B<malloc_set_state>() stellt den Zustand aller internen "
"Buchhaltungsvariablen von B<malloc>(3) auf die in der nichtransparenten "
"Datenstruktur, auf die I<Zustand> zeigt, aufgezeichneten Werte wieder her."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<malloc_get_state>()  returns a pointer to a newly allocated "
"opaque data structure.  On error (for example, memory could not be allocated "
"for the data structure), B<malloc_get_state>()  returns NULL."
msgstr ""
"Im Erfolgsfall liefert B<malloc_get_state>() einen Zeiger auf eine frisch "
"belegte, nichttransparente Datenstruktur zurück. Im Fehlerfall (falls "
"beispielsweise der Speicher für die Datenstruktur nicht belegt werden "
"konnte) liefert B<malloc_get_state>() NULL zurück."

#.  if(ms->magic != MALLOC_STATE_MAGIC) return -1;
#.  /* Must fail if the major version is too high. */
#.  if((ms->version & ~0xffl) > (MALLOC_STATE_VERSION & ~0xffl)) return -2;
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"On success, B<malloc_set_state>()  returns 0.  If the implementation detects "
"that I<state> does not point to a correctly formed data structure, "
"B<malloc_set_state>()  returns -1.  If the implementation detects that the "
"version of the data structure referred to by I<state> is a more recent "
"version than this implementation knows about, B<malloc_set_state>()  returns "
"-2."
msgstr ""
"Im Erfolgsfall liefert B<malloc_set_state>() 0 zurück. Falls die "
"Implementierung erkennt, dass I<Zustand> nicht auf eine korrekt formatierte "
"Datenstruktur zeigt, liefert B<malloc_set_state>() -1 zurück. Falls die "
"Implementierung erkennt, dass die Version der Datenstruktur, auf die sich "
"I<Zustand> bezieht, eine neuere ist, als diese Implementierung kennt, dann "
"liefert B<malloc_set_state>() -2 zurück."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<malloc_get_state>(),\n"
"B<malloc_set_state>()"
msgstr ""
"B<malloc_get_state>(),\n"
"B<malloc_set_state>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Sicher"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"These functions are useful when using this B<malloc>(3)  implementation as "
"part of a shared library, and the heap contents are saved/restored via some "
"other method.  This technique is used by GNU Emacs to implement its "
"\"dumping\" function."
msgstr ""
"Diese Funktionen sind nützlich, wenn diese Implementierung von B<malloc>(3) "
"als Teil einer dynamischen Bibliothek verwandt wird und der Inhalt des Heaps "
"mittels einer anderen Methode gespeichert und wiederhergestellt wird. Diese "
"Technik wird von GNU Emacs verwandt, um seine Methode der Funktion »dumping« "
"zu implementieren."

#.  i.e., calls __malloc_check_init()
#.  i.e., malloc checking is not already in use
#.  and the caller requested malloc checking
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Hook function pointers are never saved or restored by these functions, with "
"two exceptions: if malloc checking (see B<mallopt>(3))  was in use when "
"B<malloc_get_state>()  was called, then B<malloc_set_state>()  resets malloc "
"checking hooks if possible; if malloc checking was not in use in the "
"recorded state, but the caller has requested malloc checking, then the hooks "
"are reset to 0."
msgstr ""
"Hook-Funktionszeiger werden durch diese Funktionen niemals gespeichert oder "
"wiederhergestellt. Es gibt aber zwei Ausnahmen: Wenn Malloc-Überprüfung beim "
"Aufruf von B<malloc_get_state>() in Verwendung war (siehe B<mallopt>(3)), "
"dann wird B<malloc_set_state>() die Malloc-Überprüfungs-Hooks falls möglich "
"zurücksetzen; falls Malloc-Überprüfung im aufgezeichneten Zustand nicht in "
"Verwendung war, aber der Aufrufende um Malloc-Überprüfung gebeten hat, dann "
"werden die Hooks auf 0 zurückgesetzt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<malloc>(3), B<mallopt>(3)"
msgstr "B<malloc>(3), B<mallopt>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15. Dezember 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

# FIXME function → functions
#. type: Plain text
#: debian-bookworm
msgid "I<Note>: these function are removed in glibc 2.25."
msgstr "I<Hinweis>: Diese Funktionen wurden in Glibc 2.25 entfernt."

#. type: Plain text
#: debian-bookworm
msgid "These functions are GNU extensions."
msgstr "Diese Funktionen sind GNU-Erweiterungen."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-05-02"
msgstr "2. Mai 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-11-01"
msgstr "1. November 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (unveröffentlicht)"
