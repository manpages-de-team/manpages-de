# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.25.1\n"
"POT-Creation-Date: 2025-02-28 16:36+0100\n"
"PO-Revision-Date: 2025-01-10 14:05+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "INFO"
msgstr "INFO"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "GNU Info"
msgstr "GNU Info"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FSF"
msgstr "FSF"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "info - readable online documentation"
msgstr "info - lesbare Online-Dokumentation"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The Info file format is an easily-parsable representation for online "
"documents.  It can be read by B<emacs>(1)  and B<info>(1)  among other "
"programs."
msgstr ""
"Das Info-Dateiformat ist eine leicht auswertbare Darstellung für Online-"
"Dokumente. Es kann unter anderem mit B<emacs>(1) und B<info>(1) gelesen "
"werden."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Info files are usually created from B<texinfo>(5)  sources by "
"B<makeinfo>(1) , but can be created from scratch if so desired."
msgstr ""
"Info-Dateien werden üblicherweise durch B<makeinfo>(1) aus B<texinfo>(5)-"
"Quellen erstellt, aber können auf Wunsch auch von Grund auf neu geschrieben "
"werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For a full description of the Texinfo language and associated tools, please "
"see the Texinfo manual (written in Texinfo itself).  Most likely, running "
"this command from your shell:"
msgstr ""
"Eine vollständige Beschreibung der Texinfo-Sprache und der zugehörigen "
"Werkzeuge finden Sie im Texinfo-Handbuch (das in Texinfo selbst geschrieben "
"ist). Im Allgemeinen können Sie in der Shell mit dem Befehl"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "info texinfo\n"
msgstr "info texinfo\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "or this key sequence from inside Emacs:"
msgstr "oder der Tastenkombination"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "M-x info RET m texinfo RET\n"
msgstr "M-x info RET m texinfo RET\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "will get you there."
msgstr "in Emacs zu diesem Handbuch gelangen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: archlinux fedora-42 fedora-rawhide mageia-cauldron
msgid "https://www.gnu.org/software/texinfo/"
msgstr "https://www.gnu.org/software/texinfo/"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Please send bug reports to bug-texinfo@gnu.org, general questions and "
"discussion to help-texinfo@gnu.org."
msgstr ""
"Schicken Sie Fehlermeldungen oder Kommentare (auf Englisch) an bug-"
"texinfo@gnu.org, allgemeine Fragen und Diskussionen an help-texinfo@gnu.org."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<info>(1), B<install-info>(1), B<makeinfo>(1), B<texi2dvi>(1),"
msgstr "B<info>(1), B<install-info>(1), B<makeinfo>(1), B<texi2dvi>(1),"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<texindex>(1)."
msgstr "B<texindex>(1)."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<emacs>(1), B<tex>(1)."
msgstr "B<emacs>(1), B<tex>(1)."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<texinfo>(5)."
msgstr "B<texinfo>(5)."

# FIXME I<emacs(1)> → B<emacs>(1)
# FIXME I<info(1)> → B<info>(1)
#. type: Plain text
#: debian-bookworm
msgid ""
"The Info file format is an easily-parsable representation for online "
"documents.  It can be read by I<emacs(1)> and I<info(1)> among other "
"programs."
msgstr ""
"Das Info-Dateiformat ist eine leicht auswertbare Darstellung für Online-"
"Dokumente. Es kann unter anderem mit B<emacs>(1) und B<info>(1) gelesen "
"werden."

# FIXME I<texinfo(5)> → B<texinfo>(5)
# FIXME I<makeinfo(1)> → B<makeinfo>(1)
#. type: Plain text
#: debian-bookworm
msgid ""
"Info files are usually created from I<texinfo(5)> sources by I<makeinfo(1)>, "
"but can be created from scratch if so desired."
msgstr ""
"Info-Dateien werden üblicherweise durch B<makeinfo>(1) aus B<texinfo>(5)-"
"Quellen erstellt, aber können auf Wunsch auch von Grund auf neu geschrieben "
"werden."

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-16-0 opensuse-tumbleweed
msgid "http://www.gnu.org/software/texinfo/"
msgstr "http://www.gnu.org/software/texinfo/"

#. type: Plain text
#: debian-bookworm
msgid "info(1), install-info(1), makeinfo(1), texi2dvi(1),"
msgstr "B<info>(1), B<install-info>(1), B<makeinfo>(1), B<texi2dvi>(1),"

#. type: Plain text
#: debian-bookworm
msgid "texindex(1)."
msgstr "B<texindex>(1),"

#. type: Plain text
#: debian-bookworm
msgid "emacs(1), tex(1)."
msgstr "B<emacs>(1), B<tex>(1),"

#. type: Plain text
#: debian-bookworm
msgid "texinfo(5)."
msgstr "B<texinfo>(5)"
