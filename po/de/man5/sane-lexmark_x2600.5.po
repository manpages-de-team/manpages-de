# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2025-02-16 06:00+0100\n"
"PO-Revision-Date: 2024-06-01 16:05+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: IX
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "sane-lexmark_x2600"
msgstr "sane-lexmark_x2600"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "23 Dec 2023"
msgstr "23. Dez 2023"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE Scanner Access Now Easy"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "sane-lexmark_x2600 - SANE backend for Lexmark X2600 Series scanners"
msgstr "sane-lexmark_x2600 - SANE-Backend für Lexmark-Scanner der Serie X2600"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<sane-lexmark> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to the scanner part of Lexmark X2600 AIOs."
msgstr ""
"Die Bibliothek B<sane-lexmark> implementiert ein SANE-(Scanner Access Now "
"Easy) Backend zum Zugriff auf den Scanner-Anteil von Lexmark X2600 AIOs."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The scanners that should work with this backend are:"
msgstr "Die folgenden Scanner sollten mit diesem Backend funktionieren:"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ""
"\\f(CR   Vendor Model           status\n"
"----------------------  -----------\n"
"  Lexmark X2670           good\\fR\n"
msgstr ""
"\\f(CR   Anbieter Modell           Status\n"
"----------------------  -----------\n"
"  Lexmark   X2670         gut\\fR\n"

# FIXME Superfluous spaces before commas and incorrect fullstop before "or"
#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The options the backend supports can either be selected through command line "
"options to programs like B<scanimage>(1)  or through GUI elements in "
"B<xscanimage>(1)  , B<xsane>(1).  or B<simple-scan>(1)."
msgstr ""
"Die vom Backend unterstützten Optionen können entweder über die "
"Befehlszeilenoptionen an Programme wie B<scanimage>(1) übergeben oder über "
"Bedienelemente der graphischen Benutzeroberfläche in B<xscanimage>(1), "
"B<xsane>(1) oder B<simple-scan>(1) gesteuert werden."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If you notice any strange behavior, please report to the backend maintainer "
"or to the SANE mailing list."
msgstr ""
"Falls Sie seltsames Verhalten bemerken, melden Sie dies an den Betreuer des "
"Backends oder an die Mailingliste der SANE-Entwickler."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-lexmark_x2600.a>"
msgstr "I</usr/lib/sane/libsane-lexmark_x2600.a>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "The static library implementing this backend."
msgstr "Die statische Bibliothek, die dieses Backend implementiert."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-lexmark_x2600.so>"
msgstr "I</usr/lib/sane/libsane-lexmark_x2600.so>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Die dynamische Bibliothek, die dieses Backend implementiert (auf Systemen "
"verfügbar, die dynamisches Laden unterstützen)."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_LEXMARK_X2600>"
msgstr "B<SANE_DEBUG_LEXMARK_X2600>"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend. E.g., a value of 255 "
"requests all debug output to be printed. Smaller levels reduce verbosity."
msgstr ""
"Falls die Bibliothek mit Fehlersuch-Unterstützung kompiliert wurde, steuert "
"diese Umgebungsvariable die Fehlersuchstufe für dieses Backend. "
"Beispielsweise bewirkt ein Wert von 255 die Anzeige sämtlicher "
"Fehlersuchausgaben. Kleinere Werte reduzieren die Ausführlichkeit."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "The backend was originally written by Benoit Juin."
msgstr "Dieses Backend wurde ursprünglich von Benoit Juin geschrieben."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "E<lt>I<benoit.juin@gmail.com>E<gt>"
msgstr "E<lt>I<benoit.juin@gmail.com>E<gt>"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "CREDITS"
msgstr "DANKSAGUNGEN"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Many thanks go to:"
msgstr "Vielen Dank an:"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"@skelband aka Ralph Little who help me to dive in the sane-backencode and "
"reviewed the sources."
msgstr ""
"@skelband alias Ralph Little, der mir beim Einarbeiten in den Code von "
"Backends und beim Prüfen der Quellen half."

#. type: TP
#: debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-lexmark_x2600.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-lexmark_x2600.a>"

#. type: TP
#: debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-lexmark_x2600.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-lexmark_x2600.so>"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-lexmark_x2600.a>"
msgstr "I</usr/lib64/sane/libsane-lexmark_x2600.a>"

#. type: TP
#: fedora-42 fedora-rawhide mageia-cauldron opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-lexmark_x2600.so>"
msgstr "I</usr/lib64/sane/libsane-lexmark_x2600.so>"
