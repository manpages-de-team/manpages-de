# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2015, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:57+0100\n"
"PO-Revision-Date: 2023-05-23 08:52+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "YUM-BUILDDEP"
msgstr "YUM-BUILDDEP"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Jan 22, 2023"
msgstr "22. Januar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "4.3.1"
msgstr "4.3.1"

#. type: TH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "dnf-plugins-core"
msgstr "dnf-plugins-core"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "yum-builddep - redirecting to DNF builddep Plugin"
msgstr "yum-builddep - Weiterleitung zum Builddep-Plugin von DNF"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Install whatever is needed to build the given .src.rpm, .nosrc.rpm or .spec "
"file."
msgstr ""
"Installiert alles, was zum Erstellen eines Binärpakets aus dem "
"angegebenen .src.rpm, .nosrc.rpm oder der .spec-Datei notwendig ist."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "B<WARNING:>"
msgstr "B<WARNUNG:>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Build dependencies in a package (i.e. src.rpm) might be different than you "
"would expect because they were evaluated according macros set on the package "
"build host."
msgstr ""
"Erstellungsabhängigkeiten in einem Paket (das heißt, einem .src.rpm) können "
"von dem abweichen, was Sie erwarten würden, weil diese anhand der auf dem "
"Erstellungsrechner gesetzten Makros ermittelt werden."

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "B<dnf builddep E<lt>packageE<gt>...>"
msgstr "B<dnf builddep E<lt>PaketE<gt> …>"

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "ARGUMENTS"
msgstr "ARGUMENTE"

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<E<lt>packageE<gt>>"
msgstr "B<E<lt>PaketE<gt>>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Either path to .src.rpm, .nosrc.rpm or .spec file or package available in a "
"repository."
msgstr ""
"Entweder der Pfad zur .src.rpm-, .nosrc.rpm- oder .spec-Datei oder zu einem "
"in einer Paketquelle verfügbaren Paket."

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

# FIXME B<dnf(8)> → B<dnf>(8)
#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"All general DNF options are accepted, see I<Options> in B<dnf(8)> for "
"details."
msgstr ""
"Alle allgemeinen DNF-Optionen werden akzeptiert, siehe I<Optionen> in "
"B<dnf>(8) für Details."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--help-cmd>"
msgstr "B<--help-cmd>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Show this help."
msgstr "zeigt die Hilfe an."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<-D E<lt>macro exprE<gt>, --define E<lt>macro exprE<gt>>"
msgstr "B<-D E<lt>Makro WertE<gt>, --define E<lt>Makro WertE<gt>>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Define the RPM macro named I<macro> to the value I<expr> when parsing spec "
"files. Does not apply for source rpm files."
msgstr ""
"setzt beim Auswerten von .spec-Dateien das angegebene I<Makro> auf den "
"angegeben I<Wert>. Dies ist nicht auf .src.rpm-Dateien anwendbar."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--spec>"
msgstr "B<--spec>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Treat arguments as .spec files."
msgstr "fasst Argumente als .spec-Dateien auf."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--srpm>"
msgstr "B<--srpm>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Treat arguments as source rpm files."
msgstr "fasst Argumente als .src.rpm-Dateien auf."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<--skip-unavailable>"
msgstr "B<--skip-unavailable>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Skip build dependencies not available in repositories. All available build "
"dependencies will be installed."
msgstr ""
"überspringt Erstellungsabhängigkeiten, die in den Paketquellen nicht "
"verfügbar sind. Alle verfügbaren Erstellungsabhängigkeiten werden "
"installiert."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Note that I<builddep> command does not honor the I<–skip-broken> option, so "
"there is no way to skip uninstallable packages (e.g. with broken "
"dependencies)."
msgstr ""
"Beachten Sie, dass der Befehl I<builddep> die Option I<--skip-broken> nicht "
"respektiert, so dass es keine Möglichkeit gibt, nicht installierbare Pakete "
"zu überspringen (zum Beispiel Pakete mit nicht auflösbaren Abhängigkeiten)."

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<dnf builddep foobar.spec>"
msgstr "B<dnf builddep foobar.spec>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "Install the needed build requirements, defined in the foobar.spec file."
msgstr ""
"installiert die erforderlichen Erstellungsabhängigkeiten, die in der Datei "
"foobar.spec definiert sind."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<dnf builddep --spec foobar.spec.in>"
msgstr "B<dnf builddep --spec foobar.spec.in>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Install the needed build requirements, defined in the spec file when "
"filename ends with something different than B<\\&.spec>\\&."
msgstr ""
"installiert die erforderlichen Erstellungsabhängigkeiten, die in der Spec-"
"Datei definiert sind, wenn die Datei mit etwas anderem endet als "
"B<\\&.spec>\\&."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<dnf builddep foobar-1.0-1.src.rpm>"
msgstr "B<dnf builddep foobar-1.0-1.src.rpm>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Install the needed build requirements, defined in the foobar-1.0-1.src.rpm "
"file."
msgstr ""
"installiert die erforderlichen Erstellungsabhängigkeiten, die in der Datei "
"foobar-1.0-1.src.rpm definiert sind."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<dnf builddep foobar-1.0-1>"
msgstr "B<dnf builddep foobar-1.0-1>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Look up foobar-1.0-1 in enabled repositories and install build requirements "
"for its source rpm."
msgstr ""
"schaut in den aktivierten Paketquellen nach foobar-1.0-1 und installiert die "
"Erstellungsabhängigkeiten für dessen .src.rpm."

#. type: TP
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "B<dnf builddep -D \\(aqscl python27\\(aq python-foobar.spec>"
msgstr "B<dnf builddep -D \\(aqscl python27\\(aq python-foobar.spec>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid ""
"Install the needed build requirements for the python27 SCL version of python-"
"foobar."
msgstr ""
"installiert die erforderlichen Erstellungsabhängigkeiten für die Python27-"
"SCL-Version von python-foobar."

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
msgid "See AUTHORS in your Core DNF Plugins distribution"
msgstr "Siehe AUTHORS im Paket der Core-DNF-Plugins."

#. type: SH
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-bookworm
msgid "2023, Red Hat, Licensed under GPLv2+"
msgstr "2023, Red Hat, lizenziert unter GPLv2+"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "Feb 12, 2025"
msgstr "12. Februar 2025"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "4.10.0"
msgstr "4.10.0"

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-unstable fedora-42 fedora-rawhide
msgid "2014, Red Hat, Licensed under GPLv2+"
msgstr "2014, Red Hat, lizenziert unter GPLv2+"

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "Jan 16, 2025"
msgstr "16. Januar 2025"

#. type: Plain text
#: fedora-42 fedora-rawhide
msgid ""
"Note that I<builddep> command does not honor the I<--skip-broken> option, so "
"there is no way to skip uninstallable packages (e.g. with broken "
"dependencies)."
msgstr ""
"Beachten Sie, dass der Befehl I<builddep> die Option I<--skip-broken> nicht "
"respektiert, so dass es keine Möglichkeit gibt, nicht installierbare Pakete "
"zu überspringen (zum Beispiel Pakete mit nicht auflösbaren Abhängigkeiten)."
