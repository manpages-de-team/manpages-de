# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2025-02-16 06:01+0100\n"
"PO-Revision-Date: 2023-08-27 21:08+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "SBSIGN"
msgstr "SBSIGN"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "September 2024"
msgstr "September 2024"

#. type: TH
#: archlinux fedora-42 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "sbsign 0.9.5"
msgstr "sbsign 0.9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
msgid "sbsign - UEFI secure boot signing tool"
msgstr "sbsign - Werkzeug zum Signieren des sicheren UEFI-Systemstarts"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
msgid ""
"B<sbsign> [I<\\,options\\/>] I<\\,--key E<lt>keyfileE<gt> --cert "
"E<lt>certfileE<gt> E<lt>efi-boot-imageE<gt>\\/>"
msgstr ""
"B<sbsign> [I<\\,Optionen\\/>] I<\\,--key E<lt>SchlüsseldateiE<gt> --cert "
"E<lt>ZertifikatsdateiE<gt> E<lt>EFI-Systemstart-AbbildE<gt>\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
msgid "Sign an EFI boot image for use with secure boot."
msgstr ""
"Ein EFI-Systemstart-Abbild für den Einsatz beim sicheren Systemstart "
"signieren."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--engine> E<lt>engE<gt>"
msgstr "B<--engine> E<lt>EinheitE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
msgid "use the specified engine to load the key"
msgstr "Verwendet die angegebene Einheit, um den Schlüssel zu laden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--key> E<lt>keyfileE<gt>"
msgstr "B<--key> E<lt>SchlüsseldateiE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
msgid "signing key (PEM-encoded RSA private key)"
msgstr "Signaturschlüssel (PEM-kodierter privater Schlüssel)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--cert> E<lt>certfileE<gt>"
msgstr "B<--cert> E<lt>ZertifikatsdateiE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
msgid "certificate (x509 certificate)"
msgstr "Zertifikat (X509-Zertifikat)"

# FIXME Missing line break
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
msgid ""
"B<--addcert> E<lt>addcertfileE<gt> additional intermediate certificates in a "
"file"
msgstr ""
"B<--addcert> E<lt>ZusatzzertifikatsdateiE<gt> Zusätzliche "
"Zwischenzertifikate in einer Datei."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--detached>"
msgstr "B<--detached>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
msgid "write a detached signature, instead of a signed binary"
msgstr ""
"Schreibt eine abgetrennte Signatur anstelle eines signierten Programms."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--output> E<lt>fileE<gt>"
msgstr "B<--output> E<lt>DateiE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: opensuse-tumbleweed
msgid ""
"write signed data to E<lt>fileE<gt> (default E<lt>efi-boot-"
"imageE<gt>.signed, or E<lt>efi-boot-imageE<gt>.pk7 for detached signatures)"
msgstr ""
"schreibt signierte Daten nach E<lt>DateiE<gt> (Vorgabe E<lt>efi-boot-"
"imageE<gt>.signed oder E<lt>efi-boot-imageE<gt>.pk7 für abgetrennte "
"Signaturen)."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "June 2022"
msgstr "Juni 2022"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "sbsign 0.9.4"
msgstr "sbsign 0.9.4"

#. type: TH
#: fedora-42 fedora-rawhide
#, no-wrap
msgid "January 2025"
msgstr "Januar 2025"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "April 2023"
msgstr "April 2023"
