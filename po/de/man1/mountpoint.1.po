# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2018, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2024-03-29 09:45+0100\n"
"PO-Revision-Date: 2021-05-22 19:29+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.04.1\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "MOUNTPOINT"
msgstr "MOUNTPOINT"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm
msgid "mountpoint - see if a directory or file is a mountpoint"
msgstr ""
"mountpoint - Überprüfen, ob ein Verzeichnis oder eine Datei ein "
"Einhängepunkt ist"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm
msgid "B<mountpoint> [B<-d>|B<-q>] I<directory>|I<file>"
msgstr "B<mountpoint> [B<-d>|B<-q>] I<Verzeichnis>|I<Datei>"

#. type: Plain text
#: debian-bookworm
msgid "B<mountpoint> B<-x> I<device>"
msgstr "B<mountpoint> B<-x> I<Gerät>"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<mountpoint> checks whether the given I<directory> or I<file> is mentioned "
"in the I</proc/self/mountinfo> file."
msgstr ""
"B<mountpoint> überprüft, ob das angegebene I<Verzeichnis> oder die "
"angegebene I<Datei> in der Datei I</proc/self/mountinfo> aufgeführt ist."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm
msgid "B<-d>, B<--fs-devno>"
msgstr "B<-d>, B<--fs-devno>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Show the major/minor numbers of the device that is mounted on the given "
"directory."
msgstr ""
"zeigt die Major/Minor-Nummern des Geräts an, das in dem angegebenen "
"Verzeichnis eingehängt ist."

#. type: Plain text
#: debian-bookworm
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: debian-bookworm
msgid "Be quiet - don\\(cqt print anything."
msgstr "unterdrückt die Ausgaben."

#. type: Plain text
#: debian-bookworm
msgid "B<--nofollow>"
msgstr "B<--nofollow>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Do not follow symbolic link if it the last element of the I<directory> path."
msgstr ""
"folgt keinem symbolischen Link, wenn er das letzte Element in einem "
"I<Verzeichnis>-Pfad ist."

#. type: Plain text
#: debian-bookworm
msgid "B<-x>, B<--devno>"
msgstr "B<-x>, B<--devno>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Show the major/minor numbers of the given blockdevice on standard output."
msgstr ""
"zeigt die Major/Minor-Nummern des angegebenen blockorientierten Geräts in "
"der Standardausgabe an."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "zeigt die Versionsnummer an und beendet das Programm."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr "EXIT-STATUS"

#. type: Plain text
#: debian-bookworm
msgid "B<mountpoint> has the following exit status values:"
msgstr "B<mountpoint> hat die folgenden Exit-Status-Werte:"

#. type: Plain text
#: debian-bookworm
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: debian-bookworm
msgid ""
"success; the directory is a mountpoint, or device is block device on B<--"
"devno>"
msgstr ""
"Erfolg; das Verzeichnis ist ein Einhängepunkt, oder das Gerät ist ein "
"Blockgerät (mit B<--devno>)"

#. type: Plain text
#: debian-bookworm
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: debian-bookworm
msgid "failure; incorrect invocation, permissions or system error"
msgstr "Fehlschlag; fehlerhafter Aufruf, Zugriffsrechte- oder Systemfehler"

#. type: Plain text
#: debian-bookworm
msgid "B<32>"
msgstr "B<32>"

#. type: Plain text
#: debian-bookworm
msgid ""
"failure; the directory is not a mountpoint, or device is not a block device "
"on B<--devno>"
msgstr ""
"Fehlschlag; das Verzeichnis ist kein Einhängepunkt, oder das Gerät ist kein "
"Blockgerät (mit B<--devno>)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: Plain text
#: debian-bookworm
msgid "B<LIBMOUNT_DEBUG>=all"
msgstr "B<LIBMOUNT_DEBUG>=all"

#. type: Plain text
#: debian-bookworm
msgid "enables libmount debug output."
msgstr "aktiviert die Fehlersuchausgabe für libmount."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: debian-bookworm
msgid ""
"The util-linux B<mountpoint> implementation was written from scratch for "
"libmount. The original version for sysvinit suite was written by Miquel van "
"Smoorenburg."
msgstr ""
"Die B<mountpoint>-Implementation von Util-linux wurde für Libmount von Grund "
"auf neu geschrieben. Die Originalversion für die SysVinit-Suite wurde von "
"Miquel van Smoorenburg geschrieben."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm
msgid "B<mount>(8)"
msgstr "B<mount>(8)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<mountpoint> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<mountpoint> ist Teil des Pakets util-linux, welches "
"heruntergeladen werden kann von:"
