# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.2.0\n"
"POT-Creation-Date: 2024-03-29 09:37+0100\n"
"PO-Revision-Date: 2021-05-22 00:12+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.04.1\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "CHOOM"
msgstr "CHOOM"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm
msgid "choom - display and adjust OOM-killer score."
msgstr "choom - Bewertungen des OOM-Killers anzeigen und anpassen"

#. type: Plain text
#: debian-bookworm
msgid "B<choom> B<-p> I<PID>"
msgstr "B<choom> B<-p> I<Prozesskennung>"

#. type: Plain text
#: debian-bookworm
msgid "B<choom> B<-p> I<PID> B<-n> I<number>"
msgstr "B<choom> B<-p> I<Prozesskennung> B<-n> I<Zahl>"

#. type: Plain text
#: debian-bookworm
msgid "B<choom> B<-n> I<number> [--] I<command> [I<argument> ...]"
msgstr "B<choom> B<-n> I<Zahl> [--] I<Befehl> [I<Argument> …]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<choom> command displays and adjusts Out-Of-Memory killer score setting."
msgstr ""
"Der Befehl B<choom> zeigt und passt die Bewertungseinstellungen des "
"Speicherknappheit-Killers an."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--pid> I<pid>"
msgstr "B<-p>, B<--pid> I<Prozesskennung>"

#. type: Plain text
#: debian-bookworm
msgid "Specifies process ID."
msgstr "gibt die Prozesskennung an."

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--adjust> I<value>"
msgstr "B<-n>, B<--adjust> I<Wert>"

#. type: Plain text
#: debian-bookworm
msgid "Specify the adjust score value."
msgstr "gibt die Anpassungsbewertung an."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "zeigt die Versionsnummer an und beendet das Programm."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: debian-bookworm
msgid ""
"Linux kernel uses the badness heuristic to select which process gets killed "
"in out of memory conditions."
msgstr ""
"Der Linux-Kernel verwendet die Schlechtigkeits-Heuristik, um auszuwählen, "
"welcher Prozess getötet werden soll, wenn der Speicher ausgeschöpft ist."

#. type: Plain text
#: debian-bookworm
msgid ""
"The badness heuristic assigns a value to each candidate task ranging from 0 "
"(never kill) to 1000 (always kill) to determine which process is targeted. "
"The units are roughly a proportion along that range of allowed memory the "
"process may allocate from based on an estimation of its current memory and "
"swap use. For example, if a task is using all allowed memory, its badness "
"score will be 1000. If it is using half of its allowed memory, its score "
"will be 500."
msgstr ""
"Die Schlechtigkeits-Heuristik weist jedem möglichen Prozess einen Wert von 0 "
"(niemals töten) bis 1000 (immer töten) zu, um zu bestimmen, welcher Prozess "
"infrage kommt. Der Wert beschreibt im Wesentlichen den kontinuierlichen "
"Anteil des erlaubten Speichers, aus dem sich der Prozess Speicher zuweisen "
"darf. Als Grundlage dient dazu der aktuelle verwendete Speicher und  "
"Auslagerungsspeicher. Wenn ein Prozess beispielsweise sämtlichen erlaubten "
"Speicher nutzt, ist dessen Schlechtigkeitsbewertung bei 1000. Nutzt er die "
"Hälfte des erlaubten Speichers, beträgt die Bewertung 500."

#. type: Plain text
#: debian-bookworm
msgid ""
"There is an additional factor included in the badness score: the current "
"memory and swap usage is discounted by 3% for root processes."
msgstr ""
"Es gibt einen weiteren Faktor in der Schlechtigkeitsbewertung: Die aktuelle "
"Speicher- und Auslagerungsspeichernutzung wird für Root-Prozesse um 3% "
"zurückgesetzt."

#. type: Plain text
#: debian-bookworm
msgid ""
"The amount of \"allowed\" memory depends on the context in which the oom "
"killer was called. If it is due to the memory assigned to the allocating "
"task\\(cqs cpuset being exhausted, the allowed memory represents the set of "
"mems assigned to that cpuset. If it is due to a mempolicy\\(cqs node(s) "
"being exhausted, the allowed memory represents the set of mempolicy nodes. "
"If it is due to a memory limit (or swap limit) being reached, the allowed "
"memory is that configured limit. Finally, if it is due to the entire system "
"being out of memory, the allowed memory represents all allocatable resources."
msgstr ""
"Die Menge des »erlaubten« Speichers hängt von dem Kontext ab, in dem der OOM-"
"Killer aufgerufen wurde. Falls der Kontext ist, dass der oder die "
"Prozessoren ausgelastet sind, entspricht der erlaubte Speicher dem Speicher, "
"der diesem Cpuset zugewiesen ist. Falls der oder die Mempolicy-Knoten "
"erschöpft ist/sind, repräsentiert der erlaubte Speicher die Mempolicy-"
"Knoten. Falls eine Arbeitsspeicher- (oder Auslagerungsspeicher-) Grenze "
"erreicht wurde, entspricht der erlaubte Speicher dieser Grenze. Sollte das "
"gesamte System keinen Speicher mehr übrig haben, steht der erlaubte Speicher "
"für alle verfügbaren Ressoucen."

#. type: Plain text
#: debian-bookworm
msgid ""
"The adjust score value is added to the badness score before it is used to "
"determine which task to kill. Acceptable values range from -1000 to +1000. "
"This allows userspace to polarize the preference for oom killing either by "
"always preferring a certain task or completely disabling it. The lowest "
"possible value, -1000, is equivalent to disabling oom killing entirely for "
"that task since it will always report a badness score of 0."
msgstr ""
"Die Anpassungsbewertung wird zum Schlechtigkeitswert hinzugefügt, bevor "
"dieser zur Ermittlung des zu tötenden Prozesses verwendet wird. Zulässige "
"Werte liegen zwischen -1000 und +1000, einschließlich dieser. Dies "
"ermöglicht auf Anwendungsebene die Polarisierung der Bevorzugung für das OOM-"
"Töten entweder durch permanentes Bevorzugen eines bestimmten Prozesses oder "
"deren vollständige Deaktivierung. Der niedrigste mögliche Wert von -1000 ist "
"gleichbedeutend mit der vollständigen Deaktivierung des OOM-Tötenss für "
"diesen Prozess, da diese stets einen Schlechtigkeitswert von 0 meldet."

#. type: Plain text
#: debian-bookworm
msgid ""
"Setting an adjust score value of +500, for example, is roughly equivalent to "
"allowing the remainder of tasks sharing the same system, cpuset, mempolicy, "
"or memory controller resources to use at least 50% more memory. A value of "
"-500, on the other hand, would be roughly equivalent to discounting 50% of "
"the task\\(cqs allowed memory from being considered as scoring against the "
"task."
msgstr ""
"Die Festlegung eines Anpassungswerts von +500 ist beispielsweise etwa "
"gleichbedeutend damit, dass die übrigen Prozesse, die das gleiche System, "
"Cpuset, Mempolicy oder Speicher-Controller-Ressource mit verwenden, "
"mindestens 50% mehr Speicher verwenden. Andererseits wäre ein Wert von -500 "
"etwa gleichbedeutend damit, den erlaubten Speicher eines Prozesses bei der "
"Bewertung des erlaubten Speichers des Prozesses um 50% zu reduzieren."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<choom> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<choom> ist Teil des Pakets util-linux, welches heruntergeladen "
"werden kann von:"
