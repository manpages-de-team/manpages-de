# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2025.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.25.0\n"
"POT-Creation-Date: 2025-01-10 10:28+0100\n"
"PO-Revision-Date: 2025-01-11 08:59+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "MKARCHROOT"
msgstr "MKARCHROOT"

#. type: TH
#: archlinux
#, no-wrap
msgid "2025-01-06"
msgstr "6. Januar 2025"

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr "\\ \" "

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr "\\ \""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux
msgid ""
"mkarchroot - Creates an arch chroot in a specified location with a specified "
"set of packages"
msgstr ""
"mkarchroot - Erstellt eine Arch-Chroot an einem angegebenen Ort mit einer "
"festgelegten Gruppe an Paketen"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

# FIXME mkarchroot → B<mkarchroot>
# FIXME [options] → I<[options]>
# FIXME [location] → I<[location]>
# FIXME [packages] → I<[packages]>
#. type: Plain text
#: archlinux
msgid "mkarchroot [options] [location] [packages]"
msgstr "B<mkarchroot> I<[Optionen]> I<[Ort]> I<[Pakete]>"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME I<mkarchroot> → B<mkarchroot>
#. type: Plain text
#: archlinux
msgid ""
"I<mkarchroot> is a script to create an Arch Linux chroot at a specified "
"location with specified packages. Typically used by I<makechrootpkg> to "
"create build chroots. Apart from installing specified packages the chroot is "
"created with an en_US.UTF-8 and de_DE.UTF-8 locale and a generated machine-"
"id."
msgstr ""
"B<mkarchroot> ist ein Skript zur Erstellung einer Arch-Linux-Chroot an einem "
"angegebenen Ort mit den angegebenen Paketen. Typischerweise wird "
"B<makechrootpkg> zur Erstellung von Bau-Chroots verwandt. Abgesehen von der "
"Installation der angegebenen Pakete wird die Chroot mit einer en_US.UTF-8- "
"und de_DE.UTF-8-Locale und einer generierten Maschinenkennung erstellt."

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux
msgid "B<-U>"
msgstr "B<-U>"

# FIXME I<pacman -U> → B<pacman -U>
#. type: Plain text
#: archlinux
msgid "Use I<pacman -U> to install packages."
msgstr "Verwendet zur Installation von Paketen B<pacman -U>."

#. type: Plain text
#: archlinux
msgid "B<-C> E<lt>fileE<gt>"
msgstr "B<-C> E<lt>DateiE<gt>"

# FIXME pacman → B<pacman>(8)
#. type: Plain text
#: archlinux
msgid "Location of a pacman config file."
msgstr "Ort einer B<pacman>(8)-Konfigurationsdatei."

#. type: Plain text
#: archlinux
msgid "B<-M> E<lt>fileE<gt>"
msgstr "B<-M> E<lt>DateiE<gt>"

# FIXME makepkg → B<makepkg>(8)
#. type: Plain text
#: archlinux
msgid "Location of a makepkg config file."
msgstr "Ort einer B<makepkg>(8)-Konfigurationsdatei."

#. type: Plain text
#: archlinux
msgid "B<-c> E<lt>dirE<gt>"
msgstr "B<-c> E<lt>VerzE<gt>"

# FIXME pacman → B<pacman>(8)
#. type: Plain text
#: archlinux
msgid "Set pacman cache."
msgstr "Setzt den Zwischenspeicher von B<pacman>(8)."

#. type: Plain text
#: archlinux
msgid "B<-f> E<lt>srcE<gt>[:E<lt>dstE<gt>]"
msgstr "B<-f> E<lt>QuelleE<gt>[:E<lt>ZielE<gt>]"

# FIXME file → files?
#. type: Plain text
#: archlinux
msgid ""
"Copy file from the host to the chroot.  If I<dst> is not provided, it "
"defaults to I<src> inside of the chroot."
msgstr ""
"Kopiert Datei vom Rechner in die Chroot. Falls I<Ziel> nicht bereitgestellt "
"wird, ist die Vorgabe I<Quelle> innerhalb der Chroot."

#. type: Plain text
#: archlinux
msgid "B<-s>"
msgstr "B<-s>"

# FIXME setarch. → B<setarch>(8).
#. type: Plain text
#: archlinux
msgid "Do not run setarch."
msgstr "Führt B<setarch>(8) nicht aus."

#. type: Plain text
#: archlinux
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: archlinux
msgid "Output command line options."
msgstr "Gibt die Befehlszeilenoptionen aus."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

# FIXME pacman(1) → B<pacman>(8)
#. type: Plain text
#: archlinux
msgid "pacman(1)"
msgstr "B<pacman>(8)"

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr "HOMEPAGE"

#. type: Plain text
#: archlinux
msgid ""
"I<Please report bugs and feature requests in the issue tracker. Please do "
"your best to provide a reproducible test case for bugs.>"
msgstr ""
"I<Bitte melden Sie Fehler und Funktionalitätswünsche auf Englisch in dem "
"Fehlererfassungssystem. Bitte versuchen Sie bei Fehlern so gut wie möglich, "
"einen reproduzierbaren Testfall zu erstellen.>"
