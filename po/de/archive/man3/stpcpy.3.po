# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-08-27 17:24+0200\n"
"PO-Revision-Date: 2023-06-14 18:09+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "STPCPY"
msgstr "STPCPY"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2016-03-15"
msgstr "15. März 2016"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: opensuse-leap-15-6
msgid "stpcpy - copy a string returning a pointer to its end"
msgstr ""
"stpcpy - Eine Zeichenkette kopieren, die einen Zeiger an ihr Ende "
"zurückliefert"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid "B<char *stpcpy(char *>I<dest>B<, const char *>I<src>B<);>\n"
msgstr "B<char *stpcpy(char *>I<Ziel>B<, const char *>I<Quelle>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<stpcpy>():"
msgstr "B<stpcpy>():"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Seit Glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "Vor Glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<stpcpy>()  function copies the string pointed to by I<src> (including "
"the terminating null byte (\\(aq\\e0\\(aq)) to the array pointed to by "
"I<dest>.  The strings may not overlap, and the destination string I<dest> "
"must be large enough to receive the copy."
msgstr ""
"Die Funktion B<stpcpy>() kopiert die Zeichenkette, auf die I<Quelle> zeigt "
"(einschließlich des abschließenden Nullbytes (»\\e0«)) in das Feld, auf das "
"I<Ziel> zeigt. Die Zeichenketten dürfen sich nicht überlappen und die "
"Zielzeichenkette I<Ziel> muss groß genug sein, um die Kopie zu aufzunehmen."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<stpcpy>()  returns a pointer to the B<end> of the string I<dest> (that is, "
"the address of the terminating null byte)  rather than the beginning."
msgstr ""
"B<stpcpy>() liefert einen Zeiger auf das B<Ende> der Zeichenkette I<Ziel> "
"zurück (das heißt, die Adresse des abschließenden Nullbytes), anstatt auf "
"den Anfang."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "B<stpcpy>()"
msgstr "B<stpcpy>()"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: opensuse-leap-15-6
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This function was added to POSIX.1-2008.  Before that, it was not part of "
"the C or POSIX.1 standards, nor customary on UNIX systems.  It first "
"appeared at least as early as 1986, in the Lattice C AmigaDOS compiler, then "
"in the GNU fileutils and GNU textutils in 1989, and in the GNU C library by "
"1992.  It is also present on the BSDs."
msgstr ""
"Diese Funktion wurde in POSIX.1-2008 hinzugefügt. Vorher war sie nicht Teil "
"der C- oder POSIX.1-Standards noch auf herkömmlichen UNIX-Systemen. Sie "
"erschien erstmalig zumindest 1986, in dem Lattice-C-AmigaDOS-Compiler, dann "
"1989 in den GNU fileutils und GNU textutils und 1992 in der GNU C-"
"Bibliothek. Sie ist auch auf den BSDs vorhanden."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: opensuse-leap-15-6
msgid "This function may overrun the buffer I<dest>."
msgstr "Diese Funktion kann einen Pufferüberlauf von I<Ziel> verursachen."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "BEISPIEL"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"For example, this program uses B<stpcpy>()  to concatenate B<foo> and B<bar> "
"to produce B<foobar>, which it then prints."
msgstr ""
"Beispielsweise verwendet dieses Programm B<stpcpy>(), um B<foo> und B<bar> "
"aneinanderzuhängen, um B<foobar> zu erstellen, welches es dann ausgibt."

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    char buffer[20];\n"
"    char *to = buffer;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    char buffer[20];\n"
"    char *to = buffer;\n"

#. type: Plain text
#: opensuse-leap-15-6
#, no-wrap
msgid ""
"    to = stpcpy(to, \"foo\");\n"
"    to = stpcpy(to, \"bar\");\n"
"    printf(\"%s\\en\", buffer);\n"
"}\n"
msgstr ""
"    to = stpcpy(to, \"foo\");\n"
"    to = stpcpy(to, \"bar\");\n"
"    printf(\"%s\\en\", buffer);\n"
"}\n"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<bcopy>(3), B<memccpy>(3), B<memcpy>(3), B<memmove>(3), B<stpncpy>(3), "
"B<strcpy>(3), B<string>(3), B<wcpcpy>(3)"
msgstr ""
"B<bcopy>(3), B<memccpy>(3), B<memcpy>(3), B<memmove>(3), B<stpncpy>(3), "
"B<strcpy>(3), B<string>(3), B<wcpcpy>(3)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
