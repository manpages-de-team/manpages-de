# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2023-02-15 18:53+0100\n"
"PO-Revision-Date: 2022-04-23 22:52+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GRUB-RPM-SORT"
msgstr "GRUB-RPM-SORT"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2022"
msgstr "Oktober 2022"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemverwaltungswerkzeuge"

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: mageia-cauldron
msgid "grub-rpm-sort - sort input according to RPM version compare"
msgstr "grub-rpm-sort - Eingabe anhand eines RPM-Versionsvergleichs sortieren"

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: mageia-cauldron
msgid "B<grub-rpm-sort> [I<\\,OPTION\\/>...] [I<\\,INPUT_FILES\\/>]"
msgstr "B<grub-rpm-sort> [I<\\,OPTION\\/> …] [I<\\,EINGABEDATEIEN\\/>]"

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: mageia-cauldron
msgid "Sort a list of strings in RPM version sort order."
msgstr "Eine Liste von Zeichenketten in der RPM-Versionsreihenfolge sortieren."

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "-?, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: Plain text
#: mageia-cauldron
msgid "give this help list"
msgstr "gibt eine kurze Hilfe aus."

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: mageia-cauldron
msgid "give a short usage message"
msgstr "gibt eine kurze Meldung zur Verwendung aus."

#. type: TP
#: mageia-cauldron
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: mageia-cauldron
msgid "print program version"
msgstr "gibt die Programmversion aus."

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: mageia-cauldron
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Melden Sie Fehler (auf Englisch) an E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: mageia-cauldron
msgid ""
"The full documentation for B<grub-rpm-sort> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-rpm-sort> programs are properly installed "
"at your site, the command"
msgstr ""
"Die vollständige Dokumentation für B<grub-rpm-sort> wird als ein Texinfo-"
"Handbuch gepflegt. Wenn die Programme B<info>(1) und B<grub-rpm-sort> auf "
"Ihrem Rechner ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: mageia-cauldron
msgid "B<info grub-rpm-sort>"
msgstr "B<info grub-rpm-sort>"

#. type: Plain text
#: mageia-cauldron
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."
