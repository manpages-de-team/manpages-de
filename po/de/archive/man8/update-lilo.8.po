# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2021-12-25 19:51+0100\n"
"PO-Revision-Date: 2023-06-12 17:45+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: ds C+
#: debian-unstable
#, no-wrap
msgid "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"
msgstr "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"

#.  ========================================================================
#. type: IX
#: debian-unstable
#, no-wrap
msgid "Title"
msgstr "Title"

#.  ========================================================================
#. type: IX
#: debian-unstable
#, no-wrap
msgid "UPDATE-LILO 8"
msgstr "UPDATE-LILO 8"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "UPDATE-LILO"
msgstr "UPDATE-LILO"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2013-06-07"
msgstr "7. Juni 2013"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "24.0"
msgstr "24.0"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "lilo documentation"
msgstr "Lilo-Dokumentation"

#. type: SH
#: debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-unstable
msgid "update-lilo - execute lilo in special debconf mode"
msgstr "update-lilo - Lilo in einem besonderen Debconf-Modus ausführen"

#. type: IX
#: debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: IX
#: debian-unstable
#, no-wrap
msgid "Header"
msgstr "Header"

#. type: Plain text
#: debian-unstable
msgid "\\&B<update-lilo>"
msgstr "\\&B<update-lilo>"

#. type: IX
#: debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-unstable
msgid ""
"This special script only execute lilo with sending all output to E<gt>$2"
msgstr ""
"Dieses besondere Skript führt B<lilo>(8) nur aus, wenn alle Ausgaben nach "
"E<gt>$2 geschickt werden."

#. type: IX
#: debian-unstable
#, no-wrap
msgid "COPYRIGHT and LICENSE"
msgstr "COPYRIGHT und LIZENZ"

#. type: Plain text
#: debian-unstable
msgid "Copyright (C) 2010-2013 Joachim Wiedorn"
msgstr "Copyright (C) 2010-2013 Joachim Wiedorn"

#. type: Plain text
#: debian-unstable
msgid ""
"This script is free software; you can redistribute it and/or modify it under "
"the terms of the \\s-1GNU\\s0 General Public License as published by the "
"Free Software Foundation; either version 2 of the License, or (at your "
"option) any later version."
msgstr ""
"Dieses Skript ist freie Software; Sie können es unter den Bedingungen der "
"\\s-1GNU\\s0 General Public License weitergeben und/oder verändern, so wie "
"sie von der Free Software Foundation veröffentlicht wurde; entweder in "
"Version 2 der Lizenz oder (nach Ihrem Ermessen) in jeder späteren Version."

#. type: Plain text
#: debian-unstable
msgid ""
"On Debian systems, the complete text of the \\s-1GNU\\s0 General Public "
"License version 2 can be found in `/usr/share/common-licenses/GPL-2'."
msgstr ""
"Auf Debian-Systemen kann der vollständige Text der »General Public License« "
"in Version 2 unter I</usr/share/common-licenses/GPL-2> gefunden werden."

#. type: IX
#: debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-unstable
msgid "\\&B<update-lilo> was written by Joachim Wiedorn."
msgstr "\\&B<update-lilo> wurde von Joachim Wiedorn geschrieben."

#. type: Plain text
#: debian-unstable
msgid ""
"This manual page was written by Joachim Wiedorn E<lt>joodebian at joonet."
"deE<gt> for the Debian project (and may be used by others)."
msgstr ""
"Diese Handbuchseite wurde von Joachim Wiedorn E<lt>joodebian at joonet."
"deE<gt> für das Debian-Projekt geschrieben, darf aber auch von anderen "
"verwandt werden."

#. type: IX
#: debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-unstable
msgid "\\&B<lilo>(8), B<lilo-uuid-diskid>(1), B<liloconfig>(8)"
msgstr "\\&B<lilo>(8), B<lilo-uuid-diskid>(1), B<liloconfig>(8)"
