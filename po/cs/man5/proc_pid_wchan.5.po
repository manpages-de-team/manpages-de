# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Pavel Heimlich <tropikhajma@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2025-02-28 16:44+0100\n"
"PO-Revision-Date: 2024-03-29 09:48+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Czech <diskuze-l10n-cz@lists.openalt.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_wchan"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-05-02"
msgstr "2. května 2024"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.06"
msgid "Linux man-pages 6.12"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "/proc/pid/wchan - wait channel"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: TP
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</wchan> (since Linux 2.6.0)"
msgstr "I</proc/>pidI</wchan> (od Linuxu 2.6.0)"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The symbolic name corresponding to the location in the kernel where the "
"process is sleeping."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Permission to access this file is governed by a ptrace access mode "
"B<PTRACE_MODE_READ_FSCREDS> check; see B<ptrace>(2)."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Linux man-pages 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr "15. srpna 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Linux man-pages (nevydané)"
