# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Ondřej Pavlíček <mox@post.cz>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-29 09:57+0100\n"
"PO-Revision-Date: 2001-09-02 20:06+0100\n"
"Last-Translator: Ondřej Pavlíček <mox@post.cz>\n"
"Language-Team: Czech <diskuze-l10n-cz@lists.openalt.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "WRITE"
msgstr "WRITE"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11. května 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr "Příručka uživatele"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: debian-bookworm
msgid "write - send a message to another user"
msgstr "write - pošle zprávu jinému uživateli"

#. type: Plain text
#: debian-bookworm
msgid "B<write> I<user> [I<ttyname>]"
msgstr "B<write> I<uživatel> [I<jméno_tty>]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<write> allows you to communicate with other users, by copying lines from "
"your terminal to theirs."
msgstr ""
"B<write> vám umožňuje komunikovat s ostatními uživateli, pomocí kopírování "
"řádků z vašeho terminálu na jejich."

#. type: Plain text
#: debian-bookworm
msgid ""
"When you run the B<write> command, the user you are writing to gets a "
"message of the form:"
msgstr ""
"Když spustíte příkaz B<write>, uživatel, kterému píšete, dostane zprávu ve "
"formě:"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "Message from yourname@yourhost on yourtty at hh:mm ...\n"
msgstr "Zpráva od vaše_jméno@váš_počítač na vaše_tty v hh:mm ...\n"

#. type: Plain text
#: debian-bookworm
msgid ""
"Any further lines you enter will be copied to the specified user\\(cqs "
"terminal. If the other user wants to reply, they must run B<write> as well."
msgstr ""
"Každé další napsané řádky se zkopírují na určený uživatelský terminál.  "
"Pokud chce druhý uživatel odpovědět, musí také spustit B<write>."

#. type: Plain text
#: debian-bookworm
msgid ""
"When you are done, type an end-of-file or interrupt character. The other "
"user will see the message B<EOF> indicating that the conversation is over."
msgstr ""
"Když chcete skončit, vložte znak konce souboru (end-of-file, tj. CTRL+d) "
"nebo přerušovací znak (CTRL+c). Druhý uživatel uvidí zprávu B<EOF> "
"ukazující, že konverzace skončila."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "You can prevent people (other than the super-user) from writing to you "
#| "with the B<mesg>(1)  command.  Some commands, for example B<nroff>(1)  "
#| "and B<pr>(1), may disallow writing automatically, so that your output "
#| "isn't overwritten."
msgid ""
"You can prevent people (other than the superuser) from writing to you with "
"the B<mesg>(1) command. Some commands, for example B<nroff>(1) and B<pr>(1), "
"may automatically disallow writing, so that the output they produce isn\\"
"(cqt overwritten."
msgstr ""
"Psaní na svůj terminál můžete ostatním (kromě roota) zakázat pomocí příkazu "
"B<mesg>(1). Některé příkazy, např. B<nroff>(1), a B<pr>(1), mohou výpisy "
"zakázat automaticky, takže váš terminál nebude zprávou přepsán."

#. type: Plain text
#: debian-bookworm
msgid ""
"If the user you want to write to is logged in on more than one terminal, you "
"can specify which terminal to write to by giving the terminal name as the "
"second operand to the B<write> command. Alternatively, you can let B<write> "
"select one of the terminals - it will pick the one with the shortest idle "
"time. This is so that if the user is logged in at work and also dialed up "
"from home, the message will go to the right place."
msgstr ""
"Pokud je uživatel, kterému chcete psát, přihlášen na více terminálech, "
"můžete určit, na který terminál se bude psát zadáním jména terminálu jako "
"druhého parametru za příkazem B<write>. Můžete také nechat program B<write> "
"vybrat jeden z terminálů - vybere se ten s nejkratší časovou prodlevou (idle "
"time). Pokud je tedy uživatel přihlášen v práci a zároveň je připojen z "
"domova, zpráva dojde na správný terminál."

#. type: Plain text
#: debian-bookworm
msgid ""
"The traditional protocol for writing to someone is that the string I<-o>, "
"either at the end of a line or on a line by itself, means that it\\(cqs the "
"other person\\(cqs turn to talk. The string I<oo> means that the person "
"believes the conversation to be over."
msgstr ""
"Tradiční dohoda při psaní je, že značka I<-o>, buď na konci řádku nebo na "
"vlastním řádku, znamená, že je řada na druhé osobě. Znaky I<oo> znamenají, "
"že osoba předpokládá, že konverzace skončila."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "VOLBY"

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "Zobrazí nápovědu a skončí."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Vypíše informace o verzi a skončí."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIE"

#. type: Plain text
#: debian-bookworm
msgid "A B<write> command appeared in Version 6 AT&T UNIX."
msgstr "Příkaz B<write> se objevil v AT&T UNIXu verze 6."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: debian-bookworm
msgid "B<mesg>(1), B<talk>(1), B<who>(1)"
msgstr "B<mesg>(1), B<talk>(1), B<who>(1)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "HLÁŠENÍ CHYB"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr "Pro hlášení chyb (anglicky) použijte systém pro sledování problémů na"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "TIRÁŽ"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<write> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
