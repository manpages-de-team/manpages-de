# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Petr Kolář <Petr.Kolar@vslib.cz>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-16 06:04+0100\n"
"PO-Revision-Date: 2022-05-06 20:50+0200\n"
"Last-Translator: Petr Kolář <Petr.Kolar@vslib.cz>\n"
"Language-Team: Czech <diskuze-l10n-cz@lists.openalt.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: Dd
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "$Mdocdate: March 31 2022 $"
msgid "$Mdocdate: August 10 2023 $"
msgstr "$Mdocdate: March 31 2022 $"

#. type: Dt
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SSH-AGENT 1"
msgstr "SSH-AGENT 1"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "E<.Nm ssh-agent>"
msgstr "E<.Nm ssh-agent>"

#. type: Nd
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, fuzzy, no-wrap
#| msgid "authentication agent"
msgid "OpenSSH authentication agent"
msgstr "autentizační agent"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "POUŽITÍ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "E<.Nm ssh-agent> E<.Op Fl c Li | Fl s> E<.Op Fl d> E<.Op Fl a Ar "
#| "bind_address> E<.Op Fl t Ar life> E<.Op Ar command Op Ar arg ...> E<.Nm "
#| "ssh-agent> E<.Op Fl c Li | Fl s> E<.Fl k>"
msgid ""
"E<.Nm ssh-agent> E<.Op Fl c | s> E<.Op Fl \\&Dd> E<.Op Fl a Ar bind_address> "
"E<.Op Fl E Ar fingerprint_hash> E<.Op Fl O Ar option> E<.Op Fl P Ar "
"allowed_providers> E<.Op Fl t Ar life> E<.Nm ssh-agent> E<.Op Fl a Ar "
"bind_address> E<.Op Fl E Ar fingerprint_hash> E<.Op Fl O Ar option> E<.Op Fl "
"P Ar allowed_providers> E<.Op Fl t Ar life> E<.Ar command Op Ar arg ...> "
"E<.Nm ssh-agent> E<.Op Fl c | s> E<.Fl k>"
msgstr ""
"E<.Nm ssh-agent> E<.Op Fl c Li | Fl s> E<.Op Fl d> E<.Op Fl a Ar "
"navaž_adresu> E<.Op Fl t Ar životnost> E<.Op Ar příkaz Op Ar parametry ...> "
"E<.Nm ssh-agent> E<.Op Fl c Li | Fl s> E<.Fl k>"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"E<.Nm> is a program to hold private keys used for public key "
"authentication.  Through use of environment variables the agent can be "
"located and automatically used for authentication when logging in to other "
"machines using E<.Xr ssh 1>."
msgstr ""
"Program E<.Nm> shromažďuje soukromé autentizační klíče (RSA i DSA). Programy "
"odstartované z agenta jsou s ním propojeny a automaticky jej používají pro "
"RSA autentizaci při přihlašování na jiné stroje pomocí E<.Xr ssh 1>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid "The options are as follows:"
msgstr "Povolené jsou následující volby:"

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl a Ar bind_address"
msgstr "Fl a Ar navaž_adresu"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "Bind the agent to the unix-domain socket E<.Ar bind_address>.  The "
#| "default is E<.Pa /tmp/ssh-XXXXXXXXXX/agent.\\*(Ltppid\\*(Gt>."
msgid ""
"Bind the agent to the E<.Ux Ns -domain> socket E<.Ar bind_address>.  The "
"default is E<.Pa $TMPDIR/ssh-XXXXXXXXXX/agent.\\*(Ltppid\\*(Gt>."
msgstr ""
"Naváže agenta na unixový socket E<.Ar bind_address>. Implicitní adresa je "
"E<.Pa /tmp/ssh-XXXXXXXXXX/agent.\\*(Ltppid\\*(Gt>."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl c"
msgstr "Fl c"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Generate C-shell commands on E<.Dv stdout>.  This is the default if E<.Ev "
"SHELL> looks like it's a csh style of shell."
msgstr ""
"Generuje C-shell příkazy na standardní E<.Dv výstup>. Toto je implicitní "
"nastavení, pokud E<.Ev SHELL> vypadá jako csh styl shellu."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl D"
msgstr "Fl D"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid "Debug mode.  When this option is specified E<.Nm> will not fork."
msgid "Foreground mode.  When this option is specified, E<.Nm> will not fork."
msgstr "Ladící mód. Je-li zadán tento přepínač, E<.Nm> nikdy neprovede fork."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl d"
msgstr "Fl d"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid "Debug mode.  When this option is specified E<.Nm> will not fork."
msgid ""
"Debug mode.  When this option is specified, E<.Nm> will not fork and will "
"write debug information to standard error."
msgstr "Ladící mód. Je-li zadán tento přepínač, E<.Nm> nikdy neprovede fork."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl E Ar fingerprint_hash"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Specifies the hash algorithm used when displaying key fingerprints.  Valid "
"options are: E<.Dq md5> and E<.Dq sha256>.  The default is E<.Dq sha256>."
msgstr ""

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl k"
msgstr "Fl k"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Kill the current agent (given by the E<.Ev SSH_AGENT_PID> environment "
"variable)."
msgstr ""
"Ukončí současného ssh agenta. (dáno proměnnou prostředí E<.Ev "
"SSH_AGENT_PID>)."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl O Ar option"
msgstr "Fl O Ar volba"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Specify an option when starting E<.Nm>.  Currently two options are "
"supported: E<.Cm allow-remote-pkcs11> and E<.Cm no-restrict-websafe>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"The E<.Cm allow-remote-pkcs11> option allows clients of a forwarded E<.Nm> "
"to load PKCS#11 or FIDO provider libraries.  By default only local clients "
"may perform this operation.  Note that signalling that an E<.Nm> client is "
"remote is performed by E<.Xr ssh 1>, and use of other tools to forward "
"access to the agent socket may circumvent this restriction."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"The E<.Cm no-restrict-websafe> option instructs E<.Nm> to permit signatures "
"using FIDO keys that might be web authentication requests.  By default, "
"E<.Nm> refuses signature requests for FIDO keys where the key application "
"string does not start with E<.Dq ssh:> and when the data to be signed does "
"not appear to be a E<.Xr ssh 1> user authentication request or a E<.Xr ssh-"
"keygen 1> signature.  The default behaviour prevents forwarded access to a "
"FIDO key from also implicitly forwarding the ability to authenticate to "
"websites."
msgstr ""

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl P Ar allowed_providers"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
msgid ""
"Specify a pattern-list of acceptable paths for PKCS#11 provider and FIDO "
"authenticator middleware shared libraries that may be used with the E<.Fl S> "
"or E<.Fl s> options to E<.Xr ssh-add 1>.  Libraries that do not match the "
"pattern list will be refused.  See PATTERNS in E<.Xr ssh_config 5> for a "
"description of pattern-list syntax.  The default list is E<.Dq usr/lib*/*,/"
"usr/local/lib*/*>."
msgstr ""

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl s"
msgstr "Fl s"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Generate Bourne shell commands on E<.Dv stdout>.  This is the default if "
"E<.Ev SHELL> does not look like it's a csh style of shell."
msgstr ""
"Generuje Bourne shell příkazy na standardní E<.Dv výstup>. Toto je "
"implicitní nastavení, pokud E<.Ev SHELL> nevypadá jako csh styl shellu."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl t Ar life"
msgstr "Fl t Ar životnost"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Set a default value for the maximum lifetime of identities added to the "
"agent.  The lifetime may be specified in seconds or in a time format "
"specified in E<.Xr sshd_config 5>.  A lifetime specified for an identity "
"with E<.Xr ssh-add 1> overrides this value.  Without this option the default "
"maximum lifetime is forever."
msgstr ""
"Nastaví implicitní hodnotu pro maximální délku patnosti identit vložených v "
"agentovi.  Délka života může být specifikována v sekundách, nebo ve formátu "
"definovaném E<.Xr sshd_config 5>. Délka života specifikovaná v E<.Xr ssh-add "
"1> přepíše hodnotu definovanou pomocí parametru -t. Pokud není nastaven "
"tento parametr je maximální délka života nekonečno."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Ar command Op Ar arg ..."
msgstr "Ar příkaz Op Ar parametry ..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "The agent exits automatically when the command given on the command line "
#| "terminates."
msgid ""
"If a command (and optional arguments) is given, this is executed as a "
"subprocess of the agent.  The agent exits automatically when the command "
"given on the command line terminates."
msgstr ""
"Agent je automaticky ukončen, když přikaz zadaný na příkazovém řádku skončí."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"There are two main ways to get an agent set up.  The first is at the start "
"of an X session, where all other windows or programs are started as children "
"of the E<.Nm> program.  The agent starts a command under which its "
"environment variables are exported, for example E<.Cm ssh-agent xterm &>.  "
"When the command terminates, so does the agent."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"The second method is used for a login session.  When E<.Nm> is started, it "
"prints the shell commands required to set its environment variables, which "
"in turn can be evaluated in the calling shell, for example E<.Cm eval `ssh-"
"agent -s`>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"In both cases, E<.Xr ssh 1> looks at these environment variables and uses "
"them to establish a connection to the agent."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"The agent initially does not have any private keys.  Keys are added using "
"E<.Xr ssh-add 1> or by E<.Xr ssh 1> when E<.Cm AddKeysToAgent> is set in "
"E<.Xr ssh_config 5>.  Multiple identities may be stored in E<.Nm> "
"concurrently and E<.Xr ssh 1> will automatically use them if present.  E<.Xr "
"ssh-add 1> is also used to remove keys from E<.Nm> and to query the keys "
"that are held in one."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"Connections to E<.Nm> may be forwarded from further remote hosts using the "
"E<.Fl A> option to E<.Xr ssh 1> (but see the caveats documented therein), "
"avoiding the need for authentication data to be stored on other machines.  "
"Authentication passphrases and private keys never go over the network: the "
"connection to the agent is forwarded over SSH remote connections and the "
"result is returned to the requester, allowing the user access to their "
"identities anywhere in the network in a secure fashion."
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ENVIRONMENT"
msgstr "PROSTŘEDÍ"

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Ev SSH_AGENT_PID"
msgstr "Ev SSH_AGENT_PID"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"When E<.Nm> starts, it stores the name of the agent's process ID (PID) in "
"this variable."
msgstr ""

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Ev SSH_AUTH_SOCK"
msgstr "Ev SSH_AUTH_SOCK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "A unix-domain socket is created and the name of this socket is stored in "
#| "the E<.Ev SSH_AUTH_SOCK> environment variable.  The socket is made "
#| "accessible only to the current user.  This method is easily abused by "
#| "root or another instance of the same user."
msgid ""
"When E<.Nm> starts, it creates a E<.Ux Ns -domain> socket and stores its "
"pathname in this variable.  It is accessible only to the current user, but "
"is easily abused by root or another instance of the same user."
msgstr ""
"Je vytvořen unixový secket a jeho jméno je uloženo v proměnné E<.Ev "
"SSH_AUTH_SOCK>. Socket je dostupný pouze danému uživateli. Tato metoda může "
"být snadno zneužita kořenovým uživatelem."

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr "SOUBORY"

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Pa $TMPDIR/ssh-XXXXXXXXXX/agent.E<lt>ppidE<gt>"
msgstr "Pa $TMPDIR/ssh-XXXXXXXXXX/agent.E<lt>ppidE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"E<.Ux Ns -domain> sockets used to contain the connection to the "
"authentication agent.  These sockets should only be readable by the owner.  "
"The sockets should get automatically removed when the agent exits."
msgstr ""
"E<.Ux Ns -domain> sokety použité pro spojení s autentizačním agentem. Sokety "
"musí být čitelné pouze vlastníkem. Sokety musí být při ukončení agenta "
"automaticky zrušeny.  Nadřízený adresář adresáře ssh-$USER musí mít nastaven "
"sticky bit."

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
msgid ""
"E<.Xr ssh 1>, E<.Xr ssh-add 1>, E<.Xr ssh-keygen 1>, E<.Xr ssh_config 5>, "
"E<.Xr sshd 8>"
msgstr ""
"E<.Xr ssh 1>, E<.Xr ssh-add 1>, E<.Xr ssh-keygen 1>, E<.Xr ssh_config 5>, "
"E<.Xr sshd 8>"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "OpenSSH is a derivative of the original and free ssh 1.2.12 release by "
#| "Tatu Ylonen.  Aaron Campbell, Bob Beck, Markus Friedl, Niels Provos, Theo "
#| "de Raadt and Dug Song removed many bugs, re-added newer features and "
#| "created OpenSSH.  Markus Friedl contributed the support for SSH protocol "
#| "versions 1.5 and 2.0."
msgid ""
"E<.An -nosplit> OpenSSH is a derivative of the original and free ssh 1.2.12 "
"release by E<.An Tatu Ylonen>.  E<.An Aaron Campbell , Bob Beck , Markus "
"Friedl , Niels Provos , Theo de Raadt> and E<.An Dug Song> removed many "
"bugs, re-added newer features and created OpenSSH.  E<.An Markus Friedl> "
"contributed the support for SSH protocol versions 1.5 and 2.0."
msgstr ""
"OpenSSH je odvozeno od originálního a volného ssh 1.2.12, které bylo vydáno "
"Tatu Ylonenem. Aaron Campbell, Bob Beck, MArkus Friedl, Niels Provos, Theo "
"de Raadt and Dug Song odstranili mnohe chyby, doplnili funkčnost a vytvořili "
"tak OpenSSH. Markus Friedl přispel k vývoji SSH protokolu ve verzích 1.5 a "
"2.0."

#. type: Dd
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid "$Mdocdate: March 31 2022 $"
msgid "$Mdocdate: October 7 2022 $"
msgstr "$Mdocdate: March 31 2022 $"

#. type: Plain text
#: debian-bookworm
msgid ""
"The E<.Cm allow-remote-pkcs11> option allows clients of a forwarded E<.Nm> "
"to load PKCS#11 or FIDO provider libraries.  By default only local clients "
"may perform this operation.  Note that signalling that a E<.Nm> client "
"remote is performed by E<.Xr ssh 1>, and use of other tools to forward "
"access to the agent socket may circumvent this restriction."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The E<.Cm no-restrict-websafe>, instructs E<.Nm> to permit signatures using "
"FIDO keys that might be web authentication requests.  By default, E<.Nm> "
"refuses signature requests for FIDO keys where the key application string "
"does not start with E<.Dq ssh:> and when the data to be signed does not "
"appear to be a E<.Xr ssh 1> user authentication request or a E<.Xr ssh-"
"keygen 1> signature.  The default behaviour prevents forwarded access to a "
"FIDO key from also implicitly forwarding the ability to authenticate to "
"websites."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify a pattern-list of acceptable paths for PKCS#11 provider and FIDO "
"authenticator middleware shared libraries that may be used with the E<.Fl S> "
"or E<.Fl s> options to E<.Xr ssh-add 1>.  Libraries that do not match the "
"pattern list will be refused.  See PATTERNS in E<.Xr ssh_config 5> for a "
"description of pattern-list syntax.  The default list is E<.Dq /usr/lib/*,/"
"usr/local/lib/*>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"In Debian, E<.Nm> is installed with the set-group-id bit set, to prevent "
"E<.Xr ptrace 2> attacks retrieving private key material.  This has the side-"
"effect of causing the run-time linker to remove certain environment "
"variables which might have security implications for set-id programs, "
"including E<.Ev LD_PRELOAD>, E<.Ev LD_LIBRARY_PATH>, and E<.Ev TMPDIR>.  If "
"you need to set any of these environment variables, you will need to do so "
"in the program executed by ssh-agent."
msgstr ""
