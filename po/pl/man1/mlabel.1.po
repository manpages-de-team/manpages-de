# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Wojtek Kotwica <wkotwica@post.pl>, 1999.
# Robert Luberda <robert@debian.org>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2025-02-28 16:40+0100\n"
"PO-Revision-Date: 2014-11-16 13:43+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "mlabel"
msgstr "mlabel"

#. type: TH
#: archlinux debian-unstable fedora-42
#, no-wrap
msgid "19Jan25"
msgstr "19 stycznia 2025"

#. type: TH
#: archlinux debian-unstable fedora-42
#, no-wrap
msgid "mtools-4.0.47"
msgstr "mtools-4.0.47"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr "Nazwa"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "mlabel - make an MSDOS volume label"
msgstr "mlabel - nadaje etykietę dysku MS-DOS"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr "OSTRZEŻENIE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""
"Ta strona podręcznika ekranowego została automatycznie wygenerowana z "
"dokumentacji texinfo pakietu mtools i może nie być kompletna i całkowicie "
"dokładna. Szczegóły można znaleźć na końcu strony."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Opis"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "The \\&CW<mlabel> command adds a volume label to a disk. Its syntax is:"
msgstr ""
"Polecenie \\&CW<mdel> nadaje etykietę dyskowi. Ma następującą składnię:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "I<\\&>\\&CW<mlabel> [\\&CW<-vcsn>] [\\&CW<-N> I<serial>] I<drive>:[I<new_label>]\n"
msgstr "I<\\&>\\&CW<mlabel> [\\&CW<-vcsn>] [\\&CW<-N> I<serial>] I<dysk>:[I<nowa_etykieta>]\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\&\\&CW<Mlabel> displays the current volume label, if present. If "
"\\&I<new_label> is not given, and if neither the \\&CW<c> nor the "
"\\&\\&CW<s> options are set, it prompts the user for a new volume label.  To "
"delete an existing volume label, press return at the prompt."
msgstr ""
"\\&\\&CW<Mlabel> wyświetla bieżącą etykietę dysku, jeśli ona istnieje. Gdy "
"nie podano \\&I<nowej_etykiety> ani opcji \\&CW<c>, ani \\&\\&CW<s> , "
"użytkownik proszony jest o podanie nowej.  W celu usunięcia istniejącej "
"etykiety należy nacisnąć samo return w odpowiedzi."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The label is limited to 11 single-byte characters, e.g. \\&CW<Name1234567>."
msgstr ""
"Długość etykiety jest ograniczona do 11 znaków jednobajtowych, np. "
"\\&CW<Nazwa123456>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Reasonable care is taken to create a valid MS-DOS volume label.  If an "
"invalid label is specified, \\&CW<mlabel> changes the label (and displays "
"the new label if the verbose mode is set). \\&CW<Mlabel> returns 0 on "
"success or 1 on failure."
msgstr ""
"Polecenie stara się utworzyć prawidłową etykietę MS-DOS. W przypadku podania "
"nieprawidłowej etykiety, \\&CW<mlabel> zmienia ją (i wyświetla nową "
"etykietę, jeśli użyty jest tryb wyświetlania szczegółów). \\&CW<Mlabel> "
"zwraca 0 w przypadku powodzenia lub 1 w przypadku błędu."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Mlabel supports the following options:"
msgstr "Mlabel przyjmuje następujące opcje linii poleceń:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<c>\\ "
msgstr "\\&\\&CW<c>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Clears an existing label, without prompting the user"
msgstr "Wymazuje istniejącą etykietę, bez pytania użytkownika."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<s>\\ "
msgstr "\\&\\&CW<s>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Shows the existing label, without prompting the user."
msgstr "Pokazuje istniejącą etykietę, bez pytania użytkownika."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<n\\ >\\ "
msgstr "\\&\\&CW<n\\ >\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Assigns a new (random) serial number to the disk"
msgstr "Przypisuje dyskowi nowy (przypadkowy) numer seryjny."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<N\\ >I<serial>\\&\\ "
msgstr "\\&\\&CW<N\\ >I<serial>\\&\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"Sets the supplied serial number. The serial number should be supplied as an "
"8 digit hexadecimal number, without spaces"
msgstr ""
"Ustawia podany numer seryjny. Numer ten powinien być podany jako 8-cyfrową "
"liczba szesnastkowa, bez żadnych spacji."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "See\\ Also"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Mtools' texinfo doc"
msgstr "Dokumentacja texinfo pakietu mtools"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr "PRZEGLĄDANIE DOKUMENTACJI TEXINFO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""
"Ta strona podręcznika została utworzona automatycznie z dokumentacji texinfo "
"pakietu mtools. Proces ten jednak jest tylko przybliżony i niektóre "
"elementy, jak odnośniki czy indeksy, mogą być utracone. W rzeczywistości "
"elementy te nie mają właściwych odpowiedników w formacie stron podręcznika "
"ekranowego. Ponadto nie wszystkie informacje zostały przełożone na wersję "
"podręcznika ekranowego. Dlatego zdecydowanie zalecamy użycie oryginalnej "
"dokumentacji texinfo. Na końcu niniejszej strony znajdują się instrukcje, "
"jak przeglądać dokumentację w tym formacie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "* \\ \\ "
msgstr "* \\ \\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""
"Zdatną do wydrukowania postać dokumentacji texinfo można otrzymać, "
"uruchamiając poniższe polecenia:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr "B<    ./configure; make dvi; dvips mtools.dvi>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "To generate a html copy, run:"
msgstr "Aby utworzyć wersję html, należy uruchomić:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr "B<    ./configure; make html>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""
"Już utworzone wersje html można znaleźć na stronie \\&\\&CW<\\(ifhttp://www."
"gnu.org/software/mtools/manual/mtools.html\\(is>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr ""
"Aby utworzyć kopię info (możliwą do przeglądania w trybie info Emacsa), "
"należy uruchomić:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr "B<    ./configure; make info>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""
"Dokumentacja texinfo wygląda najlepiej wydrukowana lub w postaci html. W "
"wersji info niektóre przykłady są naprawdę trudne w czytaniu z powodu "
"konwencji cytowania używanych w formacie info."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10Jul21"
msgstr "10 lipca 2021"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "mtools-4.0.32"
msgstr "mtools-4.0.32"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "22Feb25"
msgstr "22 lutego 2025"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "mtools-4.0.48"
msgstr "mtools-4.0.48"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "02Jun24"
msgstr "2 czerwca 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "mtools-4.0.44"
msgstr "mtools-4.0.44"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "21Mar23"
msgstr "21 marca 2023"

#. type: TH
#: opensuse-leap-16-0
#, no-wrap
msgid "mtools-4.0.43"
msgstr "mtools-4.0.43"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "28Sep24"
msgstr "28 września 2024"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "mtools-4.0.45"
msgstr "mtools-4.0.45"
