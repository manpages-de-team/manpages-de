# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Pedro Pablo Fábrega <pfabrega@arrakis.es>, 2000.
# Juan Piernas <piernas@ditec.um.es>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2025-02-28 16:56+0100\n"
"PO-Revision-Date: 2020-11-24 18:27+0100\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<wcsrtombs>()"
msgid "wcsrtombs"
msgstr "B<wcsrtombs>()"

#. type: TH
#: archlinux opensuse-tumbleweed
#, no-wrap
msgid "2024-11-17"
msgstr "17 Noviembre 2024"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.10"
msgid "Linux man-pages 6.12"
msgstr "Páginas de Manual de Linux 6.10"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "wcsrtombs - convert a wide-character string to a multibyte string"
msgstr ""
"wcsrtombs - convierte una cadena de caracteres anchos a una cadena multibyte"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux opensuse-tumbleweed
msgid "Standard C library (I<libc>,\\ I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>,\\ I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr "B<#include E<lt>wchar.hE<gt>>\n"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<size_t wcsrtombs(char *>I<dest>B<, const wchar_t **>I<src>B<,>\n"
#| "B<                 size_t >I<len>B<, mbstate_t *>I<ps>B<);>\n"
msgid ""
"B<size_t wcsrtombs(char >I<dest>B<[restrict .>I<size>B<], const wchar_t **restrict >I<src>B<,>\n"
"B<                 size_t >I<size>B<, mbstate_t *restrict >I<ps>B<);>\n"
msgstr ""
"B<size_t wcsrtombs(char *>I<dest>B<, const wchar_t **>I<src>B<,>\n"
"B<                 size_t >I<len>B<, mbstate_t *>I<ps>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<dest> is not NULL, the B<wcsrtombs>()  function converts the wide-"
#| "character string I<*src> to a multibyte string starting at I<dest>.  At "
#| "most I<len> bytes are written to I<dest>.  The shift state I<*ps> is "
#| "updated.  The conversion is effectively performed by repeatedly calling "
#| "I<wcrtomb(dest, *src, ps)>, as long as this call succeeds, and then "
#| "incrementing I<dest> by the number of bytes written and I<*src> by one.  "
#| "The conversion can stop for three reasons:"
msgid ""
"If I<dest> is not NULL, the B<wcsrtombs>()  function converts the wide-"
"character string I<*src> to a multibyte string starting at I<dest>.  At most "
"I<size> bytes are written to I<dest>.  The shift state I<*ps> is updated.  "
"The conversion is effectively performed by repeatedly calling "
"I<wcrtomb(dest, *src, ps)>, as long as this call succeeds, and then "
"incrementing I<dest> by the number of bytes written and I<*src> by one.  The "
"conversion can stop for three reasons:"
msgstr ""
"Si I<dest> no es un puntero NULL, la función B<wcsrtombs>() convierte la "
"cadena de caracteres anchos I<*src> en una cadena multibyte comenzando en "
"I<dest>.  En I<dest> se escriben, como mucho, I<len> bytes.  El estado de "
"cambios I<*ps> se acualiza. La conversión se realiza de forma efectiva "
"llamando repetidamente a I<wcrtomb(dest, *src, ps)>, tantas veces como la "
"llamada sea efectiva e incrementando I<dest> con el número de bytes escritos "
"y I<*src> en uno. La conversión puede pararse por tres razones:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"A wide character has been encountered that can not be represented as a "
"multibyte sequence (according to the current locale).  In this case, I<*src> "
"is left pointing to the invalid wide character, I<(size_t)\\ -1> is "
"returned, and I<errno> is set to B<EILSEQ>."
msgstr ""
"Se ha encontrado un carácter ancho que no se puede representar como una "
"secuencia multibyte (de acuerdo con la localización actual). En este caso, "
"I<*src> se deja apuntando al carácter ancho inválido, se devuelve I<(size_t)"
"\\ -1> y I<errno> toma el valor B<EILSEQ>."

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The length limit forces a stop.  In this case, I<*src> is left pointing "
#| "to the next wide character to be converted, and the number of bytes "
#| "written to I<dest> is returned."
msgid ""
"The size limit forces a stop.  In this case, I<*src> is left pointing to the "
"next wide character to be converted, and the number of bytes written to "
"I<dest> is returned."
msgstr ""
"El límite de longitud fuerza la parada. En este caso, I<*src> se deja "
"apuntando al siguiente carácter ancho para convertir y devuelve el número de "
"bytes escritos en I<dest>."

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The wide-character string has been completely converted, including the "
#| "terminating null wide character (L\\(aq\\e0\\(aq), which has the side "
#| "effect of bringing back I<*ps> to the initial state.  In this case, "
#| "I<*src> is set to NULL, and the number of bytes written to I<dest>, "
#| "excluding the terminating null byte (\\(aq\\e0\\(aq), is returned."
msgid ""
"The wide-character string has been completely converted, including the "
"terminating null wide character (L\\[aq]\\[rs]0\\[aq]), which has the side "
"effect of bringing back I<*ps> to the initial state.  In this case, I<*src> "
"is set to NULL, and the number of bytes written to I<dest>, excluding the "
"terminating null byte (\\[aq]\\[rs]0\\[aq]), is returned."
msgstr ""
"Se ha completado la conversión de la cadena de caracteres anchos, incluyendo "
"la terminación L'\\e0' (que tiene el efecto de devolver I<*ps> al estado "
"inicial). En este caso, I<*src> se pone como NULL y devuelve el número de "
"bytes escritos en I<dest>, excluyendo el byte terminador (\\(aq\\e0\\(aq)."

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<dest> is NULL, I<len> is ignored, and the conversion proceeds as "
#| "above, except that the converted bytes are not written out to memory, and "
#| "that no length limit exists."
msgid ""
"If I<dest> is NULL, I<size> is ignored, and the conversion proceeds as "
"above, except that the converted bytes are not written out to memory, and "
"that no size limit exists."
msgstr ""
"Si I<dest> es NULL, I<len> se ignora, y la conversión procede como arriba, "
"salvo que los bytes convertidos no se escriben en memoria, y no existe "
"límite de longitud."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"In both of the above cases, if I<ps> is NULL, a static anonymous state known "
"only to the B<wcsrtombs>()  function is used instead."
msgstr ""
"En los dos casos anteriores, si I<ps> es un puntero NULL se usa en su lugar "
"un estado estático anónimo sólo conocido por la función B<wcsrtombs>()."

#. type: Plain text
#: archlinux opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The programmer must ensure that there is room for at least I<len> bytes "
#| "at I<dest>."
msgid ""
"The programmer must ensure that there is room for at least I<size> bytes at "
"I<dest>."
msgstr ""
"El programador tiene que asegurarse de que hay espacio para al menos I<len> "
"bytes en I<dest>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The B<wcsrtombs>()  function returns the number of bytes that make up the "
"converted part of multibyte sequence, not including the terminating null "
"byte.  If a wide character was encountered which could not be converted, "
"I<(size_t)\\ -1> is returned, and I<errno> set to B<EILSEQ>."
msgstr ""
"La función B<wcsrtombs>() devuelve el número de bytes que conforman la parte "
"de la secuencia multibyte convertida, sin incluir el byte nulo terminador. "
"Si encontró un carácter ancho que no pudo convertir, devuelve I<(size_t)\\ "
"-1> y I<errno> toma el valor B<EILSEQ>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "B<wcsrtombs>()"
msgstr "B<wcsrtombs>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:wcsrtombs/!ps"
msgstr "MT-Carrera insegura:wcsrtombs/!ps"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0 opensuse-tumbleweed
msgid "POSIX.1-2001, C99."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"The behavior of B<wcsrtombs>()  depends on the B<LC_CTYPE> category of the "
"current locale."
msgstr ""
"El comportamiento de B<wcsrtombs>() depende de la categoría B<LC_CTYPE> de "
"la localización actual."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid "Passing NULL as I<ps> is not multithread safe."
msgstr "Pasar NULL como valor de I<ps> no es seguro en un entorno multihilos."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-42 fedora-rawhide
#: mageia-cauldron opensuse-leap-16-0 opensuse-tumbleweed
msgid ""
"B<iconv>(3), B<mbsinit>(3), B<wcrtomb>(3), B<wcsnrtombs>(3), B<wcstombs>(3)"
msgstr ""
"B<iconv>(3), B<mbsinit>(3), B<wcrtomb>(3), B<wcsnrtombs>(3), B<wcstombs>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
#, fuzzy, no-wrap
#| msgid ""
#| "B<size_t wcsrtombs(char *>I<dest>B<, const wchar_t **>I<src>B<,>\n"
#| "B<                 size_t >I<len>B<, mbstate_t *>I<ps>B<);>\n"
msgid ""
"B<size_t wcsrtombs(char >I<dest>B<[restrict .>I<len>B<], const wchar_t **restrict >I<src>B<,>\n"
"B<                 size_t >I<len>B<, mbstate_t *restrict >I<ps>B<);>\n"
msgstr ""
"B<size_t wcsrtombs(char *>I<dest>B<, const wchar_t **>I<src>B<,>\n"
"B<                 size_t >I<len>B<, mbstate_t *>I<ps>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"If I<dest> is not NULL, the B<wcsrtombs>()  function converts the wide-"
"character string I<*src> to a multibyte string starting at I<dest>.  At most "
"I<len> bytes are written to I<dest>.  The shift state I<*ps> is updated.  "
"The conversion is effectively performed by repeatedly calling "
"I<wcrtomb(dest, *src, ps)>, as long as this call succeeds, and then "
"incrementing I<dest> by the number of bytes written and I<*src> by one.  The "
"conversion can stop for three reasons:"
msgstr ""
"Si I<dest> no es un puntero NULL, la función B<wcsrtombs>() convierte la "
"cadena de caracteres anchos I<*src> en una cadena multibyte comenzando en "
"I<dest>.  En I<dest> se escriben, como mucho, I<len> bytes.  El estado de "
"cambios I<*ps> se acualiza. La conversión se realiza de forma efectiva "
"llamando repetidamente a I<wcrtomb(dest, *src, ps)>, tantas veces como la "
"llamada sea efectiva e incrementando I<dest> con el número de bytes escritos "
"y I<*src> en uno. La conversión puede pararse por tres razones:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"The length limit forces a stop.  In this case, I<*src> is left pointing to "
"the next wide character to be converted, and the number of bytes written to "
"I<dest> is returned."
msgstr ""
"El límite de longitud fuerza la parada. En este caso, I<*src> se deja "
"apuntando al siguiente carácter ancho para convertir y devuelve el número de "
"bytes escritos en I<dest>."

#. type: Plain text
#: debian-bookworm mageia-cauldron
#, fuzzy
#| msgid ""
#| "The wide-character string has been completely converted, including the "
#| "terminating null wide character (L\\(aq\\e0\\(aq), which has the side "
#| "effect of bringing back I<*ps> to the initial state.  In this case, "
#| "I<*src> is set to NULL, and the number of bytes written to I<dest>, "
#| "excluding the terminating null byte (\\(aq\\e0\\(aq), is returned."
msgid ""
"The wide-character string has been completely converted, including the "
"terminating null wide character (L\\[aq]\\e0\\[aq]), which has the side "
"effect of bringing back I<*ps> to the initial state.  In this case, I<*src> "
"is set to NULL, and the number of bytes written to I<dest>, excluding the "
"terminating null byte (\\[aq]\\e0\\[aq]), is returned."
msgstr ""
"Se ha completado la conversión de la cadena de caracteres anchos, incluyendo "
"la terminación L'\\e0' (que tiene el efecto de devolver I<*ps> al estado "
"inicial). En este caso, I<*src> se pone como NULL y devuelve el número de "
"bytes escritos en I<dest>, excluyendo el byte terminador (\\(aq\\e0\\(aq)."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"If I<dest> is NULL, I<len> is ignored, and the conversion proceeds as above, "
"except that the converted bytes are not written out to memory, and that no "
"length limit exists."
msgstr ""
"Si I<dest> es NULL, I<len> se ignora, y la conversión procede como arriba, "
"salvo que los bytes convertidos no se escriben en memoria, y no existe "
"límite de longitud."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-42 fedora-rawhide mageia-cauldron
#: opensuse-leap-16-0
msgid ""
"The programmer must ensure that there is room for at least I<len> bytes at "
"I<dest>."
msgstr ""
"El programador tiene que asegurarse de que hay espacio para al menos I<len> "
"bytes en I<dest>."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide opensuse-leap-16-0
#, no-wrap
msgid "2024-06-15"
msgstr "15 Junio 2024"

#. type: TH
#: debian-unstable fedora-42 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.9.1"
msgstr "Páginas de Manual de Linux 6.9.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-16-0 opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de Manual de Linux (no publicadas)"
