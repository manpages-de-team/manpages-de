# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-22 07:31+0100\n"
"PO-Revision-Date: 2000-04-13 08:57+0900\n"
"Last-Translator: Unknown <>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "MKFS"
msgstr "MKFS"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "2022년 5월 11일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid "System Administration Utilities"
msgid "System Administration"
msgstr "시스템 관리 유틸리티"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: debian-bookworm
msgid "mkfs - build a Linux filesystem"
msgstr "mkfs - 리눅스 파일 시스템 만들기"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<mkfs> [ B<-V> ] [ B<-t> I<fstype> ] [ B<fs-options> ] I<filesys> "
#| "[ I<blocks> ]"
msgid "B<mkfs> [options] [B<-t> I<type>] [I<fs-options>] I<device> [I<size>]"
msgstr ""
"B<mkfs> [ B<-V> ] [ B<-t> I<형태> ] [ B<fs-options> ] I<장치이름> [ I<블럭> ]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<This mkfs frontend is deprecated in favour of filesystem specific "
"mkfs.E<lt>typeE<gt> utils.>"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<mkfs> is used to build a Linux file system on a device, usually a hard "
#| "disk partition.  I<filesys> is either the device name (e.g. /dev/hda1, /"
#| "dev/sdb2) or the mount point (e.g. /, /usr, /home) for the file system.  "
#| "I<blocks> is the number of blocks to be used for the file system."
msgid ""
"B<mkfs> is used to build a Linux filesystem on a device, usually a hard disk "
"partition. The I<device> argument is either the device name (e.g., I</dev/"
"hda1>, I</dev/sdb2>), or a regular file that shall contain the filesystem. "
"The I<size> argument is the number of blocks to be used for the filesystem."
msgstr ""
"B<mkfs> 명령은 한 장치(보통 한 하드디스크 파티션)를 리눅스 파일 시스템으로 만"
"드는데 사용된다.  I<장치이름> 은 /dev/hda1, /dev/sdb2 이런 식의 디스크 장치 "
"이름이거나, /, /usr/, /home 같은 파일시스템을 위한 마운트 경로가 사용된다.  "
"I<블럭> 인자는 그 파일 시스템을 위해 사용되는 블럭의 갯수이다."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "The exit code returned by B<mkfs> is 0 on success and 1 on failure."
msgid "The exit status returned by B<mkfs> is 0 on success and 1 on failure."
msgstr "성공적으로 끝나면, 0, 실패하면, 1을 리턴한다."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "In actuality, B<mkfs> is simply a front-end for the various file system "
#| "builders (B<mkfs>.I<fstype>)  available under Linux.  The file system-"
#| "specific builder is searched for in /sbin/fs first, then in /sbin, and "
#| "finally in the directories listed in the PATH environment variable.  "
#| "Please see the file system-specific builder manual pages for further "
#| "details."
msgid ""
"In actuality, B<mkfs> is simply a front-end for the various filesystem "
"builders (B<mkfs.>I<fstype>) available under Linux. The filesystem-specific "
"builder is searched for via your B<PATH> environment setting only. Please "
"see the filesystem-specific builder manual pages for further details."
msgstr ""
"실질적으로, B<mkfs> 명령은 파일 시스템을 만드는데 각각 그 특정 파일 시스템을 "
"만드는 명령을 사용한다. 그 풀그림들의 이름은 대게 B<mkfs.파일시스템이름> 형식"
"으로 되어 있다. 그 풀그림들은 먼저 /etc/fs 경로안에 있거나, 아니면, /sbin 경"
"로안에 있게 된다.  특정 파일 시스템을 생성하는 풀그림에 대한 보다 자세한 사항"
"은 각 파일 시스템을 만드는 각 풀그림들의 매뉴얼 페이지를 참조한다."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "옵션"

#. type: Plain text
#: debian-bookworm
msgid "B<-t>, B<--type> I<type>"
msgstr "B<-t>, B<--type> I<형식>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify the I<type> of filesystem to be built. If not specified, the default "
"filesystem type (currently ext2) is used."
msgstr ""

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<fs-options>"
msgid "I<fs-options>"
msgstr "B<fs-options>"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "File system-specific options to be passed to the real file system "
#| "builder.  Although not guaranteed, the following options are supported by "
#| "most file system builders."
msgid ""
"Filesystem-specific options to be passed to the real filesystem builder."
msgstr ""
"이것은 각 특정 파일 시스템을 만드는 풀그림에서 사용되는 옵션을 말한다.  대부"
"분 같은 형식의 옵션이다."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--verbose>"
msgstr "B<-V>, B<--verbose>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Produce verbose output, including all filesystem-specific commands that are "
"executed. Specifying this option more than once inhibits execution of any "
"filesystem-specific commands. This is really only useful for testing."
msgstr ""
"정보를 자세하게 보여준다.  여기에는 실행되는 특정 파일 시스템을 만드는 풀그림"
"에 대한 정보도 모두 포함된다.  이 옵션이 한번 이상 사용되면 특정 파일 시스템"
"을 만드는데 사용될 그 풀그림의 정보만 보여주고 실질적으로 실행은 되지 않는"
"다.  이것은 실질적으로 검사하는 것에만 유용하게 쓰인다."

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "도움말을 보여주고 마친다."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Print version and exit.  (Option B<-V> will display version information only "
"when it is the only parameter, otherwise it will work as B<--verbose>.)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "BUGS"
msgstr "버그"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "All generic options must precede and not be combined with file system-"
#| "specific options.  Some file system-specific programs do not support the "
#| "I<-v> (verbose) option, nor return meaningful exit codes.  Also, some "
#| "file system-specific programs do not automatically detect the device size "
#| "and require the I<blocks> parameter to be specified."
msgid ""
"All generic options must precede and not be combined with filesystem-"
"specific options. Some filesystem-specific programs do not automatically "
"detect the device size and require the I<size> parameter to be specified."
msgstr ""
"공통되는 일반적인 옵션은 항상 그 특정 파일 시스템을 만드는 풀그림의 옵션 보"
"다 먼저 사용되어야 한다.  특정 파일 시스템을 만드는 어떤 풀그림에서는 I<-v> "
"옵션을 지원하지 않는 경우도 있고, 리턴값이 틀리게 나올 수도 있다.  또한 어떤 "
"풀그림은 자동으로 I<블럭> 갯수를 계산해 내지 못하는 것도 있어, 이때는 직접 I<"
"블럭갯수>를 지정해 주어야한다."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr "저자"

#. type: Plain text
#: debian-bookworm
msgid ""
"The manual page was shamelessly adapted from Remy Card\\(cqs version for the "
"ext2 filesystem."
msgstr ""
"이 매뉴얼 페이지는 ext2 파일 시스템을 위한 Remy Card의 매뉴얼 페이지를 뻔뻔스"
"럽게(?) 차용했다."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fs>(5), B<badblocks>(8), B<fsck>(8), B<mkdosfs>(8), B<mke2fs>(8), "
"B<mkfs.bfs>(8), B<mkfs.ext2>(8), B<mkfs.ext3>(8), B<mkfs.ext4>(8), "
"B<mkfs.minix>(8), B<mkfs.msdos>(8), B<mkfs.vfat>(8), B<mkfs.xfs>(8)"
msgstr ""
"B<fs>(5), B<badblocks>(8), B<fsck>(8), B<mkdosfs>(8), B<mke2fs>(8), "
"B<mkfs.bfs>(8), B<mkfs.ext2>(8), B<mkfs.ext3>(8), B<mkfs.ext4>(8), "
"B<mkfs.minix>(8), B<mkfs.msdos>(8), B<mkfs.vfat>(8), B<mkfs.xfs>(8)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "버그 보고"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "가용성"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<mkfs> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
