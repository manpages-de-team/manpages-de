# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Lauri Nurmi <lanurmi@kauhajoki.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-12-22 07:47+0100\n"
"PO-Revision-Date: 1998-04-10 11:01+0200\n"
"Last-Translator: Lauri Nurmi <lanurmi@kauhajoki.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "WRITE"
msgstr "WRITE"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11. toukokuuta 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr "Käyttäjän sovellukset"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: debian-bookworm
msgid "write - send a message to another user"
msgstr "write - lähetä viesti toiselle käyttäjälle"

#. type: Plain text
#: debian-bookworm
msgid "B<write> I<user> [I<ttyname>]"
msgstr "B<write> I<käyttäjä> [I<ttynimi>]"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<write> allows you to communicate with other users, by copying lines from "
"your terminal to theirs."
msgstr ""
"B<write> mahdollistaa kommunikoinnin toisten käyttäjien kanssa kopioimalla "
"rivejä sinun päätteeltäsi heidän päätteilleen."

#. type: Plain text
#: debian-bookworm
msgid ""
"When you run the B<write> command, the user you are writing to gets a "
"message of the form:"
msgstr ""
"Kun käytät B<write> -komentoa, käyttäjä jolle olet kirjoittamassa saa "
"seuraavan kaltaisen viestin:"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "Message from yourname@yourhost on yourtty at hh:mm ...\n"
msgstr "Message from nimesi@koneesi on päätteesi at hh:mm ...\n"

#. type: Plain text
#: debian-bookworm
msgid ""
"Any further lines you enter will be copied to the specified user\\(cqs "
"terminal. If the other user wants to reply, they must run B<write> as well."
msgstr ""
"Kaikki tämän jälkeen syöttämäsi rivit kopioidaan määritetyn käyttäjän "
"päätteelle. Jos toinen käyttäjä haluaa vastata, hänen on käytettävä myös "
"B<write>'ia."

#. type: Plain text
#: debian-bookworm
msgid ""
"When you are done, type an end-of-file or interrupt character. The other "
"user will see the message B<EOF> indicating that the conversation is over."
msgstr ""
"Kun olet valmis, kirjoita rivinloppu- tai keskeytysmerkki. Toinen käyttäjä "
"näkee viestin B<EOF>, joka kertoo keskustelun loppuneen."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "You can prevent people (other than the super-user) from writing to you "
#| "with the B<mesg>(1)  command.  Some commands, for example B<nroff>(1)  "
#| "and B<pr>(1), may disallow writing automatically, so that your output "
#| "isn't overwritten."
msgid ""
"You can prevent people (other than the superuser) from writing to you with "
"the B<mesg>(1) command. Some commands, for example B<nroff>(1) and B<pr>(1), "
"may automatically disallow writing, so that the output they produce isn\\"
"(cqt overwritten."
msgstr ""
"Käyttämällä komentoa B<mesg>(1)  voit estää ihmisiä (muita kuin "
"pääkäyttäjää) kirjoittamasta sinulle.  Jotkut komennot, esimerkiksi "
"B<nroff>(1)  ja B<pr>(1), saattavat estää kirjoittamisen automaattisesti, "
"jottei tulosteesi päälle kirjoiteta."

#. type: Plain text
#: debian-bookworm
msgid ""
"If the user you want to write to is logged in on more than one terminal, you "
"can specify which terminal to write to by giving the terminal name as the "
"second operand to the B<write> command. Alternatively, you can let B<write> "
"select one of the terminals - it will pick the one with the shortest idle "
"time. This is so that if the user is logged in at work and also dialed up "
"from home, the message will go to the right place."
msgstr ""
"Jos käyttäjä, jolle haluat kirjoittaa, on kirjautuneena useammalla kuin "
"yhdellä päätteellä, voit määrittää mihin päätteeseen haluat kirjoittaa. "
"Määritys tapahtuu antamalla päätteen nimi toisena optiona B<write> "
"-komennolle. Vaihtoehtoisesti voit antaa B<writen> valita yhden päätteistä - "
"se valitsee päätteen jolla on lyhyin toimettomuusaika. Jos käyttäjä on "
"kirjautuneena palvelimelle sekä työpaikaltaan että kotoaan, viesti menee "
"oikeaan paikkaan."

#. type: Plain text
#: debian-bookworm
msgid ""
"The traditional protocol for writing to someone is that the string I<-o>, "
"either at the end of a line or on a line by itself, means that it\\(cqs the "
"other person\\(cqs turn to talk. The string I<oo> means that the person "
"believes the conversation to be over."
msgstr ""
"Perinteinen tapa kirjoitettaessa jollekulle on, että jono I<-o>, joko rivin "
"lopussa tai rivillä yksinään, tarkoittaa että on toisen henkilön vuoro "
"puhua. Jono I<oo> tarkoittaa, että henkilö uskoo keskustelun olevan ohi."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr "VALITSIMET"

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr "Näytä tämä ohje ja poistu."

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "Tulosta versiotiedot ja poistu."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: debian-bookworm
msgid "A B<write> command appeared in Version 6 AT&T UNIX."
msgstr "A B<write> -komento ilmestyi AT&T UNIXin versiossa 6."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr "KATSO MYÖS"

#. type: Plain text
#: debian-bookworm
msgid "B<mesg>(1), B<talk>(1), B<who>(1)"
msgstr "B<mesg>(1), B<talk>(1), B<who>(1)"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr "VIRHEISTÄ ILMOITTAMINEN"

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr "SAATAVUUS"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<write> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
